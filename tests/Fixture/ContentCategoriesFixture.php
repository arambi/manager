<?php
namespace Manager\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArticlesFixture
 *
 */
class ContentCategoriesFixture extends TestFixture {

/**
 * table property
 *
 * @var string
 */
  public $table = 'content_categories';

/**
 * fields property
 *
 * @var array
 */
  public $fields = array(
    'id' => ['type' => 'integer'],
    'content_id' => ['type' => 'integer', 'null' => true, 'default' => null, 'autoIncrement' => null],
    'category_id' => ['type' => 'integer', 'null' => true, 'default' => null, 'autoIncrement' => null],
    '_constraints' => ['primary' => ['type' => 'primary', 'columns' => ['id']]],

  );

/**
 * records property
 *
 * @var array
 */
  public $records = array(
    array('content_id' => 1, 'category_id' => 2),
    array('content_id' => 2, 'category_id' => 2),
  );
}