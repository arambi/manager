<?php
namespace Manager\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CategoriesFixture
 *
 */
class CategoriesFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
		'title' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'slug' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'parent_id' => ['type' => 'integer', 'null' => false, 'default' => 0, 'autoIncrement' => null],
		'position' => ['type' => 'integer', 'null' => true, 'default' => null, 'autoIncrement' => null],
		'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
		'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
		],
		'_options' => ['engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
		],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'id' => 1,
			'title' => 'Category 1',
			'parent_id' => 0,
			'position' => 1,
			'created' => '2014-12-03 14:42:21',
			'modified' => '2014-12-03 14:42:21'
		],
		[
			'id' => 2,
			'title' => 'Category 2',
			'parent_id' => 0,
			'position' => 2,
			'created' => '2014-12-03 14:42:21',
			'modified' => '2014-12-03 14:42:21'
		],
		[
			'id' => 3,
			'title' => 'SubCategory 1',
			'parent_id' => 1,
			'position' => 1,
			'created' => '2014-12-03 14:42:21',
			'modified' => '2014-12-03 14:42:21'
		],
		[
			'id' => 4,
			'title' => 'SubCategory 2',
			'parent_id' => 1,
			'position' => 2,
			'created' => '2014-12-03 14:42:21',
			'modified' => '2014-12-03 14:42:21'
		]
		
	];

}
