<?php
namespace Manager\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArticlesFixture
 *
 */
class ContentTagsFixture extends TestFixture {



/**
 * fields property
 *
 * @var array
 */
  public $fields = array(
    'id' => ['type' => 'integer'],
    'content_id' => ['type' => 'integer', 'null' => true, 'default' => null, 'autoIncrement' => null],
    'tag_id' => ['type' => 'integer', 'null' => true, 'default' => null, 'autoIncrement' => null],
    '_constraints' => ['primary' => ['type' => 'primary', 'columns' => ['id']]],

  );

/**
 * records property
 *
 * @var array
 */
  public $records = array(
    array('content_id' => 1, 'tag_id' => 1),
    array('content_id' => 1, 'tag_id' => 2),
    array('content_id' => 2, 'tag_id' => 1),
  );
}