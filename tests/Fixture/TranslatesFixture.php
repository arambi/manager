<?php
namespace Manager\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArticlesFixture
 *
 */
class TranslatesFixture extends TestFixture {

/**
 * table property
 *
 * @var string
 */
  public $table = 'i18n';

/**
 * fields property
 *
 * @var array
 */
  public $fields = array(
    'id' => ['type' => 'integer'],
    'locale' => ['type' => 'string', 'length' => 6, 'null' => false],
    'model' => ['type' => 'string', 'null' => false],
    'foreign_key' => ['type' => 'integer', 'null' => false],
    'field' => ['type' => 'string', 'null' => false],
    'content' => ['type' => 'text'],
    '_constraints' => ['primary' => ['type' => 'primary', 'columns' => ['id']]],

  );

/**
 * records property
 *
 * @var array
 */
  public $records = array(
    array('locale' => 'spa', 'model' => 'Contents', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Hola'),
    array('locale' => 'eng', 'model' => 'Contents', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Hello'),
    array('locale' => 'spa', 'model' => 'Contents', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Adios'),
    array('locale' => 'eng', 'model' => 'Contents', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Bye'),

    array('locale' => 'spa', 'model' => 'Contents', 'foreign_key' => 1, 'field' => 'body', 'content' => 'Hola body'),
    array('locale' => 'eng', 'model' => 'Contents', 'foreign_key' => 1, 'field' => 'body', 'content' => 'Hello body'),
    array('locale' => 'spa', 'model' => 'Contents', 'foreign_key' => 2, 'field' => 'body', 'content' => 'Adios body'),
    array('locale' => 'eng', 'model' => 'Contents', 'foreign_key' => 2, 'field' => 'body', 'content' => 'Bye body'),

    array('locale' => 'spa', 'model' => 'Categories', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Category 1 spa'),
    array('locale' => 'eng', 'model' => 'Categories', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Category 1 eng'),
    array('locale' => 'spa', 'model' => 'Categories', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Category 2 spa'),
    array('locale' => 'eng', 'model' => 'Categories', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Category 2 eng'),

    array('locale' => 'spa', 'model' => 'Cards', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Card 1 spa'),
    array('locale' => 'eng', 'model' => 'Cards', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Card 1 eng'),
    array('locale' => 'spa', 'model' => 'Cards', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Card 2 spa'),
    array('locale' => 'eng', 'model' => 'Cards', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Card 2 eng'),

    array('locale' => 'spa', 'model' => 'Attributes', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Attribute 1 spa'),
    array('locale' => 'eng', 'model' => 'Attributes', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Attribute 1 eng'),
    array('locale' => 'spa', 'model' => 'Attributes', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Attribute 2 spa'),
    array('locale' => 'eng', 'model' => 'Attributes', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Attribute 2 eng'),
  );
}