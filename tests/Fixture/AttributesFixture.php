<?php
namespace Manager\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CategoriesFixture
 *
 */
class AttributesFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
  public $fields = [
    'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
    'title' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
    'card_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
    'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
    'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
    '_constraints' => ['primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
    ],
    '_options' => ['engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
    ],
  ];

/**
 * Records
 *
 * @var array
 */
  public $records = [
    [
      'id' => 1,
      'title' => 'Attribute 1',
      'card_id' => 1,
      'created' => '2014-12-03 14:42:21',
      'modified' => '2014-12-03 14:42:21'
    ],
    [
      'id' => 2,
      'title' => 'Attribute 2',
      'card_id' => 1,
      'created' => '2014-12-03 14:42:21',
      'modified' => '2014-12-03 14:42:21'
    ],
   
  ];

}
