<?php
namespace Manager\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CategoriesFixture
 *
 */
class TagsFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
  public $fields = [
    'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
    'title' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
    'slug' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
    'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
    'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
    '_constraints' => [
      'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
    ],
    '_options' => ['engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
    ],
  ];

/**
 * Records
 *
 * @var array
 */
  public $records = [
    [
      'id' => 1,
      'title' => 'Tag 1',
      'created' => '2014-12-03 14:42:21',
      'modified' => '2014-12-03 14:42:21'
    ],
    [
      'id' => 2,
      'title' => 'Tag 2',
      'created' => '2014-12-03 14:42:21',
      'modified' => '2014-12-03 14:42:21'
    ],
    [
      'id' => 3,
      'title' => 'Tag 1',
      'created' => '2014-12-03 14:42:21',
      'modified' => '2014-12-03 14:42:21'
    ],
    [
      'id' => 4,
      'title' => 'Tag 2',
      'created' => '2014-12-03 14:42:21',
      'modified' => '2014-12-03 14:42:21'
    ]
    
  ];

}
