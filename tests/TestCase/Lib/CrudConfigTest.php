<?php
namespace Manager\Test\TestCase\Lib;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;
use Cake\Core\Configure;
use Manager\Lib\CrudConfig;
use Manager\Lib\Templates;
use Cake\I18n\I18n;
use I18n\Lib\Lang;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;
use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;

class ContentsTable extends Table 
{
  
}

class CategoriesTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Lib\Category');
  }
}

class RowsTable extends Table
{

}

class CardsTable extends Table
{
  public function initialize( array $config)
  {
    $this->hostia = 'hostia';
    $this->addBehavior( 'Manager.Crudable');

    $this->crud
      ->addFields([
        'title' => 'Título',
        'price' => 'Precio',
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'price',
        ],
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Ficha'),
        'plural' => __d( 'admin', 'Fichas'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'title',
                  'price',
                ]
              ]
            ]
          ],
        ],
      ], ['update']);
  }
}

class Category extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'id' => true,
    'title' => true,
    'contents' => true
  ];
}

/**
 * Manager\Lib\CrudConfig Test Case
 */
class CrudConfigTest extends TestCase 
{
  public $fixtures = [
    'plugin.manager.contents',
    'plugin.block.rows',
    'plugin.manager.categories',
    'plugin.manager.content_categories',
    'plugin.manager.cards',
    'plugin.section.sections',
    'plugin.website.sites',
    'Languages' => 'plugin.i18n.languages',
    'plugin.manager.translates',
  ];

/**
 * setUp method
 *
 * @return void
 */
  public function setUp() 
  {
    Plugin::unload( 'Website'); // Deshabilitamos plugins que dan problemas
    Plugin::unload( 'Section');
    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);
    $this->Contents->addBehavior( 'Manager.Crudable');

    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }

/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() 
  {
    unset($this->Contents);

    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }

  public function testHasMany()
  {
    $this->Contents->hasMany( 'Cards', [
      'className' => '\Manager\Test\TestCase\Lib\CardsTable'
    ]);

    $this->Contents->crud->associations( ['Cards']);
    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
        'cards' => [
          'label' => 'Fichas',
          'type' => 'hasMany'
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
        ],
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Contenido'),
        'plural' => __d( 'admin', 'Contenidos'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'title',
                  'cards',
                ]
              ]
            ]
          ],
        ],
      ], ['update']);

      $data = $this->Contents->crud->serialize( 'create');
      $this->assertTrue( array_key_exists( 'crudConfig',  $data ['view']['columns'][0]['box'][0]['elements'][1]));
      $this->assertTrue( !empty( $data ['view']['columns'][0]['box'][0]['elements'][1]));
  }

  public function testBuild()
  {
    $this->Contents->hasMany( 'Rows', [
      'alias' => 'Rows', 
      'foreignKey' => 'content_id', 
      'className' => 'Rows',
      'order' => ['Rows.position']
    ]);
    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
        'body' => [
          'label' => 'Cuerpo',

        ],
        'photo' => [
          'type' => 'upload',
          'label' => 'Foto',
          'config' => [
            'type' => 'post',
            'size' => 'thm'
          ]
        ],
        'rows' => [
          'label' => __d( 'admin', 'Filas'),
          'type' => 'hasMany',
          'template' => 'Block.rows'
        ]
      ])

      ->addIndex( 'index', [
        'fields' => [
          'title',
          'created' => 'Listado'
        ],
      ])

      ->setName( [
        'singular' => __d( 'admin', 'Noticias'),
        'plural' => __d( 'admin', 'Noticias'),
      ])

      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Creación',
                'elements' => [
                  'title',
                  'body',
                ]
              ]
            ]
          ]
        ]
      ])

      ->addView( 'update', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Edición',
                'elements' => [
                  'title',
                  'body',
                  'photo',
                  'rows'
                ]
              ]
            ]
          ]
        ]
      ]);
  }

  public function testAddIndex()
  {
    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
      ])->setName( [
        'singular' => __d( 'admin', 'Contenido'),
        'plural' => __d( 'admin', 'Contenidos'),
      ])->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Creación',
                'elements' => [
                  'title',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => [
          'create',
          [
            'url' => 'index',
            'label' => __d( 'admin', 'Listado')
          ]
        ]
      ])->addIndex( 'index', [
        'fields' => [
          'title',
          'created' => 'Listado'
        ],
        'actionButtons' => [
          'create'
        ]
      ]);

  }

  public function testAddView()
  {
    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
      ])->setName( [
        'singular' => __d( 'admin', 'Contenido'),
        'plural' => __d( 'admin', 'Contenidos'),
      ])->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Creación',
                'elements' => [
                  'title',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => [
          'create',
          [
            'url' => 'index',
            'label' => __d( 'admin', 'Listado')
          ]
        ]
      ])->addIndex( 'index', [
        'fields' => [
          'title',
          'created' => 'Listado'
        ],
        'actionButtons' => [
          'create'
        ]
      ]);

    $data = $this->Contents->crud->serialize( 'index');    
    $field = $data ['view']['fields'][0];

    $this->assertSame( $field ['key'], 'title');
    $this->assertSame( $field ['translate'], false);
    $this->assertSame( $field ['label'], 'Título');

    $field = $data ['view']['fields'][1];
    $this->assertSame( $field ['label'], 'Listado');

    

    $data = $this->Contents->crud->serialize( 'create');

    $this->assertEquals( $data ['view']['actionButtons'][0]['label'], 'Nuevo');
    $this->assertEquals( $data ['view']['actionButtons'][0]['url'], '#/admin/manager/contents/create');
    $this->assertEquals( $data ['view']['actionButtons'][1]['label'], 'Listado');
    $this->assertEquals( $data ['view']['actionButtons'][1]['url'], '#/admin/manager/contents');

    $this->assertEquals( $data ['view']['template'], 'Manager/create');

    $this->assertEquals( $data ['view']['submit']['url'], '/admin/manager/contents/create.json');
    $this->assertEquals( $data ['view']['columns'][0]['box'][0]['title'], 'Creación');

    $element = $data ['view']['columns'][0]['box'][0]['elements'][0];

    $this->assertSame( $element ['type'], 'string');
    $this->assertSame( $element ['show'], 'true');
    $this->assertSame( $element ['translate'], false);
    $this->assertSame( $element ['label'], 'Título');
    $this->assertSame( $element ['template'], 'Manager/fields/string');
    $this->assertSame( $element ['key'], 'title');

    


  }


  public function testAssociationDataSelectMultiple()
  {
    $this->Contents->belongsToMany( 'Categories', [
      'joinTable' => 'content_categories',
      'foreignKey' => 'content_id',
      'targetForeignKey' => 'category_id',
      'className' => 'Manager\Test\TestCase\Lib\CategoriesTable',
    ]);

    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
        'body' => [
          'label' => 'Cuerpo',
        ],
        'categories' => [
          'type' => 'select_multiple',
          'label' => __d( 'admin','Categorías'),
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'created' => 'Listado'
        ]
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Noticias'),
        'plural' => __d( 'admin', 'Noticias'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Edición',
                'elements' => [
                  'title',
                  'categories',
                ]
              ]
            ]
          ]
        ] 
      ]);

    $a = $this->Contents->crud->associations( ['Categories']);

    $content = $this->Contents->find( 'content')
      ->where(['Contents.id' => 1])
      ->first();

    $this->Contents->crud->setContent( $content);


    $data = $this->Contents->crud->serialize( 'create');
    $this->assertEquals( 1, $data ['view']['columns'][0]['box'][0]['elements'][1]['options'][0]['id']);

    // Se verifica que se ha seteado el key `addUrl`
    $this->assertEquals( '/admin/manager/categories/create', $data ['view']['columns'][0]['box'][0]['elements'][1]['addUrl']);
  }


  public function testAssociationDataBelongsTo()
  {    
    $this->Contents->belongsTo( 'Categories', [
      'foreignKey' => 'category_id',
      'className' => 'Blog.Categories',
    ]);

    $this->Contents->crud->associations( ['Categories']);

    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
        'body' => [
          'label' => 'Cuerpo',
        ],
        'category' => [
          'type' => 'select_multiple',
          'label' => __d( 'admin','Categorías'),
        ],
      ])->addIndex( 'index', [
        'fields' => [
          'title',
          'created' => 'Listado'
        ]
      ])->setName( [
        'singular' => __d( 'admin', 'Noticias'),
        'plural' => __d( 'admin', 'Noticias'),
      ])->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Edición',
                'elements' => [
                  'title',
                  'category',
                ]
              ]
            ]
          ]
        ] 
      ]);

    $data = $this->Contents->crud->serialize( 'create');

    $this->assertSame( 1, $data ['view']['columns'][0]['box'][0]['elements'][1]['options'][0]->id);
  }

  public function testDetectFieldTypeBelongsTo()
  {
    $this->Contents->belongsTo( 'Categories', [
      'foreignKey' => 'category_id',
      'className' => 'Blog.Categories',
    ]);

    $this->Contents->crud->associations( ['Categories']);

    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
        'body' => [
          'label' => 'Cuerpo',
        ],
        'category' => __d( 'admin','Categorías'),
      ])->addIndex( 'index', [
        'fields' => [
          'title',
          'created' => 'Listado'
        ]
      ])->setName( [
        'singular' => __d( 'admin', 'Noticias'),
        'plural' => __d( 'admin', 'Noticias'),
      ])->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Edición',
                'elements' => [
                  'title',
                  'category',
                ]
              ]
            ]
          ]
        ] 
      ]);

    $data = $this->Contents->crud->serialize( 'create');

    // Comprueba que el tipo de la asociación correcta
    $this->assertEquals( $data ['view']['columns'][0]['box'][0]['elements'][1]['type'], 'BelongsTo');

    // Comprueba que el key del campo es el correcto
    $this->assertEquals( $data ['view']['columns'][0]['box'][0]['elements'][1]['key'], 'category');
  }

  public function testValidationError()
  {
    $this->Contents->belongsTo( 'Categories', [
      'foreignKey' => 'category_id',
      'className' => 'Blog.Categories',
    ]);

    $this->Contents->validator()
      ->notEmpty( 'category_id', 'Este campo no puede estar vacío')
      ->notEmpty( 'title', 'Este campo no puede estar vacío');

    $this->Contents->crud->associations( ['Categories']);


    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
        'category' => 'Categoría'
      ])
      ->addView( 'create', [
        'template' => 'Blog/add',
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Edición',
                'elements' => [
                  'title',
                  'category'
                ]
              ]
            ]
          ]
        ] 
      ]);

    $data = [
      'title' => '',
      'category_id' => '',
    ];

    $content = $this->Contents->getNewEntity( $data);
    $this->Contents->saveContent( $content);

    $this->Contents->crud->setEntity( $content);

    $view = $data = $this->Contents->crud->serialize( 'create');

    $this->assertEquals( $view ['view']['columns'][0]['box'][0]['elements'][0]['error']['_empty'], 'Este campo no puede estar vacío');
    $this->assertEquals( $view ['view']['columns'][0]['box'][0]['elements'][1]['error']['_empty'], 'Este campo no puede estar vacío');
  }


  public function testTemplateView()
  {
    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
        'published' => 'Publicado'
      ])->addIndex( 'index', [
        'fields' => [
          'title',
          'published',
          'created'
        ]
      ])->setName( [
        'singular' => __d( 'admin', 'Noticias'),
        'plural' => __d( 'admin', 'Noticias'),
      ])->addView( 'create', [
        'template' => 'Blog/add',
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Edición',
                'elements' => [
                  'title' => [
                    'default' => 'Título'
                  ],
                ]
              ]
            ]
          ]
        ] 
      ]);

    $view = $this->Contents->crud->serialize( 'create');

    $this->assertEquals( $view ['view']['template'], 'Blog/add');
    $this->assertEquals( $view ['view']['title'], 'Nuevo');
    $this->assertEquals( $view ['view']['columns'][0]['box'][0]['elements'][0]['default'], 'Título');
    $this->assertTrue( $view ['view']['saveButton']);

    $this->Contents->crud
      ->addView( 'create', [
        'title' => 'Creación',
        'template' => 'Blog/add',
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Edición',
                'elements' => [
                  'title',
                ]
              ]
            ]
          ]
        ] 
      ]);

    $view = $this->Contents->crud->serialize( 'create');
    $this->assertEquals( $view ['view']['title'], 'Creación');

    $view = $this->Contents->crud->serialize( 'index');
    $this->assertEquals( $view ['view']['fields'][0]['label'], 'Título');
    $this->assertEquals( $view ['view']['fields'][1]['label'], 'Publicado');
  }


  public function testJSFiles()
  {
    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
      ])->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                ]
              ]
            ]
          ]
        ] 
      ])->addJsFiles(['/manager/js/file.js']);

    $view = $this->Contents->crud->serialize( 'create');

    $this->assertSame( $view ['jsFiles'], ['/manager/js/file.js']);
  }

/**
 * Comprueba que se ejecuta correctamente crud->defaults()
 */
  public function testDefaults()
  {
    // Configuración con array
    $expected = ['title' => 'Hola'];
    $this->Contents->crud->defaults( $expected);

    $defaults = $this->Contents->crud->defaults();
    $this->assertSame( $expected, $defaults);

    $this->setLanguages();

    // Añadimos un callable
    $this->Contents->crud->defaults( function( $data){
      $locales = Lang::iso3();

      foreach( $locales as $key => $locale)
      {
        $data [$key]['title'] = '';
      }

      return $data;
    });

    $defaults = $this->Contents->crud->defaults();
    $this->assertTrue( array_key_exists( 'spa', $defaults));
    $this->assertTrue( array_key_exists( 'eng', $defaults));
  }


/**
 * Verifica que se setean bien las options cuando éstas son un Closure
 */
  public function testOptionsClosure()
  {
    $this->Contents->crud
      ->addFields([
        'title' => [
          'label' => 'Título',
          'options' => function( $crud){
            return [
              'table' => $crud->table()->alias()
            ];
          }
        ],
      ])->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'title',
                ]
              ]
            ]
          ]
        ] 
      ]);

    $view = $this->Contents->crud->serialize( 'create');
    $this->assertEquals( 'Contents', $view ['view']['columns'][0]['box'][0]['elements'][0]['options']['table']);
  }
  

}
