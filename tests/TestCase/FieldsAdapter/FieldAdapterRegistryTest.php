<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://book.cakephp.org/2.0/en/development/testing.html CakePHP(tm) Tests
 * @since         2.0.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Cake\Test\TestCase\Console;

use Manager\FieldAdapter\FieldAdapterRegistry;
use Cake\Core\App;
use Cake\Core\Plugin;
use Cake\TestSuite\TestCase;

/**
 * Class TaskRegistryTest
 *
 */
class FieldAdapterRegistryTest extends TestCase 
{

/**
 * setUp
 *
 * @return void
 */
  public function setUp() 
  {
    parent::setUp();
  }

/**
 * tearDown
 *
 * @return void
 */
  public function tearDown() 
  {
    unset($this->FieldAdapters);
    parent::tearDown();
  }

/**
 * test para comprobar el set/get de FieldAdapterRegistry
 *
 * @return void
 */
  public function testLoad() 
  {
    $config = FieldAdapterRegistry::get( 'ckeditor')->get( 'Posts', 'body');

  }

  public function testAdd()
  {
    $data = [
      'one' => 'Uno',
      'two' => 'Dos',
      'three' => [
        'one' => 'Uno'
      ]
    ];

    FieldAdapterRegistry::get( 'ckeditor')->addConfig( 'special', $data);
    $result = FieldAdapterRegistry::get( 'ckeditor')->getConfig( 'special');
    $this->assertSame( $data, $result);
  }

  public function testClone()
  {
    $data = [
      'one' => 'Uno',
      'two' => 'Dos',
      'three' => [
        'one' => 'Uno'
      ]
    ];

    FieldAdapterRegistry::get( 'ckeditor')->addConfig( 'special', $data);

    $clone = [
      'four' => [
        'one' => 'Uno'
      ]
    ];

    $data ['four'] = [
      'one' => 'Uno'
    ];

    FieldAdapterRegistry::get( 'ckeditor')->cloneConfig( 'special', $clone, 'new');
    $result = FieldAdapterRegistry::get( 'ckeditor')->getConfig( 'new');
    $this->assertSame( $data, $result);

    $clone = [
      'four' => [
        'one1' => 'Uno1'
      ]
    ];

    $data ['four'] = [
      'one1' => 'Uno1',
    ];

    FieldAdapterRegistry::get( 'ckeditor')->cloneConfig( 'special', $clone, 'new');
    $result = FieldAdapterRegistry::get( 'ckeditor')->getConfig( 'new');
    $this->assertSame( $data, $result);
  }


}
