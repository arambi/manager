<?php

namespace Manager\Test\Navigation;

use Cake\TestSuite\TestCase;
use Manager\Navigation\NavigationCollection;
use Cake\Core\Plugin;


class NavigationCollectionTest extends TestCase
{

  public $fixtures = [
  ];
  
  public function setUp() 
  {
    if( Plugin::loaded( 'Section'))
    {
      Plugin::unload( 'Section');
    }

    if( Plugin::loaded( 'Website'))
    {
      Plugin::unload( 'Website');
    }

    parent::setUp();
  }


  public function tearDown() 
  {
    parent::tearDown();
  }

  public function addItems()
  {
    NavigationCollection::reset();

    $data = [
      'name' => 'Noticias',
      'icon' => 'fa fa-doc',
      'key' => 'posts'
    ];

    NavigationCollection::add( $data);

    $data = [
      'name' => 'Listado',
      'parentName' => 'Noticias',
      'plugin' => 'Blog',
      'controller' => 'Posts',
      'action' => 'index',
      'icon' => 'fa fa-doc',
      'parent' => 'posts'
    ];

    NavigationCollection::add( $data);

    $data = [
      'name' => 'Nueva',
      'parentName' => 'Noticias',
      'plugin' => 'Blog',
      'controller' => 'Posts',
      'action' => 'add',
      'icon' => 'fa fa-doc',
      'parent' => 'posts'
    ];

    NavigationCollection::add( $data);

    $data = [
      'name' => 'Productos',
      'plugin' => 'Shop',
      'controller' => 'Products',
      'action' => 'index',
      'icon' => 'fa fa-doc',
    ];

    NavigationCollection::add( $data);
  }

  public function testAdd()
  {
    $this->addItems();

    $nav = NavigationCollection::get( 'blog_posts_index');
    $this->assertEquals( 'Posts', $nav->controller);
    $this->assertEquals( 'Listado', $nav->menuName);
  }

  public function testNav()
  {
    $this->addItems();
    $nav = NavigationCollection::nav();
    $this->assertEquals( 'posts', $nav [0]['key']);
    $this->assertEquals( 'blog_posts_index', $nav [0]['children'][0]['key']);
  }

  public function testSetNav()
  {
    $this->addItems();
    $data = [
      'blog_posts_index',
      'shop_products_index',
      'posts' => [
          'blog_posts_add'
      ]
    ];

    $nav = NavigationCollection::nav( $data);
    $this->assertEquals( 'blog_posts_index', $nav [0]['key']);
    $this->assertEquals( 'Noticias', $nav [0]['parentName']);
    $this->assertEquals( 'Nueva', $nav [2]['children'][0]['menuName']);
  }

  public function testGetCurrent()
  {
    $this->addItems();
    $params = array(
        'plugin' => 'Blog',
        'controller' => 'Posts',
        'action' => 'add'
    );

    $current = NavigationCollection::getCurrent( $params);
    $this->assertEquals( 'blog_posts_add', $current->key);
    $this->assertEquals( 'Nueva', $current->menuName);
  }
}