<?php

namespace Cake\Test\TestCase\Console;

use Manager\Dummy\DummyContent;
use Cake\Core\App;
use Cake\Core\Plugin;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use I18n\Lib\Lang;

/**
 * Class TaskRegistryTest
 *
 */
class DummyContentTest extends TestCase 
{

  public $fixtures = [
    'plugin.manager.contents',
    'plugin.block.rows',
    'plugin.block.columns',
    'plugin.slug.slugs',
    'plugin.slug.slugs',
    'plugin.manager.drafts',
    'plugin.seo.seos',
    'plugin.i18n.languages',
    'plugin.upload.uploads',
    'plugin.manager.translates',
  ];

/**
 * setUp
 *
 * @return void
 */
  public function setUp() 
  {
    parent::setUp();
    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);

  }

/**
 * tearDown
 *
 * @return void
 */
  public function tearDown() 
  {
    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }

  public function testDelete()
  {
    $this->setLanguages();
    $dummy = new DummyContent( 'Blog.Posts');
    $dummy->truncate();
  }

  public function testSave()
  {
    $this->setLanguages();
    $dummy = new DummyContent( 'Blog.Posts');
    $dummy->setLimit( 1);
    $new = $dummy->save();
  }
}
