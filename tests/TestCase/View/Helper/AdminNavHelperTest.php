<?php
namespace Manager\Test\TestCase\View\Helper;

use Cake\View\View;
use Manager\View\Helper\AdminNavHelper;
use Cake\TestSuite\TestCase;

/**
 * Manager\View\Helper\AdminNavHelper Test Case
 */
class AdminNavHelperTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$view = new View();
		$this->AdminNav = new AdminNavHelper($view);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AdminNav);

		parent::tearDown();
	}


	public function testEmpty()
	{

	}

}
