<?php
namespace Manager\Test\TestCase\View\Helper;

use Cake\View\View;
use Manager\View\Helper\AdminUtilHelper;
use Cake\TestSuite\TestCase;

/**
 * Manager\View\Helper\AdminUtilHelper Test Case
 */
class AdminUtilHelperTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$view = new View();
		$this->AdminUtil = new AdminUtilHelper($view);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AdminUtil);

		parent::tearDown();
	}


	public function testEmpty()
	{

	}

}
