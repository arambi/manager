<?php
namespace Manager\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Manager\View\Helper\EditableHelper;

/**
 * Manager\View\Helper\EditableHelper Test Case
 */
class EditableHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Manager\View\Helper\EditableHelper
     */
    public $Editable;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Editable = new EditableHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Editable);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
