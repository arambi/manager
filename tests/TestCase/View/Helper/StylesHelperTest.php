<?php
namespace Manager\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Manager\View\Helper\StylesHelper;

/**
 * Manager\View\Helper\StylesHelper Test Case
 */
class StylesHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Manager\View\Helper\StylesHelper
     */
    public $Styles;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Styles = new StylesHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Styles);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
