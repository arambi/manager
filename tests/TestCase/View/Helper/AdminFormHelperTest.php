<?php
namespace Manager\Test\TestCase\View\Helper;

use Cake\View\View;
use Manager\View\Helper\AdminFormHelper;
use Cake\TestSuite\TestCase;

/**
 * Manager\View\Helper\AdminFormHelper Test Case
 */
class AdminFormHelperTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$view = new View();
		$this->AdminForm = new AdminFormHelper($view);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AdminForm);

		parent::tearDown();
	}

	public function testEmpty()
	{

	}

}
