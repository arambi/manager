<?php

namespace Manager\Test\TestCase\Controller;

use Cake\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestCase;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Manager\Controller\Component\CrudToolComponent;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Routing\Router;
use Cake\I18n\I18n;
use Manager\Controller\CrudControllerTrait;
use Cake\Controller\Controller;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\Network\Request;
use Cake\Network\Response;
use I18n\Lib\Lang;

/**
 * Content Table
 */
class ContentsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Controller\Content');

    $this->crud
      ->addFields([
        'title' => 'Título',
      ]);
  }
}

class CategoriesTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Controller\Category');
  }
}

/**
 * Content Entity
 */
class Content extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
  protected $_accessible = [
    'content_type' => true,
    'site_id' => true,
    'slug' => true,
    'title' => true,
  ];
}

/**
 * Card Entity
 */
class Card extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'title' => true,
    'attributes' => true,
    'content_type' => true,
  ];
}

/**
 * Attribute Entity
 */
class Attribute extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'title' => true,
  ];
}

/**
 * Category Entity
 */
class Category extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'id' => true,
    'title' => true,
    'contents' => true
  ];
}

/**
 * ContentController
 */
class ContentsController extends Controller
{
  use CrudControllerTrait;
}

/**
 * Manager\Controller\CrudControllerTrait Test Case
 */
class CrudControllerTraitTest extends TestCase 
{

  public $fixtures = [
    'plugin.manager.contents',
    'plugin.manager.cards',
    'plugin.manager.attributes',
    'plugin.manager.content_categories',
    'plugin.manager.categories',
    'plugin.i18n.languages',
    'plugin.manager.translates',
    'plugin.section.sections'
  ];


/**
 * setUp method
 *
 * @return void
 */
  
  public function setUp() 
  {
    parent::setUp();
    
    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);
    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);

    // Preparación del Controller
    $response = $this->getMock('Cake\Network\Response');
    $this->Controller = new ContentsController( new Request(), $response);
    $this->Controller->Contents = $this->Contents;
    $this->View = $this->Controller->createView();
    $this->Controller->startupProcess();
  }


/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() 
  {
    parent::tearDown();

  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }

/**
 * Comprueba que se ha enchufado el component CrudTool
 */
  public function testHasComponent()
  {
    $this->assertTrue( isset( $this->Controller->CrudTool));
  }

/**
 * Test con comprobaciones al crear un contenido
 */
  public function testCreate()
  {
    $this->setLanguages();
    $this->Controller->create();

    // Comprueba que CrudTool setea correctamente el array con la entidad vacía
    $this->Controller->Contents->crud->addView( 'create', [
      'columns' => [
        [
          'cols' => 8,
          'box' => [
            [
              'elements' => [
                'title',
              ]
            ]
          ]
        ]
      ]
    ]);

    $content = $this->Controller->Table->crud->getContent();
    $this->assertTrue( array_key_exists( 'title', $content));
  }

/**
 * Test con comprobaciones al crear un contenido que tiene campos traducibles
 */
  public function testCreateWithTranslation()
  {
    $this->Contents->addBehavior( 'I18n.I18nable', [
      'fields' => ['title']
    ]);
    $this->Controller->create();

    // Comprueba que CrudTool setea correctamente el array con la entidad vacía, y sus idiomas
    $content = $this->Controller->Table->crud->getContent();
    $this->assertTrue( array_key_exists( 'spa', $content));
    $this->assertTrue( array_key_exists( 'title', $content ['spa']));
  }



}