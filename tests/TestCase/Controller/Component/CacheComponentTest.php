<?php
namespace Manager\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Manager\Controller\Component\CacheComponent;

/**
 * Manager\Controller\Component\CacheComponent Test Case
 */
class CacheComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Manager\Controller\Component\CacheComponent
     */
    public $Cache;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Cache = new CacheComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cache);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
