<?php
namespace Manager\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\Controller\Component\FlashComponent;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Manager\Controller\Component\CrudToolComponent;
use Cake\TestSuite\TestCase;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\I18n\I18n;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\Network\Request;
use Cake\Network\Session;
use I18n\Lib\Lang;

class Content extends Entity {

	use TranslateTrait;

	protected $_accessible = [
		'title' => true,
		'body' => true,
		'categories' => true
	];	
}

/**
 * Manager\Controller\Component\CrudToolComponent Test Case
 */
class CrudToolComponentTest extends TestCase {

	public $fixtures = [
    'plugin.manager.contents',
    'plugin.manager.translates',
    'plugin.i18n.languages',
    'plugin.manager.categories',
    'plugin.manager.content_categories'
  ];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		Configure::write('App.namespace', 'TestApp');
		$this->Controller = new Controller(new Request(['session' => new Session()]));
		$this->ComponentRegistry = new ComponentRegistry($this->Controller);
		$this->CrudTool = new CrudToolComponent($this->ComponentRegistry);
		$this->CrudTool->Controller = $this->Controller;
    $this->Contents = TableRegistry::get('Contents');
    $this->Contents->entityClass( 'Manager\Test\TestCase\Controller\Component\Content');
    $this->Contents->addBehavior( 'Manager.Crudable');
    $this->Contents->validator()->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->notEmpty('title')
			->notEmpty('body');

    $this->CrudTool->Table = $this->Contents;
    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset( $this->CrudTool->Controller);
		unset( $this->CrudTool->Table);
		unset( $this->CrudTool);
		unset( $this->Contents);
		unset( $this->Languages);
		unset( $this->Controller);
		unset( $this->ComponentRegistry );
		parent::tearDown();
		TableRegistry::clear();
	}

	public function setLanguages()
	{
		Lang::set( $this->Languages->find()->all());
		$this->Contents->addBehavior( 'I18n.I18nable', ['fields' => ['title', 'body']]);
	}

	public function testSetAssociationData()
	{
		$this->Contents->belongsToMany( 'Categories', [
			'joinTable' => 'content_categories',
			'foreignKey' => 'content_id',
			'targetForeignKey' => 'category_id',
			'className' => 'Blog.Categories',
		]);

		$this->Contents->crud
			->addFields([
				'title' => 'Título',
				'body' => [
					'label' => 'Cuerpo',
				],
				'categories' => [
					'type' => 'select_multiple',
					'label' => __d( 'admin','Categorías'),
				],
			])->addIndex( 'index', [
				'fields' => [
					'title',
					'created' => 'Listado'
				]
			])->setName( [
				'singular' => __d( 'admin', 'Noticias'),
				'plural' => __d( 'admin', 'Noticias'),
			])->addView( 'create', [
				'columns' => [
					[
						'cols' => 8,
						'box' => [
							[
								'title' => 'Edición',
								'elements' => [
									'title',
									'categories',
								]
							]
						]
					]
				]	
			]);

		$this->Contents->crud->associations( ['Categories']);

		
	}

}
