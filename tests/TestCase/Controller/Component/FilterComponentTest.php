<?php
namespace Manager\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Manager\Controller\Component\FilterComponent;
use Cake\Controller\Controller;
use Cake\Network\Request;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Session;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use I18n\Lib\Lang;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\I18n\I18n;
use Manager\Model\Entity\CrudEntityTrait;

class ContentsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Controller\Component\ContentFilter');
  }

}

class CategoriesTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Controller\Component\CategoryFilter');
  }
}

class TagsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Controller\Component\Tag');
  }
}

class CategoryFilter extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'id' => true,
    'title' => true,
    'contents' => true
  ];
}



class ContentFilter extends Entity {

  use TranslateTrait;
  use CrudEntityTrait;

  protected $_accessible = [
    'title' => true,
    'body' => true,
    'category' => true,
    'tags'
  ];  
}

class Tag extends Entity {

  use TranslateTrait;
  use CrudEntityTrait;

  protected $_accessible = [
    'title' => true,
    'slug' => true,
  ];  
}


/**
 * Manager\Controller\Component\FilterComponent Test Case
 */
class FilterComponentTest extends TestCase
{

  public $Controller;

  public $fixtures = [
    'plugin.manager.contents',
    'plugin.manager.translates',
    'plugin.i18n.languages',
    'plugin.manager.categories',
    'plugin.manager.tags',
    'plugin.manager.content_tags'
  ];

/**
 * setUp method
 *
 * @return void
 */
  public function setUp()
  {
    parent::setUp();
    
    if( Plugin::loaded( 'Section'))
    {
      Plugin::unload( 'Section');
    }

    if( Plugin::loaded( 'Website'))
    {
      Plugin::unload( 'Website');
    }

    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);

    $this->Categories = new CategoriesTable([
      'alias' => 'Categories',
      'table' => 'categories',
      'connection' => $this->connection
    ]);

    $this->Tags = new TagsTable([
      'alias' => 'Tags',
      'table' => 'tags',
      'connection' => $this->connection
    ]);

    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset( $this->Filter);
    unset( $this->Controller);

    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
    $this->Contents->addBehavior( 'I18n.I18nable', ['fields' => ['title', 'body']]);
  }

  public function setQuery( $query)
  {
    Configure::write('App.fullBaseUrl', 'http://localhost');
    $this->Controller = new Controller( new Request([
      'session' => new Session(),
      'query' => $query,
    ]));

    $registry = new ComponentRegistry( $this->Controller);
    $this->Filter = new FilterComponent( $registry);

    $this->Filter->request->params = [
      'plugin' => false,
      'controller' => 'contents',
      'action' => 'index'
    ];
    
    $this->Contents->belongsTo( 'Categories', [
        'className' => 'Manager\Test\TestCase\Controller\Component\CategoriesTable'
    ]);

    $this->Contents->belongsToMany( 'Tags', [
      'className' => 'Manager\Test\TestCase\Controller\Component\TagsTable',
      'joinTable' => 'content_tags',
      'foreignKey' => 'content_id',
      'targetForeignKey' => 'tag_id',
    ]);

    $this->Contents->crud->associations(['Categories', 'Tags']);

    $this->Contents->crud
      ->addFields([
        'title' => 'Título',
        'body' => 'Cuerpo',
        'published' => 'Publicado',
        'published_at' => 'Fecha de publicación',
        'category' => 'Categorías',
        'tags' => 'Tags'
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'body',
          'published',
          'published_at',
          'category',
          'tags'
        ]
      ]);

    $this->Filter->setTable( $this->Contents);
    
  }

  /**
   * Query simple a un solo campo
   */
  public function testStringQuery()
  {
    $this->setQuery([
        'title' => 'Title content 1',
    ]);

    $result = $this->Filter->query()->toArray();
    $this->assertEquals( 'Title content 1', $result [0]->title);
  }

/**
 * Query simple a un solo campo con traducción
 */
  public function testStringQueryTranslate()
  {
    $this->setQuery([
        'title' => 'Hola',
    ]);

    $this->setLanguages();
    I18n::locale('spa');

    $result = $this->Filter->query()->toArray();
    $this->assertEquals( 'Hola', $result [0]->title);
  }

  public function testBooleanQuery()
  {
    $this->setQuery([
        'published' => false,
    ]);

    $result = $this->Filter->query()->toArray();
    $this->assertEquals( 2, count( $result));

    $this->setQuery([
        'published' => true,
    ]);

    $result = $this->Filter->query()->toArray();
    $this->assertEquals( 1, count( $result));
  }

  public function testDateQuery()
  {
    $this->setQuery([
        'published_at' => [
          'from' => '2014-12-01',
          'to' => '2014-12-02'
        ],
    ]);

    $result = $this->Filter->query()->toArray();
    $this->assertEquals( 2, count( $result));

    $this->setQuery([
        'published_at' => [
          'from' => '2014-12-01',
          'to' => '2014-12-01'
        ],
    ]);

    $result = $this->Filter->query()->toArray();
    $this->assertEquals( 1, count( $result));

    $this->setQuery([
        'published_at' => [
          'from' => '2014-02-01',
          'to' => '2014-02-02'
        ],
    ]);

    $result = $this->Filter->query()->toArray();
    $this->assertEquals( 0, count( $result));
  }


  public function testBelongsToQuery()
  {
    $this->setQuery([
        'category' => [
          [
            'id' => 1
          ]
        ],
    ]);

    $result = $this->Filter->query()
      ->contain( $this->Contents->containIndex())
      ->toArray();

    $this->assertEquals( 2, count( $result));

    $this->setQuery([
        'category' => [
          [
            'id' => 2
          ]
        ],
    ]);

    $result = $this->Filter->query()
      ->contain( $this->Contents->containIndex())
      ->toArray();

    $this->assertEquals( 1, count( $result));
  }

  public function testBelongsToManyQuery()
  {
    $this->setQuery([
        'tags' => [
          [
            'id' => 1
          ]
        ],
    ]);

    $result = $this->Filter->query()
      ->contain( $this->Contents->containIndex())
      ->toArray();

    $this->assertEquals( 2, count( $result));

    $this->setQuery([
        'tags' => [
          [
            'id' => 2
          ]
        ],
    ]);

    $result = $this->Filter->query()
      ->contain( $this->Contents->containIndex())
      ->toArray();

    $this->assertEquals( 1, count( $result));
  }


}
