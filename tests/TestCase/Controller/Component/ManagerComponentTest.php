<?php
namespace Manager\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Manager\Controller\Component\ManagerComponent;
use Cake\TestSuite\TestCase;

/**
 * Manager\Controller\Component\ManagerComponent Test Case
 */
class ManagerComponentTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$registry = new ComponentRegistry();
		$this->Manager = new ManagerComponent($registry);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Manager);

		parent::tearDown();
	}


	public function testEmpty()
	{

	}

}
