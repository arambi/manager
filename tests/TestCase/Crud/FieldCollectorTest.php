<?php
namespace Manager\Test\TestCase\Crud;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;
use Cake\Core\Configure;
use Manager\Lib\CrudConfig;
use Manager\Lib\Templates;
use Cake\I18n\I18n;
use I18n\Lib\Lang;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;
use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Manager\Crud\FieldCollector;

class ContentsTable extends Table 
{
  
}

class CategoriesTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Crud\Category');
  }
}

class RowsTable extends Table
{

}

class CardsTable extends Table
{
  public function initialize( array $config)
  {
    $this->addBehavior( 'Manager.Crudable');

    $this->crud
      ->addFields([
        'title' => 'Título',
        'price' => 'Precio',
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'price',
        ],
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Ficha'),
        'plural' => __d( 'admin', 'Fichas'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'title',
                  'price',
                ]
              ]
            ]
          ],
        ],
      ], ['update']);
  }
}

class Category extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'id' => true,
    'title' => true,
    'contents' => true
  ];
}

/**
 * Manager\Lib\CrudConfig Test Case
 */
class FieldCollectorTest extends TestCase 
{
  public $fixtures = [
    'plugin.manager.contents',
    'plugin.block.rows',
    'plugin.manager.categories',
    'plugin.manager.content_categories',
    'plugin.manager.cards',
    'plugin.section.sections',
    'plugin.website.sites',
    'Languages' => 'plugin.i18n.languages',
    'plugin.manager.translates',
  ];

/**
 * setUp method
 *
 * @return void
 */
  public function setUp() 
  {
    Plugin::unload( 'Website'); // Deshabilitamos plugins que dan problemas
    Plugin::unload( 'Section');
    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);
    $this->Contents->addBehavior( 'Manager.Crudable');

    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }

/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() 
  {
    unset($this->Contents);

    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }

  public function testConstruct()
  {
    $collector = new FieldCollector( $this->Contents);
    $this->assertInstanceOf( 'Manager\Crud\FieldCollector', $collector);
    $this->assertInstanceOf( 'Manager\Test\TestCase\Crud\ContentsTable', $collector->table());
  }

  public function testAddField()
  {
    $collector = new FieldCollector( $this->Contents);
    $collector->add( 'title', 'Título');
  }

  public function testBuildField()
  {
    $collector = new FieldCollector( $this->Contents);
    $collector->add( 'title', 'Título');
    $values = $collector->build( 'title', 'index');
    $this->assertEquals( 'string', $values ['type']);
    $this->assertEquals( 'Título', $values ['label']);
    $this->assertEquals( 'Manager/searches/string', $values ['templateSearch']);
  }
  
  public function testAssociationData()
  {
    $this->Contents->belongsTo( 'Categories', [
      'foreignKey' => 'category_id',
      'className' => 'Blog.Categories',
    ]);

    $this->Contents->crud->associations( ['Categories']);

    $collector = new FieldCollector( $this->Contents);
    $collector->add( 'category', 'Categoría');
    $values = $collector->build( 'category', 'edit');
    $this->assertTrue( is_array( $values ['options']));
    $this->assertTrue( count( $values ['options']) > 0);
  }


  public function testAssociationDataSelectMultiple()
  {
    $this->Contents->belongsToMany( 'Categories', [
      'joinTable' => 'content_categories',
      'foreignKey' => 'content_id',
      'targetForeignKey' => 'category_id',
      'className' => 'Manager\Test\TestCase\Crud\CategoriesTable',
    ]);

    $this->Contents->crud->associations( ['Categories']);

    $collector = new FieldCollector( $this->Contents);
    $collector->add( 'categories', [
      'type' => 'select_multiple',
      'label' => __d( 'admin','Categorías'),
    ]);

    $values = $collector->build( 'categories', 'edit');
    
    $this->assertTrue( is_array( $values ['options']));
    $this->assertTrue( count( $values ['options']) > 0);
  }


 public function testHasMany()
  {
    $this->Contents->hasMany( 'Cards', [
      'className' => '\Manager\Test\TestCase\Crud\CardsTable'
    ]);

    $this->Contents->crud->associations( ['Cards']);

    $collector = new FieldCollector( $this->Contents);
    $collector->add( 'cards', [
      'type' => 'HasMany',
      'label' => __d( 'admin','Fichas'),
    ]);

    $values = $collector->build( 'cards', 'edit');
  }


  public function testClosure()
  {
    $this->Contents->belongsTo( 'Categories', [
      'foreignKey' => 'category_id',
      'className' => 'Blog.Categories',
    ]);

    $this->Contents->crud->associations( ['Categories']);

    $collector = new FieldCollector( $this->Contents);
    $collector->add( 'category', [
      'label' => 'Categoría',
      'options' => function( $crud){
        return ['leches'];
      }
    ]);
    $values = $collector->build( 'category', 'edit');
  }
}
