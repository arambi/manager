<?php
namespace Manager\Test\TestCase\Crud;

use Cake\TestSuite\TestCase;
use Manager\Crud\Flash;


/**
 * Manager\Lib\CrudConfig Test Case
 */
class FlashTest extends TestCase 
{
/**
 * setUp method
 *
 * @return void
 */
  public function setUp() 
  {
    parent::setUp();
  }

/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() 
  {
    unset($this->Contents);

    parent::tearDown();
  }

  public function testAdd()
  {
    Flash::success( 'El contenido ha sido añadido');
    Flash::warning( 'No ha sido posible');
    Flash::error( 'Se ha producido un error');
    Flash::info( 'Es una mera información');
    Flash::info( 'Es otra información');
    $messages = Flash::get();

    $this->assertEquals( 'El contenido ha sido añadido', $messages ['success'][0]);
    $this->assertEquals( 'No ha sido posible', $messages ['warning'][0]);
    $this->assertEquals( 'Se ha producido un error', $messages ['error'][0]);
    $this->assertEquals( 'Es una mera información', $messages ['info'][0]);
    $this->assertEquals( 'Es otra información', $messages ['info'][1]);
  }

}
