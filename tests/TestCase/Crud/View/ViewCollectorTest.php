<?php
namespace Manager\Test\TestCase\Crud\View;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;
use Cake\Core\Configure;
use Manager\Lib\CrudConfig;
use Manager\Lib\Templates;
use Cake\I18n\I18n;
use I18n\Lib\Lang;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;
use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Manager\Crud\FieldCollector;
use Manager\Crud\View\ViewCollector;

class ContentsTable extends Table 
{
  
}

class CategoriesTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Crud\View\Category');
  }
}

class RowsTable extends Table
{

}

class CardsTable extends Table
{
  public function initialize( array $config)
  {
    $this->addBehavior( 'Manager.Crudable');

    $this->crud
      ->addFields([
        'title' => 'Título',
        'price' => 'Precio',
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'price',
        ],
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Ficha'),
        'plural' => __d( 'admin', 'Fichas'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'title',
                  'price',
                ]
              ]
            ]
          ],
        ],
      ], ['update']);
  }
}

class Category extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'id' => true,
    'title' => true,
    'contents' => true
  ];
}

/**
 * Manager\Lib\CrudConfig Test Case
 */
class ViewCollectorTest extends TestCase 
{
  public $fixtures = [
    'plugin.manager.contents',
    'plugin.block.rows',
    'plugin.manager.categories',
    'plugin.manager.content_categories',
    'plugin.manager.cards',
    'plugin.section.sections',
    'plugin.website.sites',
    'Languages' => 'plugin.i18n.languages',
    'plugin.manager.translates',
  ];

/**
 * setUp method
 *
 * @return void
 */
  public function setUp() 
  {
    Plugin::unload( 'Website'); // Deshabilitamos plugins que dan problemas
    Plugin::unload( 'Section');
    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);
    $this->Contents->addBehavior( 'Manager.Crudable');

    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }

/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() 
  {
    unset($this->Contents);

    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }

  public function testConstructor()
  {
    $fieldCollector = new FieldCollector( $this->Contents);
    $viewCollector = new ViewCollector( $fieldCollector);
    $this->assertInstanceOf( 'Manager\Crud\FieldCollector', $viewCollector->fieldCollector());
  }

  public function testAddView()
  {
    $fieldCollector = new FieldCollector( $this->Contents);
    $fieldCollector->add( 'title', 'Título');

    $viewCollector = new ViewCollector( $fieldCollector);
    $viewCollector->add( 'edit', 'update', [
      'columns' => [
        [
          'cols' => 8,
          'box' => [
            [
              'title' => 'Creación',
              'elements' => [
                'title',
              ]
            ]
          ]
        ]
      ],
      'actionButtons' => [
        'create',
        [
          'url' => 'index',
          'label' => __d( 'admin', 'Listado')
        ]
      ]
    ]);


    $view = $viewCollector->build( 'update');
  }


  public function testAddIndex()
  {
    $fieldCollector = new FieldCollector( $this->Contents);
    $fieldCollector->add( 'title', 'Título');

    $viewCollector = new ViewCollector( $fieldCollector);
    $viewCollector->add( 'index', 'index', [
      'fields' => [
        'title',
      ],
      'actionButtons' => [
        'create'
      ]
    ]);

    $view = $viewCollector->build( 'index');
    
  }
}
