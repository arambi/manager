<?php
namespace Manager\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use Manager\Model\Behavior\DrafterBehavior;

/**
 * Manager\Model\Behavior\DrafterBehavior Test Case
 */
class DrafterBehaviorTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Drafter = new DrafterBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Drafter);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
