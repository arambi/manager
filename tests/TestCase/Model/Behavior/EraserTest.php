<?php
namespace Manager\Test\TestCase\Model\Behavior;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;
use Cake\ORM\Entity;
use Manager\Model\Behavior\Eraser;
use Manager\Model\Behavior\EraserCollection;

class PostsTable extends Table 
{
  public function initialize( array $config)
  {
    $this->table( 'contents');
  }
}




/**
 * Manager\Lib\CrudConfig Test Case
 */
class EraserTest extends TestCase 
{
  public $fixtures = [
    'plugin.manager.contents',
  ];

/**
 * setUp method
 *
 * @return void
 */
  public function setUp() 
  {
    Plugin::unload( 'Website'); // Deshabilitamos plugins que dan problemas
    Plugin::unload( 'Section');
    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Posts = new PostsTable([
      'alias' => 'Posts',
      'table' => 'contents',
      'connection' => $this->connection
    ]);

  }

/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() 
  {
    unset($this->Posts);

    parent::tearDown();
  }

  public function testDelete()
  {
    $entities = $this->Posts->find()->all();
    $eraser = new EraserCollection();

    $first = true;

    foreach( $entities as $entity)
    {
      if( $first)
      {
        $id = $entity->id;
        $eraser->addEntity( $this->Posts, $entity);
        $first = false;
      }
      else
      {
        $eraser->addPardoned( $this->Posts, $entity);
      }
    }

    $eraser->delete();

    $entities = $this->Posts->find()->all();
    $has = $entities->firstMatch( ['id' => $id]);
    $this->assertTrue( is_null( $has));

  }

  public function testNoDelete()
  {
    $entities = $this->Posts->find()->all();
    $eraser = new EraserCollection();

    $entity = $entities->first();
    $id = $entity->id;
    $eraser->addEntity( $this->Posts, $entity);

    foreach( $entities as $entity)
    {
      $eraser->addPardoned( $this->Posts, $entity);
    }
    
    $eraser->delete();

    $entities = $this->Posts->find()->all();
    $has = $entities->firstMatch( ['id' => $id]);
    $this->assertTrue( !is_null( $has));

  }
}
