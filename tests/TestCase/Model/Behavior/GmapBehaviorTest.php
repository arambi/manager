<?php

namespace Manager\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use Manager\Model\Behavior\GmapBehavior;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class ContentsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Gmap', [
      'address' => array('address', 'postcode', 'city', 'state', 'country'),
    ]);
  }
}

/**
 * Manager\Model\Behavior\GmapBehavior Test Case
 */
class GmapBehaviorTest extends TestCase
{

  public $fixtures = [
    'plugin.manager.maps',
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
      unset($this->Gmap);

      parent::tearDown();
  }

  public function newRecord()
  {
    $data = [
      'address' => 'San Juan de la Cadena 4',
      'postcode' => '31008',
      'city' => 'Pamplona',
      'state' => 'Navarra',
      'country' => 'España'
    ];

    $entity = $this->Contents->newEntity( $data);
    $this->Contents->save( $entity);

    return $entity;
  }

  /**
   * Guarda un registro y comprueba que se ha generado correctamente las coordenadas
   */
  public function testSave()
  {
    $entity = $this->newRecord();
    $content = $this->Contents->get( $entity->id);

    $this->assertEquals( '42.8111202', $content->lat);
    $this->assertEquals( '-1.6581696', $content->lng);
  }

/**
 * Se cambia la dirección y se debe cambiar las coordenadas
 */
  public function testChanges()
  {
    $entity = $this->newRecord();
    $content = $this->Contents->get( $entity->id);
    $data = [
      'address' => 'San Juan de la Cadena 2',
      'postcode' => '31008',
      'city' => 'Pamplona',
      'state' => 'Navarra',
      'country' => 'España'
    ];

    $this->Contents->patchEntity( $entity, $data);
    $this->Contents->save( $entity);
    $content = $this->Contents->get( $entity->id);

    $this->assertEquals( '42.811536', $content->lat);
    $this->assertEquals( '-1.6577125', $content->lng);
  }

/**
 *  No se cambia la dirección pero se cambian las coordenadas
 *  El behavior en este caso no tiene que actuar puesto que el usuario ha cambiado las coordenadas a mano
 */
  public function testChangeCoords()
  {
    $entity = $this->newRecord();
    $content = $this->Contents->get( $entity->id);
    $data = [
      'lat' => '5',
      'lng' => '5'
    ];

    $this->Contents->patchEntity( $content, $data);
    $this->Contents->save( $content);

    $content = $this->Contents->get( $content->id);

    $this->assertEquals( '5', $content->lat);
    $this->assertEquals( '5', $content->lng);
  }
}
