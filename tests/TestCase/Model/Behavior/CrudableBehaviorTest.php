<?php
namespace Manager\Test\TestCase\Model\Behavior;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Manager\Model\Behavior\CrudableBehavior;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use I18n\Lib\Lang;
use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\I18n\I18n;
use Cake\Utility\Hash;
use Cake\Validation\Validator;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
use Cake\Core\Plugin;
use Cake\Core\Configure;

class ContentsTable extends Table 
{
	public function initialize(array $options) 
	{
		$this->addBehavior( 'Manager.Crudable');
		$this->addBehavior( 'Timestamp');
		$this->entityClass( 'Manager\Test\TestCase\Model\Behavior\Content');
	}

	public function validationDefault( Validator $validator) 
  {
    // $validator
    //   // ->add( 'layout_id', 'valid', ['rule' => 'numeric'])
    //   ->notEmpty( 'title')
    //   ->requirePresence( 'title')
    //   ->notEmpty( 'layout_id');

    return $validator;
  }

  public function validationTranslated( Validator $validator) 
  {
    $validator
      ->notEmpty( 'title')
      ->requirePresence( 'title');

    return $validator;
  }
}

class CategoriesTable extends Table 
{
	public function initialize(array $options) 
	{
		$this->addBehavior( 'Manager.Crudable');
		$this->addBehavior( 'Timestamp');
		$this->entityClass( 'Manager\Test\TestCase\Model\Behavior\Category');
	}
}

class CardsTable extends Table 
{
	public function initialize(array $options) 
	{
		$this->addBehavior( 'Manager.Crudable');
		$this->addBehavior( 'Timestamp');
		$this->entityClass( 'Manager\Test\TestCase\Model\Behavior\Card');
	}
}

class AttributesTable extends Table 
{
	public function initialize(array $options) 
	{
		$this->entityClass( 'Manager\Test\TestCase\Model\Behavior\Attribute');
	}
}

class RelationsTable extends Table 
{
	public function initialize(array $options) 
	{
		$this->entityClass( 'Manager\Test\TestCase\Model\Behavior\Relation');
	}
}

class Content extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

	protected $_accessible = [
		'title' => true,
		'body' => true,
    'category_id' => true,
    'content_type' => true,
    'cards' => true,
    'card' => true,
    'categories' => true,
    'category' => true,
	];
}

class Card extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

	protected $_accessible = [
		'title' => true,
    'attributes' => true,
    'content_type' => true,
    'price' => true,
    'content_id' => true,
    'id' => true,
    'categories' => true,
	];
}


class Attribute extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

	protected $_accessible = [
		'title' => true,
	];
}

class Category extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

	protected $_accessible = [
		'id' => true,
		'title' => true,
		'contents' => true
	];
}

class Relation extends Entity 
{

	protected $_accessible = [
		'content_id' => true,
		'relation_id' => true,
	];
}


/**
 * Manager\Model\Behavior\CrudableBehavior Test Case
 */
class CrudableBehaviorTest extends TestCase 
{
	public $fixtures = [
		'plugin.manager.contents',
    'plugin.manager.cards',
    'plugin.manager.cards_translations',
    'plugin.manager.contents_translations',
    'plugin.manager.categories_translations',
		'plugin.manager.attributes_translations',
		'plugin.manager.attributes',
		'plugin.manager.content_categories',
		'plugin.manager.categories',
		'plugin.i18n.languages',
		'plugin.manager.translates',
		'plugin.section.sections',
		'plugin.website.sites',
		'plugin.manager.relations',
		'plugin.manager.drafts'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() 
	{
		I18n::locale( 'spa');
		parent::setUp();
		$this->connection = ConnectionManager::get('test');
		$this->Contents = new ContentsTable([
			'alias' => 'Contents',
			'table' => 'contents',
			'connection' => $this->connection
		]);

		$this->Categories = new CategoriesTable([
			'alias' => 'Categories',
			'table' => 'categories',
			'connection' => $this->connection
		]);

		$this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() 
	{
		TableRegistry::clear();
		unset( $this->Crudable);
		unset( $this->Contents);
		unset( $this->connection);
		unset( $this->Languages);
		Plugin::unload( 'Section');
		parent::tearDown();
	}

	public function setLanguages()
	{
		Lang::set( $this->Languages->find()->all());
	}

	public function setOneLanguage()
	{
		Lang::set( $this->Languages->find()->where(['id' => 1])->all());
	}


	public function testsDefaults()
  {
  	$this->Contents->crud->defaults( ['title' => 'Amigo']);
  	$this->assertSame( $this->Contents->crud->defaults(), ['title' => 'Amigo']);
		$content = $this->Contents->getNewEntity();
		$this->assertEquals( $content->title, 'Amigo');
  }

/**
 * Comprueba el funcionamiento de CrudableBehavior::setAdmin()
 */
  public function testSetAdmin()
  {
  	$this->Contents->hasMany( 'Cards');
  	$this->Contents->Cards->addBehavior( 'Manager.Crudable');
  	$this->Contents->setAdmin();

  	$this->assertTrue( $this->Contents->isAdmin());
  	$this->assertTrue( $this->Contents->Cards->isAdmin());
  }

  public function testRowMapper()
  {
  	$this->Contents->setAdmin();

  	$content = $this->Contents->find( 'content')
  		->where( ['Contents.id' => 1])
  		->first();

  	$this->assertEquals( '#/admin/manager/contents/update/1', $content->urls ['updateHash']);
  	$this->assertEquals( '/admin/manager/contents/update/1.json', $content->urls ['update']);
  	$this->assertEquals( '/admin/manager/contents/delete.json', $content->urls ['delete']);
  	$this->assertEquals( '/admin/manager/contents/', $content->urls ['base']);
  	$this->assertEquals( 'Title content 1', $content->full_title);
  }

/**
 * Test para el finder findContent()
 */
  public function testFindContent()
  {
    $this->Contents->crud->addFields([
        'title' => 'Título'
    ]);

    $this->Contents->crud->addView( 'create', [
      'columns' => [
        [
          'box' => [
            [
              'elements' => [
                'title',
              ]
            ]
          ]
        ]
      ],
    ], ['update'])
    ;

  	$content = $this->Contents->find( 'content', ['view' => 'update'])->where( ['Contents.id' => 1])->first();
  	$this->assertEquals( $content->title, 'Title content 1');
  	$this->assertEquals( $content->id, '1');
  }

/**
 * Test para el finder findContent() con traducciones
 */
  public function testFindContentWithTranslations()
  {
  	$this->Contents->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);
		I18n::locale( 'eng');
  	$content = $this->Contents->find( 'content')->where( ['Contents.id' => 1])->first();

  	$this->assertEquals( $content->title, 'Hello');

  	I18n::locale( 'spa');
  	$content = $this->Contents->find( 'content')->where( ['Contents.id' => 1])->first();

  	$this->assertEquals( $content->title, 'Hola');
  }

/**
 * Test para getNewEntity()
 */
  public function testNewEntity()
  {
  	$this->Contents->hasMany( 'Cards');
  	$content = $this->Contents->getNewEntity();
  	$content = $this->Contents->emptyEntityProperties( $content);
  	// $this->assertEquals( $content ['cards'], []);
  	$this->assertEquals( $content ['cards'], null);
  }

/**
 * Comprueba la toma de una nueva entidad y su posterior guardado
 */
	public function testNewEntityAndSave()
	{
		$data = [
			'title' => 'Hola'
		];

		$content = $this->Contents->getNewEntity( $data);
		$this->assertEquals( $content->title, 'Hola');

		$saved = $this->Contents->save( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);
		$this->assertEquals( 'Hola', $saved->title);
	}

/**
 * Comprueba la toma de una nueva entidad con campos traducibles y su posterior guardado
 */
	public function testNewEntityTranslationsAndSave()
	{
		$this->setLanguages();
		I18n::locale( 'spa');

		$this->Contents->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title', 'body']
		]);


		$data = [
			'_translations' => [
        'spa' => [
          'title' => 'Hola',
          'body' => 'Amigo'
        ],
        'eng' => [
          'title' => 'Hello',
          'body' => 'Friend'
        ],
      ]
		];

		// Toma la entidad y comprueba los datos correctos
		$content = $this->Contents->getNewEntity( $data);
		// $this->assertEquals( $content->title, 'Hola');
		$this->assertEquals( $content->translation( 'eng')->title, 'Hello');
		// $this->assertEquals( $content->body, 'Amigo');
		$this->assertEquals( $content->translation( 'eng')->body, 'Friend');

		// Guarda el contenido
		$saved = $this->Contents->saveContent( $content);
		// El contenido se ha guardado correctamente, entonces el objeto devuelto será una instancia de Content,
		// 	por el contrario, devolverá false
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		// Busca el nuevo contenido en forma de objeto
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->translation( 'spa')->title, 'Hola');
		$this->assertEquals( $new->translation( 'eng')->title, 'Hello');
		$this->assertEquals( $new->translation( 'spa')->body, 'Amigo');
		$this->assertEquals( $new->translation( 'eng')->body, 'Friend');

		// Busca el nuevo contenido en forma de array
		$new = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new ['title'], 'Hola');
		$this->assertEquals( $new ['body'], 'Amigo');
		$this->assertEquals( $new ['_translations']['spa']['title'], 'Hola');
		$this->assertEquals( $new ['_translations']['eng']['title'], 'Hello');
	}

	/**
 * Comprueba la toma de una nueva entidad con campos traducibles y su posterior guardado con error
 */
	public function testNewEntityTranslationsAndSaveWithValidationErrors()
	{
		$this->setLanguages();
		I18n::locale( 'spa');

		$this->Contents->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title'],
      'validator' => 'translated'
		]);


    $content = $this->Contents->newEntity([
      '_translations' => [
        'spa' => [
          'title' => '',
        ],
        'eng' => [
          'title' => 'hello',
        ]
      ]
    ], [
      'translations' => true
    ]);

		$this->assertEquals( 'This field cannot be left empty', $content->errors()['spa']['title']['_empty']);


		// Comprueba que el guardado nos da false
		$saved = $this->Contents->saveContent( $content);
		$this->assertTrue( !$saved);


		$content = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();
		$content ['spa']['title'] = null;
		$entity = $this->Contents->find( 'content')->where( ['Contents.id' => 1])->first();
		$content = $this->Contents->patchContent( $entity, $content);
	}

/**
 * Comprueba la lectura, modificación y guardado de un contenido
 */
	public function testEditEntity()
	{
		$content = $this->Contents->find( 'array')->first();
		$entity = $this->Contents->find( 'content')->first();

		$content ['title'] = 'Hola';
		$content = $this->Contents->patchContent( $entity, $content);

		$this->assertEquals( $content->title, 'Hola');

		$saved = $this->Contents->save( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		// Busca el nuevo contenido en forma de objeto
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->title, 'Hola');

		// Busca el nuevo contenido en forma de array
		$new = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new ['title'], 'Hola');
	}

/**
 * Comprueba la edición de un contenido con traducciones
 */
	public function testEditEntityWithTranslations()
	{
		$this->setLanguages();
		I18n::locale( 'spa');

		$this->Contents->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title', 'body']
		]);

		$this->Contents->crud->associations(['Cards']);

		$content = $this->Contents->find( 'array')->first();
		$entity = $this->Contents->find( 'content')->first();

    $content ['_translations']['spa']['title'] = 'Adiós';
		$content ['_translations']['eng']['title'] = 'Bye';
		$entity = $this->Contents->patchContent( $entity, $content);
		$saved = $this->Contents->saveContent( $entity);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);
		// Busca el nuevo contenido en forma de objeto
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $entity->id])->first();
    $this->assertEquals( $new->translation( 'spa')->title, 'Adiós');
		$this->assertEquals( $new->translation( 'eng')->title, 'Bye');
		
		// Busca el nuevo contenido en forma de array
		$new = $this->Contents->find( 'array')->where( ['Contents.id' => $entity->id])->first();
		$this->assertEquals( $new ['title'], 'Adiós');
	}

/**
 * Hace los tests para un model que tiene una relación de belongsTO
 * Comprueba el guardado de datos
 */
	public function testSaveWithBelongsTo()
	{
		$this->Contents->belongsTo( 'Categories');
		$this->Contents->crud->associations(['Categories']);

		$data = [
			'title' => 'Hola mundo'
		];

		// Toma una categoría cualquiera
		$category = $this->Contents->Categories->find()->where( ['Categories.id' => 2])->first()->toArray();
		
		// La coloca en la clave y guarda los datos
		$data ['category'] = $category;
		$content = $this->Contents->getNewEntity( $data);
		$saved = $this->Contents->saveContent( $content);

		// Toma el contenido
		$content = $this->Contents->find( 'content')->where( ['Contents.id' => $saved->id])->first();
		$data = $this->Contents->find( 'array')->where( ['Contents.id' => $saved->id])->first();

		// Comprueba el correcto guardado de los datos
		$this->assertEquals( $category ['title'], $data ['category']['title']);
		$this->assertEquals( $category ['id'], $data ['category']['id']);

		// Aquí vamos a comprobar que se guarda correctamente otra categoría para el mismo contenido
		
		// Toma otra categoría
		$category = $this->Contents->Categories->find()->where( ['Categories.id' => 4])->first()->toArray();
		
		// La coloca en la clave y guarda los datos
		$data ['category'] = $category;
		$content = $this->Contents->patchContent( $content, $data);
		$saved = $this->Contents->saveContent( $content);

		// Toma el contenido y comprueba
		$data = $this->Contents->find( 'array')->where( ['Contents.id' => $saved->id])->first();
		$this->assertEquals( $category ['title'], $data ['category']['title']);
		$this->assertEquals( $category ['id'], $data ['category']['id']);

		// Aquí elimina cualquier relación con una categoría
		$data ['category'] = array( 'id' => null);
		$content = $this->Contents->patchContent( $content, $data);
		$saved = $this->Contents->saveContent( $content);

		$data = $this->Contents->find( 'array')->where( ['Contents.id' => $saved->id])->first();
	}


	public function testNewEntityHasManyAndSave()
	{
		$this->Contents->hasMany( 'Cards');
		$this->Contents->crud->associations(['Cards']);

		$data = [
			'title' => 'Hola',
			'cards' => [
				[
					'title' => 'Hola Card'
				]
			]
		];

		$content = $this->Contents->getNewEntity( $data);
		$this->assertEquals( $content->title, 'Hola');
		$this->assertEquals( $content->cards [0]->title, 'Hola Card');
		$saved = $this->Contents->save( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->cards [0]->title, 'Hola Card');
	}



/**
 * 1. Crea un contenido con datos hasMany asociados que han sido creados previamente
 * 2. Añade nuevos datos hasMany que también han sido añadidos previamente
 */
	public function _testNewEntityHasManyPresaveAndSave()
	{
		$this->setLanguages();
		I18n::locale( 'spa');
		$this->Contents->hasMany( 'Cards', [
			'className' => 'Manager\Test\TestCase\Model\Behavior\CardsTable'
		]);

		$this->Contents->crud->associations(['Cards']);

		$this->Contents->Cards->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);

		// Crea datos hasMany
		$data = [
			'_translations' => [
        'spa' => [
          'title' => 'Hola Card'
        ],
        'eng' => [
          'title' => 'Hello Card'
        ]
      ]
		];

		$card = $this->Contents->Cards->getNewEntity( $data);
		$saved = $this->Contents->Cards->saveContent( $card);
		$card_id = $card->id;

		// Crea datos de Contents con los datos creados anteriormente como hasMany
		$data = [
			'title' => 'Hola',
			'cards' => [
				$card->toArray()
			]
		];

		$content = $this->Contents->getNewEntity( $data);
		$saved = $this->Contents->saveContent( $content);

		// Comprueba su inserción correcta
		$content = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $card_id, $content->cards [0]->id);

		// Crea nuevos datos hasMany
		$data = [
			'_translations' => [
        'spa' => [
          'title' => 'Hola Card 2'
        ],
        'eng' => [
          'title' => 'Hello Card 2'
        ]
      ]
		];

		$card = $this->Contents->Cards->getNewEntity( $data);
		$saved = $this->Contents->Cards->saveContent( $card);
		$card_id = $card->id;

		// Añade al contenido los nuevos datos hasMany creados
		$data = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$data ['cards'][] = $card->toArray();

		// Mezcla los datos
		$content = $this->Contents->patchContent( $content, $data);
		$saved = $this->Contents->saveContent( $content);

		$content = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $card_id, $content->cards [1]->id);

	}

	public function testHasManyAndSaveWithTranslations()
	{
		$this->setLanguages();
		I18n::locale( 'spa');
		
		$this->Contents->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);

		$this->Contents->hasMany( 'Cards');
		$this->Contents->crud->associations(['Cards']);
		$this->Contents->Cards->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);
		$this->Contents->Cards->addBehavior( 'Timestamp');
		$this->Contents->Cards->entityClass( 'Manager\Test\TestCase\Model\Behavior\Card');

		$data = [
			'_translations' => [
        'spa' => [
          'title' => 'Hola'
        ],
        'eng' => [
          'title' => 'Hello'
        ],
      ],
			'cards' => [
				[
					'_translations' => [
            'spa' => [
              'title' => 'Hola Card'
            ],
            'eng' => [
              'title' => 'Hello Card'
            ]
          ]
				]
			]
		];

		// Nueva entidad
		$content = $this->Contents->getNewEntity( $data);
		// $this->assertEquals( $content->title, 'Hola');
		$this->assertEquals( $content->translation( 'eng')->title, 'Hello');
		// $this->assertEquals( $content->cards [0]->translation( 'spa')->title, 'Hola Card');
		$this->assertEquals( $content->cards [0]->translation( 'eng')->title, 'Hello Card');

		// Guardado de datos
		$saved = $this->Contents->save( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		// Lectura de los datos guardados
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->translation( 'spa')->title, 'Hola');
		$this->assertEquals( $new->translation( 'eng')->title, 'Hello');
		// $this->assertEquals( $new->cards [0]->translation( 'spa')->title, 'Hola Card');z
		$this->assertEquals( $new->cards [0]->translation( 'eng')->title, 'Hello Card');

		// Lectura de los datos en array y modificación de los datos
		$array = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$array ['cards'][0]['_translations']['spa']['title'] = 'Adiós Card';
		$content = $this->Contents->patchContent( $new, $array);

		$saved = $this->Contents->save( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->cards [0]->translation( 'spa')->title, 'Adiós Card');

		// Inserción de un nuevo card
		$array = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$array ['cards'][] = [
			'_translations' => [
        'spa' => [
          'title' => 'Nueva Card'
        ],
        'eng' => [
          'title' => 'New Card'
        ]
      ]
		];

		$content = $this->Contents->patchContent( $new, $array);
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		
		$this->assertEquals( $new->cards [1]->translation( 'spa')->title, 'Nueva Card');
		$this->assertEquals( $new->cards [1]->translation( 'eng')->title, 'New Card');

		// Borrado de un card
		$array = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$deleted_id = $array ['cards'][1]['id'];
		unset( $array ['cards'][1]);

		$content = $this->Contents->patchContent( $new, $array);
		$saved = $this->Contents->saveContent( $content);
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first()->toArray();
		
		$ids = Hash::extract( $new ['cards'], '{n}.id');
		$this->assertTrue( !in_array( $deleted_id, $ids));
	}

/**
 * Comprueba inserciones, lecturas y modificaciones de datos para un model y sus asociaciones en dos niveles.
 */
	public function testHasMany2AndSave()
	{
		$this->Contents->hasMany( 'Cards');
		$this->Contents->crud->associations(['Cards', 'Attributes']);

		$this->Contents->Cards->addBehavior( 'Timestamp');
		$this->Contents->Cards->entityClass( 'Manager\Test\TestCase\Model\Behavior\Card');

		$this->Contents->Cards->hasMany( 'Attributes');
		$this->Contents->Cards->Attributes->addBehavior( 'Timestamp');
		$this->Contents->Cards->Attributes->entityClass( 'Manager\Test\TestCase\Model\Behavior\Attribute');

		$data = [
			'title' => 'Hola',
			'cards' => [
				[
					'title' => 'Hola Card',
					'attributes' => [
						[
							'title' => 'Hola Attribute'
						],

					]
				],
				[
					'title' => 'Hola 1 Card'
				]
			]
		];

		// Nueva entidad
		$content = $this->Contents->getNewEntity( $data);
		$this->assertEquals( $content->title, 'Hola');
		$this->assertEquals( $content->cards [0]->title, 'Hola Card');
		$this->assertEquals( $content->cards [0]->attributes [0]->title, 'Hola Attribute');
		
		// Guardado de datos
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		// Lectura de datos guardados
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->title, 'Hola');
		$this->assertEquals( $new->cards [0]->title, 'Hola Card');
		$this->assertEquals( $new->cards [0]->attributes [0]->title, 'Hola Attribute');

		// Modificación de datos de un elemento hasMany de segundo nivel
		$array = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$array ['cards'][0]['attributes'][0]['title'] = 'Attributo modificado';
		$content = $this->Contents->patchContent( $new, $array);
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->cards [0]->attributes [0]->title, 'Attributo modificado');

		// Borrado de un elemento hasMany de segundo nivel
		$array = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$deleted_id = $array ['cards'][0]['attributes'][1]['id'];
		unset( $array ['cards'][0]['attributes'][1]);

		$content = $this->Contents->patchContent( $new, $array);
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$ids = Hash::extract( $new ['cards'][0]['attributes'], '{n}.id');
		$this->assertTrue( !in_array( $deleted_id, $ids));


		$data = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
	}


	/**
 * Comprueba inserciones, lecturas y modificaciones de datos para un model y sus asociaciones en dos niveles.
 */
	public function testHasManyAndBelongsToMany()
	{
		$this->Contents->hasMany( 'Cards');
		$this->Contents->crud->associations(['Cards', 'Categories']);

		$this->Contents->Cards->addBehavior( 'Timestamp');
		$this->Contents->Cards->entityClass( 'Manager\Test\TestCase\Model\Behavior\Card');

		$this->Contents->Cards->belongsToMany( 'Categories', [
			'joinTable' => 'content_categories',
			'through' => 'ContentCategories',
			'foreignKey' => 'content_id',
			'targetForeignKey' => 'category_id',
			'className' => 'Categories',
		]);
		$this->Contents->Cards->Categories->addBehavior( 'Timestamp');
		$this->Contents->Cards->Categories->entityClass( 'Manager\Test\TestCase\Model\Behavior\Category');

		$data = [
			'title' => 'Hola',
			'cards' => [
				[
					'title' => 'Hola Card',
					'categories' => [
						[
							'title' => 'Hola Categoría'
						],
						[
							'title' => 'Hola 2 Categoría'
						]
					]
				],
				[
					'title' => 'Hola 1 Card'
				]
			]
		];

		// Nueva entidad
		$content = $this->Contents->getNewEntity( $data);
		$this->assertEquals( $content->title, 'Hola');
		$this->assertEquals( $content->cards [0]->title, 'Hola Card');
		
		// Guardado de datos
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		// Lectura de datos guardados
		$content = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $content->title, 'Hola');
		$this->assertEquals( $content->cards [0]->title, 'Hola Card');

		$data = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();

		$this->Contents->patchContent( $content, $data);
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);
	}

/**
 * Comprueba inserciones, lecturas y modificaciones de datos para un model y sus asociaciones en dos niveles con idiomas
 */
	public function testHasMany2WithTranslationsAndSave()
	{
		$this->setLanguages();
		I18n::locale( 'spa');
		
		$this->Contents->hasMany( 'Cards');
		$this->Contents->crud->associations(['Cards', 'Attributes']);

		$this->Contents->Cards->addBehavior( 'Timestamp');
		$this->Contents->Cards->entityClass( 'Manager\Test\TestCase\Model\Behavior\Card');

		$this->Contents->Cards->hasMany( 'Attributes');
		$this->Contents->Cards->Attributes->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);
		$this->Contents->Cards->Attributes->addBehavior( 'Timestamp');
		$this->Contents->Cards->Attributes->entityClass( 'Manager\Test\TestCase\Model\Behavior\Attribute');


		$data = [
			'title' => 'Hola',
			'cards' => [
				[
					'title' => 'Hola Card',
					'attributes' => [
						[
							'_translations' => [
                'spa' => [
                  'title' => 'Hola Attribute'
                ],
                'eng' => [
                  'title' => 'Hello Attribute'
                ]
              ]
						],
						[
							'_translations' => [
                'spa' => [
                  'title' => 'Hola 2 Attribute'
                ],
                'eng' => [
                  'title' => 'Hello 2 Attribute'
                ]
              ]
						],
					]
				],
				[
					'title' => 'Hola 1 Card'
				]
			]
		];

		// Nueva entidad
		$content = $this->Contents->getNewEntity( $data);
		// $this->assertEquals( $content->cards [0]->attributes [0]->title, 'Hola Attribute');
		// $this->assertEquals( $content->cards [0]->attributes [0]->translation( 'eng')->title, 'Hello Attribute');
		// Guarda
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		// Comprueba el guardado
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->cards [0]->attributes [0]->translation( 'spa')->title, 'Hola Attribute');
		$this->assertEquals( $new->cards [0]->attributes [0]->translation( 'eng')->title, 'Hello Attribute');

		// Modifica el contenido
		$array = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$array ['cards'][0]['attributes'][0]['_translations']['spa']['title'] = 'Attributo modificado';
		$content = $this->Contents->patchContent( $new, $array);
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->cards [0]->attributes [0]->translation( 'spa')->title, 'Attributo modificado');
	}


/**
 * Comprueba inserciones, lecturas y modificaciones de datos para un model y sus asociaciones en dos niveles con idiomas
 */
	public function testHasMany3WithTranslationsAndSave()
	{
		$this->setLanguages();
		I18n::locale( 'spa');
		
		$this->Contents->hasMany( 'Cards');
		$this->Contents->Cards->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);
		$this->Contents->crud->associations(['Cards', 'Attributes']);

		$this->Contents->Cards->addBehavior( 'Timestamp');
		$this->Contents->Cards->entityClass( 'Manager\Test\TestCase\Model\Behavior\Card');

		$this->Contents->Cards->hasMany( 'Attributes');
		$this->Contents->Cards->Attributes->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);
		$this->Contents->Cards->Attributes->addBehavior( 'Timestamp');
		$this->Contents->Cards->Attributes->entityClass( 'Manager\Test\TestCase\Model\Behavior\Attribute');


		$data = [
			'title' => 'Hola',
			'cards' => [
				[
					'_translations' => [
            'spa' => [
              'title' => 'Hola Card'
            ],
            'eng' => [
              'title' => 'Hello Card'
            ],
          ],
					'attributes' => [
						[
							'_translations' => [
                'spa' => [
                  'title' => 'Hola Attribute'
                ],
                'eng' => [
                  'title' => 'Hello Attribute'
                ]
              ]
						],
						[
							'_translations' => [
                'spa' => [
                  'title' => 'Hola 2 Attribute'
                ],
                'eng' => [
                  'title' => 'Hello 2 Attribute'
                ]
              ]
						],
					]
				],
				[
					'_translations' => [
            'spa' => [
              'title' => 'Hola 1 Card'
            ],
            'eng' => [
              'title' => 'Hello 1 Card'
            ]
          ]
				]
			]
		];

		// Nueva entidad
		$content = $this->Contents->getNewEntity( $data);
		// $this->assertEquals( $content->cards [0]->attributes [0]->title, 'Hola Attribute');
		// $this->assertEquals( $content->cards [0]->attributes [0]->translation( 'eng')->title, 'Hello Attribute');
		// Guarda
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		// Comprueba el guardado
		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->cards [0]->attributes [0]->translation( 'spa')->title, 'Hola Attribute');
		$this->assertEquals( $new->cards [0]->attributes [0]->translation( 'eng')->title, 'Hello Attribute');

		// Modifica el contenido
		$query = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id]);

		$array = $query->first();

		$array ['cards'][0]['attributes'][0]['_translations']['spa']['title'] = 'Attributo modificado';
		$content = $this->Contents->patchContent( $new, $array);
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		$new = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $new->cards [0]->attributes [0]->translation( 'spa')->title, 'Attributo modificado');
	}


	public function testReadHasMany2()
	{
		$this->setLanguages();
		I18n::locale( 'spa');
		
		$this->Contents->hasMany( 'Cards');
		$this->Contents->crud->associations(['Cards', 'Attributes']);

		$this->Contents->Cards->addBehavior( 'Timestamp');
		$this->Contents->Cards->entityClass( 'Manager\Test\TestCase\Model\Behavior\Card');

		$this->Contents->Cards->hasMany( 'Attributes');
		$this->Contents->Cards->Attributes->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);
		$this->Contents->Cards->Attributes->addBehavior( 'Timestamp');
		$this->Contents->Cards->Attributes->entityClass( 'Manager\Test\TestCase\Model\Behavior\Attribute');

		$content = $this->Contents->find( 'array')->where( [ $this->Contents->alias() .'.id' => 3])->first();
	}



	public function testSaveBelongsToManyNew()
	{
		$this->Contents->belongsToMany( 'Categories', [
			'joinTable' => 'content_categories',
			'through' => 'ContentCategories',
			'foreignKey' => 'content_id',
			'targetForeignKey' => 'category_id',
			'className' => 'Categories',
		]);
		$this->Contents->Categories->entityClass( 'Manager\Test\TestCase\Model\Behavior\Category');

		$this->Contents->crud->associations(['Categories']);

		$categories = $this->Contents->Categories->find( 'all')->hydrate( false)->toArray();

		$data = [
			'title' => 'Hola amigo',
			'categories' => $categories
		];

		$content = $this->Contents->getNewEntity( $data);

		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		$array = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $array ['categories'][0]['id'], 1);
		$this->assertEquals( $array ['categories'][1]['id'], 2);
	}

	public function testSaveBelongsToManyNew2()
	{
		$this->Contents->belongsToMany( 'Categories', [
			'joinTable' => 'content_categories',
			'through' => 'ContentCategories',
			'foreignKey' => 'content_id',
			'targetForeignKey' => 'category_id',
			'className' => 'Categories',
		]);
		$this->Contents->Categories->entityClass( 'Manager\Test\TestCase\Model\Behavior\Category');

		$this->Contents->Categories->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);

		$this->Contents->crud->associations(['Categories']);


		// Inserta categorías a un contenido que ya existe
		$categories = $this->Contents->Categories->find( 'all')->hydrate( false)->toArray();

		$data = $this->Contents->find( 'array')->where( ['Contents.id' => 3])->first();
		$data ['categories'] = $categories;
			
		$content = $this->Contents->find( 'content')->where( ['Contents.id' => 3])->first();
		$content = $this->Contents->patchContent( $content, $data);
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		$data = $this->Contents->find( 'array')->where( ['Contents.id' => 3])->first();

		// Se han insertado correctamente las categorías
		$this->assertTrue( !empty( $data ['categories']));


		// Quita las categorías
		$data ['categories'] = [];
			
		$content = $this->Contents->find( 'content')->where( ['Contents.id' => 3])->first();
		$content = $this->Contents->patchContent( $content, $data);
		$this->Contents->saveContent( $content);

		$data = $this->Contents->find( 'array')->where( ['Contents.id' => 3])->first();

		// Se han borrado correctamente las categorías
		$this->assertTrue( empty( $data ['categories']));

		// Guardado
		$data = $this->Contents->find( 'array')->where( ['Contents.id' => 3])->first();

		$content = $this->Contents->find( 'content')->where( ['Contents.id' => 3])->first();
		$content = $this->Contents->patchContent( $content, $data);
		$this->Contents->saveContent( $content);

		$data = $this->Contents->find( 'array')->where( ['Contents.id' => 3])->first();
	}


	public function testSaveBelongsToManyEdit()
	{
		$this->Contents->belongsToMany( 'Categories', [
			'joinTable' => 'content_categories',
			'through' => 'ContentCategories',
			'foreignKey' => 'content_id',
			'targetForeignKey' => 'category_id',
			'className' => 'Categories',
		]);
		$this->Contents->Categories->entityClass( 'Manager\Test\TestCase\Model\Behavior\Category');

		$this->Contents->Categories->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);

		$this->Contents->crud->associations(['Categories']);

		$categories = $this->Contents->Categories->find( 'all')->hydrate( false)->toArray();

		$data = $this->Contents->find( 'array')->where( ['Contents.id' => 3])->first();
		$data ['categories'] = $categories;
		$content = $this->Contents->find( 'content')->where( ['Contents.id' => 3])->first();
		$content = $this->Contents->patchContent( $content, $data);
		$saved = $this->Contents->saveContent( $content);
		$this->assertInstanceOf( 'Manager\Test\TestCase\Model\Behavior\Content', $saved);

		$array = $this->Contents->find( 'array')->where( ['Contents.id' => $content->id])->first();
		$this->assertEquals( $array ['categories'][0]['id'], 1);
		$this->assertEquals( $array ['categories'][1]['id'], 2);
	}


	public function testSaveAndValidate()
	{
		$this->Contents->crud
			->addFields([
				'title' => 'Título',
			])
			->setName( [
				'singular' => __d( 'admin', 'Noticias'),
				'plural' => __d( 'admin', 'Noticias'),
			])
			->addView( 'create', [
				'columns' => [
	          [
	            'cols' => 8,
	            'box' => [
	              [
	                'elements' => [
	                  'title',
	                ]
	              ]
	            ]
	          ]
	       ] 
			]);
		
		$this->Contents->validator()
			->add( 'id', 'valid', ['rule' => 'numeric'])
			->allowEmpty( 'id', 'create')
			// ->add('published_at', 'valid', ['rule' => 'date'])
			// ->allowEmpty('published_at')
			->requirePresence( 'title', 'create')
			->notEmpty( 'title');

		$data = [
			// 'title' => 'Hola'
		];

		

		$content = $this->Contents->getNewEntity( $data);
		$saved = $this->Contents->saveContent( $content);
		$this->Contents->crud->setEntity( $content);
		$view = $this->Contents->crud->serialize( 'create');
		$this->assertTrue( !$saved);
		$this->assertTrue( isset( $view ['view']['columns'][0]['box'][0]['elements'][0]['error']));
	}


	public function testDeleteHasMany()
	{
		$this->Contents->hasMany( 'Cards');
		$this->Contents->Cards->hasMany( 'Attributes');
		$this->Contents->crud->associations(['Cards', 'Attributes']);
		$content = $this->Contents->find( 'content')->first();
		$array = $this->Contents->find( 'array')->first();

		unset( $array ['cards'][0]);
		unset( $array ['cards'][1]);
		$content = $this->Contents->patchContent( $content, $array);
		$saved = $this->Contents->saveContent( $content);

		$array = $this->Contents->find( 'array')->first();
		$this->assertTrue( empty( $array ['cards']));
	}


	public function testSetTranslatedFields()
	{
		$this->setLanguages();
		I18n::locale( 'spa');
		$fields = ['title', 'text'];
		$content = [];

		$content = $this->Contents->setTranslatedFields( $content, $fields);
		$this->assertTrue( array_key_exists( 'spa', $content));
		$this->assertTrue( array_key_exists( 'eng', $content));
		$this->assertTrue( array_key_exists( 'title', $content ['spa']));
		$this->assertTrue( array_key_exists( 'text', $content ['spa']));
	}


	public function __testFieldHasOne()
  {
    $this->Contents->hasOne( 'Cards', ['className' => 'Manager\Test\TestCase\Model\Behavior\CardsTable' ]);
    
    $this->Contents->crud
			->addFields([
				'Cards.price' => 'Precio',
				'title' => 'Título',
				
			])
			->setName( [
				'singular' => __d( 'admin', 'Noticias'),
				'plural' => __d( 'admin', 'Noticias'),
			])
			->addView( 'create', [
				'columns' => [
	          [
	            'cols' => 8,
	            'box' => [
	              [
	                'elements' => [
	                  'Cards.price',
	                  'title',
	                ]
	              ]
	            ]
	          ]
	       ] 
			]);

		$this->Contents->crud->associations( ['Cards']);
		$config = $this->Contents->crud->serialize( 'create');

		$content = $this->Contents->getNewEntity( );
		$array = $this->Contents->emptyEntityProperties( $content);

		$data = [
			'title' => 'Hola',
			'card' => [
				'price' => 20
			]
		];

		$content = $this->Contents->getNewEntity( $data);
		$array = $this->Contents->emptyEntityProperties( $content);

		$saved = $this->Contents->saveContent( $content);
		$content = $this->Contents->find( 'content')->where( ['Contents.id' => $content->id])->first();
		$cards = $this->Contents->Cards->find()->count();
  }


  public function testJsonableField()
  {
  	$this->Contents->addBehavior( 'Cofree.Jsonable', [
			'fields' => [
				'settings'
			]
		]);

  	$this->Contents->crud
			->addFields([
				'settings' => [
					'type' => 'multi',
					'fields' => [
						'limit' => [
							'label' => 'Límite',
							'type' => 'numeric'
						]
					]
				]
			])
			->setName( [
				'singular' => __d( 'admin', 'Noticias'),
				'plural' => __d( 'admin', 'Noticias'),
			])
			->addView( 'create', [
				'columns' => [
	          [
	            'cols' => 8,
	            'box' => [
	              [
	                'elements' => [
	                  'settings',
	                ]
	              ]
	            ]
	          ]
	       ] 
			]);

		$config = $this->Contents->crud->serialize( 'create');
  }

  // Test para ver el funcionamiento de contain() cuando se hace findContent() 
  // Relación de un model
  public function testFindContentContain()
  {
  	$this->Contents->hasMany( 'Cards', [
			'className' => 'Manager\Test\TestCase\Model\Behavior\CardsTable'
		]);

		$this->Contents->crud->associations( ['Cards']);

		$query = $this->Contents->find( 'content');
		
		$expected = [
			'Cards' => []
		];

		$this->assertSame( $expected, $query->contain());
  }

  // Test para ver el funcionamiento de contain() cuando se hace findContent() 
  // Relación de dos models recursivos
  public function testFindContentContain2()
  {
  	$this->Contents->hasMany( 'Cards', [
			'className' => 'Manager\Test\TestCase\Model\Behavior\CardsTable'
		]);

		$this->Contents->Cards->belongsToMany( 'Attributes', [
      'joinTable' => 'relations',
      'foreignKey' => 'content_id',
      'targetForeignKey' => 'related_id',
      'className' => 'Manager\Test\TestCase\Model\Behavior\AttributesTable'
    ]);

		$this->Contents->crud->associations( ['Cards', 'Attributes']);

		$query = $this->Contents->find( 'content');
		
		$expected = [
			'Cards' => [
				'Attributes' => []
			]
		];

		$this->assertSame( $expected, $query->contain());
  }

/**
 * Test para comprobar que con findContent() se realiza correctamente el contain() cuando hay relaciones hasMany->belongsToMany y la relación hasMany tiene traducciones
 */
  public function testFindContentContain3()
  {
  	// La relación con Cards
  	$this->Contents->hasMany( 'Cards', [
			'className' => 'Manager\Test\TestCase\Model\Behavior\CardsTable'
		]);

  	// La relación con Attributes
  	$this->Contents->Cards->belongsToMany( 'Attributes', [
      'joinTable' => 'relations',
      'foreignKey' => 'content_id',
      'targetForeignKey' => 'related_id',
      'className' => 'Manager\Test\TestCase\Model\Behavior\AttributesTable'
    ]);

  	// Pone translation en Cards
  	$this->Contents->Cards->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);

  	// Marca las asociaciones disponibles en la búsqueda 
		$this->Contents->crud->associations( ['Cards', 'Attributes']);

		$query = $this->Contents->find( 'content')
			->where( ['Contents.id' => 1]);
		
		// Comprueba contain
		$contain = $query->contain();

		$this->assertTrue( array_key_exists( 'Cards', $contain));
		$this->assertTrue( array_key_exists( 'queryBuilder', $contain ['Cards']));
		$this->assertTrue( array_key_exists( 'Attributes', $contain ['Cards']));
		
		// Comprueba la búsqueda
		$content = $query->first();

		$this->assertTrue( !empty( $content->cards [0]->attributes));
		$this->assertTrue( !empty( $content->cards [0]->translation( 'spa')->title));
  }


  public function testRelations()
  {
  	$this->Contents->hasMany( 'Cards', [
			'className' => 'Manager\Test\TestCase\Model\Behavior\CardsTable'
		]);

		$this->Contents->Cards->belongsToMany( 'Attributes', [
      'joinTable' => 'relations',
      'foreignKey' => 'content_id',
      'targetForeignKey' => 'related_id',
      'className' => 'Manager\Test\TestCase\Model\Behavior\AttributesTable'
    ]);

		$this->Contents->crud->associations( ['Cards', 'Attributes']);
		$this->Contents->Cards->crud->associations( ['Attributes']);

		// Guarda el Content
    $data = [
    	'title' => 'Título Content'
    ];

    $content = $this->Contents->getNewEntity( $data);
    $this->Contents->saveContent( $content);


    // Guarda los attributos
    
    $data = [
    	'title' => 'Título Atributo',
    ];
    $attribute = $this->Contents->Cards->Attributes->newEntity( $data);
    $this->Contents->Cards->Attributes->save( $attribute);
    $attribute = $this->Contents->Cards->Attributes->find()
    	->where( ['Attributes.id' => $attribute->id])
    	->first();

    // Card
    $data = [
    	'title' => 'Título Card',
    ];

    $card = $this->Contents->Cards->newEntity( $data);
    $this->Contents->Cards->saveContent( $card);

    // Añade atributos al Card
    
    $card = $this->Contents->Cards->find()
    	->where( ['Cards.id' => $card->id])
    	->first();

    $this->Contents->Cards->patchContent( $card, ['attributes' => [$attribute->toArray()]]);
    $this->Contents->Cards->saveContent( $card);
		
    $card = $this->Contents->Cards->find( 'content')
    	->where( ['Cards.id' => $card->id])
    	->first();


    // Añade la card al content
    $content->cards = [$card];
    $this->Contents->saveContent( $content);
    $content = $this->Contents->find( 'content')
    	->where( ['Contents.id' => $content->id])
    	->first();

    // _d( $content);

  }


/**
 * Comprueba el funcionamiento de CrudableBehavior::tree()
 */
  public function testTree()
  {
  	$contents = $this->Categories->tree();
  	$this->assertEquals( 0, $contents [0]->parent_id);
  	$this->assertEquals( 1, $contents [0]->position);
  	$this->assertTrue( is_array( $contents[0]->nodes));
  	$this->assertTrue( is_array( $contents[0]->nodes));
  }

/**
 * Comprueba el funcionamiento de CrudableBehavior::reordering()
 */
  public function testReordering()
  {
  	$contents = $this->Categories->tree();
  	$contents = json_decode( json_encode( $contents), true);
  	$contents [] = $contents [0]['nodes'][0];
  	$last_id = $contents [0]['nodes'][0]['id'];
  	unset( $contents [0]['nodes'][0]);
  	
  	$this->Categories->reordering( $contents);

  	$contents = $this->Categories->tree();
  	$this->assertEquals( 1, count( $contents [0]->nodes));
  	$this->assertEquals( $last_id, $contents [2]->id);
  }

/**
 * Verifica el funcionamiento de duplicate()
 */
  public function testDuplicate()
  {
  	$this->Contents->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);

  	$this->Contents->belongsToMany( 'Categories', [
			'joinTable' => 'content_categories',
			'through' => 'ContentCategories',
			'foreignKey' => 'content_id',
			'targetForeignKey' => 'category_id',
			'className' => 'Categories',
		]);

		$this->Contents->crud->associations(['Categories']);

		$this->Contents->hasMany( 'Cards');
		$this->Contents->crud->associations(['Cards']);
		$this->Contents->Cards->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);
		$this->Contents->Cards->addBehavior( 'Timestamp');
		$this->Contents->Cards->entityClass( 'Manager\Test\TestCase\Model\Behavior\Card');

  	$row = $this->Contents->find( 'content')->first();
  	$duplicate = $this->Contents->duplicate( $row->id);
  	$row = $this->Contents->find( 'content')->where(['Contents.id' => $row->id])->first();
  	
  	$this->assertTrue( $row->id != $duplicate->id);
  	$this->assertTrue( $row->title == $duplicate->title);

  	foreach( $row->cards as $key => $card)
  	{
  		$this->assertTrue( $card->id != $duplicate->cards [$key]->id);
  	}

  	foreach( $row->categories as $key => $category)
  	{
  		$this->assertTrue( $category->id == $duplicate->categories [$key]->id);
  	}
  }


  public function testDrafts()
  {
  	$this->Contents->addBehavior( 'Manager.Drafter');
  	$row = $this->Contents->find( 'content')->first();
  	$draft = $this->Contents->createDraft( $row->id);

  	$this->Contents->setAdmin();
    $this->Contents->crud->action( 'update');
  	$content = $this->Contents->find( 'content')
  		->where([
  			'Contents.id' => $draft->id
  		])
  		->first();

  	$this->assertTrue( $content->draft->has( 'id'));
  }

  public function testSaveDraft()
  {
  	$this->Contents->setAdmin();
    $this->Contents->crud->action( 'update');

  	$this->Contents->addBehavior( 'Manager.Drafter');

  	$this->Contents->belongsToMany( 'Categories', [
			'joinTable' => 'content_categories',
			'through' => 'ContentCategories',
			'foreignKey' => 'content_id',
			'targetForeignKey' => 'category_id',
			'className' => 'Categories',
		]);

		$this->Contents->crud->associations(['Categories']);

		$this->Contents->hasMany( 'Cards');
		$this->Contents->crud->associations(['Cards']);
		$this->Contents->Cards->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);
		$this->Contents->Cards->addBehavior( 'Timestamp');
		$this->Contents->Cards->entityClass( 'Manager\Test\TestCase\Model\Behavior\Card');
  	$row = $this->Contents->find( 'content')->first();
  	$draft = $this->Contents->createDraft( $row->id);

  	$draft->set( 'title', 'Soy un draft');
  	$draft->cards[0]->set( 'title', 'Soy un draft Card');
  	$card_id = $draft->cards [0]->id;
  	$this->Contents->saveContent( $draft);
  	$this->Contents->saveDraft( $draft->id);
  	$row = $this->Contents->find( 'content')->first();
  	$this->assertEquals( 'Soy un draft', $row->title);
  	$this->assertEquals( $card_id, $row->cards[0]->id);

  }

}
