<?php
namespace Manager\Test\TestCase\Shell;

use Cake\TestSuite\TestCase;
use Manager\Shell\BulkShell;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use I18n\Lib\Lang;
use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\I18n\I18n;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Cake\Core\Plugin;


class ContentsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Manager.Bulker');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Model\Behavior\Content');

    $this->crud->addFields([
      'title' => 'Título',
    ]);


  }

  public function validationDefault( Validator $validator) 
  {
    $validator
      // ->add( 'layout_id', 'valid', ['rule' => 'numeric'])
      ->notEmpty( 'title')
      ->requirePresence( 'title')
      ->notEmpty( 'layout_id');

    return $validator;
  }
}

class CategoriesTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Model\Behavior\Category');
  }
}

class CardsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Manager\Test\TestCase\Model\Behavior\Card');
  }
}

class AttributesTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->entityClass( 'Manager\Test\TestCase\Model\Behavior\Attribute');
  }
}

class RelationsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->entityClass( 'Manager\Test\TestCase\Model\Behavior\Relation');
  }
}

class Content extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'title' => true,
    'body' => true,
    'category_id' => true,
    'content_type' => true,
    'cards' => true,
    'card' => true,
    'categories' => true,
    'category' => true
  ];
}

class Card extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'title' => true,
    'attributes' => true,
    'content_type' => true,
    'price' => true,
    'content_id' => true,
    'id' => true,
    'categories' => true
  ];
}


class Attribute extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'title' => true,
  ];
}

class Category extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'id' => true,
    'title' => true,
    'contents' => true
  ];
}

class Relation extends Entity 
{

  protected $_accessible = [
    'content_id' => true,
    'relation_id' => true,
  ];
}


/**
 * Manager\Shell\BulkShell Test Case
 */
class BulkShellTest extends TestCase
{

  public $fixtures = [
    'plugin.manager.contents',
    'plugin.manager.cards',
    'plugin.manager.attributes',
    'plugin.manager.content_categories',
    'plugin.manager.categories',
    'plugin.i18n.languages',
    'plugin.manager.translates',
    'plugin.section.sections',
    'plugin.website.sites',
    'plugin.manager.relations'
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->io = $this->getMock('Cake\Console\ConsoleIo');

    $this->Bulk = $this->getMock(
            'Bake\Shell\Task\PluginTask',
            ['in', 'err', '_stop', 'clear', 'callProcess', '_rootComposerFilePath', 'findComposer'],
            [$this->io]
        );
    // $this->Bulk = new BulkShell($this->io);

    I18n::locale( 'spa');
    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);

    $this->Categories = new CategoriesTable([
      'alias' => 'Categories',
      'table' => 'categories',
      'connection' => $this->connection
    ]);

    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);

  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset($this->Bulk);
    TableRegistry::clear();
    unset( $this->Crudable);
    unset( $this->Contents);
    unset( $this->connection);
    unset( $this->Languages);
    Plugin::unload( 'Section');
    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }


  public function testMain()
  {
    $this->Bulk->expects($this->at(0))
      ->method( 'in')
      ->will( $this->returnValue('xxd'));

    $this->Bulk->main();
    // $this->Bulk->main('SimpleExample');
  }
}
