<?php
namespace Manager\Test\TestCase\Shell;

use Cake\TestSuite\TestCase;
use Manager\Shell\CrudShell;

/**
 * Manager\Shell\CrudShell Test Case
 */
class CrudShellTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->io = $this->getMock('Cake\Console\ConsoleIo');
        $this->Crud = new CrudShell($this->io);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Crud);

        parent::tearDown();
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
