'use strict';

function bindChangeLang($scope) {
  function changeLang(e) {

    if (e.charCode == 230) {
      e.preventDefault();
      var index;

      for (var i in $scope.data.languages) {
        if ($scope.data.languages[i].id == $scope.data.currentLanguage.id) {
          index = (parseInt(i) + 1);

          if (index >= $scope.data.languages.length) {
            index = 0;
          }
        }
      }

      $scope.data.currentLanguage = $scope.data.languages[index];
      $scope.$apply();
    }
  }

  $(document).off('keypress.changelang');
  $(document).on('keypress.changelang', changeLang);
}
angular.module('admin')


  /**
   * Controlador general a donde irán todas las peticiones GET angularjs del admin
   */
  .controller('crudCtrl', function ($scope, $sce, $http, $location, $ocLazyLoad, cfError, isJson, cfSpinner) {
    bindChangeLang($scope);
    var url = $location.url();
    $("#nav-main").removeClass('collapse-in');

    // La URL está vacía, return.
    if (url === '') {
      $http.get('/admin/manager/pages/dashboard.json')
        .success(function (result) {
          if (result.data.redirect) {
            $location.url(result.data.redirect);
          }
          $scope.data = result.data;
        })
      return;
    }

    $scope.evalContent = function (variable) {
      return $scope.$eval('content.' + variable);
    }

    var parser = document.createElement('a');
    parser.href = url;

    cfSpinner.enable();

    var urlRequest = parser.pathname + '.json';

    if (parser.search) {
      urlRequest += parser.search;
    }

    $scope.trustAsHtml = function (string) {
      return $sce.trustAsHtml(string);
    };

    $http.get(urlRequest)
      .success(function (result, responseCode) {
        isJson.check(result);

        if (result.redirect) {
          $location.url(result.redirect);
        }

        if (!result.crudConfig) {
          window.location.href = '/admin';
          return;
        }
        // Título de la página
        if (result.crudConfig.name.plural) {
          angular.element('title').html(result.crudConfig.name.plural + ' - ' + result.crudConfig.view.title);
        }
        // Lee las dependencias JS que han sido seteadas en 'dependences' desde el controller de Cakephp
        if (result.crudConfig.jsFiles) {
          $ocLazyLoad.load({
            name: 'admin',
            files: result.crudConfig.jsFiles
          }).then(function () {
            $scope.data = result;
          });
        } else {
          $scope.data = result;
        }
        $("html,body").animate({ scrollTop: 0 }, "slow");
        cfSpinner.disable();

        // Se lanza el evento 'openView' para que se muestre la pantalla en el frontend
        $('body').trigger('openView');

      })
      .error(function (error, responseCode) {
        if (responseCode == 302) {
          window.location.href = '/admin';
          return;
        }
        cfError.set(error);
        cfSpinner.disable();
      })

    // checkboxes index
    $scope.indexBox = [];

    // Añade un id al index
    $scope.indexToBox = function indexToBox(id) {

      var idx = $scope.indexBox.indexOf(id);

      // Is currently selected
      if (idx > -1) {
        $scope.indexBox.splice(idx, 1);
      }
      // Is newly selected
      else {
        $scope.indexBox.push(id);
      }
    };

    // resetea el index o lo rellena
    $scope.resetIndexBox = function () {

      if ($scope.indexBox.length < $scope.data.contents.length) {
        for (var index = 0; index < $scope.data.contents.length; index++) {
          var content = $scope.data.contents[index];
          var idx = $scope.indexBox.indexOf(content.id);
          if (idx == -1) {
            $scope.indexBox.push(content.id);
          }
        }
      } else {
        $scope.indexBox = [];
      }
    }



    // Lanza la acción
    $scope.indexAction = function ($scope, all) {
      var method = $scope.indexActionValue;
      var ids = $scope.indexBox;

      var params = {
        method: method,
        ids: ids,
        all: all
      };

      var parser = document.createElement('a');
      parser.href = url;

      var urlRequest = parser.pathname + '.json';

      if (parser.search) {
        urlRequest += parser.search;
      }

      if (all) {
        var count = $scope.data.paging.count;
      } else {
        var count = ids.length;
      }

      swal({
        title: "¿Estás seguro de realizar esa operación para los " + count + " contenidos?",
        type: "warning",
        closeOnConfirm: false,
        buttons: {
          cancel: "No",
          yes: {
            text: '¡Sí, borrarlo!',
            value: true,
            closeModal: false
          },
        },
      }).then(function (result) {
        if (!result) {
          return;
        }
        swal.close();
        cfSpinner.enable();
        $http.post($scope.data.crudConfig.adminUrl + 'bulk.json' + parser.search, params)
          .success(function (result) {
            $location.search('_', Math.floor(Date.now() / 1000));
            ;
          })
      });

      $scope.indexActionValue = '';
    }




  })

  /**
   * Controlador general a donde irán todas las peticiones POST angularjs del admin
   */
  .controller('crudUpdateCtrl', function ($scope, $http, cfError, cfSpinner, $location) {
    bindChangeLang($scope);
    $scope.send = function (params, data, preview) {
      cfSpinner.enable();
      var currentLanguage = $scope.data.currentLanguage;
      $http.post(params.url, data)
        .success(function (result) {
          $scope.data = result;
          $scope.data.currentLanguage = currentLanguage;
          cfSpinner.disable();

          if (result.crudConfig.previewUrl) {
            if ($("#preview-link").length == 0) {
              $('body').append('<a id="preview-link" class="venobox_custom" data-type="iframe" href=""></a>');
            }

            if (preview && result.crudConfig.previewUrl) {
              $("#preview-link").attr('href', result.crudConfig.previewUrl);
              $("#preview-link").venobox({
                framewidth: '100%'      // default: ''
              }).trigger('click');
            }
          }

          if (preview && !result.crudConfig.previewUrl) {
            window.open(preview);
          }

          if (result.redirect) {
            $location.url(result.redirect);
          }

          // Lanza el evento saveView
          $('body').trigger('saveView');
        })
        .error(function (error) {
          cfError.set(error);
          cfSpinner.disable();
        })
    }
  })
  ;