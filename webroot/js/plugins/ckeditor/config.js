/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.dtd.$removeEmpty['i'] = false;
CKEDITOR.editorConfig = function( config ) {
  // Define changes to default configuration here. For example:
  // config.language = 'fr';
  // config.uiColor = '#AADC6E';
  config.extraPlugins = 'simpleuploads,colordialog,colorbutton,font,justify,fontawesome,sourcedialog,oembed,stylescombo,inlinesave';
  config.filebrowserUploadUrl = '/upload/uploads/uploadfile';
  config.allowedContent = true;
  config.contentsCss = '/css/ckeditor.css';
  config.extraAllowedContent = 'p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*}';
};
