/**
 * @license Copyright © 2013 Stuart Sillitoe <stuart@vericode.co.uk>
 * This work is mine, and yours. You can modify it as you wish.
 *
 * Stuart Sillitoe
 * stuartsillitoe.co.uk
 *
 */

 CKEDITOR.plugins.add('insertlink',
 {
     requires : ['richcombo'],
     init : function( editor )
     {
         //  array of strings to choose from that'll be inserted into the editor
         var strings = editor.config.strinsert_strings;
 
         // add the menu to the editor
         editor.ui.addRichCombo('insertlink',
         {
             label: 		editor.config.strinsert_title,
             title: 		editor.config.strinsert_title,
             voiceLabel: editor.config.strinsert_title,
             className: 	'cke_format',
             multiSelect:false,
             panel:
             {
                 css: [ editor.config.contentsCss, CKEDITOR.skin.getPath('editor') ],
                 voiceLabel: editor.lang.panelVoiceLabel
             },
 
             init: function()
             {
                 this.startGroup( "Insert Content" );
                 console.log(strings);
                 for (var i in strings)
                 {
                     this.add(strings[i][0], strings[i][1], strings[i][2]);
                 }
             },
 
             onClick: function( value )
             {
                 editor.focus();
                 editor.fire( 'saveSnapshot' );
                 var html = editor.getSelection().getSelectedText();
                 var label;

                 for (var i in strings) {
                     if (strings[i][0] == value) {
                         label = strings[i][1];
                     }
                 }
                 html = '<a title="' + label + '" href="#' + value + '" >' + html + '</a>';
                 editor.insertHtml(html);
                 editor.fire( 'saveSnapshot' );
             }
         });
     }
 });