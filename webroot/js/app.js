/**
 * INSPINIA - Responsive Admin Them
 * Copyright 2014 Webapplayers.com
 *
 */
(function () {
  angular.module( 'admin', [
        // 'ui.router',
        'ngRoute',
        'ui.calendar',
        'ui.bootstrap',
        'ui.checkbox',
        'ui.knob',
        'ui.switchery',
        'ui.tree',
        'angular-peity',
        'ngSanitize',
        // 'easypiechart',
        'angular-flot',
        'angular-rickshaw',
        'summernote',
        'nouislider',
        'datePicker',
        'datatables',
        'localytics.directives',
        'checklist-model',
        'plupload',
        // 'ngCkeditor',
        'ckeditor',
        'ui.load',
        'ui.calendar',
        'oc.lazyLoad',
        'ui.sortable',
        'isteven-multi-select',
        'ngTagsInput',
        'brantwills.paging',
        'colorpicker.module',
        'ngDragDrop',
        'ngAnimate',
        'toastr',
        'angucomplete-alt',
        'ngMap',
        'monospaced.elastic',
        'highcharts-ng',
        'ui.bootstrap.datepicker',
        'ui.bootstrap.datetimepicker'
    ])
})();