/**
 * INSPINIA - Responsive Admin Theme
 * Copyright 2014 Webapplayers.com
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written stat for all view in theme.
 *
 */

angular.module('admin')
    .config(function( $routeProvider) {
      $routeProvider
        .otherwise({
          templateUrl: '/admin/manager/pages/base',
          controller: 'crudCtrl'
        })
    });
;

