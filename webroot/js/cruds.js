
function crString($timeout) {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'string.html',
        controller: function ($scope, $element) {
          
        }
    };
};

function crText($timeout) {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'text.html',
        controller: function ($scope, $element) {
          
        }
    };
}; 

function crField($timeout) {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'text.html',
        controller: function ($scope, $element) {
          
        }
    };
}; 

angular
    .module('admin')
    .directive('crString', crString)
    .directive('crText', crText)
    .directive('crField', crField)
;