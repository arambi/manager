(function($){
  var iframe = window.parent.$('iframe.manager-bar');

  $('.admin-bar-open').click(function(){
    var status = $('body').data( 'status');
    var view = $('body').data( 'view');

    
    if( status == 'closed'){
      iframe.width( '100%');
      $('body').data( 'status', 'opened');
      $('a', this).html( '<i class="fa fa-close"></i>');
      $('.admin-bar-navtop').show();
    } else if( status == 'opened') {
      iframe.width( '50');
      iframe.height( '35');
      $('body').data( 'status', 'closed');
      $('.admin-view-open').hide();
      $('.admin-bar-content').show();
      $('a', this).html( '<i class="fa fa-wrench"></i>');
      $('.admin-bar-navtop, .admin-bar-content').hide();
      $('body').trigger( 'closeView');
    }
  });

  $('.admin-view-open').click(function(){
    $(this).show();
    var view = $('body').data( 'view');
    if( view == 'closed'){
      $('body').trigger( 'openView');
    } else if( view == 'opened'){
      $('body').trigger( 'closeView');
    }
  })

  $(".admin-bar-menu").mouseenter(function(){
    $(".admin-bar-navmain").show();
    iframe.height( '100%');
  });
  $(".admin-bar-menu").mouseleave(function(){
    $(".admin-bar-navmain").hide();
    if( $('body').data( 'status') != 'opened') {
      iframe.height( '35');
    }
    
  })

  $('.admin-bar-link-content a').click(function(){
    $('body').trigger( 'openView');
  })

  // openView event
  $('body').bind( 'openView', function(){
    $('.admin-view-open').show();
    $('.admin-bar-content').show();
    $('body').data( 'view', 'opened');
    iframe.height( '100%');
    $('a', '.admin-view-open').html( '<i class="fa fa-chevron-down"></i>');
    $(".admin-bar-menu").trigger( 'mouseleave');
  });

  // closeView event
  $('body').bind( 'closeView', function(){
    $('.admin-bar-content').hide();
    $('a', '.admin-view-open').html( '<i class="fa fa-chevron-right"></i>');
    $('body').data( 'view', 'closed');
    iframe.height( '35');
  });

  $('.admin-bar-logout').click( function(){
    window.parent.$('.manager-bar').hide();
  });

  $('body').bind( 'saveView', function(){
    // window.parent.location.reload(true);
  });

})(jQuery)