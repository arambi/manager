(function($) {
  function startBar(){
    iframeBar = $('<iframe>');
    iframeBar.attr('style', 'position: fixed; height: 35px; width: 50px; top: 0; right: 0; border: 0; outline: 0; overflow: hidden; z-index: 99999;');
    iframeBar.attr( 'height', 35);
    iframeBar.attr( 'width', 50);
    iframeBar.attr( 'src', __manager_base_url);
    iframeBar.attr( 'class', 'manager-bar');

    $('body').append( iframeBar);
  }
  $(function(){
    startBar();
  })
}(jQuery));

