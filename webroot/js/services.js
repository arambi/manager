;(function(){

  function cfSend( $http, cfSpinner, cfError, $timeout, $rootScope, $q){
    this.send = function( scope){
      cfSpinner.enable();
      var params = scope.data.crudConfig.view.submit;
      var data = scope.data.content;

      var deferred = $q.defer();

      $http.post( params.url, data)
        .success( function( result){
          deferred.resolve( result);
          cfSpinner.disable();
        })
        .error( function( error){
          cfError.set( error);
          cfSpinner.disable();
        });

      return deferred.promise;
    }
      
  }

/**
 * Ventana de errores
 */
  function cfError( $modal){

    this.set = function( error){
      sweetAlert( "Oops...", "El servidor encontró algo inesperado que no deje que complete la solicitud, recarga la página a la que estás intentando acceder o haz click en inicio.", "error");
    }
  }

  function isJson( cfError) {
      this.check = function( str){
        if( typeof( str) != 'object'){
          cfError.set();
        }
      }
  }

  function cfSpinner( $rootScope){
    this.enable = function(){
      $rootScope.activeSpinner = true;
    }

    this.disable = function(){
      $rootScope.activeSpinner = false;
    }
  }

  angular.module( 'admin')
    .service( 'cfError', cfError)
    .service( 'cfSpinner', cfSpinner)
    .service( 'isJson', isJson)
    .service( 'cfSend', cfSend)
  ;
})();