/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation() {
    return {
        restrict: 'A',
        link: function (scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            element.metisMenu();
        }
    };
};


/**
 * minimalizaSidebar - Directive for minimalize sidebar
 */
function minimalizaSidebar($timeout) {
    return {
        restrict: 'A',
        template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: function ($scope, $element) {
            $scope.minimalize = function () {
                $("body").toggleClass("mini-navbar");
                if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                    // Hide menu in order to smoothly turn on when maximize menu
                    $('#side-menu').hide();
                    // For smoothly turn on menu
                    $timeout(function () {
                        $('#side-menu').fadeIn(500);
                    }, 100);
                } else {
                    // Remove all inline style from jquery fadeIn function to reset menu state
                    $('#side-menu').removeAttr('style');
                }
            }
        }
    };
};



/**
 * morrisBar - Directive for Morris chart - type Bar
 */
function morrisBar() {
    return {
        restrict: 'A',
        scope: {
            chartOptions: '='
        },
        link: function (scope, element, attrs) {
            var chartDetail = scope.chartOptions;
            chartDetail.element = attrs.id;
            var chart = new Morris.Bar(chartDetail);
            return chart;
        }
    }
}

/**
 * icheck - Directive for custom checkbox icheck
 */
function icheck($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function ($scope, element, $attrs, ngModel) {
            return $timeout(function () {
                var value;
                value = $attrs['value'];
                $scope.$watch($attrs['ngModel'], function (newValue) {
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'

                }).on('ifChanged', function (event) {
                    if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                        $scope.$apply(function () {
                            // var value  = event.target.checked ? "1" : "0";
                            return ngModel.$setViewValue('value');
                        });
                    }
                    if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                        return $scope.$apply(function () {
                            return ngModel.$setViewValue(value);
                        });
                    }
                });
            });
        }
    };
}

function cfCheckboxes($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            return $timeout(function () {
                var has = false;
                var option = scope.$eval(attrs.cfCheckboxes);
                var selecteds = ngModel.$modelValue;
                if (!selecteds) {
                    selecteds = [];
                }
                for (var i = 0; i < selecteds.length; i++) {
                    if (ngModel.$modelValue[i].id == option.id) {
                        $('input', $(element)).prop('checked', true);
                    }
                }

                scope.$watch(attrs.ngModel, function (newValue) {
                    var has = false;
                    for (i in newValue) {
                        if (newValue[i].id == option.id) {
                            has = true;
                            break;
                        }
                    }

                    if (has) {
                        $('input', $(element)).prop('checked', has);
                    } else {
                        $('input', $(element)).removeAttr('checked');
                    }
                });

                element.bind('change', function () {
                    if ($('input', $(element)).prop('checked') == true) {
                        ngModel.$modelValue.push(option);
                        scope.$apply();
                    } else {
                        for (var i = 0; i < ngModel.$modelValue.length; i++) {
                            if (ngModel.$modelValue[i].id == option.id) {
                                ngModel.$modelValue.splice(i, 1);
                                scope.$apply();
                            }
                        }
                    }
                })
            });

        }
    }
}

function cfCheckoptions($timeout, $parse) {
    return {
        priority: 20,
        restrict: 'A',
        // require: 'ngModel',
        link: function (scope, element, attrs) {
            $timeout(function () {
                var model = 'content.' + scope.$eval(attrs.cfModel);
                var $model = scope.$eval(model);

                if (!$model) {
                    $model = '';
                    scope.$apply();
                }

                var option = scope.$eval(attrs.cfCheckoptions);

                var array = $model.split(',');
                for (var i = 0; i < array.length; i++) {
                    if (array[i] == option) {
                        $('input', $(element)).prop('checked', true);
                    }
                }

                element.bind('change', function () {
                    var model = 'content.' + scope.$eval(attrs.cfModel);
                    var $model = scope.$eval(model);
                    if (!$model) {
                        $model = '';
                    }
                    var array = $model.split(',');

                    if ($('input', $(element)).prop('checked') == true) {
                        array.push(option);
                    } else {
                        for (var i = 0; i < array.length; i++) {
                            if (array[i] == option) {
                                array.splice(i, 1);
                            }
                        }
                    }
                    $model = array.join(',');
                    var parse = $parse(model);
                    parse.assign(scope, $model);
                    scope.$apply();
                })
            });

        }
    }
}

function checkSelectAll($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            element.click(function () {
                var options = scope.$eval(attrs.options);
                var values = ngModel.$viewValue;
                if (attrs.checkSelectAll == 1) {
                    for (i in options) {
                        var has = false;
                        for (z in values) {
                            if (values[z].id == options[i].id) {
                                has = true;
                            }
                        }

                        if (!has) {
                            values.push(options[i]);
                        }
                    }

                    var result = [];
                    for (i in values) {
                        result.push(values[i]);
                    }

                    ngModel.$setViewValue(result);
                } else {
                    var values = ngModel.$viewValue;
                    for (i in options) {
                        for (z in values) {
                            if (options[i].id == values[z].id) {
                                values.splice(z, 1);
                            }
                        }
                    }

                    var result = [];
                    for (i in values) {
                        result.push(values[i]);
                    }

                    ngModel.$setViewValue(result);
                }
            });
        }
    }
}

/**
 * fancyBox - Directive for Fancy Box plugin used in simple gallery view
 */
function fancyBox() {
    return {
        restrict: 'A',
        link: function (scope, element) {
            element.fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });
        }
    }
}

function uiTabs() {
    return {
        restrict: 'E',
        scope: false,
        link: function (scope, element, attrs) {
            $(element).tabs();
        }
    }
}

function crTabs() {
    return {
        restrict: 'E',
        replace: true,
        scope: '=',
        template: [
            '<div ng-if="columns.length > 1" class="tabsnav">',
            '<ul>',
            '<li ng-repeat="column in columns" ng-class="{active: currentColumn == $index}" ng-click="changeColumn($index)">{{ column.title }}</li>',
            '</ul>',
            '</div>'
        ].join(' '),
        link: function (scope, element, attrs) {
            scope.columns = scope.$eval(attrs.columns);
            scope.currentColumn = 0;
            scope.changeColumn = function (i) {
                scope.currentColumn = i;
                return false;
            }
        }
    }
}


/**
 * Dibuja las columnas de una vista de ediciÃ³n con todos los campos indicados en CrudConfig
 */
function crudCol() {
    return {
        restrict: 'E',
        replace: true,
        scope: '=',
        template: [
            '<div class="crud-col">',
            '<div class="row">',
            '<div class="{{ box.class }}" ng-init="content = data.content;" ng-repeat="box in column.box" ng-show="{{ box.show }}">',
            '<div class="ibox float-e-margins">',
            '<div ng-if="box.title" class="ibox-title">',
            '<h5>{{ box.title }}</h5>',
            '</div>',
            '<div class="ibox-content" ng-class="{ \'ibox-padding-top\': $index == 0 && !box.title, \'box-inline\': box.inline }">',
            '<div ng-repeat="field in box.elements">',
            '<crud-include></crud-include>',
            '</div>',
            '</div>',
            '</div>',
            '</div>',
            '</div>',
            '</div>'
        ].join(' '),
        link: function (scope, element, attrs) {
            var totalColumns = scope.data.crudConfig.view.columns.length;
            if (totalColumns > 1) {
                scope.colWidth = 10;
            } else {
                scope.colWidth = 12;
            }
        }
    }
}

/**
 * Hace el include de un campo
 */
function crudInclude() {
    return {
        restrict: 'E',
        replace: false,
        scope: '=',
        template: [
            // Es un campo de hasMany de otro modelo
            '<ng-include ng-if="field.hasOne != false" ng-init="content = data.content[field.hasOne]; crudConfig = data.crudConfig; administrator = data.administrator" ng-show="{{ field.show }}" src="template"></ng-include>',
            // Es un campo del modelo principal
            '<ng-include ng-if="field.hasOne == false" ng-init="content = data.content; crudConfig = data.crudConfig; administrator = data.administrator" ng-show="{{ field.show }}" src="template"></ng-include>',
            '<div ng-if="field.after" cf-parse-variable="field.after" class="cf-field-after col-md-offset-2"></div>'
        ].join(' '),
        link: function (scope, element, attrs) {
            scope.template = scope.field.template + '.html';
            if (scope.field.hasOne != false && scope.data.content[scope.field.hasOne] == null) {
                scope.data.content[scope.field.hasOne] = {};
            }
        }
    }
}

function cfParseVariable($compile) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            var str = scope.$eval(attrs.cfParseVariable);
            element.html(str);
            element[0].removeAttribute('cf-parse-variable');
            $compile(element[0])(scope);
        }
    }
}

function cfChange($timeout, $http, cfSpinner) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            if (attrs.cfChange && !$(element).data('setedChange')) {
                $(element).data('setedChange', true);
                element.change(function () {
                    eval(scope.$eval(attrs.cfChange));
                })
            }

        }
    }
}

function cfModelChange($timeout, $http) {
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (attrs.cfModelChange && !$(element).data('setedChange')) {
                $(element).data('setedChange', true);
                scope.$watch(attrs.ngModel, function (newValue) {
                    eval(scope.$eval(attrs.cfModelChange));
                });
            }
        }
    }
}



function crudField($timeout) {
    return {
        restrict: 'E',
        replace: false,
        scope: true,
        template: [
            '<ng-include ng-show="{{ field.show }}" src="template"></ng-include>',
        ].join(' '),
        link: function (scope, element, attrs) {
            scope.field = scope.crudConfig.view.fields[attrs.field];
            scope.template = scope.field.template + '.html';
        }
    }
}

function inlineEdit($http, $timeout, $compile) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            element.click(function () {
                if (scope.isEditing) {
                    return;
                }

                $timeout(function () {
                    scope.isEditing = true;
                });

                if (!scope.hasEditing) {
                    scope.hasEditing = true;
                    var data = scope.$eval(attrs.inlineEdit);
                    var html = '<crud-field field="' + data.field + '" ng-init="content = content; crudConfig = crudConfig"></crud-field>';
                    var button = angular.element('<button class="btn btn-default" inline-edit-save><i class="fa fa-check"></i></button>');
                    scope.content = data.content;
                    scope.crudConfig = data.crudConfig;
                    var newElement = angular.element('<div class="inline-edit" ng-show="isEditing"></div>');
                    newElement.html(html);
                    newElement.append(button);
                    $timeout(function () {
                        $compile(newElement)(scope);
                        element.append(newElement);
                    })
                }
            });
        }
    }
}

function inlineEditSave($http, $timeout, $compile) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            element.click(function () {
                var url = scope.crudConfig.adminUrl +
                    'field/' + scope.field.key +
                    '/' + scope.content.id + '.json';

                var data = {
                    editabledata: scope.content[scope.field.key]
                }

                $timeout(function () {
                    scope.isEditing = false;
                })

                $http.post(url, data)
                    .success(function (data) {

                    });
            });
        }
    }
}

function inlineEditBoolean($http, $timeout, $compile) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            element.click(function () {
                var url = scope.content.urls.base +
                    'field/' + scope.field.key +
                    '/' + scope.content.id + '.json';

                var value = scope.content[scope.field.key] ? 0 : 1;
                var data = {
                    editabledata: value
                }

                $http.post(url, data)
                    .success(function (data) {
                        $timeout(function () {
                            scope.content[scope.field.key] = data.content[scope.field.key];
                        })
                    });
            });
        }
    }
}

/**
 * Setea los idiomas en los campos
 */
function setLanguages($compile, $document, $timeout, dropdownService) {

    return {
        restrict: 'E',
        replace: true,
        template: [
            '<div class="input-group-btn" dropdown ng-show="data.languages.length > 1">',
            '<button tabindex="-1" class="btn btn-white cf-lang" type="button" ng-click="nextLanguage()">{{ data.currentLanguage.iso2 }}</button>',
            '<button class="btn btn-white dropdown-toggle" data-toggle="dropdown" type="button"><span class="caret"></span></button>',
            '<ul class="dropdown-menu cf-languages-dropdown" role="menu">',
            ' <li ng-click="changeLanguage(lang)" ng-repeat="lang in data.languages"><a href="">{{ lang.name }}</a></li>',
            '</ul>',
            '</div>'
        ].join(' '),
        scope: '=',
        link: function ($scope, $element, $attrs) {
            if ($attrs.scopeData) {
                $scope.data = $scope.$eval($attrs.scopeData);
            }
            $scope.changeLanguage = function (lang) {
                $scope.data.currentLanguage = lang;
                angular.element('.open').removeClass('open');
            }

            $scope.nextLanguage = function () {
                for (var i in $scope.data.languages) {
                    if ($scope.data.languages[i].id == $scope.data.currentLanguage.id) {
                        var index = (parseInt(i) + 1);

                        if (index == $scope.data.languages.length) {
                            index = 0;
                        }
                    }
                }

                $scope.data.currentLanguage = $scope.data.languages[index];
            }
        }
    };
}

/**
 * Ventanas modales
 * @example cf-modal="url/to/without/admin"
 */
function cfModal($modal, $http, $timeout, cfSpinner) {
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {

            var modalCtrl = function ($scope, $modalInstance) {
                var url = scope.$eval(attrs.cfModal);
                if (url.indexOf('.json') == -1) {
                    url += '.json';
                }

                if (attrs.cfModalParentId) {
                    if (attrs.cfModalParentIdField) {
                        var field = attrs.cfModalParentIdField;
                    } else {
                        var field = 'parent_id';
                    }
                    url += '?' + field + '=' + attrs.cfModalParentId;
                }

                if (attrs.cfModalExtraFields) {
                    var extra_fields = scope.$eval(attrs.cfModalExtraFields);
                    for (var i = 0; i < extra_fields.length; i++) {
                        var element = extra_fields[i];
                        url += '&' + element.field + '=' + element.value;
                    }
                }

                $http.get(url)
                    .success(function (result) {
                        $scope.data = result;
                    })
                    .error(function (result) {
                        console.log(result)
                    })

                // Pone en el scope el contenido padre
                $scope.parentContent = scope.$parent.content;
                $scope.parentData = scope.$parent.data;
                $scope.modalInstance = $modalInstance;

                $scope.applyParent = function (data) {
                    scope.$parent.data = data;
                }

                $scope.send = function (params, data, preview) {
                    cfSpinner.enable();
                    $http.post(params.url, data)
                        .success(function (result) {
                            $scope.data = result;
                            cfSpinner.disable();

                            if (result.crudConfig.previewUrl) {
                                if ($("#preview-link").length == 0) {
                                    $('body').append('<a id="preview-link" class="venobox_custom" data-type="iframe" href=""></a>');
                                }

                                if (preview && result.crudConfig.previewUrl) {
                                    $("#preview-link").attr('href', result.crudConfig.previewUrl);
                                    $("#preview-link").venobox({
                                        framewidth: '100%'      // default: ''
                                    }).trigger('click');
                                }
                            }

                            if (preview && !result.crudConfig.previewUrl) {
                                window.open(preview);
                            }

                            if (result.errors) {
                                return;
                            }

                            if (attrs.cfEvalModal) {
                                eval(attrs.cfEvalModal);
                            }

                            // Actualiza el model del scope padre
                            if (ngModel && attrs.cfAfterAction == 'push') {
                                // Actualiza las options si las hubiera
                                $timeout(function () {
                                    if (attrs.cfOptions) {
                                        eval('scope.' + attrs.cfOptions + '.splice( (scope.' + attrs.cfOptions + '.length - 1), 1);')
                                        // eval('result.content.position = ');
                                        eval('scope.' + attrs.cfOptions + '.push(result.content);');
                                        eval('scope.' + attrs.cfOptions + '.push({msGroup: false});');
                                    } else {
                                        // AÃ±ade la propiedad `ticked` para que pueda aÃ±adirse
                                        result.content.ticked = true;
                                        if (!ngModel.$viewValue) {
                                            ngModel.$viewValue = [];
                                        }

                                        if (scope.field.crudConfig.view.sortable) {
                                            result.content.position = scope.$parent.content[scope.field.key].length + 1;
                                        }

                                        ngModel.$viewValue.push(result.content);
                                        ngModel.$setViewValue(ngModel.$viewValue);
                                    }

                                });

                                if (!preview) {
                                    $modalInstance.dismiss('cancel');
                                }

                            } else if (ngModel && attrs.cfAfterAction == 'update') {
                                ngModel.$setViewValue(result.content);
                                $timeout(function () {
                                    scope.$parent.content[scope.$parent.field.key][scope.$index] = ngModel.$viewValue;
                                    scope.$apply();
                                })

                                if (!preview) {
                                    $modalInstance.dismiss('cancel');
                                }
                            }

                            // Se ha seteado en el controller (de cake) updateScope para actualizar el scope padre con nuevos datos
                            if (result.updateScope) {
                                angular.forEach(result.updateScope, function (value, key) {
                                    eval('scope.$parent.data.' + key + '=value;');
                                })
                            }

                            if (result.updateContent) {
                                angular.forEach(result.updateContent, function (value, key) {
                                    eval('scope.data.content.' + key + '=value;');
                                });

                                if (!preview) {
                                    $modalInstance.dismiss('cancel');
                                }
                            }
                        })
                        .error(function (result) {
                            console.log(result)
                        })
                }

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            };

            var size = attrs.size ? attrs.size : 'big';

            element.click(function () {
                $modal.open({
                    templateUrl: '/admin/manager/pages/modal',
                    controller: modalCtrl,
                    size: size
                });
                return false;
            })
        }
    }
}


function cfModalUpdate($modal, $http, $timeout, cfSpinner) {
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {

            var modalUpdateCtrl = function ($scope, $modalInstance) {
                var url = scope.$eval(attrs.cfModalUpdate);
                if (url.indexOf('.json') == -1) {
                    url += '.json';
                }
                $http.get(url)
                    .success(function (result) {
                        $scope.data = result;
                        $scope.data.content = ngModel.$viewValue;
                    })
                    .error(function (result) {
                        console.log(result)
                    })

                // Pone en el scope el contenido padre
                $scope.parentContent = scope.$parent.content;

                $scope.applyParent = function (data) {
                    scope.$parent.data = data;
                }

                $scope.send = function (params, data) {
                    $modalInstance.dismiss('cancel');
                }

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            };

            element.click(function () {
                $modal.open({
                    templateUrl: '/admin/manager/pages/modal',
                    controller: modalUpdateCtrl,
                    size: 'lg'
                });
                return false;
            })
        }
    }
}


/**
 * Enlaces usados en las ventanas modal
 * Cada vez que se quiera poner un enlace en una ventana moda, hay que usar esta directiva
 */
function cfModalLink($http, $timeout) {
    return {
        restrict: 'A',
        scope: false,
        controller: function ($scope) {
            $scope.changeParent = function (result) {
                $scope.$parent.$parent.data = result;
            }
        },
        link: function (scope, element, attrs) {
            element.click(function () {
                $http.get(scope.$eval(attrs.cfModalLink) + '.json')
                    .success(function (result) {
                        $timeout(function () {
                            scope.changeParent(result);
                        });

                    })
                    .error(function (result) {
                        console.log(result)
                    });

                return false;
            })
        }
    }
}

/**
 * Enlaces usados en las ventanas modal
 * Cada vez que se quiera poner un enlace en una ventana moda, hay que usar esta directiva
 */
function cfActionLink($http, $timeout, cfSpinner) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element, attrs) {
            element.click(function () {
                var data = scope.$eval(attrs.params);
                cfSpinner.enable();
                $http.post(scope.$eval(attrs.cfActionLink) + '.json', data)
                    .success(function (result) {
                        cfSpinner.disable();

                        if (attrs.evalReturn) {
                            eval(attrs.evalReturn);
                        }
                    })
                    .error(function (result) {
                        cfSpinner.disable();
                    });

                return false;
            })
        }
    }
}

function cfActionLinkPromt($http, $timeout, cfSpinner) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element, attrs) {
            element.click(function () {
                swal({
                    title: attrs.promt,
                    icon: "warning",
                    buttons: {
                        cancel: "Cancelar",
                        yes: {
                            text: "Sí",
                            value: "yes",
                        },
                    },
                })
                    .then(function (value) {
                        if (!value) {
                            return;
                        }


                        if (value == 'yes') {
                            var data = scope.$eval(attrs.params);
                            cfSpinner.enable();

                            $http.post(scope.$eval(attrs.cfActionLinkPromt) + '.json', data)
                                .success(function (result) {
                                    cfSpinner.disable();

                                    if (attrs.evalReturn) {
                                        eval(attrs.evalReturn);
                                    }
                                })
                                .error(function (result) {
                                    cfSpinner.disable();
                                });

                            return false;
                        } 

                    })

            })
        }
    }
}

function sortPaginator($http, $timeout, $location) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element, attrs) {
            var key = scope.$eval(attrs.sortPaginator);
            var direction = $location.search().direction;

            if (!direction) {
                direction = 'desc';
            }

            var _direction = direction == 'desc' ? 'asc' : 'desc';

            if ($location.search().sort && $location.search().sort == key) {
                var icon_direction = _direction == 'desc' ? 'up' : 'down';
                element.append('<i class="fa fa-caret-' + icon_direction + '"></i>');
            }

            element.click(function () {
                $location.search('sort', key);
                $location.search('direction', _direction);
                window.location.href = '/admin#' + $location.url();
            })
        }
    }
}

function editExpress($http) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element, attrs) {
            element.click(function () {

            });
        }
    }
}







/**
 * Directiva para confirmar el borrado de un elemento
 *
 * @attribute string cf-destroy ExpresiÃ³n que se evaluarÃ¡ al aceptar el borrado 
 * @attribute string cf-url URL que se postearÃ¡ al aceptar el borrado
 */
function confirmDelete($modal, $http, $timeout, cfError, cfSpinner, $modalStack) {
    return {
        restrict: 'A',
        require: '?ngModel',
        scope: true,
        link: function (scope, el, attrs, ngModel) {
            el.click(function (e) {
                e.preventDefault();
                swal({
                    title: "¿Estás seguro?",
                    text: "El contenido será borrado",
                    icon: "warning",
                    buttons: {
                        cancel: "No",
                        yes: {
                            text: '¡Sí, borrarlo!',
                            value: true,
                            closeModal: false
                        },
                    },
                })
                    .then(function (result) {
                        if (!result) {
                            return;
                        }

                        $timeout(function () {
                            if (!attrs.cfUrl && attrs.cfDestroy) {
                                scope.$apply(function () {
                                    eval(attrs.cfDestroy);
                                });
                            }
                        });

                        // Si existe el atributo cf-url
                        if (attrs.cfUrl) {
                            $http.post(attrs.cfUrl, {
                                id: scope.content.id,
                                salt: scope.content.salt
                            })
                                .success(function (data) {
                                    scope.data.flash = data.flash;

                                    // Se ha borrado
                                    if (data.isDeleted) {
                                        swal("¡Eliminado!", "El contenido ha sido borrado", "success");
                                        if (attrs.cfDestroy) {
                                            eval(attrs.cfDestroy);
                                        }
                                    } else {
                                        if (!data.deleteErrors) {
                                            swal("No se ha podido borrar", '', "error");
                                        } else if (data.deleteErrors.message) {
                                            swal("No se ha podido borrar", data.deleteErrors.message, "error");
                                        } else if (data.deleteErrors.modal) {
                                            var controller = function ($scope, $modalInstance) {
                                                var url = data.deleteErrors.modal.url;
                                                if (url.indexOf('.json') == -1) {
                                                    url += '.json';
                                                }
                                                $http.get(url)
                                                    .success(function (result) {
                                                        $scope.data = result;
                                                    })
                                                    .error(function (result) {
                                                        console.log(result)
                                                    })

                                                // Pone en el scope el contenido padre
                                                $scope.parentContent = scope.$parent.content;

                                                $scope.applyParent = function (data) {
                                                    scope.$parent.data = data;
                                                }

                                                $scope.send = function (params, data) {
                                                    $modalInstance.dismiss('cancel');
                                                }

                                                $scope.cancel = function () {
                                                    $modalInstance.dismiss('cancel');
                                                };
                                            };

                                            swal.close();
                                            $modal.open({
                                                templateUrl: '/admin/manager/pages/modal',
                                                controller: controller,
                                                size: 'lg'
                                            });
                                            return false;
                                        }
                                    }
                                })
                                .error(function (error) {
                                    cfError.set(error);
                                    cfSpinner.disable();
                                })
                        } else if (attrs.href) {
                            swal.close();
                            cfSpinner.enable();
                            window.location.href = attrs.href;
                        } else {
                            swal.close();
                        }
                    });

            });

            return false;
        }
    }
}

/**
 * Ayuda para el campo
 */
function ngHelp($timeout) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attrs) {
            if (scope.field.help) {
                element.addClass('cursor-help');
                element.attr('tooltip', scope.field.help);
                $timeout(function () {
                    element.html('<span class="control-tooltip">' + element.text() + '</span>');
                })
            }
        }
    }
}


function cfSpinner() {
    return {
        restrict: 'E',
        scope: false,
        replace: true,
        template: '<div class="circularG-wrap">' +
            '<div class="circularG circularG_1"></div>' +
            '<div class="circularG circularG_2"></div>' +
            '<div class="circularG circularG_3"></div>' +
            '<div class="circularG circularG_4"></div>' +
            '<div class="circularG circularG_5"></div>' +
            '<div class="circularG circularG_6"></div>' +
            '<div class="circularG circularG_7"></div>' +
            '<div class="circularG circularG_8"></div>' +
            '</div>',
        link: function (scope, element, attrs) {
            var marginTop = (element.parent().height() - element.height()) / 2;
            element.css('margin-top', marginTop);
        }
    }
}

function cfSpinAction(cfSpinner) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element, attrs) {
            element.click(function () {
                cfSpinner.enable();
            })
        }
    }
}


function cfTree($http, cfError, isJson) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            scope.treeOptions = {
                dragStop: function (event) {
                    $http.post(scope.data.crudConfig.adminUrl + 'order.json', event.dest.nodesScope.data.contents)
                        .success(function (result) {
                            isJson.check(result);
                        })
                        .error(function (error) {
                            cfError.set(error);
                        });
                }
            }
        }
    }
}

function cfForm() {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            $(element).delegate('input', 'keydown', function (e) {
                if (e.keyCode == 13) {
                    scope.send(scope.data.crudConfig.view.submit, scope.data.content);
                    return false;
                }
            })
        }
    }
}


function cfPaginator($location) {
    return {
        restrict: 'E',
        scope: '=',
        template: [
            '<paging ng-show="data.paging.pageCount > 1"',
            'class="small"',
            'page="data.paging.page"',
            'page-size="data.paging.perPage" ',
            'total="data.paging.count"',
            // 'adjacent="{{adjacent}}"',
            // 'dots="{{dots}}"',
            // 'scroll-top="{{scrollTop}}" ',
            // 'hide-if-empty="{{hideIfEmpty}}"',
            'ul-class="pagination"',
            'active-class="active"',
            // 'disabled-class="{{disabledClass}}"',
            // 'show-prev-next="{{showPrevNext}}"',
            'paging-action="pageUrl( page)">',
            '</paging>'
        ].join(' '),
        link: function (scope, element, attrs) {
            scope.pageUrl = function (page) {
                // var url = scope.data.crudConfig.adminUrl;
                var url = $location.path();
                // var path = url.substr(0, url.length-1); 
                // $location.path( path);
                $location.search('page', page);
                // $location.url( scope.data.crudConfig.adminUrl + '?page=' + page);

            }
        }
    }
}


function cfPaginatorLimit($location) {
    return {
        restrict: 'E',
        scope: '=',
        template: [
            `   
                <div ng-if="data.paging.count > 40">Resultados por página</div>
                <div ng-if="data.paging.count > 40"><ul class="pagination">
                <li ng-if="data.paging.count > 20" ng-class="{ active: data.paging.perPage == 20 }"><a ng-click="pageUrlLimit(20)">20</a></li>
                <li ng-if="data.paging.count > 40" ng-class="{ active: data.paging.perPage == 40 }"><a ng-click="pageUrlLimit(40)">40</a></li>
                <li ng-if="data.paging.count > 60" ng-class="{ active: data.paging.perPage == 60 }"><a ng-click="pageUrlLimit(60)">60</a></li>
                <li ng-if="data.paging.count > 80" ng-class="{ active: data.paging.perPage == 80 }"><a ng-click="pageUrlLimit(80)">80</a></li>
                <li ng-if="data.paging.count > 100" ng-class="{ active: data.paging.perPage == 100 }"><a ng-click="pageUrlLimit(100)">100</a></li>
                </ul></div>
            `
        ].join(' '),
        link: function (scope, element, attrs) {
            scope.pageUrlLimit = function (limit) {
                $location.search('limit', limit);
            }
        }
    }
}

function cfPaginatorModal($http, cfSpinner) {
    return {
        restrict: 'E',
        scope: '=',
        template: [
            '<paging ng-show="data.paging.pageCount > 1"',
            'class="small"',
            'page="data.paging.page"',
            'page-size="data.paging.perPage" ',
            'total="data.paging.count"',
            'ul-class="pagination"',
            'active-class="active"',
            'paging-action="pageUrl( page)">',
            '</paging>'
        ].join(' '),
        link: function (scope, element, attrs) {
            scope.pageUrl = function (page) {
                cfSpinner.enable();
                $http.get(scope.$parent.data.crudConfig.currentUrl + '.json?page=' + page).success(function (result) {
                    scope.$parent.data = result;
                    cfSpinner.disable();
                    $('.modal-content').animate({
                        scrollTop: $('.modal-body').offset().top - 250
                    }, 500);
                });
            }
        }
    }
}

function cfSearchUpload($http, cfSpinner) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            element.on('submit', function () {
                cfSpinner.enable();
                $http.get(scope.$parent.data.crudConfig.currentUrl + '.json?filename=' + scope.uploadSearch).success(function (result) {
                    scope.$parent.data = result;
                    cfSpinner.disable();
                });
            });
        }
    }
}

function cfPushUpload($timeout) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            element.on('click', function (event) {
                var parentContent = scope.$parent.$parent.$parent.parentContent;

                if (scope.data.uploadType == 'single') {
                    parentContent[scope.data.uploadField] = scope.$eval(attrs.cfPushUpload);

                    $timeout(function () {
                        scope.$apply();
                        scope.$parent.$parent.$parent.modalInstance.dismiss('cancel');
                    })

                } else {
                    return;
                }
            })
        }
    }
}

function cfUploadSelect($timeout) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            element.on('click', function (event) {
                if (scope.data.uploadType == 'single') {
                    return;
                }
                var upload = scope.$eval(attrs.cfUploadSelect);
                var selecteds = scope.data.selecteds || [];
                var has = false;

                for (var i = 0; i < selecteds.length; i++) {
                    var element = selecteds[i];
                    if (upload.id == element.id) {
                        selecteds.splice(i, 1);
                        has = true;
                        upload.selected = false;
                    }
                }

                if (!has) {
                    selecteds.push(upload);
                    upload.selected = true;
                }

                scope.data.selecteds = selecteds;


                $timeout(function () {
                    scope.$apply();
                })
            })
        }
    }
}

function cfUploadAddMultiple($timeout) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            element.on('click', function (event) {
                var parentContent = scope.$parent.$parent.parentContent;

                if (!parentContent[scope.data.uploadField]) {
                    parentContent[scope.data.uploadField] = [];
                }

                var selecteds = scope.data.selecteds || [];

                for (var i = 0; i < selecteds.length; i++) {
                    var upload = selecteds[i];
                    parentContent[scope.data.uploadField].push(upload);
                }

                $timeout(function () {
                    scope.$apply();
                    scope.$parent.$parent.modalInstance.dismiss('cancel');
                })
            })
        }
    }
}



function cfSubmitSearch($location) {
    var qs = function (obj, prefix) {
        var str = [];
        for (var p in obj) {
            var k = prefix ? prefix + "[" + p + "]" : p,
                v = obj[k];

            if (!v) {
                v = obj[p];
            }

            if (angular.isDate(v)) {
                str.push(k + '=' + moment(v).format('YYYY-MM-DD'));
            } else {
                str.push(angular.isObject(v) ? qs(v, k) : (k) + "=" + encodeURIComponent(v));
            }

        }
        return str.join("&");
    }


    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            scope.search = function () {
                // console.log( scope.data.search);
                $location.search(qs(scope.data.search));
            }
            scope.reset = function () {
                // console.log( scope.data.search);
                $location.url(scope.data.crudConfig.currentUrl);
            }
        }
    }
}

function cfEnterSubmit($parse) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            element.bind('keyup', function (event) {
                var fn = $parse(attrs.cfEnterSubmit);
                if (event.which == 13) {
                    scope.$apply(function () {
                        fn(scope);
                    });
                }
            });
        }
    }
}


function cfMessage($timeout) {
    return {
        restrict: 'A',
        scope: '=',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            scope.$watch(attrs['ngModel'], function (newValue, oldVale) {
                if (newValue && newValue.length > 0) {
                    element.removeClass('animated fadeOutUp')
                        .addClass('animated fadeInDown');
                    $timeout(function () {
                        element.removeClass('animated fadeInDown')
                            .addClass('animated fadeOutUp');
                    }, 5000);
                }
            });
        }
    }
}

function cfFlash(toastr) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            toastr.success(attrs.cfMessage);

        }
    }
}

function enterNumbers() {
    return {
        restrict: 'EA',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            scope.$watch(attrs.ngModel, function (newValue, oldValue) {
                var spiltArray = String(newValue).split("");

                if (attrs.allowNegative == "false") {
                    if (spiltArray[0] == '-') {
                        newValue = newValue.replace("-", "");
                        ngModel.$setViewValue(newValue);
                        ngModel.$render();
                    }
                }

                if (attrs.allowDecimal == "false") {
                    newValue = parseInt(newValue);
                    ngModel.$setViewValue(newValue);
                    ngModel.$render();
                }

                if (attrs.allowDecimal != "false") {
                    if (attrs.decimalUpto) {
                        var n = String(newValue).split(".");
                        if (n[1]) {
                            var n2 = n[1].slice(0, attrs.decimalUpto);
                            newValue = [n[0], n2].join(".");
                            ngModel.$setViewValue(newValue);
                            ngModel.$render();
                        }
                    }
                }


                if (spiltArray.length === 0) return;
                if (spiltArray.length === 1 && (spiltArray[0] == '-' || spiltArray[0] === '.')) return;
                if (spiltArray.length === 2 && newValue === '-.') return;

                /*Check it is number or not.*/
                if (isNaN(newValue)) {
                    ngModel.$setViewValue(oldValue);
                    ngModel.$render();
                }
            });
        }
    };
}

function touchspin($timeout) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            var postfix = scope.field.after || false;
            var decimals = scope.field.decimals || false;

            var options = {
                verticalbuttons: true,
                min: scope.field.range[0],
                max: scope.field.range[1],
                step: scope.field.range[2],
                boostat: 5,
                maxboostedstep: 10
            };

            if (postfix) {
                options.postfix = postfix;
            }

            if (decimals) {
                options.decimals = decimals;
            }

            $(element).TouchSpin(options);
        }
    }
}


function autocompleteData() {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: false,
        link: function (scope, element, attrs, ngModel) {
            var val = scope.$eval(attrs.scope);
            scope.beforeSelect = function (result) {
                var value = scope.content[attrs.autocompleteData];
                if (value == null) {
                    value = [result.originalObject];
                } else {
                    value.push(result.originalObject);
                }
            }
        }
    }
}

function autocompleteDataBt($timeout) {
    return {
        restrict: 'A',
        require: '?ngModel',
        scope: false,
        link: function (scope, element, attrs, ngModel) {
            scope.beforeSelect = function (result) {
                scope.content[attrs.autocompleteDataBt] = result.originalObject;
            }
        }
    }
}

function cfBindModel($compile) {
    return {
        link: function (scope, element, attr) {
            var field = scope.$eval(attr.cfBindModel);
            if ((!scope.field.hasOne && field.indexOf('.') >= 0) ||
                scope.crudConfig.name.singular != scope.data.crudConfig.name.singular ||
                scope.data.contents
            ) {
                var namespace = 'content';
            } else {
                var namespace = 'data.content';
            }
            element[0].removeAttribute('cf-bind-model');
            // element[0].setAttribute('ng-model', 'content.' + scope.$eval(attr.cfBindModel));
            element[0].setAttribute('ng-model', namespace + '.' + field);
            $compile(element[0])(scope);
        }
    }
}

function cfBindSelected($timeout) {
    return {
        priority: 10,
        scope: '=',
        link: function (scope, element, attrs) {
            $timeout(function () {
                var variable = 'content.' + scope.$eval(attrs.cfBindSelected);

                var value = scope.$eval(variable);
                if (value == '') {
                    value = false;
                }
                if (element.val() == value) {
                    element[0].setAttribute('selected', true);
                }
            })
        }
    }
}

/**
 * 
 */
function bindContent($timeout) {
    return {
        scope: '=',
        link: function (scope, element, attrs) {
            $timeout(function () {
                var variable = 'content.' + scope.$eval(attrs.bindContent);
                var value = scope.$eval(variable);
                element.html(value);
            })
        }
    }
}

function cfSwitch($window, $timeout, $log, $parse) {
    function linkSwitchery(scope, elem, attrs, ngModel) {
        if (!attrs.ngModel) {
            return false;
        };
        var options = {};
        try {
            options = $parse(attrs.cfSwitch)(scope);
        } catch (e) { }
        $timeout(function () {
            var switcher = new $window.Switchery(elem[0], options);
            var element = switcher.element;
            element.checked = scope.initValue;
            switcher.setPosition(false);
            element.addEventListener('change', function (evt) {
                scope.$apply(function () {
                    ngModel.$setViewValue(element.checked);
                })
            })
        }, 0);
    }
    return {
        priority: 1,
        require: 'ngModel',
        restrict: 'AE',
        scope: {
            initValue: '=ngModel'
        },
        link: linkSwitchery
    }
}

angular.module('admin').service('cfTags', function ($q, $http) {
    this.load = function (url, query) {
        var deferred = $q.defer();
        var tags = [{
            text: "Tag1"
        },
        {
            text: "Tag2"
        },
        {
            text: "Tag3"
        }
        ];
        $http.get(url + '?query=' + query).success(function (data) {
            deferred.resolve(data.data);
        });
        return deferred.promise;
    }
});

function cfTags($http, cfTags) {
    return {
        scope: '=',
        link: function (scope, element, attrs) {
            scope.loadTags = function (url, query) {
                return cfTags.load(url, query);
            }
        }
    }
}

function cfGoogleEvents($timeout, $parse) {
    return {
        scope: '=',
        link: function (scope, element, attrs) {
            var options = scope.$eval(attrs.cfGoogleEvents);
            var lat = $parse(options.lat);
            var lng = $parse(options.lng);
            var zoom = $parse(options.zoom);

            if (!scope.gmap) {
                scope.gmap = {
                    dragend: function (el) {
                        scope.content.lat = el.latLng.lat();
                        scope.content.lng = el.latLng.lng();

                        if (scope.content.settings) {
                            scope.content.settings.lat = el.latLng.lat();
                            scope.content.settings.lng = el.latLng.lng();
                        }

                        scope.$apply();
                    },
                    simpleMapLocation: function (place) {
                        var place = this.getPlace();
                        if (!scope.content) {
                            scope.content = {};
                        }
                        scope.content.lat = place.geometry.location.lat();
                        scope.content.lng = place.geometry.location.lng();

                        if (scope.content.settings) {
                            scope.content.settings.lat = place.geometry.location.lat();
                            scope.content.settings.lng = place.geometry.location.lng();
                        }

                        scope.$apply();
                    },
                    zoomChanged: function (data) {
                        scope.content.zoom = this.zoom;
                        if (scope.content.settings) {
                            scope.content.settings.zoom = this.zoom;
                        }
                        scope.$apply();
                    }
                }
            }
        }
    }
}

function scrolltofixed() {
    return {
        restrict: 'C',
        scope: '=',
        link: function (scope, element, attrs) {
            $(element).scrollToFixed({
                marginTop: 200
            });
        }
    }
}

function cfConfirm() {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            $(element).scrollToFixed({
                marginTop: 200
            });
        }
    }
}

function cfResetModel($timeout) {
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            $(element).click(function () {
                ngModel.$setViewValue(false);
            });
        }
    }
}

function cfTreeOptions($timeout) {

    function positione(contents, parent) {
        for (var i = 0; i < contents.length; i++) {
            contents[i].position = i + 1;
            if (parent) {
                contents[i].parent_id = parent.id;
            } else {
                contents[i].parent_id = null;
            }
            if (contents[i].nodes.length > 0) {
                positione(contents[i].nodes, contents[i]);
            }
        }

        return contents;
    }

    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            scope.treeOptions = {
                dragStop: function (event) {
                    console.log(ngModel.$viewValue);
                    positione(ngModel.$viewValue);
                }
            };
        }
    }
}


function cfAddHasMany($timeout) {
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            element.click(function () {
                var config = scope.$eval(attrs.cfAddHasMany);
                var fields = config.view.fields;
                var content = [];
                angular.forEach(fields, function (value, key) {
                    if (value.translate) { } else {

                    }
                });
                var value = ngModel.$viewValue;
                value.push({});
                ngModel.$setViewValue(value);
            })
        }
    }
}

function postForm($http, $timeout, cfSpinner) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            element.on('click', function (event) {
                event.preventDefault();
                var data = scope.$eval(attrs.scope);
                cfSpinner.enable();
                $http.post(attrs.postForm + '.json', data)
                    .success(function (result) {
                        $(element).data('sending', false);

                        if (attrs.updateScope) {
                            var scopes = attrs.updateScope.split(',');

                            for (var i = 0; i < scopes.length; i++) {
                                scope[scopes[i]] = result[scopes[i]];
                            };

                            $timeout(function () {
                                scope.$apply();
                            })
                        }

                        if (attrs.success) {
                            eval('var func = ' + attrs.success);
                            func(result.response, result);
                        }

                        if (result.errors) {
                            scope.errors = result.errors;
                        }

                        cfSpinner.disable();
                    })
                    .error(function (result) {
                        element.attr('sending', false);
                        cfSpinner.disable();
                    })
            })
        }
    }
}

function sortableOptions($timeout, $http) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            scope.sortableOptions = {
                stop: function (e, ui) {
                    var contents = scope.$eval(attrs.sortableOptions);
                    var data = {};
                    for (var i = 0; i < contents.length; i++) {
                        if (scope.data.paging && scope.data.paging.page > 1) {
                            var index = (scope.data.paging.page * scope.data.paging.perPage) + 1 - scope.data.paging.perPage + i;
                        } else {
                            var index = i;
                        }

                        data[index] = contents[i].id;
                        contents[i].position = index;
                    };
                    var config = scope.$eval(attrs.config);
                    $http.post(config.adminUrl + 'sortable.json', {
                        data: data
                    });
                }
            }
        }
    }
}

function autocompleteSortableOptions($timeout, $http) {
    return {
        restrict: 'A',
        scope: '=',
        link: function (scope, element, attrs) {
            scope.sortableOptions = {
                stop: function (e, ui) {
                    var contents = scope.$eval(attrs.autocompleteSortableOptions);
                    for (var i = 0; i < contents.length; i++) {
                        contents[i]._joinData.position = i + 1;
                    }
                }
            }
        }
    }
}



/**
 *
 * Pass all functions into module
 */
angular
    .module('admin')
    .directive('sideNavigation', sideNavigation)
    .directive('minimalizaSidebar', minimalizaSidebar)
    .directive('editExpress', editExpress)
    .directive('inlineEdit', inlineEdit)
    .directive('inlineEditSave', inlineEditSave)
    .directive('inlineEditBoolean', inlineEditBoolean)
    .directive('icheck', icheck)
    .directive('cfCheckboxes', cfCheckboxes)
    .directive('cfCheckoptions', cfCheckoptions)
    .directive('checkSelectAll', checkSelectAll)
    .directive('fancyBox', fancyBox)
    .directive('crudCol', crudCol)
    .directive('cfChange', cfChange)
    .directive('crudInclude', crudInclude)
    .directive('setLanguages', setLanguages)
    .directive('cfModal', cfModal)
    .directive('cfModalUpdate', cfModalUpdate)
    .directive('cfModalLink', cfModalLink)
    .directive('confirmDelete', confirmDelete)
    .directive('ngHelp', ngHelp)
    .directive('cfSpinner', cfSpinner)
    .directive('cfSpinAction', cfSpinAction)
    .directive('crTabs', crTabs)
    .directive('uiTabs', uiTabs)
    .directive('cfTree', cfTree)
    .directive('cfForm', cfForm)
    .directive('cfPaginator', cfPaginator)
    .directive('cfPaginatorModal', cfPaginatorModal)
    .directive('cfPaginatorLimit', cfPaginatorLimit)
    .directive('cfSubmitSearch', cfSubmitSearch)
    .directive('cfEnterSubmit', cfEnterSubmit)
    .directive('cfMessage', cfMessage)
    .directive('enterNumbers', enterNumbers)
    .directive('crudField', crudField)
    .directive('touchspin', touchspin)
    .directive('cfFlash', cfFlash)
    .directive('autocompleteData', autocompleteData)
    .directive('autocompleteDataBt', autocompleteDataBt)
    .directive('cfBindModel', cfBindModel)
    .directive('cfBindSelected', cfBindSelected)
    .directive('bindContent', bindContent)
    .directive('cfActionLink', cfActionLink)
    .directive('cfActionLinkPromt', cfActionLinkPromt)
    .directive('sortPaginator', sortPaginator)
    .directive('cfSwitch', cfSwitch)
    .directive('cfTags', cfTags)
    .directive('cfParseVariable', cfParseVariable)
    .directive('cfGoogleEvents', cfGoogleEvents)
    .directive('scrolltofixed', scrolltofixed)
    .directive('cfResetModel', cfResetModel)
    .directive('cfAddHasMany', cfAddHasMany)
    .directive('postForm', postForm)
    .directive('sortableOptions', sortableOptions)
    .directive('autocompleteSortableOptions', autocompleteSortableOptions)
    .directive('cfPushUpload', cfPushUpload)
    .directive('cfSearchUpload', cfSearchUpload)
    .directive('cfUploadSelect', cfUploadSelect)
    .directive('cfUploadAddMultiple', cfUploadAddMultiple)
    .directive('cfTreeOptions', cfTreeOptions)
    .directive('cfModelChange', cfModelChange);
