'use strict';

angular.module('plupload', [])
	.provider('plUploadService', function() {

		var config = {
			flashPath: 'bower_components/plupload-angular-directive/dist/plupload.flash.swf',
			silverLightPath: 'bower_components/plupload-angular-directive/dist/plupload.silverlight.xap',
			uploadPath: 'upload.php'
		};

		this.setConfig = function(key, val) {
	        	config[key] = val;
	        };

	    this.getConfig =  function(key) {
	        	return config[key];
	        };

	    var that = this;

	    this.$get = [function(){

		    return {
		        getConfig: that.getConfig,
		        setConfig: that.setConfig
		    };
		}];
	})


  .filter( 'filesize', function () {
    var units = [
      'bytes',
      'KB',
      'MB',
      'GB',
      'TB',
      'PB'
    ];

    return function( bytes, precision ) {
      if ( isNaN( parseFloat( bytes )) || ! isFinite( bytes ) ) {
        return '?';
      }

      var unit = 0;

      while ( bytes >= 1024 ) {
        bytes /= 1024;
        unit ++;
      }

      return bytes.toFixed( + precision ) + ' ' + units[ unit ];
    };
  })

	.directive( 'plUploadProgress', function(){
		return {
			restrict: 'E',
			require: '?ngModel',
			template: 
      '<div class="inv-progress-bar" ng-show="plUploading">' + 
        '<div class="file-upload-in-progress">' + 
          '<div class="progress-icon">' + 
            '<i class="fa fa-cog fa-2x fa-spin fa-fw" ng-show="percent == 100"></i>' +
          '</div>' +
          '<div class="inv-progress-bar">' +
            '<div class="inv-progress ng-binding" style="width: {{ percent }}%"></div>' +
          '</div>' +
        '</div>' + 
      '</div>',
      link: function( scope, element, attrs, ngModel) {
      	scope.$watch( attrs.ngModel, function( value){
      		scope.plUploading = value;
      	});

        scope.$watch( attrs.plPercent, function(value){
          scope.percent = value;
        })
      }

		}
	})

	.directive( 'plUploadImage', function( $modal){
		return {
			restrict: 'E',
			scope: {
				'plFilesModel': '=',
				'plSrc' : '=',
				'plIndex': '=',
				'plFieldConfig': '=',
        'plRepeatScope': '=',
        'plContents': '='
			},
			template: [
        '<div class="pl-upload-image">',
          '<pl-upload-progress ng-model="plFilesModel.isUploading" pl-percent="plFilesModel.percent"></pl-upload-progress>',
			   	'<img ng-if="plSrc" style="width: 100%" ng-show="plSrc" ng-src="{{ plSrc }}?{{ plFilesModel.modified | date:\'yyyyMMddHHmmss\' }}" />',
          '<div class="pl-upload-info" title="{{ plFilesModel.filename }}">{{ plFilesModel.filename }}</div>',
          '<span ng-if="plType == \'multi\'  && plFilesModel.id && !plFilesModel.isUploading && !plFilesModel.error" confirm-delete class="pl-upload-buttons pl-upload-delete" cf-destroy="scope.plRepeatScope.splice( scope.plIndex, 1)"  pl-files-model="plFilesModel"><i class="fa fa-trash"></i></span>',
			   	'<span ng-if="plType == \'single\'  && plFilesModel.id && !plFilesModel.isUploading && !plFilesModel.error" confirm-delete class="pl-upload-buttons pl-upload-delete" cf-destroy="scope.deleteUpload()"  pl-files-model="plFilesModel"><i class="fa fa-trash"></i></span>',
          '<div ng-if="plFieldConfig.config.langs && !plFilesModel.isUploading  && !plFilesModel.error" class="m-t-s">',
            '<label>Idioma</label>',
            '<select class="col-sm-10 form-control" style="width:100%;"',
              'ng-model="plFilesModel.locale">',
              '<option value="">-- Todos --</option>',
              '<option ng-selected="plFilesModel.locale == lang.iso3" ng-repeat="lang in $parent.$parent.data.languages" value="{{ lang.iso3 }}">{{ lang.name }}</option>',
            '</select>',
          '</div>',
          '<div ng-if="plFieldConfig.config.langs_multiple && !plFilesModel.isUploading  && !plFilesModel.error" class="m-t-s">',
            '<label>Idioma</label>',
            '<div ng-repeat="lang in $parent.$parent.data.languages">',
              '<label><input value="{{ lang.iso3 }}" ng-model="plFilesModel.locales[lang.iso3]" type="checkbox"> {{ lang.name }}</label>',
            '</div>',
          '</div>',
          '<span ng-if="!plFilesModel.error && !plFilesModel.isUploading " ng-click="plFilesModel.edit = true" class="pl-upload-buttons pl-upload-edit" pl-contents="plContents" pl-index="plIndex" pl-files-model="plFilesModel" ng-model="plFilesModel"><i class="fa fa-pencil"></i></span>',
          '<span ng-if="!plFilesModel.isUploading && plFilesModel.id && !plFilesModel.error" im-crop class="pl-upload-buttons pl-upload-crop" pl-contents="plContents" pl-files-model="plFilesModel" ><i class="fa fa-crop"></i></span>',
          '<span ng-if="!plFilesModel.isUploading && plFilesModel.id && !plFilesModel.error" class="pl-upload-buttons"><a href="{{ plFilesModel.paths.org }}" download><i class="fa fa-download"></i></a></span>',
          '<div class="upload-edit" ng-show="plFilesModel.edit">',
          '<div class="form-group">',
              '<label class="col-lg-2 control-label _short">Título</label>',
              '<div class="col-lg-9">',
                '<div ng-class="{\'input-group\': $parent.$parent.data.languages.length > 1}">',
                  '<set-languages scope-data="$parent.$parent.data"></set-languages>',
                  '<input ng-class="{ hide: $parent.$parent.data.currentLanguage.iso3 != lang.iso3 }" ng-repeat="lang in $parent.$parent.data.languages" ng-model="plFilesModel[lang.iso3].title" type="text" class="form-control">',
                '</div>',
              '</div>',
            '</div>',
            '<div class="form-group">',
              '<label class="col-lg-2 control-label _short">Alt</label>',
              '<div class="col-lg-9">',
                '<div ng-class="{\'input-group\': $parent.$parent.data.languages.length > 1}">',
                  '<set-languages scope-data="$parent.$parent.data"></set-languages>',
                  '<input ng-class="{ hide: $parent.$parent.data.currentLanguage.iso3 != lang.iso3 }" ng-repeat="lang in $parent.$parent.data.languages" ng-model="plFilesModel[lang.iso3].alt" type="text" class="form-control">',
                '</div>',
              '</div>',
            '</div>',
            '<div class="form-group asset-author">',
              '<label class="col-lg-2 control-label _short">Autor</label>',
              '<div class="col-lg-9">',
                '<div>',
                  '<input ng-model="plFilesModel.author" type="text" class="form-control">',
                '</div>',
              '</div>',
            '</div>',
            '<div class="form-group">',
              '<button ng-click="plFilesModel.edit = false" class="btn btn-white">OK</button>',
            '</div>',
          '</div>',
        '</div>'
			].join( ''),
      link: function( scope, element, attrs, ngModel) {
        scope.plType = attrs.plType;
        scope.deleteUpload = function(){
          scope.plFilesModel = null;
        }
      }
		}
	})

  .directive( 'plUploadDoc', function( $modal){
    return {
      restrict: 'E',
      scope: {
        'plFilesModel': '=',
        'plFieldConfig': '=',
        'plSrc' : '=',
        'plIndex': '=',
        'plRepeatScope': '=',
        'plContents': '='
      },
      template: [
        '<div class="pl-upload-doc">',
          '<pl-upload-progress ng-model="plFilesModel.isUploading" pl-percent="plFilesModel.percent"></pl-upload-progress>',
          '<div ng-if="plFilesModel.filename"><i class="pl-upload-fileicon fa fa-file-o"></i> <span class="pl-upload-extension">{{ plFilesModel.extension }}</span>  <span class="pl-upload-filesize">{{ plFilesModel.filesize | filesize }}</span></div>',
          '<div ng-if="plFilesModel.filename" class="pl-upload-info" title="{{ plFilesModel.filename }}">{{ plFilesModel.filename }}</div>',
          '<div ng-if="plFieldConfig.config.langs && !plFilesModel.isUploading  && !plFilesModel.error" class="m-t-s">',
            '<label>Idioma</label>',
            '<select class="col-sm-10 form-control" style="width:100%;"',
              'ng-model="plFilesModel.locale">',
              '<option value="">-- Todos --</option>',
              '<option ng-selected="plFilesModel.locale == lang.iso3" ng-repeat="lang in $parent.$parent.data.languages" value="{{ lang.iso3 }}">{{ lang.name }}</option>',
            '</select>',
          '</div>',
          '<div ng-if="plFieldConfig.config.langs_multiple && !plFilesModel.isUploading  && !plFilesModel.error" class="m-t-s">',
            '<label>Idioma</label>',
            '<div ng-repeat="lang in $parent.$parent.data.languages">',
              '<label><input value="{{ lang.iso3 }}" ng-model="plFilesModel.locales[lang.iso3]" type="checkbox"> {{ lang.name }}</label>',
            '</div>',
          '</div>',
          '<div ng-if="plFilesModel.extension == \'svg\'"><img style="width: 70px" ng-src="{{ plFilesModel.paths[0] }}"></div>',
          '<span ng-if="plType == \'multi\' && !plFilesModel.isUploading && !plFilesModel.error" confirm-delete class="pl-upload-buttons pl-upload-delete" cf-destroy="scope.plRepeatScope.splice( scope.plIndex, 1)" pl-files-model="plFilesModel"><i class="fa fa-trash"></i></span>',
          '<span ng-if="plType == \'single\' && !plFilesModel.isUploading && !plFilesModel.error" confirm-delete class="pl-upload-buttons pl-upload-delete" cf-destroy="scope.deleteUpload()" pl-files-model="plFilesModel"><i class="fa fa-trash"></i></span>',
          '<span ng-click="plFilesModel.edit = true" class="pl-upload-buttons pl-upload-edit" pl-contents="plContents" pl-index="plIndex" pl-files-model="plFilesModel" ng-model="plFilesModel"><i class="fa fa-pencil"></i></span>',
          '<span ng-if="!plFilesModel.isUploading && plFilesModel.id && !plFilesModel.error" class="pl-upload-buttons"><a href="{{ plFilesModel.paths[0] }}" download><i class="fa fa-download"></i></a></span>',
          '<div class="upload-edit" ng-show="plFilesModel.edit">',
          '<div class="form-group">',
              '<label class="col-lg-2 control-label _short">Título</label>',
              '<div class="col-lg-9">',
                '<div ng-class="{\'input-group\': $parent.$parent.data.languages.length > 1}">',
                  '<set-languages scope-data="$parent.$parent.data"></set-languages>',
                  '<input ng-class="{ hide: $parent.$parent.data.currentLanguage.iso3 != lang.iso3 }" ng-repeat="lang in $parent.$parent.data.languages" ng-model="plFilesModel[lang.iso3].title" type="text" class="form-control">',
                '</div>',
              '</div>',
            '</div>',
            '<div class="form-group">',
              '<button ng-click="plFilesModel.edit = false" class="btn btn-white">OK</button>',
            '</div>',
          '</div>',
        '</div>'
        
      ].join( ''),
      link: function( scope, element, attrs, ngModel) {
        scope.plType = attrs.plType;
        scope.deleteUpload = function(){
          scope.plFilesModel = null;
        }
      }
    }
  })
	
/**
 * Edición de uploads
 */
	.directive( 'imEdit', function( $http, $rootScope, $modal, $timeout) {
	  return {
	    restrict: 'A',
      scope: '=',
	    link: function( scope, element, attrs, ngModel){
	    	var ModalInstanceCtrl = function ($scope, $modalInstance) {
          $http.post( '/admin/upload/uploads/edit.json', {
            upload: scope.plFilesModel
          }).success(function( result){
            $scope.data = result;
            scope.plFilesModel = result.content;
            $scope.plFilesModel = scope.plFilesModel;
          });
          $scope.send = function () {
            // Actualiza el scope padre con la información del upload
            $timeout(function(){
              scope.$parent.$apply(function(){
                scope.plContents[scope.plIndex] = scope.plFilesModel;
              });
            });
            $modalInstance.dismiss('cancel');
          };
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        };

	      element.click(function(){
          $modal.open({
            templateUrl: '/admin/manager/pages/modal',
            controller: ModalInstanceCtrl
          })
        });
	    }
	  }
	})

  .directive( 'imCrop', function( $http, $modal, $timeout) {
  return {
    restrict: 'A',
    scope: '=',
    link: function( scope, el, attrs){
      var ModalInstanceCtrl = function ($scope, $rootScope, $modalInstance) {
        var height = (angular.element( window).height() - 100);
        $timeout(function(){
          angular.element( '.upload-dialog .modal-content').css( 'height', height + 'px');
        });
        $http.post( '/admin/upload/uploads/crop.json', {
          id: scope.plFilesModel.id
        }).success(function( result){
          $scope.data = result;
        });

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };

        $scope.send = function(){
          $http.post( '/admin/upload/uploads/proccess.json', {
            upload: $scope.data.current
          }).success( function(){
            // Actualiza la fecha para que se recargue la imagen
            $timeout(function(){
              scope.$parent.$apply(function(){
                scope.plContents[scope.plIndex].modified = new Date().toString();
              });
            });
            $modalInstance.dismiss('cancel');
          });

          return false;
        }
      };

      el.click(function(){
        $modal.open({
          templateUrl: '/admin/manager/pages/modal',
          controller: ModalInstanceCtrl,
          windowClass: 'upload-dialog'
        })
      });
    }
  }
})


	.directive( 'plUploadDelete', ['$modal', function( $modal){
		return {
			restrict: 'A',
			scope: {
				'plFilesModel': '=',
				'plIndex': '=',
				'plRepeatScope': '='
			},
			link: function( scope, element, attrs){
				var ModalInstanceCtrl = function ($scope, $modalInstance) {
          $scope.ok = function () {
          	if( scope.plRepeatScope) {
          		scope.plRepeatScope.splice( scope.plIndex, 1);
          	} else {
            	scope.plFilesModel = false;
          	}
            $modalInstance.dismiss('cancel');
            $scope.upload_delete_file = false;
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        };

        element.click(function(){
          $modal.open({
            templateUrl: 'modal-delete.html',
            controller: ModalInstanceCtrl
          })
        })
			}
		}
	}])
	.directive('plUpload', ['$parse', '$modal', 'plUploadService', function ($parse, $modal, plUploadService) {
		return {
			restrict: 'A',
			scope: {
				'plProgressModel': '=',
				'plFilesModel': '=',
				'plFiltersModel': '=',
				'plMultiParamsModel':'=',
				'plMultiple':'=',
				'plInstance': '=',
        'plUploading': '=',
				'plErrors': '='
			},
			link: function (scope, element, attrs) {

        // Sortable
        scope.sortableUpload = {
          handle:'.handle',
          update: function( event, ui){
            var index = 1;

            var rows = ui.item.scope().$parent.content.rows;
            for( var i = rows.length - 1; i >= 0; i--){
              rows[i].position = index;
              ui.item.scope().$apply();
              index++;
            }
          }
        }

				scope.randomString = function(len, charSet) {
					charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
					var randomString = '';
					for (var i = 0; i < len; i++) {
						var randomPoz = Math.floor(Math.random() * charSet.length);
						randomString += charSet.substring(randomPoz,randomPoz+1);
					}
					return randomString;
				}

				if( !attrs.id){
					var randomValue = scope.randomString(5);
					attrs.$set( 'id',randomValue);	
				}
				if( !attrs.plAutoUpload){
					attrs.$set( 'plAutoUpload','true');
				}
				if( !attrs.plMaxFileSize){
					attrs.$set( 'plMaxFileSize','500mb');
				}
				if( !attrs.plUrl){
					attrs.$set( 'plUrl', plUploadService.getConfig('uploadPath'));
				}
				if( !attrs.plFlashSwfUrl){
					attrs.$set( 'plFlashSwfUrl', plUploadService.getConfig('flashPath'));
				}
				if( !attrs.plSilverlightXapUrl){
					attrs.$set( 'plSilverlightXapUrl', plUploadService.getConfig('silverLightPath'));
				}
				if( typeof scope.plFiltersModel=="undefined"){
					scope.filters = [{title : "Image files", extensions : "jpg,jpeg,gif,csv,png,tiff,svg,pdf,ppt,pptx,swf,mp3,doc,docx,mp4,m4v,mov,webm,ogg,ogv,zip,svg"}];
					//alert('sf');
				} else {
					scope.filters = scope.plFiltersModel;
				}


				var options = {
					  runtimes : 'html5,flash,silverlight',
						browse_button : attrs.id,
						multi_selection: true,
				//		container : 'abc',
						max_file_size : attrs.plMaxFileSize,
						url : attrs.plUrl,
						flash_swf_url : attrs.plFlashSwfUrl,
						silverlight_xap_url : attrs.plSilverlightXapUrl,
						filters : scope.filters,
						file_data_name: 'filename',
            dragdrop: true
				}


				if(scope.plMultiParamsModel){
					options.multipart_params = scope.plMultiParamsModel;
				}

				var uploader = new plupload.Uploader(options);
				uploader.settings.headers = plUploadService.getConfig('headers');

				uploader.init();

				// Error de subida de fichero
				uploader.bind('Error', function(up, err) {
					if(attrs.onFileError){
						scope.$parent.$apply(onFileError);
					}

          if( scope.plMultiple) {
            if( !angular.isArray( scope.plFilesModel)) {
              scope.plFilesModel = [];
            }
            scope.$apply( function(){
              angular.forEach( up.files, function( file, key){
                angular.forEach( scope.plFilesModel, function( upload, key){
                  if( upload._id == file.id) {
                    // Acciones para el icono de upload progress
                    upload.error = 'Hubo un error en la subida. Intentálo de nuevo o reduce el tamaño del fichero.';
                    if( !angular.isArray( scope.plErrors)) {
                      scope.plErrors = [];
                    }
                    
                    scope.plErrors.push({
                      message: err.message
                    });
                    upload.isUploading = false;
                  }
                })
              });
            });
          } else {
            scope.plFilesModel = {
              error: 'Hubo un error en la subida. Intentálo de nuevo o reduce el tamaño del fichero.'
            };
            scope.$apply();
          }

          // Muestra el boton de subir ficheros
	        scope.$parent.plUploading = false;
					up.refresh(); // Reposition Flash/Silverlight
 				});


				// Cuando se añade el upload
				uploader.bind('FilesAdded', function(up,files) {
					scope.$parent.plUploading = false;
					scope.$apply(function() {
          var maxSize = 30;

						if(attrs.plFilesModel) {
							angular.forEach(files, function(file,key) {

                // Tamaño del fichero
                if( file.type.indexOf( 'image') > -1 && file.size > (1024000 * maxSize)) {
                  file.error = 'El de fichero no debe superar los ' + maxSize + 'MB';
                  file.isUploading = false;
                  file.percent = 100;
                  file.status = 4;
                  if( scope.plMultiple) {
                    if( !angular.isArray( scope.plFilesModel)) {
                      scope.plFilesModel = [];
                    }

                    scope.plFilesModel.push( file);
                  } else {
                    scope.plFilesModel = file;
                  }

                  return;
                }
                
								if( scope.plMultiple) {
									if( !angular.isArray( scope.plFilesModel)) {
										scope.plFilesModel = [];
                  }
                  
									scope.plFilesModel.push( {
										_id: file.id,
										isUploading: true
									});
								} else {
                  file.isUploading = true;
									scope.plFilesModel = file;
								}
							});
						}
							
						if(attrs.onFileAdded){
							scope.$parent.$eval(attrs.onFileAdded);
						}
					});

					uploader.start();
				});


				// Cuando el fichero se ha subido
				uploader.bind('FileUploaded', function(up, file, res) {
					scope.$parent.plUploading = false;
					var $res = JSON.parse( res.response);
					if( scope.plMultiple) {
						if( !angular.isArray( scope.plFilesModel)) {
							scope.plFilesModel = [];
						}
						scope.$apply( function(){
							angular.forEach( scope.plFilesModel, function( upload, key){
								if( upload._id == file.id) {
									upload.isUploading = false;

                  if( $res.success){
                    angular.extend( upload, $res.upload);
                  } else {
                    angular.extend( upload, $res);
                  }
									
									scope.plFilesModel [key] = upload;
								}
							})
						})
					} else {
						scope.$apply(function() {
							scope.plFilesModel = $res.upload;
						})
					}
					

					if(attrs.onFileUploaded) {
						scope.$parent.$apply(attrs.onFileUploaded);
					}
				});

				uploader.bind('UploadProgress',function( up,file){

					if(!attrs.plProgressModel){
						// return;
					}
					
					if(attrs.plFilesModel){
						scope.$apply(function() {
							angular.forEach(scope.plFilesModel, function(upload,key) {
                if( upload._id == file.id) {
                  upload.percent = file.percent;
                }
							});

							// scope.plProgressModel = scope.sum/scope.plFilesModel.length;
						});
					} else {
						scope.$apply(function() {
							scope.plProgressModel = file.percent;
						});
					}


					if(attrs.onFileProgress){
						scope.$parent.$eval(attrs.onFileProgress);
					}
				});

				if(attrs.plInstance){
					scope.plInstance = uploader;	
				}
			}
		};
	}])




/**
 * Cropper image
 * @param  {[type]} $http [description]
 * @return {[type]}       [description]
 */
  .directive( 'imCropper', [ '$http', '$rootScope', function( $http, $rootScope) {
    return {
      require: 'ngModel',
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs, ngModel){
      	scope.$watch( 'data.current', function(){
      		if( scope.data.current && attrs.src) {
      			if( $(element).data( 'cropper')) {      				
      				$(element).cropper( 'destroy');
      			}

      			$(element).cropper({
              build: function(){
                var width = element.width();
                var height = element.height();
                var maxHeight = (angular.element( window).height() - 100) - angular.element( '.upload-dialog .header-content').height() - angular.element( '.upload-dialog .nav-tabs').height();
                element.height( maxHeight);
                element.width( (maxHeight * width) / height);
              },
              crop: function(data) {
                ngModel.$setViewValue( data);
              },
		          done: function(data) {
		            scope.$apply( function(){
		              ngModel.$setViewValue( data);
		            })
		          },
		          aspectRatio: scope.data.current.width / scope.data.current.height,
		          preview: attrs.preview,
              mouseWheelZoom: false,
              background: false
		        });
      		}
      	});

      }
    }
  }])

  .directive( 'imGet', [ '$http', '$timeout', function( $http, $timeout) {
  	return {
  		restrict: 'A',
  		scope: '=',
  		controller: function( $rootScope, $scope){
  			$scope.setCurrent = function( size){
  				$scope.data.current = eval( '$scope.data.configs.' + size);
  			}
  		},
  		link: function( scope, element, attrs){
  			element.click(function(){
  				scope.setCurrent( attrs.imGet)
  			})
  		}
  	}
  }])

  
;
