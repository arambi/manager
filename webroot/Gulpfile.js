var gulp = require( 'gulp'),
    uglify = require('gulp-uglifyjs'),
    concat = require('gulp-concat'),
    minifyCSS = require('gulp-minify-css'),
    rename    = require('gulp-rename');

gulp.task('css', function () {
    gulp.src([
        'css/bootstrap.min.css',
        'css/fullcalendar.css',
        'css/jquery.steps.css',
        'css/jquery.fancybox.css',
        'css/dataTables.bootstrap.css',
        'css/chosen.css',
        'css/basic.css',
        'css/dropzone.css',
        'css/switchery.css',
        'css/jquery.nouislider.css',
        'css/angular-datapicker.css',
        'css/ion.rangeSlider.css',
        'css/ion.rangeSlider.skinFlat.css',
        'css/animate.css',
        'css/style.css',
        'css/angular-ui-tree.min.css',
        'css/ng-tags-input.css',
        'css/isteven-multi-select.css',
        'css/cropper.css',
  ])
  .pipe(gulp.dest('css'))
  .pipe(concat('application.css'))
  .pipe(gulp.dest('./css'))
  .pipe(minifyCSS())
  .pipe(rename('application.min.css'))
  .pipe(gulp.dest('./css'));
});

gulp.task( 'library', function() {
  return gulp.src([
    // jQuery and Bootstrap 
      'js/jquery/jquery-2.1.1.min.js',
      'js/plugins/jquery-ui/jquery-ui.js',
      'js/bootstrap/bootstrap.min.js',

      // Plupload
      'js/plupload/moxie.min.js',
      'js/plupload/plupload.min.js',
      // MetsiMenu
      'js/plugins/metisMenu/jquery.metisMenu.js',

      // SlimScroll
      'js/plugins/slimscroll/jquery.slimscroll.min.js',

      // Flot 
      // 'js/plugins/flot/jquery.flot.js',
      // 'js/plugins/flot/jquery.flot.tooltip.min.js',
      // 'js/plugins/flot/jquery.flot.spline.js',
      // 'js/plugins/flot/jquery.flot.resize.js',
      // 'js/plugins/flot/jquery.flot.pie.js',
      // 'js/plugins/flot/curvedLines.js',

      //Peity 
      'js/plugins/peity/jquery.peity.min.js',

      // Morris 
      // 'js/plugins/morris/raphael-2.1.0.min.js',
      // 'js/plugins/morris/morris.js',

      // iCheck 
      'js/plugins/iCheck/icheck.min.js',

      //Chosen
      'js/plugins/chosen/chosen.jquery.js',

      // Peace JS 
      'js/plugins/pace/pace.min.js',

      // Fancy box 
      'js/plugins/fancybox/jquery.fancybox.js',

      // Rickshaw
      'js/plugins/rickshaw/vendor/d3.v3.js',
      'js/plugins/rickshaw/rickshaw.min.js',

      // Ion Range Slider
      'js/plugins/ionRangeSlider/ion.rangeSlider.min.js',

      // NouSlider 
      'js/plugins/nouslider/jquery.nouislider.min.js',

      // Jvectormap 
      // 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
      // 'js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',

      // Input Mask
      // 'js/plugins/jasny/jasny-bootstrap.min.js',

      // Switchery
      'js/plugins/switchery/switchery.js',

      // Data Tables
      'js/plugins/dataTables/jquery.dataTables.js',
      'js/plugins/dataTables/dataTables.bootstrap.js',

      // EasyPIE 
      // 'js/plugins/easypiechart/easypiechart.js',

      // Sparkline 
      // 'js/plugins/sparkline/jquery.sparkline.min.js',

      // Dropzone 
      // 'js/plugins/dropzone/dropzone.js',

      // Cahrt JS
      // 'js/plugins/chartJs/Chart.min.js',

      // Knob 
      'js/plugins/jsKnob/jquery.knob.js',

      // SUMMERNOTE 
      'js/plugins/summernote/summernote.min.js',

      // Full Calendar 
      'js/plugins/fullcalendar/fullcalendar.min.js',

      // Custom and plugin javascript
      // 'js/inspinia.js',

      'js/cropper/cropper.js',

  ])
    .pipe(uglify('application.js', {
      mangle: false,
      output: {
        beautify: false
      }
    }))
    .pipe(gulp.dest('js'))
});

gulp.task( 'angulars', function() {
  return gulp.src([
      // Angular scripts
      // 'js/angular/angular.min.js',
      'js/angular/angular.1.3.14.min.js',
      'js/angular/angular-route.min.js',
      // 'js/bootstrap/ui-bootstrap-tpls-0.11.0.min.js',
      'js/bootstrap/ui-bootstrap-tpls-0.12.1.js',
      'js/angular/checklist-model.js',
      // 'js/angular/plupload-angular.js',
      
      'js/angular/angular-ui-tree.min.js',
      'js/angular/ui-load.js',
      'js/angular/ocLazyLoad.min.js',
      'js/angular/sortable.js',
      'js/angular/isteven-multi-select.js',
      'js/angular/ng-tags-input.min.js',


      // Angular Dependiences 
      'js/plugins/peity/angular-peity.js',
      // 'js/plugins/easypiechart/angular.easypiechart.js',
      'js/plugins/flot/angular-flot.js',
      'js/plugins/rickshaw/angular-rickshaw.js',
      'js/plugins/summernote/angular-summernote.min.js',  
      'js/bootstrap/angular-bootstrap-checkbox.js',
      'js/plugins/jsKnob/angular-knob.js',
      'js/plugins/switchery/ng-switchery.js',
      'js/plugins/nouslider/angular-nouislider.js',
      'js/plugins/datapicker/datePicker.js',
      'js/plugins/chosen/chosen.js',
      'js/plugins/dataTables/angular-datatables.min.js',
      'js/plugins/fullcalendar/gcal.js',
      'js/plugins/fullcalendar/calendar.js',

      // 'js/plugins/chartJs/angles.js',
  ])
    .pipe(uglify('angulars.js', {
      mangle: false,
      output: {
        beautify: false
      }
    }))
    .pipe(gulp.dest('js'))
});

gulp.task( 'default', [  'library', 'angulars', 'css']);