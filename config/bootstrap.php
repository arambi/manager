<?php
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\Core\Plugin;
use User\Auth\Access;
use Cake\Core\Configure;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\Routing\DispatcherFactory;
use Manager\Routing\Filter\ManagerBarFilter;

// Date::setJsonEncodeFormat('yyyy-MM-dd');
// Time::setJsonEncodeFormat('yyyy-MM-dd HH:mm:ss');

// require_once ROOT . DS . 'plugins' .DS. 'Cofree' .DS. 'src' .DS. 'Lib' .DS. 'basics.php';
require_once Plugin::path( 'Cofree') . 'src' .DS. 'Lib' .DS. 'basics.php';


if( file_exists( ROOT .DS. 'config' .DS. 'manager.php'))
{
  Configure::load( 'manager');
}

// Plugin::load( 'User', ['bootstrap' => true, 'routes' => true, 'autoload' => true]);

Access::add( 'admin', [
  'name' => 'Entrada al administrador',
    'options' => [
      'view' => [
          'name' => 'Acceso',
          'nodes' => [
            [
              'prefix' => 'admin',
              'plugin' => 'Manager',
              'controller' => 'Pages',
              'action' => 'home',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'User',
              'controller' => 'Users',
              'action' => 'logout',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'User',
              'controller' => 'Users',
              'action' => 'data',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Manager',
              'controller' => 'Pages',
              'action' => 'bar',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Manager',
              'controller' => 'Pages',
              'action' => 'dashboard',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Manager',
              'controller' => 'Pages',
              'action' => 'nav',
            ],
          ]
      ]
    ]
]);

DispatcherFactory::add( new ManagerBarFilter(['priority' => 1]));
