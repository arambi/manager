<?php

use Phinx\Migration\AbstractMigration;

class Drafter extends AbstractMigration
{
  public function up()
  {
    $drafts = $this->table( 'drafts');
    $drafts
      ->addColumn( 'content_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'foreign_key', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'model', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['content_id', 'foreign_key', 'model'], ['unique' => true])
      ->addIndex( ['foreign_key'])
      ->addIndex( ['model'])
      ->save();
  }

  public function down()
  {
    $this->dropTable( 'drafts');
  }
}
