<?php

use Migrations\AbstractMigration;

class IsDraft extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('sections');

        if (!$table->hasColumn('is_draft')) {
            $table
                ->renameColumn('draft', 'is_draft')
                ->update();
        }

        $table = $this->table('contents');
        if (!$table->hasColumn('is_draft')) {
            $table
                ->renameColumn('draft', 'is_draft')
                ->update();
        }
    }
}
