<?php
use Cake\Routing\Router;
use Cake\Core\Plugin;
use Cake\Utility\Inflector;

Router::plugin('Manager', function($routes) {
	$routes->fallbacks();
});

Router::prefix( 'admin', function( $routes) {

  // Recorre todos los plugins para crear las rutas
  foreach( Plugin::loaded() as $plugin)
  {
    $routes->connect( '/'. Inflector::underscore( $plugin) .'/:controller', ['action' => 'index', 'plugin' => $plugin], ['routeClass' => 'InflectedRoute']);
    $routes->connect( '/'. Inflector::underscore( $plugin) .'/:controller/:action', ['plugin' => $plugin], ['routeClass' => 'InflectedRoute']);
    $routes->connect( '/'. Inflector::underscore( $plugin) .'/:controller/:action/*', ['plugin' => $plugin], ['routeClass' => 'InflectedRoute']);
  }
  
  $routes->connect( '/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
  $routes->connect( '/:controller/:action', [], ['routeClass' => 'InflectedRoute']);
  $routes->connect( '/:controller/:action/*', [], ['routeClass' => 'InflectedRoute']);
  $routes->routeClass( 'InflectedRoute');
  $routes->fallbacks();
});

Router::connect( '/admin', [
  'prefix' => 'admin',
  'plugin' => 'Manager',
  'controller' => 'Pages',
  'action' => 'home'
], ['routeClass' => 'Route']);

Router::connect( '/admin/manager/pages/base', [
  'plugin' => 'Manager',
  'controller' => 'Templates',
  'action' => 'base'
], ['routeClass' => 'Route']);

Router::connect( '/admin/manager/pages/modal', [
  'plugin' => 'Manager',
  'controller' => 'Templates',
  'action' => 'modal'
], ['routeClass' => 'Route']);

Router::connect( '/admin/manager/pages/error', [
  'plugin' => 'Manager',
  'controller' => 'Templates',
  'action' => 'error'
], ['routeClass' => 'Route']);

Router::connect( '/admin/manager/pages/dashboard', [
  'prefix' => 'admin',
  'plugin' => 'Manager',
  'controller' => 'Pages',
  'action' => 'dashboard'
]);

Router::connect( '/admin/bar', [
  'prefix' => 'admin',
  'plugin' => 'Manager',
  'controller' => 'Pages',
  'action' => 'bar'
], ['routeClass' => 'Route']);

Router::connect( '/manager/tutorials', [
  'plugin' => 'Manager',
  'controller' => 'Tutorials',
  'action' => 'index'
], ['routeClass' => 'Route']);
Router::connect( '/manager/tutorials/:video', [
  'plugin' => 'Manager',
  'controller' => 'Tutorials',
  'action' => 'view',
], ['routeClass' => 'Route']);

