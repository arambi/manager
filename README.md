# Configuración del CRUD

## Controller

Se debe añadir un controlador con el prefijo Admin, preferiblemente usando la consola con:

`bin/cake Bake.bake controller Plugin.Controller --prefix Admin`

En este controller es necesario añadir al inicio del fichero el Trait `CrudControllerTrait`:

`use Manager\Controller\CrudControllerTrait;`

y dentro de la class

`use CrudControllerTrait;`

## Model

Se debe crear el model con la consola, usando:

`bin/cake Bake.bake model Plugin.Model`

poniendo el nombre del model en plural. De esta manera se creará tanto la Table como la Entity.

En el fichero de la Table debemos añadir lo indicado en CrudConfig (ver más adelante)

En el fichero de la Entity debemos añadir el CrudEntityTrait, de esta manera:

Añadiendo al principio del fichero la siguiente linea:

`use Manager\Model\Entity\CrudEntityTrait;`

Dentro de la class:

`use CrudEntityTrait;`


## Events
* Se produce un evento cada vez que una Table es inicializada desde el CrudableBehavior. Así pues se puede modificar cada model llamando al siguiente evento
`Manager.Behavior.Crudable.TableName.initialize`

* Se produce un evento antes de que se genere una vista para la edición, pudiendo modificar tanto la table como el crud
`Manager.View.TableName.beforeBuild`

* `Crud.afterSetEmptyProperties`
Lanzado cuando se rellenan las propiedades de un contenido, al crear uno uno, cuando se setean a la vista


## CrudConfig

Al instanciar CrudConfig en un model (Table), se genera un objeto que contendrá todas las propiedades de la edición del Model en un sistema de AngularJS

El objeto se instancia automáticamente al inyectar Manager.CrudableBehavior a un model


* Para hacerlo, dentro de Table:
```php
public function initialize(array $config)
{
  $this->addBehavior( 'Manager.Crudable');
}
```

Una vez hecho esto, podemos acceder al objeto desde el Table, con $this->crud

Si se quiere que el model tenga presente las asociaciones dentro de la edición de los contenidos, es necesario indicarlo mediante este método:

`$this->crud->associations( ['Cards', 'Blocks'])`

Donde el parámetro pasado es un array con todas las asociaciones que se van a usar, independientemente de los niveles que éstas tengan.


Dentro de la propia función de initialize usamos los siguientes métodos para configurar el objeto

### Para añadir campos:
Los campos se añaden de manera permanente para ser usados en las vistas. De eso se habla más adelante.

```php
$this->crud
  ->addFields([
    'title' => [
      'label' => 'Título'
    ],
    'body' => [
      'label' => 'Cuerpo'
    ],
    'photo' => [
      'type' => 'upload',
      'label' => 'Foto',
      'config' => [
        'type' => 'post',
        'size' => 'thm'
      ]
    ]
  ])
```

Cada campo tendrá las opciones por defecto del Schema en la base de datos y además estas otras:

* `type`: determina el tipo de campo. Por defecto es el type marcado en schema
* `label`: el nombre humano del label para el campo. Por defecto es el nombre del campo, generalmente en inglés.
* `options`: para los casos de tipos de campo con opciones (p.e. select o checkbox), el array de opciones
* `adapter`: el nombre clave de la configuración del FieldAdapter
* `adapterConfig`: valores que modificarán la configuración del FieldAdapter
* `template`: la ruta hacia el element (separada por puntos). p.e. `Block.fields.blocks` para un fichero que esté en plugins/Block/Template/Element/crud/fields/blocks.ctp. Por defecto tomará el valor de type e irá a la ruta de plugins/Manager/Template/Element/crud/fields/
* `show`: String que será "true" o "false" y que puede ser una expresión JS con datos del scope de AngularJS. Sirve para mostrar o no el campo dependiendo de los valores de otros campos.
* `config`: string o array que puede ser usada en el element. 


También se puede añadir un campo con `key => value`, donde `value` sería el `label`
```php
$this->crud
  ->addFields([
    'fields' => [
      'title' => 'Título',
      'body' => 'Cuerpo'
    ]
  ])
```

### Tipos de campo especiales

#### belongsTo
Esta opción no es necesaria indicarla en el key `type`. Se detecta automáticamente cuando el nombre del campo es el nombre de la propiedad. Por ejemplo para una relación belongsTo con el model Categories, el key debería ser `category`

#### hasMany
Se usa esta opción para mostrar el listado de contenidos asociados mediante la asociación hasMany indicada en el model.
El nombre del campo debe ser el nombre de la propiedad, es decir, para una asociación de `Cards` se usará `cards`

#### select_multiple
Se usa esta opción para relaciones belongsToMany. En la vista se mostrará un select con la posibilidad de seleccionar uno o más elementos.
El nombre del campo debe ser el nombre de la propiedad, es decir, para una asociación de `Categories` se usará `categories`

#### tags
Se usa esta opción para relaciones belongsToMany de tipo tags. En la vista se un input para poder escribir las tags.
El nombre del campo debe ser el nombre de la propiedad, es decir, para una asociación de `Categories` se usará `categories`



### Para añadir un index
Las vistas creadas serán accesibles desde `/admin/#/admin/plugin/controller/vista`

```php
$this->crud
  ->addIndex( 'index', [
    'fields' => [
      'title',
      'created' => [
        'label' => 'Creado'
      ]
    ]
  ])
```

Este método admite un primer parámetro que indica el nombre de la vista, que tendrá que coincidir con el nombre del método y del template. El segundo parámetro es un array con las siguientes opciones:

* `fields`: Un array con los campos que contendrá el index, cuyas opciones serán las mimas que las definidas con `addFields`. Si se pasa un array asociativo el valor de cada elemento será el label y el key será el nombre del campo.
* `saveButton`: Un booleano que si es true, hará que aparezca el botón de guardar. Por defecto es `false`.
* `actionButtons`: Botones de acciones posibles dentro de la vista (añadir, listado...). Es un array de configuración con los siguientes keys: `url`, que indica la url (si solamente indicamos un string, éste será el parámetro action de la URL), `label`: El nombre del botón: 


### Para añadir una vista

Las vistas creadas serán accesibles desde /admin/#/plugin/controller/vista

```php
$this->crud
  ->addView( 'create', [
    'columns' => [
      [
        'cols' => 8,
        'box' => [
          [
            'title' => 'Creación',
            'elements' => [
              'title',
              'body'
            ]
          ]
        ],
      ]
    ],
    'saveButton' => true,
    'actionButtons' => [
      [
        'url' => 'add',
        'label' => __d( 'admin', 'Nuevo')
      ]
    ]
  ])
```

Este método admite un primer parámetro que indica el nombre de la vista, que tendrá que coincidir con el nombre del método y del template. El segundo parámetro es un array de arrays, donde cada array es un conjunto de campos. Cada array tiene las siguientes opciones:

* `columns`: Array con los diferentes tabs que habrá en la vista
* `saveButton`: Un booleano que si es true, hará que aparezca el botón de guardar. Por defecto es `true`.
* `actionButtons`: Botones de acciones posibles dentro de la vista (añadir, listado...). Es un array de configuración con los siguientes keys: `url`, que indica la url (si solamente indicamos un string, éste será el parámetro action de la URL), `label`: El nombre del botón: 

Cada columna tendrá los siguientes parámetros:

* `title`: El título de la columna
* `cols`: El número de columnas que ocupa el grupo de campos
* `box`: Array con los distintos bloques de elementos

Cada bloque de elementos tendrá los siguientes parámetros:

* `title`: El título del bloque
* `elements`: Array con las keys de los campos que previamente tendrán que estar definidos con `addFields`.


El método admite un tercer parámetro que será un array con el nombre de las vistas que tendrán la misma configuración que la definida.

### Para setear el nombre
```php
$this->crud
  ->setName( [
    'singular' => __d( 'admin', 'Noticias'),
    'plural' => __d( 'admin', 'Noticias'),
  ])
```

### Añadir ficheros JS a las vistas
Mediante este método se pueden añadir ficheros js para ser usado en todas las vistas del CRUD

```php
$this->crud->addJsFiles([
  '/shop/js/directives.js',
]);
```

## Uso de las variables en AngularJS
En las vistas es posible usar las variables seteadas desde crudConfig. Estas variables están seteadas en el objeto `scope.data` (disponibles en el HTML en `{{ data }}´).
Este objeto tiene las siguientes propiedades

* `content`: El contenido a editar, con todos las propiedades de la entidad, incluidas las asociaciones.
* `contents`: Listado de contenidos, disponible en los índices
* `languages`: array de objetos JSON de los idiomas 
* `currentLanguage`: Objeto JSON con el idioma actual
* `crudConfig`: objeto JSON claves que dan la información básica de la configuración CRUD (`name`, `view`, `jsFiles`, `model`, `plugin`, `adminUrl`)

Dentro del JSON `crudConfig` nos encontramos con las siguientes claves:

* `name`: El nombre del contenido (Noticias, Proyectos...)
* `view`: El objeto JSON de la vista que contiene la información de las pestañas y los campos a editar
* `jsFiles`: array con los ficheros JS extra
* `model`: El nombre del model
* `plugin`: El nombre del plugin
* `adminUrl`: La url del controller (sin los actions)

Dentro del objeto `view` nos encontramos con las siguientes claves:

* `template`: El nombre del template (ctp) a renderizar (tipo Plugin/name)
* `submit`: Contiene la clave `url` que indica la URL a donde se envia el formulario
* `columns`: array con las pestañas que tendrá la vista
* `saveButton': Indica si aparece o no el botón de guardar
* `title`: El título de la vista (Nuevo, Listado...)

El objeto `columns` es un array con las pestañas que tiene una vista (por lo general solo tendrá una). Este objeto tiene dos claves: `cols` que indica el número de columnas que ocupa en la vista y `box` que es un array con los bloques de campos que tiene la columna. Cada objeto `box` contiene una dos claves, una `name` con el nombre del bloque (si lo hubiese) y otra clave `elements` que es un array con los campos que se mostrarán en la vista.

Cada objeto element contiene las siguientes claves:

* `type`: El tipo de campo
* `translate`: boolean que indica si es un campo con traducción o no
* `adapter`: la clave de FieldAdapter
* `show`: boolean que indica si se muestra el campo o no.
* `help`: el texto de ayuda
* `hasOne`: indica si el campo pertenece a un model hasOne
* `label`: el texto del label
* `key`: el nombre de la columna (o propiedad del objeto Entity)
* `template`: el nombre del template de tipo Plugin/fields/nombre_campo
* `templateIndex`: el nombre del template para el campo de índice, de tipo Plugin/indexes/nombre_campo
* `error`: el texto de error si lo hubiese y si no, false'

## Setear variables desde cualquier parte
Desde cualquier parte del código (Tables, Controllers, Behaviors, Components) se puede setear contenido que irá situado en el objeto `crudConfig`.
Esto se hace de la siguiente manera:

```php
$Table->crud->addConfig([
  'key_to_set' => 'value_to_set'
]);
```

En el scope de AngularJS de la vista, podremos acceder a `scope.crudConfig.key_to_set`.




## Access

Se debe setear en el fichero access.php situado en config los nuevos nodos a los que dar acceso
```php
$config ['Access'] = [
  'languages' => [
    'name' => 'Entrada al administrador',
    'options' => [
      'view' => [
          'name' => 'Acceso',
          'nodes' => [
            [
              'prefix' => 'admin',
              'plugin' => 'Manager',
              'controller' => 'Pages',
              'action' => 'home',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Manager',
              'controller' => 'users',
              'action' => 'index',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Manager',
              'controller' => 'users',
              'action' => 'update',
            ]
          ]
      ]
    ]
  ]
];

```
