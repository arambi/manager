
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import del from 'del';
import gulp from 'gulp';
import uglify from 'gulp-uglify';
import browserSync from 'browser-sync';
import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import postcssCopy from 'postcss-copy';
import cleanCSS from 'gulp-clean-css';
import sourcemaps from 'gulp-sourcemaps';
import bulkSass from 'gulp-sass-bulk-import';
import postcssPresetEnv from "postcss-preset-env";

const server = browserSync.create();

const clean = () => del(['dist']);

const sourcesCSS = [
  './src/sass/styles.scss',
];

function reload(done) {
  server.reload();
  done();
}

const processors = [
  postcssPresetEnv()
];


function sassWatch(){
  return gulp.src( sourcesCSS)
    .pipe( sourcemaps.init())
    .pipe( bulkSass())
    .pipe( sass().on( 'error', sass.logError))
    .pipe( concat( 'application.css'))
    .pipe( postcss( processors))
    .pipe( cleanCSS())
    .pipe( sourcemaps.write( 'maps'))
    .pipe( gulp.dest( './src/assets'))
    ;
}

function serve(done) {
  server.init({
      proxy: "http://localhost:4200",
      open: false,
      files: ['./src/assets/*.css'],
      port: 3222
  });
  done();
}

const watchCss = () => gulp.watch( './src/sass/**/*.scss', gulp.series( sassWatch));
const dev = gulp.series( clean, sassWatch, serve, watchCss);
export default dev;

