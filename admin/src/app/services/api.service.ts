import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  url( url: string) {
    return environment.endPoint + url;
  }

  get$( url): Observable<any> {
    return this.http.get( this.url( url + '.json'));
  }

  update$(): Observable<any> {
    return this.http.get( '/assets/responses/update.json');
  }

  index$(): Observable<any> {
    return this.http.get( this.url( '/admin/section/sections/index.json'), {
      withCredentials: true
    });
  }

  login$( data: any): Observable<any> {
    return this.http.post( this.url( '/admin/user/users/token.json'), data);
  }

  nav$(): Observable<any> {
    return this.http.get( this.url( '/admin/manager/pages/nav.json'));
  }
}
