import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';


@Injectable({
  providedIn: 'root'
})
export class RequestInterceptorService implements HttpInterceptor {

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  public intercept(
      req: HttpRequest<any>,
      next: HttpHandler
    ): Observable<HttpEvent<any>> {
      const successCallback = this.interceptResponse.bind(this);
      const errorCallback = this.catchError.bind(this);
      const interceptionOperator = tap<HttpEvent<any>>(
        successCallback,
        errorCallback
      );
      
      const token = localStorage.getItem( 'token');

      if( token) {
        const cloned = req.clone({
          headers: req.headers.set( 'Authorization',
              'Bearer ' + token)
        });

        return next.handle(cloned).pipe( catchError((error, caught) => {
          console.log(error);
          this.catchError(error);
          return of(error);
        }) as any);

        // return next.handle( cloned);
      } else {
        const handledRequest = next.handle(req);
        return handledRequest.pipe(interceptionOperator);
      }
  }

  private interceptResponse(event: HttpEvent<any>) {
    if (event instanceof HttpResponse) {
    }
  }

  private catchError(err) {   
    if (err instanceof HttpErrorResponse) {
      if (err.status === 401) {
        this.navigateToLogin();
      } else if (err.status === 403) {
        this.navigateTo403();
      } else {
        console.warn(err.statusText);
      }
      this.spinner.hide();
      this.catchHttpError(err);
    } else {
    }
  }

  private catchHttpError(err: HttpErrorResponse) {
    this.spinner.hide();
    if (err.status === 401 || err.error.message === 'Expired token') {
      this.navigateToLogin();
    } else if (err.status === 403) {
      this.navigateTo403();
    } else if (err.status === 404) {
      console.warn(err.statusText);
      // Swal( 'Página no encontrada', 'La página a la que intentas acceder no existe o ha sido borrada', 'error');
    } else {
      console.warn(err.statusText);
      // Swal( 'Ooops', 'Ocurrió un error en la petición', 'error');
    }
  }

  private navigateToLogin() {
    this.router.navigateByUrl( '/login');
  }
  
  private navigateTo403() {
    console.log('403');
    this.router.navigateByUrl( '/page403');
  }
  
}
