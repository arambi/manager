import { ApiService } from '../services/api.service';
import { RetriveViewService } from './../plugins/manager/services/retrive.view.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ViewDirective } from '../plugins/manager/directives/view.directive';
import { Views } from 'src/app/providers/providers.views';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  
  @ViewChild(ViewDirective, {static: true})
  view: ViewDirective;

  constructor(
    private retrive: RetriveViewService,
    private api: ApiService
  ) { }

  ngOnInit() {
    this.loadView();
  }

  loadView() {
    
  }

}
