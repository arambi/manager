import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
})
export class NavComponent implements OnInit {

  nav: any;

  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
    this.api.nav$().subscribe( result => {
      this.nav = result.nav;
    });
  }

}
