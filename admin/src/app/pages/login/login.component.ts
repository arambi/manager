import { ApiService } from './../../services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  form: FormGroup = new FormGroup({
    email: new FormControl(),
    password: new FormControl()
  });

  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
  }

  submit() {
    this.api.login$( this.form.value).subscribe( response => {
      console.log( response);
      if( response.success) {
        localStorage.setItem( 'token', response.token);
      }
    });
  }

}
