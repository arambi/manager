import { ApiService } from './../../services/api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Event, NavigationStart, RoutesRecognized, 
  RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd, 
  NavigationCancel, NavigationError } from '@angular/router';
import { RetriveViewService } from 'src/app/plugins/manager/services/retrive.view.service';
import { ViewDirective } from 'src/app/plugins/manager/directives/view.directive';
import { Views } from 'src/app/providers/providers.views';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';
import { ViewInterfaceComponent } from 'src/app/modules/layout/view.interface.component';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dispatcher',
  templateUrl: './dispatcher.component.html',
})
export class DispatcherComponent implements OnInit {

  @ViewChild(ViewDirective, {static: true})
  view: ViewDirective;
  
  constructor(
    private router: Router,
    private api: ApiService,
    private retrive: RetriveViewService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.router.events.subscribe( (event: Event) => {
      if (event instanceof NavigationStart) {
        // Navigation started.
      } else if (event instanceof RoutesRecognized) { 
          // Router parses the URL and the routes are recognized.
      } else if (event instanceof RouteConfigLoadStart) {
        // Before the Router lazyloads a route configuration.
      } else if (event instanceof RouteConfigLoadEnd) { 
        // Route has been lazy loaded.
      } else if (event instanceof NavigationEnd) {
          // Navigation Ended Successfully.
        this.load();
      } else if (event instanceof NavigationCancel) { 
          // Navigation is canceled as the Route-Guard returned false during navigation.
      } else if (event instanceof NavigationError) {
        // Navigation fails due to an unexpected error.
          console.log(event.error);
      }
    });

    this.load();
  }

  load() {
    this.spinner.show();
    this.api.get$( this.router.url).subscribe( result => {
      this.view.viewContainerRef.clear();
      const viewName = result.crudConfig.view.template;
      const view = Views.getView( viewName);
      const componentRef = this.retrive.createComponent( view, this.view.viewContainerRef);
      (componentRef.instance as ViewInterfaceComponent).data = result;
      this.spinner.hide();
    });
  }

}
