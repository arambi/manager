import { Component, OnInit } from '@angular/core';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';
import { FieldComponent } from 'src/app/plugins/manager/fields/field.component';

@Component({
  selector: 'app-blocks',
  templateUrl: './blocks.component.html',
})
export class BlocksComponent extends FieldComponent implements OnInit, FieldInterfaceComponent {

  constructor() { 
    super();
  }

  ngOnInit() {
  }

}
