import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlocksComponent } from './fields/blocks/blocks.component';

@NgModule({
  declarations: [
    BlocksComponent
  ],
  imports: [
    CommonModule
  ],
  entryComponents: [
    BlocksComponent
  ]
})
export class BlockModule { }
