import { BlocksComponent } from './fields/blocks/blocks.component';
import { Type } from '@angular/core';
import { FieldCollection } from '../manager/collection/field.collection';

export class BlockFieldCollection implements FieldCollection  {
  fields: Type<any>[] = [
    BlocksComponent
  ];

  indexesFields: string[] = [
    'blocks',
  ];

  views: Type<any>[] = [
  ];

  indexesViews: string[] = [
  ];
}
