import { Type } from '@angular/core';
import { ViewCollection } from '../manager/collection/view.collection';

export class BlockViewCollection implements ViewCollection  {

  views: Type<any>[] = [
  ];

  indexesViews: string[] = [
  ];
}
