import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './fields/product/product.component';



@NgModule({
  declarations: [
    ProductComponent
  ],
  imports: [
    CommonModule
  ],
  entryComponents: [
    ProductComponent
  ]
})
export class LacunzaModule { }
