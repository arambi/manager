import { Component, OnInit } from '@angular/core';
import { FieldComponent } from 'src/app/plugins/manager/fields/field.component';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
})
export class ProductComponent extends FieldComponent implements OnInit, FieldInterfaceComponent {
  
  constructor() { 
    super();
  }

  ngOnInit() {
  }

}
