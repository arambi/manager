import { Type } from '@angular/core';
import { ProductComponent } from './fields/product/product.component';
import { FieldCollection } from '../manager/collection/field.collection';

export class LacunzaFieldCollection implements FieldCollection  {
  fields: Type<any>[] = [
    ProductComponent
  ];

  indexesFields: string[] = [
    'product',
  ];

}
