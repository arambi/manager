import { Type } from '@angular/core';
import { ViewCollection } from '../manager/collection/view.collection';

export class LacunzaViewCollection implements ViewCollection  {

  views: Type<any>[] = [
  ];

  indexesViews: string[] = [
  ];

}
