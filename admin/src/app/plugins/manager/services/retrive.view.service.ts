import { Injectable, Type, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RetriveViewService {

  fields: Type<any>[] = [];
  indexesFields: string[] = [];

  views: Type<any>[] = [];
  indexesViews: string[] = [];

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
  ) { 
    
  }

  createComponent( component, viewRef: ViewContainerRef) {
    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory( component);
    
    return viewRef.createComponent( componentFactory);
    
  } 


}
