import { DesignModule } from './../../modules/design/design.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StringComponent } from './fields/string/string.component';
import { SelectComponent } from './fields/select/select.component';
import { HttpClientModule } from '@angular/common/http';
import { UpdateComponent } from './views/update/update.component';
import { FieldsDirective } from './directives/fields.directive';
import { IndexComponent } from './views/update/index.component';
import { IndexStringComponent } from './fields/index-string/index-string.component';
import { IndexBooleanComponent } from './fields/index-boolean/index-boolean.component';
import { TextComponent } from './fields/text/text.component';
import { CkeditorComponent } from './fields/ckeditor/ckeditor.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { BooleanComponent } from './fields/boolean/boolean.component';
import { CheckboxesComponent } from './fields/checkboxes/checkboxes.component';

@NgModule({
  declarations: [
    StringComponent, 
    SelectComponent, 
    UpdateComponent,
    IndexComponent,
    FieldsDirective,
    IndexStringComponent,
    IndexBooleanComponent,
    TextComponent,
    CkeditorComponent,
    BooleanComponent,
    CheckboxesComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    DesignModule,
    CKEditorModule
  ],
  entryComponents: [
    StringComponent, 
    SelectComponent,
    UpdateComponent,
    IndexComponent,
    IndexStringComponent,
    IndexBooleanComponent,
    TextComponent,
    CkeditorComponent,
    BooleanComponent,
    CheckboxesComponent,
  ]
})
export class ManagerModule { }
