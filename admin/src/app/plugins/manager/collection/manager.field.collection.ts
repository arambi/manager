
import { Type } from '@angular/core';
import { FieldCollection } from './field.collection';
import { StringComponent } from '../fields/string/string.component';
import { SelectComponent } from '../fields/select/select.component';
import { IndexStringComponent } from '../fields/index-string/index-string.component';
import { IndexBooleanComponent } from '../fields/index-boolean/index-boolean.component';
import { TextComponent } from '../fields/text/text.component';
import { CkeditorComponent } from '../fields/ckeditor/ckeditor.component';
import { BooleanComponent } from '../fields/boolean/boolean.component';
import { CheckboxesComponent } from '../fields/checkboxes/checkboxes.component';

export class ManagerFieldCollection implements FieldCollection  {
  fields: Type<any>[] = [
    StringComponent,
    SelectComponent,
    TextComponent,
    BooleanComponent,
    CkeditorComponent,
    CheckboxesComponent,
    IndexStringComponent,
    IndexBooleanComponent,
  ];

  indexesFields: string[] = [
    'Manager/fields/string',
    'Manager/fields/select',
    'Manager/fields/text',
    'Manager/fields/boolean',
    'Manager/fields/ckeditor',
    'Manager/fields/checkboxes',
    'Manager/indexes/string',
    'Manager/indexes/boolean',
  ];
}
