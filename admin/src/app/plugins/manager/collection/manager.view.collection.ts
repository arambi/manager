import { CkeditorComponent } from './../fields/ckeditor/ckeditor.component';
import { TextComponent } from './../fields/text/text.component';
import { ViewCollection } from './view.collection';
import { Type } from '@angular/core';
import { UpdateComponent } from '../views/update/update.component';
import { IndexComponent } from '../views/update/index.component';

export class ManagerViewCollection implements ViewCollection  {

  views: Type<any>[] = [
    IndexComponent,
    UpdateComponent
  ];

  indexesViews: string[] = [
    'Manager/index',
    'Manager/update',
  ];
}
