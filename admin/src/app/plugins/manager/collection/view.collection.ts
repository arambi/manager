import { Type } from '@angular/core';

export interface ViewCollection {
  
  views: Type<any>[];
  indexesViews: string[];
}
