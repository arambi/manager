import { Type } from '@angular/core';

export interface FieldCollection {
  fields: Type<any>[];
  indexesFields: string[];
}
