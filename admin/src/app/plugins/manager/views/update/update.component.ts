import { ViewComponent } from '../../../../modules/layout/components/view.component';
import { Component, OnInit } from '@angular/core';
import { ViewInterfaceComponent } from 'src/app/modules/layout/view.interface.component';


@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
})
export class UpdateComponent extends ViewComponent implements OnInit, ViewInterfaceComponent {
  
  columns: any;

  ngOnInit() {
    this.columns = this.data.crudConfig.view.columns;
    this.createForm();
  }

  save() {
    console.log( this.form.value);
  }
}
