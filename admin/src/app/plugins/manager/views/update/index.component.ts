import { Field } from './../../models/field';
import { ViewComponent } from 'src/app/modules/layout/components/view.component';
import { Component, OnInit, Input } from '@angular/core';
import { RetriveComponentService } from '../../services/retrive.component.service';
import { ApiService } from '../../../../services/api.service';
import { ViewInterfaceComponent } from 'src/app/modules/layout/view.interface.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
})
export class IndexComponent extends ViewComponent implements OnInit, ViewInterfaceComponent {
  
  iterateFields: Field[] = [];
  
  ngOnInit() {
    const fields = this.data.crudConfig.view.fields;
    const keys = Object.keys( fields);

    for( let prop of keys) { 
      this.iterateFields.push( fields [prop]);
    }
  }

}
