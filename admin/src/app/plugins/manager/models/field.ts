export interface Field {
  translate: boolean;
  adapter: any;
  adapterConfig: any;
  show: string;
  help: string;
  hasOne: any;
  hasSearch: boolean;
  before: boolean;
  after: boolean;
  change: boolean;
  label: string;
  key: string;
  type: string;
  hasSort: boolean;
  template: string;
  error: any;
  options: any[];
  templateIndex?: string;
  templateSearch?: string;
}
