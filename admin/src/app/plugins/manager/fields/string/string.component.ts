import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { FieldComponent } from '../field.component';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';

@Component({
  selector: 'app-string',
  templateUrl: './string.component.html',
})
export class StringComponent extends FieldComponent implements OnInit, FieldInterfaceComponent {

  formGroup: FormGroup;

  ngOnInit() {
    if( this.field.hasOne !== false) {
      this.formGroup = this.form.get( this.field.hasOne) as FormGroup;
    } else {
      this.formGroup = this.form;
    }
  }

}
