import { Component, OnInit } from '@angular/core';
import { FieldComponent } from '../field.component';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';

@Component({
  selector: 'app-boolean',
  templateUrl: './boolean.component.html',
})
export class BooleanComponent extends FieldComponent implements OnInit, FieldInterfaceComponent {

  ngOnInit() {
  }

}
