import { Component, OnInit } from '@angular/core';
import { FieldComponent } from '../field.component';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
})
export class TextComponent extends FieldComponent implements OnInit, FieldInterfaceComponent {

  formGroup: FormGroup;

  ngOnInit() {
    if( this.field.hasOne !== false) {
      this.formGroup = this.form.get( this.field.hasOne) as FormGroup;
    } else {
      this.formGroup = this.form;
    }
  }

}
