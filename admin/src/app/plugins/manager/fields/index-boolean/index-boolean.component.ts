import { Component, OnInit } from '@angular/core';
import { FieldComponent } from '../field.component';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';

@Component({
  selector: 'app-index-boolean',
  templateUrl: './index-boolean.component.html',
})
export class IndexBooleanComponent extends FieldComponent implements OnInit, FieldInterfaceComponent {

  ngOnInit() {
  }

}
