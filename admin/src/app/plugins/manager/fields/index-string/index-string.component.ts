import { Component, OnInit } from '@angular/core';
import { FieldComponent } from '../field.component';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';

@Component({
  selector: 'app-index-string',
  templateUrl: './index-string.component.html',
})
export class IndexStringComponent extends FieldComponent implements OnInit, FieldInterfaceComponent {


  ngOnInit() {
  }

}
