import { Component, OnInit } from '@angular/core';
import { FieldComponent } from '../field.component';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import * as DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';

@Component({
  selector: 'app-ckeditor',
  templateUrl: './ckeditor.component.html',
})
export class CkeditorComponent extends FieldComponent implements OnInit, FieldInterfaceComponent {

  public Editor = ClassicEditor;

  ngOnInit() {
    
  }

}
