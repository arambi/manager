import { Component, OnInit } from '@angular/core';
import { FieldComponent } from '../field.component';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';

@Component({
  selector: 'app-checkboxes',
  templateUrl: './checkboxes.component.html',
})
export class CheckboxesComponent extends FieldComponent implements OnInit, FieldInterfaceComponent {

  ngOnInit() {
  }

  isChecked( option) {
    let isChecked = false;
    const value = this.form.get( this.field.key).value as any[];
    value.map( ( el, i) => {
      if( el.id === option.id) {
        isChecked = true;
      }
    });

    return isChecked;
  }

  changeValue( option: any, isChecked: boolean) {
    let value = this.form.get( this.field.key).value as any[];

    if( isChecked) {
      value.push( option);
    } else  {
      let index;

      this.field.options.map( ( el, i) => {
        if( el.id === option.id) {
          index = i;
        }
      });

      value.splice( index, 1);
    }

    this.form.get( this.field.key).setValue( value);
  }
}
