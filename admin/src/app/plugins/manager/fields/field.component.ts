import { Field } from './../models/field';
import { FormGroup } from '@angular/forms';
import { Input } from '@angular/core';

export class FieldComponent  {

  @Input()
  field: Field;

  @Input()
  content: any;

  @Input()
  form: FormGroup;

  @Input()
  key: string;

  @Input()
  data: any;
  
  constructor() { }

  changeLang() {
    let i = 0;
    let key: number;
    for( let lang of this.data.languages) {
      if( lang.id === this.data.currentLanguage.id) {
        key = i;
      }

      i++;
    }
    
    key++;

    if( (key + 1) > this.data.languages.length) {
      key = 0;
    }

    this.data.currentLanguage = this.data.languages [key];
  }

}
