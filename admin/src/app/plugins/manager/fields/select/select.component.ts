import { Component, OnInit } from '@angular/core';
import { FieldComponent } from '../field.component';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
})
export class SelectComponent extends FieldComponent implements OnInit, FieldInterfaceComponent {

  ngOnInit() {
  }

}
