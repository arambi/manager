import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[fields]',
})
export class FieldsDirective {
  constructor(
    public viewContainerRef: ViewContainerRef
  ) { }
}
