
import { Type } from '@angular/core';
import { FieldCollection } from '../plugins/manager/collection/field.collection';
import { ManagerFieldCollection } from '../plugins/manager/collection/manager.field.collection';
import { BlockFieldCollection } from '../plugins/block/block.field.collection';
import { LacunzaFieldCollection } from '../plugins/lacunza/lacunza.field.collection';


const providers: FieldCollection[] = [
  new ManagerFieldCollection(),
  new BlockFieldCollection(),
  new LacunzaFieldCollection(),
];


class ProvideFields {
  fields: Type<any>[] = [];
  indexesFields: string[] = [];
  
  constructor() {
    providers.forEach( collection => {
      this.indexesFields = this.indexesFields.concat( collection.indexesFields);
      this.fields = this.fields.concat( collection.fields);
    });
  }

  getField( key) {
    const index = this.indexesFields.indexOf( key);
    return this.fields [index];
  }

  hasField( key) {
    return this.indexesFields.indexOf( key) > -1;
  }
}

export const Fields = new ProvideFields();