import { BlockViewCollection } from '../plugins/block/block.view.collection';
import { ViewCollection } from '../plugins/manager/collection/view.collection';
import { ManagerViewCollection } from '../plugins/manager/collection/manager.view.collection';
import { LacunzaViewCollection } from '../plugins/lacunza/lacunza.view.collection';
import { Type } from '@angular/core';


const providers: ViewCollection[] = [
  new ManagerViewCollection(),
  new BlockViewCollection(),
  new LacunzaViewCollection(),
];

class ProvideViews {
  views: Type<any>[] = [];
  indexesViews: string[] = [];
  
  constructor() {
    providers.forEach( collection => {
      this.indexesViews = this.indexesViews.concat( collection.indexesViews);
      this.views = this.views.concat( collection.views);
    });
  }

  getView( key) {
    const index = this.indexesViews.indexOf( key);
    return this.views [index];
  }
}

export const Views = new ProvideViews();