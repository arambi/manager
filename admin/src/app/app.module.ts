import { ViewDirective } from './plugins/manager/directives/view.directive';
import { BlockModule } from './plugins/block/block.module';
import { ManagerModule } from './plugins/manager/manager.module';
import { ArticlesModule } from './articles/articles.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LacunzaModule } from './plugins/lacunza/lacunza.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './components/nav/nav.component';
import { ApiService } from './services/api.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptorService } from './services/request.interceptor.service';
import { LoginComponent } from './pages/login/login.component';
import { DesignModule } from './modules/design/design.module';
import { DispatcherComponent } from './pages/dispatcher/dispatcher.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ViewDirective,
    NavComponent,
    LoginComponent,
    DispatcherComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ManagerModule,
    BlockModule,
    LacunzaModule,
    BrowserAnimationsModule,
    DesignModule,
  ],
  providers: [
    ApiService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
