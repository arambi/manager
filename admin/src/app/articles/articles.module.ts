import { ArticleService } from './article.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent } from './post/post.component';
import { CategoryComponent } from './category/category.component';



@NgModule({
  declarations: [
    PostComponent,
    CategoryComponent
  ],
  entryComponents: [
    PostComponent,
    CategoryComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    ArticleService
  ]
})
export class ArticlesModule { }
