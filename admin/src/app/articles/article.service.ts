import { CategoryComponent } from './category/category.component';
import { Injectable, Type } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  comp: Type<any>[] = [
    CategoryComponent
  ];
  
  constructor() { }

  getComp( index) {
    return this.comp [index];
  }
}
