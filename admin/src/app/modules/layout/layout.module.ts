import { BoxComponent } from 'src/app/modules/layout/box.component';
import { ColumnsComponent } from 'src/app/modules/layout/columns.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldContainerComponent } from './components/field-container/field.container.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    FieldContainerComponent,
    ColumnsComponent,
    BoxComponent,
    HeaderComponent
  ],
  exports: [
    FieldContainerComponent,
    ColumnsComponent,
    BoxComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
  ]
})
export class LayoutModule { }
