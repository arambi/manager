import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FieldsDirective } from '../../plugins/manager/directives/fields.directive';
import { Field } from '../../plugins/manager/models/field';
import { Fields } from 'src/app/providers/providers.fields';
import { FieldInterfaceComponent } from './field.interface.component';
import { RetriveComponentService } from '../../plugins/manager/services/retrive.component.service';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
})
export class BoxComponent implements OnInit {
  
  @ViewChild(FieldsDirective, {static: true})
  fieldsDirective: FieldsDirective;
  
  @Input()
  key: string;

  @Input()
  title: string;

  @Input()
  elements: any[];

  @Input()
  form: FormGroup;

  @Input()
  data: any;

  ngOnInit() {
  }

}
