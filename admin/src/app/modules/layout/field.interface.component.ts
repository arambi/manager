import { Field } from './../../plugins/manager/models/field';
import { FormGroup } from '@angular/forms';

export interface FieldInterfaceComponent {
  
  field: Field;

  content: any;

  data: any;

  form: FormGroup;

  key: string;
}
