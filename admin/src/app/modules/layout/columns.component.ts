import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-columns',
  templateUrl: './columns.component.html',
})
export class ColumnsComponent implements OnInit {
  
  @Input()
  key: string;

  @Input()
  title: string;

  @Input()
  boxes: any[];
  
  @Input()
  form: FormGroup;

  @Input()
  data: any;
  
  ngOnInit() {
    
  }


}
