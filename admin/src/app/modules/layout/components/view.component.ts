import { FormGroup, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Input } from '@angular/core';
import { Field } from 'src/app/plugins/manager/models/field';

export class ViewComponent {

  @Input()
  data: any;
  
  @Input()
  contents: any;
  
  @Input()
  form: FormGroup;
  
  constructor( ) { }

  createForm() {
    this.form = new FormGroup({});
    const fields = this.data.crudConfig.view.fields;
    const content = this.data.content;
    
    for( let key of Object.keys( fields)) {
      const field: Field = fields[key];
      
      if( field.hasOne !== false) {
        if( !this.form.get( field.hasOne)) {
          this.form.addControl( field.hasOne, new FormGroup({}));
        }
        const group = this.form.get( field.hasOne) as FormGroup;

        if( field.translate) {
          this.addTranslateField( field, group, this.data.content);
        } else {
          group.addControl( field.key, new FormControl());
        }
      } else {
        if( field.translate) {
          this.addTranslateField( field, this.form, this.data.content);
        } else {
          const control = new FormControl( content [key]);
          this.form.addControl( field.key, control);
        }
      }
    }

    console.log( this.form.value);
    
  }

  addTranslateField( field: Field, formGroup: FormGroup, content: any) {
    let groupTranslation = formGroup.get( '_translations') as FormGroup;
    if( !groupTranslation) {
      formGroup.addControl( '_translations', new FormGroup({}));
      groupTranslation = formGroup.get( '_translations') as FormGroup;

      for( let lang of this.data.languages) {
        const langGroup = new FormGroup({});
        groupTranslation.addControl( lang.iso3, langGroup);
      }
    }

    for( let lang of this.data.languages) {
      const value = content._translations[lang.iso3][field.key] || '';
      const groupLang = groupTranslation.get( lang.iso3) as FormGroup;
      groupLang.addControl( field.key, new FormControl( value));
    }
  }

  createTranslationGroup( formGroup, content) {
    if( !formGroup.get( '_translations')) {
      const group = new FormGroup({});
      for( let lang of this.data.languages) {
        const langGroup = new FormGroup({});

        for( let key of Object.keys( content._translations[lang.iso3])) {
          langGroup.addControl( key, new FormControl( content._translations[lang.iso3][key]));
        }

        group.addControl( lang.iso3, langGroup);
      }
  
      formGroup.addControl( '_translations', group);
    }
  }
}
