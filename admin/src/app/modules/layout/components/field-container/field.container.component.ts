import { FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild, Input, AfterViewInit, ViewContainerRef } from '@angular/core';
import { FieldsDirective } from 'src/app/plugins/manager/directives/fields.directive';
import { RetriveComponentService } from 'src/app/plugins/manager/services/retrive.component.service';
import { Fields } from 'src/app/providers/providers.fields';
import { Field } from 'src/app/plugins/manager/models/field';
import { FieldInterfaceComponent } from 'src/app/modules/layout/field.interface.component';

@Component({
  selector: 'app-field-container',
  templateUrl: './field.container.component.html',
})
export class FieldContainerComponent implements OnInit {

  @Input()
  template: string;

  @Input()
  field: Field;

  @Input()
  form: FormGroup;

  @Input()
  content: any;

  @Input()
  data: any;

  @ViewChild( 'field', { read: ViewContainerRef, static: true }) 
  myRef: ViewContainerRef;
  
  constructor(
    protected retrive: RetriveComponentService,
  ) { }

  ngOnInit() {
    this.loadField();
  }

  loadField() {
    if( Fields.hasField( this.template)) {
      const component = Fields.getField( this.template);
      const componentRef = this.retrive.createComponent( component, this.myRef);
      (componentRef.instance as FieldInterfaceComponent).field = this.field;
      (componentRef.instance as FieldInterfaceComponent).form = this.form;
      (componentRef.instance as FieldInterfaceComponent).data = this.data;
      (componentRef.instance as FieldInterfaceComponent).content = this.content;
    }
  }
}
