import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, 
  MatInputModule, MatRadioModule, MatSelectModule, MatOptionModule, 
  MatSlideToggleModule, MatSliderModule, MatAutocompleteModule, 
  MatDatepickerModule } from '@angular/material';
import { LayoutModule } from '../layout/layout.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [],
  exports: [
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatOptionModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    LayoutModule,
    AppRoutingModule,
    NgxSpinnerModule
  ],
  imports: [
    CommonModule
  ]
})
export class DesignModule { }
