<?php

namespace Manager\FieldAdapter;

use Manager\FieldAdapter\FieldAdapter;

class UploadFieldAdapter extends FieldAdapter
{
  private $__fieldType = 'upload';

  protected $_default = [
    'type' => 'image',
    'template' => 'image',
    'thumbail' => 'med',
    'config' => [
      'thumbnailQuality' => 100,
      'thumbnailSizes' => [
          'thm' => [
            'size' => '100w',
            'name' => 'Miniatura',
          ],
          'hor' => [
            'size' => '400x300',
            'name' => 'Horizontal',
          ],
          'square' => [
            'size' => '400x400',
            'name' => 'Cuadrada'
          ],
          'big' => [
            'size' => '800w',
            'name' => 'Grande'
          ]
      ],
    ]
  ];

  private $__logo = [
    'type' => 'image',
    'template' => 'image',
    'thumbail' => 'med',
    'config' => [
      'thumbnailQuality' => 100,
      'thumbnailSizes' => [
          'thm' => [
            'size' => '100w',
            'name' => 'Miniatura',
          ],
      ],
    ]
  ];

  private $__doc = [
    'type' => 'doc',
    'template' => 'doc'
  ];

  public function initialize()
  {
    $this->addConfig( 'post', $this->__default);
    $this->addConfig( 'logo', $this->__logo);
    $this->addConfig( 'doc', $this->__doc);
  }
}