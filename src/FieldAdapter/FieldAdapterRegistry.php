<?php
namespace Manager\FieldAdapter;

use Cake\Core\App;
use Cake\Core\ObjectRegistry;
use Cake\Event\EventManager;
use Cake\Event\EventManagerTrait;


class FieldAdapterRegistry
{
  protected static $_adaptersTypes = [
    // 'upload' => 'Manager\FieldAdapter\UploadFieldAdapter',
    // 'uploads' => 'Manager\FieldAdapter\UploadFieldAdapter',
    'ckeditor' => 'Manager\FieldAdapter\CkeditorFieldAdapter',
    'block' => 'Manager\FieldAdapter\CkeditorFieldAdapter'
  ];

  protected static $_adapters = [];

  public static function keys()
  {
    return array_keys( static::$_adaptersTypes);
  }

  public static function has( $key)
  {
    return array_key_exists( $key, static::$_adaptersTypes);
  }

  public static function get( $fieldType)
  {
    if( !isset( static::$_adapters [$fieldType]))
    {
      if( !isset( static::$_adaptersTypes [$fieldType]))
      {
        throw new \RuntimeException( vsprintf( "Unable to find adapter type %s.", [$fieldType]));
      }

      $className = static::$_adaptersTypes [$fieldType];
      $adapter = new $className;
      static::$_adapters [$fieldType] = $adapter;
    }

    return static::$_adapters [$fieldType];
  }


}