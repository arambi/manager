<?php

namespace Manager\FieldAdapter;

use Manager\FieldAdapter\FieldAdapter;
use Cake\Routing\Router;
use Manager\Lib\CSSParser;

/**
 * Adaptador para CKeditor
 */
class CkeditorFieldAdapter extends FieldAdapter
{
  private $__fieldType = 'ckeditor';

  protected static $_fonts = [];

  protected static $_customCSS;

  protected static $_parsedCSS;


  protected $_default = [
    'width' => '100%',
    'height' => '400px',
    'inline' => true,
    'entities' => false,
    'simpleuploads_maxFileSize' => 10*1024*1024,
    'extraPlugins' => 'simpleuploads,colordialog,colorbutton,font,justify,fontawesome,sourcedialog,oembed,stylescombo,inlinesave',
    'toolbar' => [
        [ 
          'name' => 'clipboard',
          'groups' => ['clipboard', 'undo'],
          'items' => [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord']
        ],
        [
          'name' => 'basicstyles',
          'groups' => ['basicstyles', 'cleanup', 'subsup'],
          'items' => ['Bold', 'Italic', 'Underline', 'RemoveFormat', 'Subscript', 'Superscript']
        ],
        [
          'name' => 'links',
          'items' => ['Link', 'Unlink', 'Anchor', 'Image', 'addImage', 'addFile']
        ],
        [
          'name' => 'styles',
          'items' => ['Styles', 'Format', 'Blockquote']
        ],
        [
          'name' => 'paragraph',
          'groups' => ['list', 'indent', 'blocks', 'align'],
          'items' => ['NumberedList', 'BulletedList', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'Table', 'Oembed', 'Fontawesome', 'Sourcedialog']
        ],
        [
          'name' => 'document',
          'groups' => ['mode', 'document', 'doctools'],
          'items' => ['Source' ]
        ],
        [
          'name' => 'video',
          'groups' => ['video'],
          'items' => ['oembed']
        ]
    ],
    'allowedContent' => true,
    // 'contentsCss' => '/cofree/font-awesome/4.4.0/css/font-awesome.css',
    'contentsCss' => '/css/ckeditor.css',
  ];


/**
 * Setea las fuentes para ser utilizadas en CKEditor
 * Esta función debería llamarse en bootstrap.php de la app
 *   
 * @param string $fonts Las fuentes, separadas por punto y comas (Lato;Helvetica)
 */
  public static function setFonts( $fonts)
  {
    static::$_fonts = $fonts;
  }

/**
 * Devuelve las fuentes que van a ser usadas en CKEditor
 * Esta función es llamada en el admin.ctp para que se puedan visualizar las fuentes
 * 
 * @return array
 */
  public static function getFonts( $fonts)
  {
    return explode( ';', static::$_fonts);
  }

/**
 * Setea el fichero CSS para ser cargado por CKeditor
 * Esta función debería llamarse en bootstrap.php de la app
 * 
 * @param string $file
 */
  public static function setCustomCSS( $file)
  {
    static::$_customCSS = $file;
  }

  public static function setCustomDefaults( $data)
  {
    static::$__customDefaults = $data;
  }

  public static function getCustomCSS()
  {
    if( !empty( static::$_customCSS))
    {
      $parsedCss = self::parseCss();
      return '<style> ' . $parsedCss ['css'] .' </style>';
    }

    return false;
  }

  public function beforeConfig( $data)
  {
    if( !empty( static::$_fonts))
    {
      $data ['font_names'] = static::$_fonts;
    }

    if( !empty( static::$_customCSS))
    {
      $parsedCss = self::parseCss();
      $data ['stylesSet'] = $parsedCss ['js'];
      $data ['contentsCss'] = static::$_customCSS;
    }

    return $data;
  }

  protected static function parseCss()
  {
    if( !empty( self::$_parsedCSS))
    {
      return self::$_parsedCSS;
    }
    $css = file_get_contents( WWW_ROOT. static::$_customCSS);
    $css = str_replace( '@charset "UTF-8";', '', $css);

    if( !empty( $css))
    {
      $parser = new CSSParser();
      $index = $parser->ParseCSS( $css);
      $classes = $parser->GetCSSArray( $index);
      $stylesheet = [];
      foreach( $classes as $class => $styles)
      {        
        if( strpos( $class, ' ') !== false)
        {
          continue;
        }

        if (strpos($class, ':hover') 
          || strpos($class, 'cke_editable') 
          || strpos($class, '[') 
          || strpos($class, '@') 
          || strpos($class, '.icon') 
          || strpos($class, ':after') 
          || strpos($class, ':before')  ) {
          continue;
        }

        $parts = explode( ' ', $class);

        $el = $parts [0];
        
        if( strpos( $el, '.') !== false)
        {
          list( $tag, $className) = explode( '.', $el);
          $stylesheet [] = [
            'name' => $className,
            'element' => $tag,
            'attributes' => [
              'class' => $className
            ]
          ];
        }
      }

      $data ['stylesSet'] = $stylesheet;

      $css = str_replace( 'body', '.cke_editable_inline', $css);
      self::$_parsedCSS = [
        'js' => $stylesheet,
        'css' => $css
      ];

      return self::$_parsedCSS;
    }

    
      
  }
}