<?php
namespace Manager\FieldAdapter;


abstract class FieldAdapter 
{
  private $__configs = [];

  private $_models = [];

  protected static $__customDefaults = [];

  public function __construct()
  {
    $this->addConfig( 'default', $this->_default);
    
    if( !empty( static::$__customDefaults))
    {
      $this->__configs ['default'] = array_merge( $this->__configs ['default'], static::$__customDefaults);
    }
  }

  public function addConfig( $key, $config)
  {
    $this->__configs [$key] = $config;

  }

  public function cloneConfig( $reference, $clone, $key)
  {
    $data = $this->getConfig( $reference);
    $config = array_replace_recursive( $data, $clone);
    $this->addConfig( $key, $config);
  }

  public function set( $key, $model = false, $column = false)
  {
    if( !$model)
    {
      $this->addConfig( 'default', $this->getConfig( $key));
    }
    elseif( !$column)
    {
      $this->_models [$model]['default'] = $key; 
    }
    else
    {
      $this->_models [$model][$column] = $key; 
    }
  }

/**
 * Toma una configuración dado un nombre de Model y una columna (campo)
 * Si no existe algo para el campo, devuelve lo que hay para el Model
 * Si no hay nada para el model devuelve los valores por defecto
 * 
 * @param  string $model
 * @param  string $column
 * @return array
 */
  public function get( $model, $column)
  {
    if( isset( $this->_models [$model][$column]))
    {
      $key = $this->_models [$model][$column];
    }
    elseif( isset( $this->_models [$model]['default']))
    {
      $key = $this->_models [$model]['default'];
    }
    else
    {
      $key = 'default';
    }

    return $this->getConfig( $key);
  }

  public function getConfig( $key)
  {
    if( !isset( $this->__configs [$key]))
    {
      return $this->_default;
    }

    $data = $this->__configs [$key];

    if( method_exists( $this, 'beforeConfig'))
    {
      $data = $this->beforeConfig( $data);
    }

    return $data;
  }

}
