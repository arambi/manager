<?php
namespace Manager\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Core\App;
use Cake\Utility\Inflector;
use Cofree\Traits\TemplateTrait;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;

/**
 * Crud shell command.
 */
class CrudShell extends Shell
{
  use TemplateTrait;


  public $tasks = [
    'Bake.BakeTemplate',
  ];

  private $Table;

/**
 * Labels sugeridos durante la elección de labels para las columnas
 * @var array
 */
  private $humanFieldsNames = [
    'title' => 'Título',
    'subtitle' => 'Subtítulo',
    'antetitle' => 'Antetítulo',
    'body' => 'Cuerpo de texto',
    'published' => 'Publicado',
    'published_at' => 'Fecha de publicación'
  ];

/**
 * Crea un CRUD
 * Crea el Table, Entity, Admin/Controller, Controller y vistas
 * 
 * @return void
 */
  public function create()
  {
    $default_plugin = $this->getPlugin();
    $plugin = $this->in( 'Indica el plugin donde irá el CRUD', [], $default_plugin);
    $plugin = Inflector::camelize( $plugin);
    $this->__setPathTemplates( 'Manager');

    $name = $this->in( 'Indica el nombre computerizado del CRUD');
    $name = Inflector::camelize( $name);

    $humanName = $this->in( 'Indica el nombre humano del CRUD');
    $default_db = strtolower( $plugin) . '_'. Inflector::tableize( $name);
    $table = $this->in( 'Indica el nombre de la tabla en la base de datos', false, $default_db);

    $editFieldsKeys = $this->__promptFields( $table);

    $fields = $indexes = [];

    $this->out( 'A continuación indica el nombre humano de cada campo');

    $this->Table = TableRegistry::get( $table);
    $columns = $this->Table->schema()->columns();

    foreach( $editFieldsKeys as $key)
    {
      $column = $columns [(int)$key];
      $label = $this->in( $column, [], $this->__suggestLabel( $column));
      $fields [$column] = $label;
    }

    $this->out( 'A continuación indica si quieres que los siguienes campos vayan en el listado de contenidos');

    foreach( $fields as $column => $label)
    {
      $bool = $this->in( $column, ['s', 'n'], 's');
      if( $bool == 's')
      {
        $indexes [] = $column;
      }
    }

    $translateFields = $this->getTranslateFields( $fields);

    $hasContentable = $this->in( '¿Usará el behavior Contentable?', ['s', 'n'], 's');
    $hasSluggable = $this->in( '¿Usará el behavior Slugglable?', ['s', 'n'], 's');
    $hasPublisher = $this->in( '¿Usará el behavior Publisher?', ['s', 'n'], 'n');
    $hasSortable = $this->in( '¿Usará el behavior Sortable?', ['s', 'n'], 'n');
    $hasPhoto = $this->in( '¿Tendrá una fotografía en su contenido?', ['s', 'n'], 'n');
    $hasBlockable = $this->in( '¿Tendrá el behavior Blockable?', ['s', 'n'], 'n');
    $hasGallery = $this->in( '¿Tendrá galería de fotos?', ['s', 'n'], 'n');
    $createAction = $this->in( '¿Crear acción de secciones?', ['s', 'n'], 's');

    $params = compact( 
        'plugin', 
        'name', 
        'humanName', 
        'table', 
        'translateFields', 
        'hasContentable', 
        'hasSluggable', 
        'hasBlockable',
        'hasPublisher', 
        'hasSortable', 
        'hasPhoto', 
        'hasGallery',
        'fields',
        'indexes'
    );

    $this->__createTable( $params);
    $this->__modifyEntity( $params);
    $this->__createController( $params);
    $this->__createAccess( $params);
    $this->__createNavigation( $params);
    $this->__modifyTest( $params);

    if( $createAction == 's')
    {
      $this->__createAction( $params);
    }

    $this->out( '****************************************');
    $this->out( 'Creado el CRUD '. $name);
    $this->out( '****************************************');
    $this->out( '');
    $this->out( 'Ejecuta el siguiente test para corregir errores');
    $this->out( 'phpunit plugins/'. $plugin .'/tests/TestCase/Model/Table/'. $name .'TableTest.php');

  }

  private function getPlugin()
  {
    $dir = (new Folder( ROOT .DS. 'plugins'))->read();
    return $dir [0][0];
  }

  private function getTranslateFields( $fields)
  {
    $translates = [];
    $i = 1;
    
    foreach( $fields as $column => $label)
    {
      if( in_array( $this->Table->schema()->getColumnType( $column), ['string', 'text']))
      {
        $translates [$i] = $column;
        $this->out( $i . '. '. $column);
        $i++;
      }
    }

    $translateFields = $this->in( 'Indica los campos de traducción separados por comas (enter para ninguno)');

    if( !empty( $translateFields))
    {
      foreach( explode( ',', $translateFields) as $value)
      {
        if( !array_key_exists( $value, $translates))
        {
          $this->out( '¡¡¡ Has indicado los números incorrectamente!!');
          return $this->getTranslateFields( $fields);
        }
      }

      $translateFields = array_map( function( $value) use ($translates){
        $value = $translates [$value];
        return "'$value'";
      }, explode( ',', $translateFields));

      $translateFields = implode( ',', $translateFields);

      return $translateFields;
    }

    return '';
  }

  public function copy()
  {
    $project = $this->in( 'Indica el nombre del proyecto');
    $parts = explode( DS, ROOT);
    $dir = str_replace( end( $parts), $project, ROOT);
    
    if( !is_dir( $dir))
    {
      $this->error( "No existe el directorio $dir");
    }
  }

  private function __promptFields( $table)
  {
    $Table = TableRegistry::get( $table);
    $columns = $Table->schema()->columns();

    foreach( $columns as $key => $column)
    {
      $this->out( "$key. $column");
    }

    $string = $this->in( 'Indica, separado por comas, los campos que irán en la edición (indica el número)');
    $editFieldsKeys = explode( ',', $string);

    foreach( $editFieldsKeys as $key)
    {
      if( !array_key_exists( $key, $columns))
      {
        $this->out( vsprintf( '¡¡¡¡¡ El número de campo %s no existe !!!!!', [ $key]));
        return $this->__promptFields( $table);
      }
    }

    return array_values( $editFieldsKeys);
  }

/**
 * Crea la Tabla del CRUD
 * 
 * @param  array $params
 * @return void
 */
  private function __createTable( $params)
  {
    extract( $params);
    $fields = '*';

    if( $params ['hasSluggable'] == 's')
    {
      $fields .= ',slugs';
    }

    $this->dispatchShell( 'bake', 'model', $plugin .'.'. $name, 
      '--table', $table, '--no-associations', '--no-validation', '--no-rules', '--fields='. $fields);

    $pathTable = Plugin::path( $plugin) . 'src' .DS. 'Model' .DS. 'Table' .DS. $name . 'Table.php';
    $tableContent = file_get_contents( $pathTable);

    $initializeFunction = $this->__getInitializeFunction( $tableContent);
    $this->BakeTemplate->set( $params);
    $contents = $this->BakeTemplate->generate( 'Crud/crud_table');

    $tableContent = str_replace( $initializeFunction, $initializeFunction . $contents, $tableContent);
    $tableContent = $this->__putTableFunctions( $tableContent, $params);
    file_put_contents( $pathTable, $tableContent);    
  }

/**
 * Toma la función initialize de la Table
 * 
 * @param  string $content
 * @return string
 */
  private function __getInitializeFunction( $content)
  {
    $pattern = "/\{[^\]]*\}/";
    preg_match_all( $pattern, $content, $matches);
    $new = substr( substr( $matches[0][0], 0, -1), 1);
    preg_match_all( $pattern, $new, $matches1);
    $initialize = substr( substr( $matches1[0][0], 0, -1), 1);
    return $initialize;
  }

  private function __putTableFunctions( $content, $params)
  {
    $functions = $this->BakeTemplate->generate( 'Crud/table_functions');
    $pos = strrpos( $content, '}');
    $content = substr_replace( $content, $functions, $pos, 0);
    return $content;
  }

/**
 * Modifica el fichero entity
 * 
 * @param  array $params
 * @return void
 */
  private function __modifyEntity( $params)
  {
    extract( $params);
    $path = Plugin::path( $plugin) . 'src' .DS. 'Model' .DS. 'Entity' .DS. Inflector::singularize( $name) . '.php';
    $content = file_get_contents( $path);

    $namespaces = ['use Manager\Model\Entity\CrudEntityTrait;'];
    $uses = ['use CrudEntityTrait;'];

    if( !empty( $translateFields))
    {
      $namespaces [] = 'use Cake\ORM\Behavior\Translate\TranslateTrait;';
      $uses [] = 'use TranslateTrait;';
    }

    if( $hasSluggable == 's')
    {
      $namespaces [] = 'use Slug\Model\Entity\SlugTrait;';
      $uses [] = 'use SlugTrait;';
    }

    $pos = strpos( $content, 'use Cake\ORM\Entity;');
    $content = substr_replace( $content, implode( "\n", $namespaces), $pos, 0);

    $pos = strpos( $content, '{');
    $uses = array_map( function( $value){
      return "    $value";
    }, $uses);

    $content = substr_replace( $content, "\n" . implode( "\n", $uses), $pos + 1, 0);

    if( $hasBlockable == 's')
    {
      $pos = strpos( $content, '];');
      $content = substr_replace( $content, "    'rows' => true,\n    ", $pos, 0);
    }

    file_put_contents( $path, $content);
  }

/**
 * Crea los controllers
 * 
 * @param  array $params
 * @return void
 */
  private function __createController( $params)
  {
    extract( $params);
    $this->dispatchShell( 'bake', 'controller', $plugin .'.'. $name, '--no-actions', '--force');
    $this->dispatchShell( 'bake', 'controller', $plugin .'.'. $name, '--prefix', 'admin', '--no-actions');

    // Admin controller
    $path = Plugin::path( $plugin) . 'src' .DS. 'Controller' .DS. 'Admin' .DS. Inflector::camelize( $name) . 'Controller.php';
    $content = file_get_contents( $path);

    $namespaces = ['use Manager\Controller\CrudControllerTrait;'];
    $uses = ['use CrudControllerTrait;'];

    $pos = strpos( $content, 'use '. $plugin .'\Controller\AppController;');
    $content = substr_replace( $content,  implode( "\n", $namespaces) ."\n", $pos, 0);
    
    $pos = strpos( $content, '{');
    $uses = array_map( function( $value){
      return "    $value";
    }, $uses);
    $content = substr_replace( $content, "\n" . implode( "\n", $uses), $pos + 1, 0);
    file_put_contents( $path, $content);    


    //  Front Controller
    $path = Plugin::path( $plugin) . 'src' .DS. 'Controller' .DS. Inflector::camelize( $name) . 'Controller.php';
    $content = file_get_contents( $path);
    
    $functions = $this->BakeTemplate->generate( 'Crud/controller_functions');
    $pos = strrpos( $content, '}');
    $content = substr_replace( $content, $functions, $pos, 0);
    file_put_contents( $path, $content);    

  }

/**
 * Añade a bootstrap.php los accesos para el CRUD creado
 *   
 * @param  array $params
 * @return void
 */
  private function __createAccess( $params)
  {
    $params ['key'] = strtolower( $params ['name']);
    extract( $params);
    
    $this->BakeTemplate->set( $params);
    $contents = $this->BakeTemplate->generate( 'Crud/access');
      $bootstrap = $this->__getBootstrap( $plugin);
    $bootstrap = $this->__addUseNamespace( $bootstrap, 'User\Auth\Access');
    $bootstrap .= "\n\n\n\n" . $contents;
    $bootstrapPath = Plugin::path( $plugin) . 'config' .DS. 'bootstrap.php';
    file_put_contents( $bootstrapPath, $bootstrap);
  }
  
/**
 * Añade a bootstrap.php la navegación para el CRUD creado
 * @param  array $params
 * @return void
 */
  private function __createNavigation( $params)
  {
    extract( $params);
    $this->BakeTemplate->set( $params);
    $contents = $this->BakeTemplate->generate( 'Crud/navigation');
    $bootstrap = $this->__getBootstrap( $plugin);
    $bootstrap = $this->__addUseNamespace( $bootstrap, 'Manager\Navigation\NavigationCollection');
    $bootstrap .= "\n\n\n\n" . $contents;
    $bootstrapPath = Plugin::path( $plugin) . 'config' .DS. 'bootstrap.php';
    file_put_contents( $bootstrapPath, $bootstrap);
  }

/**
 * Añade a bootstrap.php la action para el CRUD creado
 * @param  array $params
 * @return void
 */
  private function __createAction( $params)
  {
    extract( $params);
    $this->BakeTemplate->set( $params);
    $contents = $this->BakeTemplate->generate( 'Crud/action');
    $bootstrap = $this->__getBootstrap( $plugin);
    $bootstrap = $this->__addUseNamespace( $bootstrap, 'Section\Action\ActionCollection');
    $bootstrap .= "\n\n\n\n" . $contents;
    $bootstrapPath = Plugin::path( $plugin) . 'config' .DS. 'bootstrap.php';
    file_put_contents( $bootstrapPath, $bootstrap);
  }

/**
 * Modifica el test de la Table, para incluir funciones de utilidad para las comprobaciones del CRUD
 * 
 * @param  array $params 
 * @return void         
 */
  private function __modifyTest( $params)
  {
    extract( $params);
    $path = Plugin::path( $plugin) . 'tests' .DS. 'TestCase' .DS. 'Model' .DS. 'Table' .DS. $name . 'TableTest.php';
    $content = file_get_contents( $path);
    $content = str_replace( 'extends TestCase', 'extends CrudTestCase', $content);
    $content = $this->__insertAfter( $content, $name .'Table;', "\n" .'use Manager\TestSuite\CrudTestCase;' . "\n");

    $pattern = "/public function testInitialize[^\]]*\}/";
    preg_match_all( $pattern, $content, $matches);
    $function = $matches [0][0];

    $this->BakeTemplate->set( $params);
    $newFunction = $this->BakeTemplate->generate( 'Crud/model_test');
    $content = str_replace( $function, $newFunction, $content);
    file_put_contents( $path, $content);
  }

  
  private function __suggestLabel( $column)
  {
    if( isset( $this->humanFieldsNames [$column]))
    {
      return $this->humanFieldsNames [$column];
    }

    return Inflector::humanize( $column);
  }
}
