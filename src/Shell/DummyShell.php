<?php
namespace Manager\Shell;

use Cake\Console\Shell;
use Cake\Core\App;
use Manager\Dummy\DummyContent;
use Cake\ORM\TableRegistry;

/**
 * Bulk shell command.
 */
class DummyShell extends Shell
{

  /**
   * main() method.
   *
   * @return bool|int Success or error code.
   */
  public function main() 
  {
    $model = $this->in( 'Indica un model (plugin.model)');
    $limit = $this->in( 'Indica un límite de registros');

    $dummy = new DummyContent( $model);
    $dummy->setLimit( $limit)->save();
  }

  public function truncate()
  {
    $model = $this->in( 'Indica un model (plugin.model)');
    $dummy = new DummyContent( $model);

    $option = $this->in( "Vas a borrar TODOS los registros del model $model, ¿estás seguro?", ['y', 'n'], 'n');

    if( $option != 'y')
    {
      exit();
    }

    $dummy->truncate();
  } 
}
