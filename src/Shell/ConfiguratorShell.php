<?php
namespace Manager\Shell;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\Filesystem\Folder;

/**
 * Realiza la instalación de la aplicación
 */
class ConfiguratorShell extends Shell
{

/**
 * Crea el fichero de configuración por defecto del plugin
 * 
 * @return void
 */
  public function main()
  {
    $data = "<?php

\$config ['Manager']['nav'] = array(
    [
      'label' => 'Grupos',
      'url' => [ 'prefix' => 'admin', 'plugin' => 'User', 'controller' => 'Groups', 'action' => 'index' ],
      'icon' => 'fa fa-user'
    ],
    [
      'label' => 'Configuración',
      'url' => [ 'prefix' => 'admin', 'plugin' => 'Website', 'controller' => 'Sites', 'action' => 'update' ],
      'icon' => 'fa fa-user',
      'children' => [
        [
          'label' => 'General',
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Website', 'controller' => 'Sites', 'action' => 'update' ],
          'icon' => 'fa fa-user',
        ],
        [
          'label' => 'Idiomas',
          'url' => [ 'prefix' => 'admin', 'plugin' => 'I18n', 'controller' => 'Languages', 'action' => 'index' ],
          'icon' => 'fa fa-user',
        ]
      ]
    ],
    [
      'label' => 'Noticias',
      'url' => [ 'prefix' => 'admin', 'plugin' => 'Blog', 'controller' => 'Posts', 'action' => 'index' ],
      'icon' => 'fa fa-user',
      'children' => [
        [
          'label' => 'Nueva noticia',
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Blog', 'controller' => 'Posts', 'action' => 'create' ],
          'icon' => 'fa fa-user',
        ],
        [
          'label' => 'Listado',
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Blog', 'controller' => 'Posts', 'action' => 'index' ],
          'icon' => 'fa fa-user',
        ],
        [
          'label' => 'Categorías',
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Blog', 'controller' => 'Categories', 'action' => 'index' ],
          'icon' => 'fa fa-user',
        ]
      ]
    ],
    [
      'label' => __d( 'admin', 'Catálogo'),
      'url' => false,
      'icon' => 'fa fa-user',
      'children' => [
        [
          'label' => __d( 'admin', 'Productos'),
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Shop', 'controller' => 'Products', 'action' => 'index' ],
          'icon' => 'fa fa-user'
        ],
        [
          'label' => __d( 'admin', 'Atributos de producto'),
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Shop', 'controller' => 'AttributeGroups', 'action' => 'index' ],
          'icon' => 'fa fa-user'
        ]
      ]
    ],
    [
      'label' => __d( 'admin', 'Pedidos'),
      'url' => [ 'prefix' => 'admin', 'plugin' => 'Shop', 'controller' => 'Orders', 'action' => 'index' ],
      'icon' => 'fa fa-user'
    ],
    [
      'label' => __d( 'admin', 'Configuración Tienda'),
      'url' => [ 'prefix' => 'admin', 'plugin' => 'Shop', 'controller' => 'Countries', 'action' => 'index' ],
      'icon' => 'fa fa-user',
      'children' => [
        [
          'label' => __d( 'admin', 'Monedas'),
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Shop', 'controller' => 'Currencies', 'action' => 'index' ],
          'icon' => 'fa fa-user',
        ],
        [
          'label' => __d( 'admin', 'Impuestos'),
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Shop', 'controller' => 'Taxes', 'action' => 'index' ],
          'icon' => 'fa fa-user',
        ],
        [
          'label' => __d( 'admin', 'Países'),
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Shop', 'controller' => 'Countries', 'action' => 'index' ],
          'icon' => 'fa fa-user',
        ],
        [
          'label' => __d( 'admin', 'Zonas'),
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Shop', 'controller' => 'Zones', 'action' => 'index' ],
          'icon' => 'fa fa-user',
        ],
        [
          'label' => __d( 'admin', 'Transporte'),
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Shop', 'controller' => 'Carriers', 'action' => 'index' ],
          'icon' => 'fa fa-user',
        ],
        [
          'label' => __d( 'admin', 'Métodos de pago'),
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Shop', 'controller' => 'PaymentMethods', 'action' => 'index' ],
          'icon' => 'fa fa-user',
        ]
      ]
    ],
    [
      'label' => __d( 'admin', 'Taxonomía'),
      'url' => false,
      'icon' => 'fa fa-user',
      'children' => [
        [
          'label' => __d( 'admin', 'Categorías'),
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Taxonomy', 'controller' => 'Categories', 'action' => 'index' ],
          'icon' => 'fa fa-user'
        ],
        [
          'label' => __d( 'admin', 'Etiquetas'),
          'url' => [ 'prefix' => 'admin', 'plugin' => 'Taxonomy', 'controller' => 'Tags', 'action' => 'index' ],
          'icon' => 'fa fa-user'
        ]
      ]
    ],
    [
      'label' => 'Secciones',
      'url' => [ 'prefix' => 'admin', 'plugin' => 'Section', 'controller' => 'Sections', 'action' => 'index' ],
      'icon' => 'fa fa-user'
    ],
    [
      'label' => 'Layouts',
      'url' => [ 'prefix' => 'admin', 'plugin' => 'Section', 'controller' => 'Layouts', 'action' => 'index' ],
      'icon' => 'fa fa-user'
    ],
    
);
";

    // Creación de ficheros de configuración
    $this->writefile( $data );
  }

  public function writefile( $data )
  {
    $file = fopen( "config/manager.php", "wb");
    fwrite( $file, $data);
    fclose( $file);
    $this->out( 'Configuración para el plugin Manager creada');
  }

}