
  <?= $this->Html->script([
      '/manager/js/jquery/jquery-2.1.1.min.js',
      '/manager/js/plugins/jquery-ui/jquery-ui.js',
      '/manager/js/bootstrap/bootstrap.min.js',
      '/manager/js/plupload/moxie.min.js',
      '/manager/js/plupload/plupload.min.js',
      '/manager/js/plugins/metisMenu/jquery.metisMenu.js',
      '/manager/js/plugins/slimscroll/jquery.slimscroll.min.js',
      '/manager/js/plugins/peity/jquery.peity.min.js',
      '/manager/js/plugins/iCheck/icheck.min.js',
      '/manager/js/plugins/chosen/chosen.jquery.js',
      '/manager/js/plugins/pace/pace.min.js',
      '/manager/js/plugins/fancybox/jquery.fancybox.js',
      '/manager/js/plugins/rickshaw/vendor/d3.v3.js',
      '/manager/js/plugins/rickshaw/rickshaw.min.js',
      '/manager/js/plugins/ionRangeSlider/ion.rangeSlider.min.js',
      '/manager/js/plugins/nouslider/jquery.nouislider.min.js',
      '/manager/js/plugins/switchery/switchery.js',
      '/manager/js/plugins/dataTables/jquery.dataTables.js',
      '/manager/js/plugins/dataTables/dataTables.bootstrap.js',
      '/manager/js/plugins/jsKnob/jquery.knob.js',
      '/manager/js/plugins/summernote/summernote.min.js',
      '/manager/js/plugins/fullcalendar/fullcalendar.min.js',
      '/manager/js/cropper/cropper.js',
      


      // CKEditor
      '/manager/js/plugins/ckeditor/ckeditor.js',
      '/manager/js/plugins/ckeditor/adapters/jquery.js',

      /*
      // Angular scripts
      // '/manager/js/angular/angular.min.js',
      '/manager/js/angular/angular.1.3.14.min.js',
      '/manager/js/angular/angular-route.min.js',
      // '/manager/js/bootstrap/ui-bootstrap-tpls-0.11.0.min.js',
      '/manager/js/bootstrap/ui-bootstrap-tpls-0.12.1.js',
      '/manager/js/angular/checklist-model.js',
      '/manager/js/angular/plupload-angular.js',
      '/manager/js/angular/ng-ckeditor.min.js',
      '/manager/js/angular/angular-ui-tree.min.js',
      '/manager/js/angular/ui-load.js',
      '/manager/js/angular/ocLazyLoad.min.js',
      '/manager/js/angular/sortable.js',
      '/manager/js/angular/isteven-multi-select.js',
      '/manager/js/angular/ng-tags-input.min.js',


      // Angular Dependiences 
      '/manager/js/plugins/peity/angular-peity.js',
      // '/manager/js/plugins/easypiechart/angular.easypiechart.js',
      '/manager/js/plugins/flot/angular-flot.js',
      '/manager/js/plugins/rickshaw/angular-rickshaw.js',
      '/manager/js/plugins/summernote/angular-summernote.min.js',
      '/manager/js/bootstrap/angular-bootstrap-checkbox.js',
      '/manager/js/plugins/jsKnob/angular-knob.js',
      '/manager/js/plugins/switchery/ng-switchery.js',
      '/manager/js/plugins/nouslider/angular-nouislider.js',
      '/manager/js/plugins/chosen/chosen.js',
      '/manager/js/plugins/dataTables/angular-datatables.min.js',
      '/manager/js/plugins/fullcalendar/gcal.js',
      '/manager/js/plugins/fullcalendar/calendar.js',

      // '/manager/js/plugins/chartJs/angles.js',
      
      */
      
      '/manager/js/angular/angular.1.3.14.min.js',
      '/manager/js/angular/i18n/angular-locale_es-es.js',
      '/manager/js/angular/angular-route.min.js',
      '/manager/js/bootstrap/ui-bootstrap-tpls-0.12.1.js',
      '/manager/js/angular/checklist-model.js',
      '/manager/js/angular/angular-ui-tree.min.js',
      '/manager/js/angular/ui-load.js',
      '/manager/js/angular/ocLazyLoad.min.js',
      '/manager/js/angular/sortable.js',
      '/manager/js/angular/isteven-multi-select.js',
      '/manager/js/angular/ng-tags-input.min.js',
      '/manager/js/plugins/peity/angular-peity.js',
      '/manager/js/plugins/flot/angular-flot.js',
      '/manager/js/plugins/rickshaw/angular-rickshaw.js',
      '/manager/js/plugins/summernote/angular-summernote.min.js',  
      '/manager/js/bootstrap/angular-bootstrap-checkbox.js',
      '/manager/js/plugins/jsKnob/angular-knob.js',
      '/manager/js/plugins/switchery/ng-switchery.js',
      '/manager/js/plugins/nouslider/angular-nouislider.js',
      '/manager/js/plugins/datapicker/datePicker.js',
      '/manager/js/plugins/chosen/chosen.js',
      '/manager/js/plugins/dataTables/angular-datatables.min.js',
      '/manager/js/plugins/fullcalendar/gcal.js',
      '/manager/js/plugins/fullcalendar/calendar.js',






      '/manager/js/angular/angular-sanitize.js',
      '/manager/js/angular/angular-animate.min.js',
      // '/manager/js/angular/ng-ckeditor.min.js',
      // '/manager/js/angular/angular-ckeditor.min.js',
      // '/manager/js/plugins/datapicker/datePicker.js',
      '/manager/js/angular/angular-ckeditor.js',
      '/manager/js/angular/bootstrap-colorpicker-module.min.js',
      '/manager/js/angular/paging.js',
      '/manager/js/angular/plupload-angular.js',
      '/manager/js/angular/angular-dragdrop.min.js',
      '/manager/js/angular/angular-toastr.min.js',
      '/manager/js/angular/angular-toastr.tpls.min.js',
      '/manager/js/angular/angucomplete-alt.js',
      '/manager/js/angular/ng-map.min.js',
      '/manager/js/angular/elastic.js',
      '/manager/js/plugins/fullcalendar/gcal.js',
      '/manager/js/plugins/fullcalendar/calendar.js',
      '/manager/js/jquery/moment.min.js',
      '/manager/js/jquery/moment-timezone.min.js',
      '/manager/js/moment-with-locales.min.js',
      '/manager/js/angular/datetimepicker.js',
      '/manager/js/angular/datepicker.js',
      '/manager/js/jquery/jquery.bootstrap-touchspin.js',
      '/manager/js/jquery/jquery-scrolltofixed-min.js',
      '/cofree/venobox/venobox.min.js',
      'https://unpkg.com/sweetalert/dist/sweetalert.min.js',
      '/cofree/highcharts/highcharts.js',
      '/cofree/highcharts/highcharts-more.js',
      '/cofree/highcharts/highcharts-ng.min.js',
      // Anglar App Script
      '/manager/js/app.js',
      '/manager/js/controllers.js',
      '/manager/js/config.js',
      '/manager/js/services.js',
      '/manager/js/directives.js?3',
      '/manager/js/filters.js?v2',
      '/manager/js/cruds.js',
      '/block/js/directives.js',
      '/block/js/services.js'
  ]) ?>

  <?= $this->AdminUtil->customJS() ?>

  <script>
    $('body').delegate('.venobox', 'click', function(e){
      e.preventDefault();
      if( $("#preview-link").length == 0){
        $('body').append('<a id="preview-link" class="venobox_custom" data-type="iframe" href=""></a>');
      }

      $("#preview-link").attr( 'href', $(this).attr('href'));
      $("#preview-link").venobox({
          framewidth: '100%'
      }).trigger('click');
    });
  </script>

  <?php if( !empty(\Website\Lib\Website::get( 'settings.google_maps'))): ?>
    <script src='//maps.googleapis.com/maps/api/js?libraries=places&key=<?= \Website\Lib\Website::get( 'settings.google_maps') ?>'></script>
  <?php endif ?>

  <?= $this->Html->script( '/manager/js/angular/i18n/angular-locale_es') ?>
  <?= $this->fetch( 'scriptsBottom') ?>

  <?= $this->fetch( 'script') ?>
  <?= $this->fetch( 'scripts') ?>
  <?= $this->fetch( 'css') ?>