<?php foreach( $this->AdminUtil->templates() as $element => $path): ?>
     <script type="text/ng-template" id="<?= $path ['template'] ?>.html">
        <?= $this->element( $path ['path'] . $path ['element']) ?>
     </script>
<?php endforeach ?>

<script type="text/ng-template" id="header.html">
  <?= $this->element( 'Manager.blocks/header') ?>
</script>

<script type="text/ng-template" id="confirm.html">
  <?= $this->element( 'Manager.blocks/confirm') ?>
</script>
