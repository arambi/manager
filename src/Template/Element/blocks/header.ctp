<div class="col-lg-12 header-content">
  <h2><span class="header-content-h"><i class="{{ data.crudConfig.navigation.icon }}"></i> {{ data.crudConfig.name.plural }} <small ng-if="data.crudConfig.view.title"> / {{ data.crudConfig.view.title }}</small></span>
  
  <button class="btn btn-primary btn-sm cf-save-button" 
      ng-if="data.crudConfig.view.saveButton" 
      ng-click="send( data.crudConfig.view.submit, data.content, false)">
      <i class="fa fa-check"></i> <?= __d( 'admin', 'Guardar') ?>
  </button>

  <button class="btn btn-warning btn-sm cf-save-button" 
      ng-if="data.crudConfig.view.previewButton && data.content.id" 
      ng-click="send( data.crudConfig.view.submit, data.content, true)">
      <i class="fa fa-television"></i> <?= __d( 'admin', 'Guardar y previsualizar') ?>
  </button>

  <span ng-if="data.crudConfig.drafter && data.content.id" class="btn btn-info btn-xs cf-action-button"><a cf-spin-action href="{{ data.crudConfig.adminUrl }}draft/{{ data.content.id }}"><i class="fa fa-eraser"></i> <?= __d( 'admin', 'Crear borrador') ?></a></span>

  <div class="btn-group" ng-if="data.crudConfig.drafter && data.content.draft.id">
    <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle"><?= __d( 'admin', 'Opciones de borrador') ?> <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li ng-if="data.crudConfig.drafter && data.content.original.id"><a cf-spin-action href="{{ data.crudConfig.adminUrl }}savedraft/{{ data.content.id }}"><i class="fa fa-life-buoy"></i> <?= __d( 'admin', 'Copiar a original') ?></a></li>

      <li ng-if="data.content.draft.id"><a href="#{{ data.crudConfig.adminUrl }}update/{{ data.content.draft.foreign_key }}"><i class="fa fa-edit"></i> <?= __d( 'admin', 'Editar original') ?></a></li>
     
      <li ng-if="data.crudConfig.drafter && data.content.draft.id"><a confirm-delete href="{{ data.crudConfig.adminUrl }}deletedraft/{{ data.content.id }}/{{ data.content.salt }}"><i class="fa fa-trash"></i> <?= __d( 'admin', 'Eliminar borrador') ?></a></li>
    </ul>
  </div>

  <div class="btn-group" ng-if="data.crudConfig.drafter && (data.content.drafters.length) > 0">
    <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle"><?= __d( 'admin', 'Editar borradores') ?> <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li ng-repeat="draft in data.content.drafters"><a href="#{{ data.crudConfig.adminUrl }}update/{{ draft.content_id }}"><?= __d( 'admin', 'Creado el ') ?>{{ draft.modified | date:'dd-MM-yyyy HH::mm' }}</a></li>
    </ul>
  </div>

  <span ng-if="data.crudConfig.view.actionButtons" class="btn btn-default btn-xs cf-action-button" ng-repeat="action in data.crudConfig.view.actionButtons">
    <a ng-if="action.action && action.action == 'delete' && !action.modal" ng-init="content = data.content" href confirm-delete cf-url="{{ action.url }}" cf-destroy="$modalStack.dismissAll()"><i class="fa fa-remove"></i> {{ action.label }}</a>
    <a ng-if="!(action.action && action.action == 'delete') && !action.modal" href="{{ action.url }}">{{ action.label }}</a>
    <a ng-if="action.modal" ng-model="data" cf-modal="action.url" href>{{ action.label }}</a>
  </span>

  <div class="btn-group" ng-if="data.tutorials">
    <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle"><?= __d( 'admin', 'Tutoriales') ?> <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li ng-repeat="video in data.tutorials"><a class="venobox" href="/manager/tutorials/{{ video.param }}">{{ video.title }}</a></li>
    </ul>
  </div>

  </h2>
</div>
<div class="col-lg-2"></div>

