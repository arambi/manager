<div class="modal-header">
  <h3 class="modal-title"><?= __d( 'admin', 'Borrar contenido') ?></h3>
</div>
<div class="modal-footer">                  
    <button class="btn btn-default" ng-click="cancel()"><?= __d( 'admin', 'Cancelar') ?></button>
    <button class="btn btn-primary" ng-click="ok()"><?= __d( 'admin', 'Borrar') ?></button>
</div>