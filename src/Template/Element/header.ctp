<div class="row border-bottom">
  <nav class="navbar navbar-fixed-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <span data-collapse="collapse" data-target="#nav-main" aria-expanded="false" class="navbar-minimalize minimalize-styl-2 btn btn-primary collapsed "  aria-controls="navbar"href=""  ><i class="fa fa-bars"></i></span>
    </div>
    <?php if( \Cake\Core\Configure::read( 'Admin.withLogo')): ?>
      <a target="_blank" href="/"><img class="admin-logo" src="/img/logo-admin.png" /></a>
    <?php endif ?>

    <?php if( \Cake\Core\Configure::read( 'Admin.headerElement')): ?>
      <?= $this->element( \Cake\Core\Configure::read( 'Admin.headerElement')) ?>
    <?php endif ?>
    <ul class="nav navbar-top-links navbar-right">
      <?php if( \Cake\Core\Configure::read('Tutorials.pdf_general')): ?>
        <li><a target="_blank" href="/tutorials/<?= \Cake\Core\Configure::read('Tutorials.pdf_general') ?>"><i class="fa fa-file-pdf-o"></i> Descargar tutorial</a></li>
      <?php endif ?>
      <li><?= __d( 'admin', 'Hola {0}', [$this->Auth->user( 'name')]) ?></li>
      <li><a href="#admin/user/users/data"><?= __d( 'admin', 'Mis datos') ?></a></li>
      <li>
          <?= $this->Html->link( '<i class="fa fa-sign-out"></i> '. __d( 'admin', 'Salir'), [
            'prefix' => 'admin',
            'plugin' => 'user',
            'controller' => 'users',
            'action' => 'logout'
          ], [
            'escape' => false
          ]) ?>
      </li>
    </ul>
  </nav>
</div>

<?= $this->Buffer->start() ?>
  <script type="text/javascript">
    $('[data-collapse=collapse]').click(function(){
      var el = $(this).data( 'target');
      $(el).toggleClass( 'collapse-in');
    })
  </script>
<?= $this->Buffer->end() ?>