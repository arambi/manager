    <?php $buttons = array() ?>

    <?php if( $this->AdminNav->asActionButton( 'index') && $this->request->action != 'admin_index'): ?>
        <?php $buttons [] = $this->Html->link( __d( 'admin', 'Listado'), $this->AdminUtil->url( $this->AdminNav->getAction( 'index')), array(
            'escape' => false
        )) ?>

    <?php endif ?>
    
    <?php if( $this->AdminNav->asActionButton( 'add') && $this->request->action != 'admin_add'): ?>
        <?php $buttons [] = $this->Html->link( __d( 'admin', 'Crear'), $this->AdminUtil->url( $this->AdminNav->getAction( 'add')), array(
            'escape' => false
        )) ?>
    <?php endif ?>
    
    <?php if( $this->AdminNav->asActionButton( 'create') && $this->request->action != 'create'): ?>
        <?php $buttons [] = $this->Html->link( __d( 'admin', 'Crear'), $this->AdminUtil->url( $this->AdminNav->getAction( 'create')), array(
            'escape' => false
        )) ?>
    <?php endif ?>

    <?php if( !empty( $buttons)): ?>
        <div class="btn-group dropdown">
          <button type="button" class="dropdown-toggle">
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <?php foreach( $buttons as $button): ?>
                <li><?= $button ?></li>
            <?php endforeach ?>
          </ul>
        </div>
    <?php endif ?> 