<nav id="nav-main" class="navbar-default navbar-static-side navbar-collapse collapse" role="navigation">
<!-- <nav id="nav-main" class="navbar-default navbar-static-side navbar-collapse collapse" role="navigation"> -->
  <div class="slimScrollDiv">
    <div class="sidebar-collapse">
        <?= $this->AdminNav->nav() ?>
    </div>
  </div>
  
  <?php if( \Cake\Core\Configure::read( 'Admin.footElement')): ?>
      <?= $this->element( \Cake\Core\Configure::read( 'Admin.footElement')) ?>
  <?php endif ?>
</nav>


