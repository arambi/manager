<div ng-if="data.errors" class="alert alert-danger content-errors">
  <?= __d( 'admin', 'Hay errores en el contenido') ?> <span>(<?= __d( 'admin', 'Corríjelos y guarda de nuevo') ?>)</span>
  <div class="content-error" ng-repeat="(field, errors) in data.errors" ng-if="field != '_translations'">
    <span ng-repeat="(type, error) in errors"><i class="fa fa-check"></i> {{ error }}</span>
  </div>
</div>