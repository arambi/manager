<div class="form-group" ng-init="localLang={selectAll: '<?= __d( 'admin', 'Todo') ?>', selectNone: '<?= __d( 'admin', 'Nada')  ?>', reset: '<?= __d( 'admin', 'Restaurar')  ?>', search: '<?= __d( 'admin', 'Escribe algo para buscar...') ?>', nothingSelected: '<?= __d( 'admin', 'No has seleccionado nada') ?>'}">
  <label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  <div class="pull-left isteven-multi-select"
    isteven-multi-select
    input-model="field.options"
    output-model="content[field.key]"
    button-label="full_title"
    item-label="full_title"
    tick-property="ticked"
    group-property="msGroup">
  </div>
  <?php if( !\Website\Lib\Website::get( 'settings.without_add_categories')): ?>
      <span ng-if="field.addNew" class="btn" cf-modal="field.addUrl" cf-after-action="push"  ng-model="content[field.key]" cf-options="field.options"><i class="fa fa-plus-square"></i></span>
  <?php endif ?>
  
</div>
