<div class="form-group" ng-if="field.options.length > 0">
  <label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  
  <div class="cf-input-checkboxes">
    <div class="row">
      <div class="col-md-3" ng-repeat="option in field.options" cf-checkboxes="option" ng-model="content[field.key]">
        <label><input type="checkbox" /> {{ option.full_title }}</label>
      </div>
    </div>
    <div class="m-t" ng-if="field.options.length > 0">
      <span class="pointer" check-select-all="1" ng-model="content[field.key]" options="field.options"><?= __d( 'admin', 'Todas') ?></span> | 
      <span class="pointer" check-select-all="0" ng-model="content[field.key]" options="field.options"><?= __d( 'admin', 'Ninguna') ?></span>
    </div>
      
  </div>
</div>
