<div class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <input ng-init="content[field.key] = ''" ng-model="content[field.key]" type="password" class="form-control">
    <span class="form-error" ng-if="field.error" ng-repeat="error in field.error">{{ error }}</span>
  </div>
</div>