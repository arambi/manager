<div class="form-group">
  <label class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <select class="form-control" style="max-width:350px;"
      ng-model="content[field.key]" 
      ng-options="item as item[field.displayField] for item in field.options track by item.id">
      <option value="" ng-if="false"></option>
    </select>
    
    <?php if( !\Website\Lib\Website::get( 'settings.without_add_categories')): ?>
      <span ng-if="field.addNew" class="btn" cf-modal="field.addUrl" cf-after-action="push"  ng-model="content[field.key]" cf-options="field.options"><i class="fa fa-plus-square"></i></span>
    <?php endif ?>
  </div>
</div>