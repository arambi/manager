<div class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <!-- Sin rango de números -->
    <input ng-if="!field.range" style="width: 100px" cf-bind-model="field.key"  type="numeric" class="form-control">
    

    <input ng-if="field.spin" touchspin cf-bind-model="field.key"  type="text" class="form-control">


    <!-- Con rango de números -->
    <div class="col-lg-3">
      <select ng-if="!field.spin && field.range" class="form-control" style="width:100%;"
        cf-bind-model="field.key"
        ng-options="n for n in [] | range:field.range[0]:field.range[1]:field.range[2]">
      </select>
      <span class="form-error" ng-if="field.error" ng-repeat="error in field.error">{{ error }}</span>
    </div>
      
  </div>
</div>
