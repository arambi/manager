
<div ng-if="!field.translate" class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  <div>
    <textarea ng-model="content[field.key]" cf-bind-model="field.key" msd-elastic class="form-control"></textarea>
    <span>{{ content[field.key].length }}</span>
  </div>
</div>

<div ng-if="field.translate" class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  
  <div>
    <set-languages></set-languages>
    <textarea msd-elastic ng-class="{ hide: data.currentLanguage.iso3 != lang.iso3 }" ng-repeat="lang in data.languages" ng-model="content._translations[lang.iso3][field.key]" class="form-control"></textarea>
    <small ng-show="data.currentLanguage.iso3 == lang.iso3" ng-repeat="lang in data.languages">{{ content._translations[lang.iso3][field.key].length }}</small>
  </div>
</div>