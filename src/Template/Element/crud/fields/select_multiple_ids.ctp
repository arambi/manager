<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <select cf-bind-model="field.key" class="form-control" style="width:100%;" multiple>
      <option style="display: none" value="">{{ field.empty }}</option>
      <option ng-repeat="option in field.options" value="{{ option.key }}" cf-bind-selected="field.key">{{ option.value }}</option>
    </select>
  </div>
</div>