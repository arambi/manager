<div ng-if="!field.translate" class="form-group"><label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <div ckeditor="field.adapterConfig" contenteditable="true" ng-model="content[field.key]" class="form-control"></div>
  </div>
</div>
<div ng-if="field.translate" class="form-group"><label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9 m-t-s">
    <set-languages></set-languages>
    <div ng-show="data.currentLanguage.iso3 == lang.iso3" contenteditable="true" ng-repeat="lang in data.languages" ckeditor="field.adapterConfig" ng-model="content._translations[lang.iso3][field.key]"></div>
  </div>
</div>
