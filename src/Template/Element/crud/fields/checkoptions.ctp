<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  
  <div class="col-lg-9 cf-input-checkboxes">
    <div class="row">
      <div class="col-lg-3" ng-repeat="(value, title) in field.options" cf-checkoptions="value" cf-model="field.key">
        <label><input type="checkbox" /> {{ title }}</label>
      </div>
    </div>
  </div>
</div>


