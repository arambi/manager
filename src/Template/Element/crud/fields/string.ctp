<div ng-if="!field.translate" class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class=" control-label">{{ field.label }}</label>
  <div>
    <input 
      cf-bind-model="field.key" 
      ng-class="{inputbig: field.key == data.crudConfig.model.displayField}" 
      type="text" class="form-control">
    <span class="form-error" ng-if="field.error" ng-repeat="(key, error) in field.error">{{ error }}</span>
  </div>
</div>

<div ng-if="field.translate" class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  <div>
    <div ng-class="{'input-group': data.languages.length > 1}">
      <set-languages></set-languages>
      <input ng-class="{inputbig: field.key == data.crudConfig.model.displayField && data.crudConfig.model.alias == crudConfig.model.alias, hide: data.currentLanguage.iso3 != lang.iso3}" ng-repeat="lang in data.languages" ng-model="content._translations[lang.iso3][field.key]" type="text" class="form-control">
    </div>
    <div ng-repeat="lang in data.languages" ng-class="{ hide: data.currentLanguage.iso3 != lang.iso3 }">
      <span class="form-error" ng-if="field.error[lang.iso3]" ng-repeat="(key, error) in field.error[lang.iso3]">{{ error }}</span>
    </div>
  </div>
</div>