<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <div class="row">
      <div class="col-sm-12">
        <button class="btn btn-success pl-upload-button" 
            pl-upload pl-files-model="content[field.key]" 
            pl-url="/upload/Uploads/upload.json?key={{ field.config.type }}" 
            ng-show="!plUploading">
            <i class="fa fa-arrow-circle-o-up"></i>
        </button>
        <button class="btn btn-success pl-upload-button" 
            size="big"
            cf-modal="'/admin/upload/uploads/index/' + field.key + '/' + field.config.type + '/single'" ng-model="content[field.key]">
            <i class="fa fa-file-image-o" aria-hidden="true"></i>
            Selecciona
        </button>
      </div>
    </div>
    <div class="pl-upload" ng-if="content[field.key] && field.upload.type == 'image'">    
        <div class="pl-upload-error" ng-if="content[field.key].error">
          <div class="pl-upload-error-filename">{{ content[field.key].upload.filename.name }}</div>
          <div class="pl-upload-error-msg">{{ content[field.key].error }}</div>
        </div>
        <pl-upload-image pl-type="single" pl-src="content[field.key].paths[field.config.size]" pl-field-config="field" pl-files-model="content[field.key]"></pl-upload-image>
    </div>
    <div ng-if="field.upload.type == 'doc'">
      <pl-upload-doc ng-if="content[field.key]" pl-type="single" pl-src="content[field.key].paths[field.config.size]" pl-field-config="field" pl-files-model="content[field.key]"></pl-upload-doc>
    </div>
  </div>
</div>
