<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    
    <div ng-if="!data.content.id">
      <div class="form-control">
        <?= __d( 'admin', 'Guarda el contenido para poder crear {0}', ['{{ field.crudConfig.name.plural }}']) ?>
      </div> 
    </div>
    <div ng-if="data.content.id">
      <span class="btn btn-default" cf-modal="field.addUrl" cf-modal-parent-id="{{ content.id }}" cf-modal-parent-id-field="{{field.parentField}}" ng-model="content[field.key]"><i class="fa fa-plus-square"></i> <?= __d( 'admin', 'Añadir') ?></span>

      <!-- Headers -->
      <div class="cf-tree-header row" ng-if="data.contents.length > 0">
        <div class="cf-tree-handler-header">&nbsp;</div>
        <div class="col-md-2" ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index">{{ field.label }}</div>
      </div>

      <!-- Data -->
      <div ui-tree="treeOptions" cf-tree-options class="dd cf-tree-list" data-max-depth="3" ng-model="content[field.key]" ng-if="content[field.key].length > 0">
        <ol class="dd-list" ui-tree-nodes="" ng-model="content[field.key]" ng-init="_parent = content[field.key]">
          <li class="dd-item" ng-repeat="content in content[field.key]" ui-tree-node ng-include="'nodes_renderer.html'"></li>
        </ol>
      </div>
      
    </div>
  </div>
</div>

<!-- Nested node template -->
<script type="text/ng-template" id="nodes_renderer.html">
  <div class="dd-handle-tree">
    <span ui-tree-handle class="handle cf-tree-handler"><i class="fa fa-arrows"></i></span>
    <span ng-repeat="_field in field.crudConfig.view.fields | orderObjectBy:_index" class="col-md-4">
      <ng-include ng-init="hasmany=true; crudConfig = field.crudConfig; field = _field" src="_field.templateIndex + '.html'"></ng-include>
    </span>
    <a ng-if="!data.crudConfig.view.nolevels" tooltip="<?= __d( 'admin', 'Crear nuevo dentro de esta categoría') ?>" class="pull-right btn btn-white btn-xs" cf-modal="field.addUrl" cf-modal-extra-fields="[{ field: field.parentField, value: content[field.parentField] }]" cf-modal-parent-id="{{ content.id }}" ng-model="content[field.key]" cf-after-action="push"><i class="fa fa-plus"></i></a>
    <a class="pull-right btn btn-white btn-xs" ng-model="content" cf-modal="content.urls.update" href ><i class="fa fa-pencil"></i></a>
    <a class="pull-right btn btn-white btn-xs" href confirm-delete cf-url="{{ content.urls.delete }}" cf-destroy="if( scope._parent.nodes) {scope._parent.nodes.splice( scope.$index, 1)} else {scope.$parent.data.content[scope.$parent.field.key].splice( scope.$index, 1)}"><i class="fa fa-trash"></i></a> 
  </div>
  <ol class="dd-list" ng-class="{ hidden: collapsed }" ui-tree-nodes="" ng-model="content.nodes" ng-init="_parent = content">
    <li class="dd-item" ng-repeat="content in content.nodes" ui-tree-node ng-include="'nodes_renderer.html'">
    </li>
  </ol>
</script>
