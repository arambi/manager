<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="dropdown btn-group" class="col-lg-9">
    <a class="dropdown-toggle btn btn-outline btn-default btn-sm" data-toggle="dropdown">
      {{(data.content[field.key] | datetimeCustom )||"<?= __d( 'admin', 'Selecciona una fecha') ?>"}}
    </a>
    <div class="dropdown-menu" role="menu" ng-click="$event.preventDefault();$event.stopPropagation()">
      <datetimepicker data-minute-step="15" data-ng-model="data.content[field.key]" data-datetimepicker-config="{ startView:'day', minView:'minute', 'modelType': 'YYYY-MM-DD HH:mm' }" />
    </div>
    <span class="form-error" ng-if="field.error" ng-repeat="(key, error) in field.error">{{ error }}</span>
  </div>
</div>