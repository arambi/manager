<div class="form-group cf-colorpicker" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <span class="cf-colorpicker-sample" style="background-color: {{ $eval( 'content.' + field.key) }}"></span>
    <input 
      colorpicker
      cf-bind-model="field.key" 
      ng-class="{inputbig: field.key == data.crudConfig.model.displayField}" 
      type="text" class="form-control">
    <span class="form-error" ng-if="field.error" ng-repeat="(key, error) in field.error">{{ error }}</span>
  </div>
</div>

