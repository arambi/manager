<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <div>
      <span ng-if="!field.noAdd" class="btn btn-default" cf-modal="field.addUrl" cf-modal-parent-id="{{ content.id }}" ng-model="content[field.key]" cf-after-action="push"><i class="fa fa-plus-square"></i> <?= __d('admin', 'Añadir') ?></span>
      <table class="table table-striped table-bordered table-hover dataTables-example" sortable-options="content[field.key]" config="field.crudConfig">
        <thead ng-show="content[field.key].length > 0">
          <tr>
            <th ng-if="field.crudConfig.view.sortable"><i class="fa fa-arrows"></i></th>
            <th ng-repeat="(key, _field) in field.crudConfig.view.fields | orderObjectBy:_index">
              {{ _field.label }}
            </th>
            <th></th>
          </tr>
        </thead>
        <tbody ng-if="field.crudConfig.view.sortable" ng-include="'content_hasmany_index.html'" ui-sortable="sortableOptions" ng-model="content[field.key]"></tbody>
        <tbody ng-if="!field.crudConfig.view.sortable" ng-include="'content_hasmany_index.html'"></tbody>
        <thead ng-show="content[field.key].length > 0">
          <tr>
            <th ng-if="field.crudConfig.view.sortable"><i class="fa fa-arrows"></i></th>
            <th ng-repeat="(key, _field) in field.crudConfig.view.fields | orderObjectBy:_index">
              {{ _field.label }}
            </th>
            <th></th>
          </tr>
        </thead>
      </table>
    </div>
  </div>


</div>

<script type="text/ng-template" id="content_hasmany_index.html">
  <tr ng-repeat="content in content[field.key]" ng-init="_index_row = $index" class="gradeX">
    <td ng-if="field.crudConfig.view.sortable" class="sortable-handle"><i class="fa fa-arrows"></i></td>
    <td ng-repeat="(key, _field) in field.crudConfig.view.fields | orderObjectBy:_index">
      <ng-include ng-init="hasmany=true; crudConfig = field.crudConfig; field = _field; _index_row = _index_row" src="_field.templateIndex + '.html'"></ng-include>
    </td>
    <td>
      <a class="btn btn-success btn-bitbucket btn-xs" ng-model="content" cf-modal="content.urls.update" href cf-after-action="update"><i ng-if="field.crudConfig.view.editLink" class="fa fa-pencil"></i></a>
      <a class="btn btn-danger btn-bitbucket btn-xs" ng-if="field.crudConfig.view.deleteLink" href confirm-delete cf-destroy="scope.$parent.data.content[scope.$parent.field.key].splice( scope.$index, 1)"><i class="fa fa-trash"></i></a>
    </td>
  </tr> 
</script>