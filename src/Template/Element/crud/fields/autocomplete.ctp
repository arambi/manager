<div class="form-group"><label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  <div class="clearfix">
    <div angucomplete-alt 
      autocomplete-data="{{ field.key }}"
      placeholder="<?= __d( 'admin', 'Introduce un texto') ?>"
      ng-model="content[field.key]"
      pause="400"
      selected-object="beforeSelect"
      remote-url="{{ field.crudConfig.adminUrl }}autocomplete.json?q="
      remote-url-data-field="results"
      title-field="full_title"
      minlength="2"
      clear-selected="true"
      input-class="form-control form-control-small">
    </div>
  </div>
</div>
<div class="form-group" autocomplete-sortable-options="content[field.key]">
  <label class="control-label"></label>
  <div class="clearfix">
  <ul class="list-group clear-list" ui-sortable="sortableOptions" ng-model="content[field.key]" config="field.crudConfig">
    <li ng-repeat="_content in content[field.key]" class="list-group-item fist-item">
      <span class="sortable-handle"><i class="fa fa-arrows"></i></span>
      <span class="pull-right"><a class="btn btn-danger btn-bitbucket btn-xs" href confirm-delete cf-destroy="scope.$parent.data.content[scope.$parent.field.key].splice( scope.$index, 1)"><i class="fa fa-trash"></i></a></span>
      {{ _content.full_title }}
    </li>
  </ul>
  </div>
</div>