<div class="form-group form-group-checkbox">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label" ng-class="{ 'active': content[field.key] }">{{ field.label }}</label>
  <div class="col-lg-9">
    <input type="checkbox" class="js-switch" cf-switch="{color:'#1AB394'}" cf-bind-model="field.key"  />
    <span class="form-error" ng-if="field.error" ng-repeat="(key, error) in field.error">{{ error }}</span>
  </div>
</div>
