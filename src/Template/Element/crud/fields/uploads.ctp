<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9" >
    <div class="row">
      <div class="col-sm-12">
        <button class="btn btn-success pl-upload-button" 
            pl-upload 
            pl-files-model="content[field.key]" 
            pl-url="/upload/Uploads/upload.json?key={{ field.config.type }}" 
            pl-multiple="true"
            ng-show="!plUploading"
            pl-errors="errors">
            <i class="fa fa-arrow-circle-o-up"></i>
        </button>
        <button class="btn btn-success pl-upload-button" 
            size="big"
            cf-modal="'/admin/upload/uploads/index/' + field.key + '/' + field.config.type + '/multiple'" ng-model="content[field.key]">
            <i class="fa fa-file-image-o" aria-hidden="true"></i> Selecciona
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 m-t uploads-assets-index" ui-sortable="sortableUpload" ng-model="content[field.key]">
        <div class="pl-upload" ng-repeat="photo in content[field.key]">
          <div class="pl-upload-error" ng-if="photo.error">
            <div class="pl-upload-error-filename">{{ photo.upload.filename.name }}</div>
            <div class="pl-upload-error-msg">{{ photo.error }}</div>
          </div>

          <div ng-if="field.upload.type == 'image' && !photo.error">
            <pl-upload-image pl-type="multi" class="handle" pl-repeat-scope="content[field.key]" pl-field-config="field" pl-contents="content[field.key]" pl-index="$index" pl-src="photo.paths[field.config.size]" pl-files-model="photo"></pl-upload-image>
          </div>
          <div ng-if="field.upload.type == 'doc' && !photo.error">
            <pl-upload-doc pl-type="multi" class="handle" pl-repeat-scope="content[field.key]" pl-field-config="field" pl-contents="content[field.key]" pl-index="$index" pl-src="photo.paths.square" pl-files-model="photo"></pl-upload-doc>
          </div>
          <ng-include ng-if="field.extraTemplate  && !photo.error" ng-init="photo = photo" src="field.extraTemplate + '.html'"></ng-include>
        </div>
      </div>
    </div>
  </div>
</div>