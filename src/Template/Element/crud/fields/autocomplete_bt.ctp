<div class="form-group"><label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  <div class="clearfix">
    <div angucomplete-alt 
    autocomplete-data-bt="{{ field.key }}"
    placeholder="<?= __d( 'admin', 'Introduce un texto') ?>"
    ng-model="content[field.key]"
    pause="400"
    selected-object="beforeSelect"
    remote-url="{{ field.crudConfig.adminUrl }}autocomplete.json?q="
    remote-url-data-field="results"
    title-field="full_title"
    minlength="2"
    clear-selected="true"
    input-class="form-control form-control-small"
    ></div>
  </div>
</div>
<div class="form-group">
  <label class="control-label"></label>
  <div class="clearfix">
    <h4>{{ content[field.key].full_title }}</h4>
  </div>
</div>

