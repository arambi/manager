
<div cf-google-events="{ lat: field.options.fields.lat, lng: field.options.fields.lng, zoom: field.options.fields.zoom }">
<input places-auto-complete types="['geocode']" class="form-control"  on-place-changed="gmap.simpleMapLocation()" />
<map center="{{ content.lat }}, {{ content.lng }}" zoom="15">
  <marker position="{{ content.lat }}, {{ content.lng }}" draggable="true" on-dragend="gmap.dragend()"></marker>
</map>
</div>
{{ content.lat }}, {{ content.lng }} 