<div class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <div ng-class="{'input-group': data.languages.length > 1}">
      <set-languages></set-languages>
      <tags-input
        ng-class="{ hide: data.currentLanguage.iso3 != lang.iso3 }"
        ng-repeat="lang in data.languages" 
        ng-model="content._translations[lang.iso3][field.key]"
        min-length="2"
        cf-tags
        placeholder="<?= __d( 'admin', 'Escribe aquí las etiquetas') ?>"
        replace-spaces-with-dashes="false">
        <auto-complete source="loadTags( field.crudConfig.adminUrl + 'autocomplete.json', $query)"></auto-complete>
      </tags-input>
    </div>
  </div>
