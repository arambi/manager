<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  <div>
    <select cf-bind-model="field.key" cf-change="field.change" class="form-control" style="width:100%;">
      <option style="display: none" value="">{{ field.empty }}</option>
      <option ng-repeat="option in field.options" value="{{ option.key }}" cf-bind-selected="field.key">{{ option.value }}</option>
    </select>
    <span class="form-error" ng-if="field.error" ng-repeat="(key, error) in field.error">{{ error }}</span>
  </div>
</div>