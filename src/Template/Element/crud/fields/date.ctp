
<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  <div class="dropdown btn-group">
    <a class="dropdown-toggle btn btn-outline btn-default btn-sm" data-toggle="dropdown">
      {{(content[field.key] | date : 'd/M/yyyy')||"<?= __d( 'admin', 'Selecciona una fecha') ?>"}}
    </a>
    <div class="dropdown-menu dropdown-menu-date" role="menu" ng-click="$event.preventDefault();$event.stopPropagation()">
      <datepicker 
        avoid-weekend="{{ field.customOptions && field.customOptions.avoidWeekend ? 'true' : 'false' }}"
        avoid-past="{{ field.customOptions && field.customOptions.avoidPast ? 'true' : 'false' }}" model-type="Date" data-ng-model="content[field.key]" data-datepicker-config="{ startView:'day', minView:'day'}" />
      <div cf-model-change="field.change" ng-model="content[field.key]"></div>
    </div>
  </div>
  <div ng-if="field.error" class="row"><div class="col-lg-push-3 "><span class="form-error" ng-repeat="(key, error) in field.error">{{ error }}</span></div></div>
</div>