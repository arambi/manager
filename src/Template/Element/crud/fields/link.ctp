<div class="form-group">
  <label></label>
  <div>
    <p><i class="fa fa-link"></i> <a target="_blank" href="{{ content[field.key] }}">{{ content[field.key] }}</a></p>
  </div>
</div>
