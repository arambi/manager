
<div class="row wrapper wrapper-index border-bottom white-bg page-heading">
  <ng-include src="'header.html'"></ng-include>
</div>
<div cf-tree class="wrapper wrapper-content animated fadeIn">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-content">

          <div ng-if="data.contents.length == 0">
            <?= __d( 'admin', 'Todavía no hay ningún contenido creado para {0}.', ['{{ data.crudConfig.name.plural }}']) ?>
            <div class="m-t">
              <a class="btn btn-default" href="#{{ data.crudConfig.adminUrl }}create"><?= __d( 'admin', 'Crear {0}', ['{{ data.crudConfig.name.singular }}']) ?></a>
            </div>
          </div>
  
          <!-- Headers -->
          <div class="cf-tree-header row" ng-if="data.contents.length > 0">
            <div class="cf-tree-handler-header">&nbsp;</div>
            <div class="col-md-2" ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index">{{ field.label }}</div>
          </div>

          <!-- Data -->
          <div ui-tree="treeOptions" class="dd cf-tree-list" data-max-depth="4"  ng-if="data.contents.length > 0">
            <ol class="dd-list" ui-tree-nodes="" ng-model="data.contents" ng-init="_contents = data.nodes">
              <li class="dd-item" ng-repeat="content in data.contents" ui-tree-node ng-model="data.contents" cf-tree-contents="data.contents" ng-include="'nodes_renderer.html'"></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Nested node template -->
<script type="text/ng-template" id="nodes_renderer.html">
  <div class="dd-handle-tree">
    <span class="" style="width: 20px; flex-grow: 0"  ng-click="collapsed = !collapsed">
      <i ng-show="content.nodes.length > 0" ng-if="!collapsed" class="fa fa-plus"></i><i ng-show="content.nodes.length > 0" ng-if="collapsed" class="fa fa-minus"></i>
    </span>
    <span ui-tree-handle style="width: 20px; flex-grow: 0" class="handle cf-tree-handler"><i class="fa fa-arrows"></i></span>
    <span style="flex-grow: 1" ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index" class="col-md-4"><ng-include src="field.templateIndex + '.html'"></ng-include></span>
    <a ng-if="!data.crudConfig.view.nolevels" tooltip="<?= __d( 'admin', 'Crear nuevo dentro de esta categoría') ?>" class="pull-right btn btn-white btn-xs" href="#{{ data.crudConfig.adminUrl }}create?parent_id={{ content.id }}"><i class="fa fa-plus"></i></a>
    <a class="pull-right btn btn-white btn-xs" href="#{{ data.crudConfig.adminUrl }}update/{{ content.id }}"><i class="fa fa-pencil"></i></a>
    <a class="pull-right btn btn-white btn-xs" href confirm-delete cf-url="{{ content.urls.delete }}" cf-destroy="scope._contents.splice( scope.$index, 1)"><i class="fa fa-trash"></i></a> 
  </div>
  <ol class="dd-list" ng-show="collapsed" ui-tree-nodes="" ng-model="content.nodes" ng-init="_contents = content.nodes">
    <li class="dd-item" ng-repeat="content in content.nodes" ui-tree-node ng-include="'nodes_renderer.html'">
    </li>
  </ol>
</script>
