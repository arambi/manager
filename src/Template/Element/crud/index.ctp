<div class="row wrapper wrapper-index border-bottom white-bg page-heading">
  <ng-include src="'header.html'"></ng-include>
</div>
<div class="wrapper animated fadeIn">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">

          <div ng-if="data.contents.length == 0 && !data.search">
            <?= __d( 'admin', 'Todavía no hay ningún contenido creado para {0}.', ['{{ data.crudConfig.name.plural }}']) ?>
            <div class="m-t">
              <a class="btn btn-default" href="#{{ data.crudConfig.adminUrl }}create"><?= __d( 'admin', 'Crear {0}', ['{{ data.crudConfig.name.singular }}']) ?></a>
            </div>
          </div>

          <div ng-if="data.contents.length == 0 && data.search">
            <?= __d( 'admin', 'No hay resultados a la búsqueda') ?>
          </div>
          <div class="table-responsive">

            <div ng-if="data.crudConfig.view.exportCsv" class="m-t">
              <a class="btn btn-primary btn-outline btn-xs" href="{{ data.crudConfig.currentUrl }}.csv?{{ data.crudConfig.queryString }}"><i class="fa fa-file-excel-o"></i> <?= __d( 'admin', 'Descargar CSV') ?></a>
            </div>

            <div ng-if="data.crudConfig.view.topLinks" class="m-t index-top-links">
              <a ng-repeat="link in data.crudConfig.view.topLinks" 
                class="btn btn-primary btn-outline btn-xs" 
                href="{{ link.url }}">
                <i ng-if="link.icon" class="{{ link.icon }}"></i>
                {{ link.label }}
              </a>
            </div>

            <div ng-if="data.paging.count > 0" class="m-t m-b index-top-links">
              Total de {{ data.crudConfig.name.plural }}: <strong>{{ data.paging.count }}</strong>
            </div>
            <div ng-if="data.crudConfig.view.actionsBox.length > 0" class="action-box">
              <div class="action-box__action" ng-if="indexBox.length == 0">
                <span>Para los <strong>{{ data.paging.count }}</strong> resultados de búsqueda</span>
                <select ng-model="indexActionValue" ng-change="indexAction( this, true)">
                  <option value="">-- Selecciona --</option>
                  <option ng-repeat="option in data.crudConfig.view.actionsBox" value="{{ option.method }}">{{ option.title }}</option>
                </select>
              </div>
              <div class="action-box__action" ng-if="indexBox.length > 0">
                <span>Para los contenidos seleccionados</span>
                <select ng-model="indexActionValue" ng-change="indexAction( this, false)">
                  <option value="">-- Selecciona --</option>
                  <option ng-repeat="option in data.crudConfig.view.actionsBox" value="{{ option.method }}">{{ option.title }}</option>
                </select>
              </div>
            </div>

            <table class="table table-striped table-bordered table-hover dataTables-example" sortable-options="data.contents" config="data.crudConfig">
              <thead>
                <!-- Encabezados -->
                <tr>
                  <th width="30" ng-if="data.crudConfig.view.actionsBox.length > 0" class="index-box-input"><input ng-checked="indexBox.length == data.contents.length" ng-click="resetIndexBox()" type="checkbox"></th>
                  <th ng-if="data.crudConfig.view.sortable"><i class="fa fa-arrows"></i></th>
                  <th ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index">
                   <a class="table-flex" ng-if="field.hasSort" href sort-paginator="field.key">{{ field.label }}</a>
                   <span ng-if="!field.hasSort">{{ field.label }}</span>
                  </th>
                  <th></th>
                </tr>
                <!-- /Encabezados -->

                <?= $this->element( 'Manager.search') ?>

              </thead>
              <tbody ng-if="data.crudConfig.view.sortable" ng-include="'content_index.html'" ui-sortable="sortableOptions" ng-model="data.contents"></tbody>
              <tbody ng-if="!data.crudConfig.view.sortable" ng-include="'content_index.html'"></tbody>
              <thead ng-show="data.contents.length > 10">
                <!-- Encabezados -->
                <tr>
                  <th width="30" ng-if="data.crudConfig.view.actionsBox.length > 0" class="index-box-input"><input ng-checked="indexBox.length == data.contents.length" ng-click="resetIndexBox()" type="checkbox"></th>
                  <th ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index">
                    {{ field.label }}
                  </th>
                  <th></th>
                </tr>
                <!-- /Encabezados -->
              </thead>
            </table>
          </div>
          <cf-paginator></cf-paginator>
          <cf-paginator-limit></cf-paginator-limit>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Nested node template -->
<script type="text/ng-template" id="content_index.html">
  <tr ng-repeat="content in data.contents " class="gradeX">
    <td width="30" ng-if="data.crudConfig.view.actionsBox.length > 0" class="index-box-input"><input type="checkbox" ng-click="indexToBox( content.id)" ng-checked="indexBox.indexOf( content.id) > -1" value="{{ content.id }}"></td>
    <td ng-if="data.crudConfig.view.sortable" class="sortable-handle"><i class="fa fa-arrows"></i></td>
    <td ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index" class="{{ field.class }}">
      <ng-include src="field.templateIndex + '.html'"></ng-include>
    </td>
    <td>
      <a class="btn btn-default cf-index-extra-link" ng-repeat="link in data.crudConfig.view.extraLinks" href="#{{ link.url }}/{{ content.id }}">{{ link.label }}</a>
      <a tooltip="Editar" ng-if="data.crudConfig.view.editLink" class="btn btn-success btn-bitbucket btn-xs" href="#{{ data.crudConfig.adminUrl }}{{ data.crudConfig.view.actionLink }}/{{ content.id }}"><i class="{{ data.crudConfig.view.editIcon }}"></i></a>
      <a tooltip="Eliminar" ng-if="!data.crudConfig.view.deleteLink && data.crudConfig.view.deleteLinkUrl " class="btn btn-danger btn-bitbucket btn-xs" href confirm-delete cf-url="{{ data.crudConfig.adminUrl }}{{ data.crudConfig.view.deleteLinkUrl }}.json" cf-destroy="scope.data.contents.splice( scope.$index, 1)"><i class="fa fa-trash"></i></a>
      <a tooltip="Eliminar" ng-if="data.crudConfig.view.deleteLink" class="btn btn-danger btn-bitbucket btn-xs" href confirm-delete cf-url="{{ data.crudConfig.adminUrl }}{{ data.crudConfig.view.deleteUrl }}.json" cf-destroy="scope.data.contents.splice( scope.$index, 1)"><i class="fa fa-trash"></i></a>
      <a tooltip="Duplicar" ng-if="data.crudConfig.view.duplicate" class="btn btn-info btn-bitbucket btn-xs" href="#{{ data.crudConfig.adminUrl }}duplicate/{{ content.id }}"><i class="fa fa-copy"></i></a>
    </td>
  </tr> 
</script>
