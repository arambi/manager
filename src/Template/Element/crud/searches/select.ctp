<select ng-model="data.search[field.key]" type="text" class="form-control">
  <option ng-selected="data.search[field.key] == option.key" ng-repeat="option in field.options" value="{{ option.key }}">{{ option.value }}</option>
</select>