<div ng-init="localLang={selectAll: '<?= __d( 'admin', 'Todo') ?>', selectNone: '<?= __d( 'admin', 'Nada')  ?>', reset: '<?= __d( 'admin', 'Restaurar')  ?>', search: '<?= __d( 'admin', 'Escribe algo para buscar...') ?>', nothingSelected: '<?= __d( 'admin', 'No has seleccionado nada') ?>'}">
  <div class="pull-left isteven-multi-select"
    isteven-multi-select
    input-model="field.options"
    output-model="data.search[field.key]"
    button-label="{{ field.displayField }}"
    output-properties="{{ field.primaryKey }}"
    item-label="{{ field.displayField }}"
    tick-property="ticked"
    group-property="msGroup">
  </div>
</div>