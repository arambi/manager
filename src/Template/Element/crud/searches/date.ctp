<div class="dropdown btn-group">
  <a class="dropdown-toggle btn btn-white btn-sm" data-toggle="dropdown">
    <i class="fa fa-calendar"></i> {{(data.search[field.key].from | date : 'd/M/yyyy')||"<?= __d( 'admin', 'Desde') ?>"}}
  </a>

  <div class="dropdown-menu" role="menu" ng-click="$event.preventDefault();$event.stopPropagation()">
    <div date-picker="data.search[field.key].from" max-view="date" format="d/M/y" min-view="date"></div>
  </div>
</div>
- 
<div class="dropdown btn-group">
  <a class="dropdown-toggle btn btn-white btn-sm" data-toggle="dropdown">
    <i class="fa fa-calendar"></i> {{(data.search[field.key].to | date : 'd/M/yyyy')||"<?= __d( 'admin', 'Hasta') ?>"}}
  </a>

  <div class="dropdown-menu" role="menu" ng-click="$event.preventDefault();$event.stopPropagation()">
    <div date-picker="data.search[field.key].to" max-view="date" format="d/M/y" min-view="date"></div>
  </div>
</div>
