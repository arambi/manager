<i ng-if="content[field.key]" inline-edit-boolean class="text-success fa fa-check"></i>
<i ng-if="!content[field.key]" inline-edit-boolean class="text-muted fa fa-remove"></i>