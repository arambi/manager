<div inline-edit="{
  content: content, 
  crudConfig: data.crudConfig,
  field: field.key
  }">
    <span ng-show="!isEditing">
      {{ content[field.key].full_title || ' - ' }}
    </span>
</div>

