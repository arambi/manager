<div inline-edit="{
  content: content, 
  crudConfig: crudconfig || data.crudConfig,
  field: field.key
  }">
    <span ng-show="!isEditing" ng-if="!content [field.key]">
      - 
    </span>
    <div ng-show="!isEditing" ng-if="content [field.key]">
      <span ng-repeat="option in field.options" ng-if="option.key == content [field.key]" ng-bind-html="option.value || ' - '">{{ option.value || ' - ' }}</span>
    </div>
</div>
