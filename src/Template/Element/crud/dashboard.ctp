<?php $this->loadHelper( 'Section.Nav') ?>
<div class="ibox float-e-margins">
  <div class="ibox-content">
    <h1><?= __d( 'admin', 'Administración') ?></h1>
    <div class="row">
      <div ng-repeat="nav in data.nav" class="col-md-2">
        <div class="widget widget-dashboard gray-bg text-center">
          <div class="m-b-md">
            <a href="#{{ nav.url || nav.children[0].url }}">
              <i class="fa-4x {{ nav.icon }}"></i>
              <h4 class="m-sm">{{ nav.menuName }} </h4>
            </a>
            <a ng-if="nav.children.length > 0"  data-toggle="dropdown" class="btn-dashboard dropdown-toggle"><span class="caret"></span></a>
            <ul ng-if="nav.children.length > 0" class="dropdown-menu dropdown-menu-dashboard">
              <li ng-repeat="_nav in nav.children"><a href="#{{ _nav.url }}">{{ _nav.menuName }}</a></li> 
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>