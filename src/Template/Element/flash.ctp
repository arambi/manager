
<div cf-message ng-model="data.flash.success" class="alert-pop alert alert-success" ng-show="data.flash.success.length > 0">
  <ul>
    <li ng-repeat="message in data.flash.success">{{ message }}</li>
  </ul>
</div>
<div cf-message ng-model="data.flash.error" class="alert-pop alert alert-danger" ng-show="data.flash.error.length > 0">
  <ul>
    <li ng-repeat="message in data.flash.error">{{ message }}</li>
  </ul>
</div>
