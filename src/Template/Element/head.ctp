<meta charset="utf-8" />
<title><?= $this->fetch( 'title') ?> - Administración</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href='//fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
<?= $this->Html->css( array(
    /*
    '/manager/css/bootstrap.min.css',
    '/manager/css/fullcalendar.css',
    '/manager/css/jquery.steps.css',
    '/manager/css/jquery.fancybox.css',
    '/manager/css/dataTables.bootstrap.css',
    '/manager/css/custom.css',
    '/manager/css/chosen.css',
    '/manager/css/basic.css',
    '/manager/css/dropzone.css',
    '/manager/css/switchery.css',
    '/manager/css/jquery.nouislider.css',
    '/manager/css/angular-datapicker.css',
    '/manager/css/ion.rangeSlider.css',
    '/manager/css/ion.rangeSlider.skinFlat.css',
    '/manager/css/animate.css',
    '/manager/css/style.css',
    '/manager/css/angular-ui-tree.min.css',
    '/manager/css/ng-tags-input.css',
    '/manager/css/isteven-multi-select.css',
    '/manager/css/cropper.css',
    */
    '/manager/css/application.min.css',
    '/manager/css/angular-ui-tree.min.css',
    '/manager/js/plugins/jquery-ui/jquery-ui.css',
    '/cofree/font-awesome/4.7.0/css/font-awesome.min.css',
    '/cofree/venobox/venobox.css',
    '/manager/js/plugins/sweetalert/sweetalert.css',
    '/manager/css/angucomplete-alt.css',
    '/manager/css/custom.css',
    '/manager/css/msp.css',
    '/manager/css/colorpicker.min.css',
    '/manager/css/plugins/iCheck/custom.css',
    '/manager/css/jquery.bootstrap-touchspin.css',
    '/manager/css/angular-toastr.min.css',
    '/manager/js/angular/datetimepicker.css',
)) ?>

<?= \Manager\FieldAdapter\CkeditorFieldAdapter::getCustomCSS() ?>

<?= $this->AdminUtil->customCSS() ?>

<?= $this->AdminUtil->getCssStyles() ?>