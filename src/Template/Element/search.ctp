<!-- Búsqueda -->
<tr class="table-search" ng-if="!data.crudConfig.view.noSearch">
  <td ng-if="data.crudConfig.view.actionsBox.length > 0"></td>
  <td ng-if="data.crudConfig.view.sortable"></td>
  <td class="table-search" ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index">
    <ng-include ng-if="field.hasSearch" src="field.templateSearch + '.html'"></ng-include>
  </td>
  <td class="table-search">
    <span cf-submit-search ng-click="search()" class="btn btn-white"><i class="fa fa-search"></i> <?= __d( 'admin', 'Buscar') ?></span>
    <span ng-click="reset()" class="btn btn-white btn-xs"><i class="fa fa-refresh"></i> <?= __d( 'admin', 'Reiniciar') ?></span>
  </td>
</tr>
<!-- /Búsqueda -->