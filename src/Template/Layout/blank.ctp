<?php use Cake\Core\Configure;  ?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>
<?= $this->fetch( 'title') ?></title>

  <?= $this->Html->css([
      '/manager/css/bootstrap.min.css',
      '/manager/font-awesome/css/font-awesome.css',
      '/manager/css/animate.css',
      '/manager/css/style.css',
      '/manager/css/toastr.min.css',
      '/manager/css/custom.css',
  ]) ?>
  <?= $this->AdminUtil->customCSS() ?>
    
</head>
<body class="gray-bg" ng-app="admin">
  <div class="loginscreen  animated fadeInDown" <?= Configure::read( 'Admin.backgroundImage') ? 'style="background-image: url(\'/img/'. Configure::read( 'Admin.backgroundImage') . '\')"' : '' ?>>
    <div ng-controller="crudUpdateCtrl" class="login-wrapper">
      <?= $this->Flash->render( 'success') ?>
      <?= $this->fetch( 'content') ?>
    </div>
  </div>
  <?= $this->element( 'Manager.bottom') ?>
</body>


</html>
