<?php use Cake\Core\Configure ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?= $this->element( 'Manager.head') ?>
</head>
<body class="pace-done fixed-nav fixed-sidebar" ng-app="admin">
  <!-- Wrapper-->
  <div id="wrapper">
    <!-- Navigation -->
    <?= $this->element( 'Manager.aside') ?>

    <!-- Page wraper -->
    <!-- ng-class with current state name give you the ability to extended customization your view -->
    <div id="page-wrapper" class="gray-bg ashboards.dashboard_1">
        <!-- Page wrapper -->
        <?= $this->element( 'Manager.header') ?>         

        <!-- Main view  -->
        <div ng-view>
          <?= $this->fetch( 'content') ?>
        </div>

        <!-- Footer -->
        <?= $this->element( 'Manager.footer') ?>
    </div>
    <!-- End page wrapper-->
  </div>
  <!-- End wrapper-->

<?= $this->element( 'Manager.templates') ?>
<?= $this->element( 'Manager.bottom') ?>

  <div ng-show="activeSpinner" class="ng-hide loading">
    <div class="sk-spinner sk-spinner-double-bounce">
        <div class="sk-double-bounce1"></div>
        <div class="sk-double-bounce2"></div>
    </div>
  </div>
</body>
<?= $this->Buffer->write() ?>
</html>
