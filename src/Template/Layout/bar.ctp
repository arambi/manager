<?php use Cake\Core\Configure ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?= $this->element( 'Manager.head') ?>
  <?= $this->Html->css([
    '/manager/css/bar.css',
  ]) ?>
</head>
<body class="admin-bar-body" data-status="closed" data-view="closed" ng-app="admin">
  <!-- Wrapper-->
  <div id="wrapper">
    <!-- Page wraper -->
    <div class="admin-bar">
      <div class="admin-bar-navtop">
        <ul>
          <li class="admin-bar-menu"><a href><i class="fa fa-bars"></i></a>
            <div class="admin-bar-navmain hide-bar">
              <?= $this->AdminNav->nav() ?>
            </div>
          </li>
        </ul>
      </div>   
      <ul  class="admin-bar-links">
        <li class="admin-bar-open"><a href><i class="fa fa-wrench"></i></a></li>
        <li class="admin-view-open"><a href><i class="fa fa-chevron-right"></i></a></li>
        <?= $this->AdminNav->frontLinks() ?>
        <li class="admin-bar-logout"><a href="<?= $this->Url->build([
          'prefix' => 'admin',
          'plugin' => 'user',
          'controller' => 'users',
          'action' => 'logout'
        ]) ?>"><i class="fa fa-sign-out"></i> Salir</a></li>
      </ul>   
    </div>
      
    <!-- Main view  -->
    <div class="admin-bar-content hide-bar" ng-view>
      <?= $this->fetch( 'content') ?>
    </div>

    <!-- Footer -->
    <?#= $this->element( 'Manager.footer') ?>
    <!-- End page wrapper-->
  </div>
  <!-- End wrapper-->

<?= $this->element( 'Manager.templates') ?>
<?= $this->element( 'Manager.bottom') ?>
<?= $this->Html->script( 'Manager.bar-app.js') ?>
  <div ng-show="activeSpinner" class="ng-hide loading">
    <div class="sk-spinner sk-spinner-double-bounce">
        <div class="sk-double-bounce1"></div>
        <div class="sk-double-bounce2"></div>
    </div>
  </div>
</body>
</html>
