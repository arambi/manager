<!DOCTYPE html>
<html lang="en">
<head>
  <?= $this->element( 'Manager.head') ?>
</head>
<body>
  <div class="tutorials">
    <?php if (!empty($relateds)) : ?>
      <div class="tutorial-relateds">
        <h2>Vídeos relacionados</h2>
        <div class="tutorial-relateds-links">
          <?php foreach ($relateds as $param => $related) : ?>
            <div><a href="/manager/tutorials/<?= $param ?>"><?= $related['title'] ?></a></div>
          <?php endforeach ?>
        </div>
      </div>
    <?php endif ?>
    
    <div class="tutorial">
      <h1><?= $video['title'] ?></h1>
      <video controls>
        <source src="/tutorials/<?= $video['file'] ?>" type="video/mp4">
      </video>
    </div>
    

  </div>
  
</body>
</html>