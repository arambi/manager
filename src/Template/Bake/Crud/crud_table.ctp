    
      // Behaviors
      $this->addBehavior( 'Manager.Crudable');
<% if( $hasBlockable == 's'): %>
      $this->addBehavior( 'Block.Blockable', [
        'blocks' => [
          'text', 'gallery'
        ],
        'defaults' => [
          'text'
        ]
      ]);
<% endif %>
<% if( $hasSluggable == 's'): %>
        $this->addBehavior( 'Slug.Sluggable');
<% endif %>
<% if( $hasPublisher == 's'): %>
        $this->addBehavior( 'Cofree.Publisher');
<% endif %>
<% if( $hasSortable == 's'): %>
        $this->addBehavior( 'Cofree.Sortable');
<% endif %>
<% if( $hasContentable == 's'): %>
        $this->addBehavior( 'Cofree.Contentable');
<% endif %>
<% if( $hasGallery == 's' || $hasPhoto == 's'): %>
        $this->addBehavior( 'Upload.UploadJsonable', [
          'fields' => [
<% if( $hasGallery == 's'): %>
            'photos',
<% endif %>
<% if( $hasPhoto == 's'): %>
            'photo',
<% endif %>
          ]
        ]);
<% endif %>
<% if( !empty( $translateFields)): %>
        $this->addBehavior( 'I18n.I18nTranslate', [
          'fields' => [<%= $translateFields %>]
        ]);
<% endif %>

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        // $this->crud->associations([]);

        $this->crud
          ->addFields([
<% foreach( $fields as $field => $label): %>
            '<%= $field %>' => __d( 'admin', '<%= $label %>'),
<% endforeach %>
<% if( $hasPhoto == 's'): %>
            'photo' => [
              'type' => 'upload',
              'label' => 'Foto',
              'config' => [
                'type' => 'post',
                'size' => 'thm'
              ]
            ],
<% endif %>
<% if( $hasGallery == 's'): %>
            'photos' => [
              'type' => 'uploads',
              'label' => __d( 'admin', 'Galería de fotos'),
              'config' => [
                'type' => 'post',
                'size' => 'thm'
              ]
            ],
<% endif %>
            
          ])
          ->addIndex( 'index', [
            'fields' => [
<% foreach( $indexes as $field): %>
              '<%= $field %>',
<% endforeach %>        
            ],
            'actionButtons' => ['create'],
            'saveButton' => false,
          ])
          ->setName( [
            'singular' => __d( 'admin', '<%= $humanName %>'),
            'plural' => __d( 'admin', '<%= $humanName %>'),
          ])
          ->addView( 'create', [
            'columns' => [
              [
                'cols' => 8,
                'box' => [
                  [
                    'elements' => [
<% foreach( $fields as $field => $label): %>
                    '<%= $field %>',
<% endforeach %>
<% if( $hasSluggable == 's'): %>
                      'slugs',
<% endif %>
<% if( $hasGallery == 's'): %>
                      'photos',
<% endif %>
<% if( $hasPhoto == 's'): %>
                      'photo',
<% endif %>
<% if( $hasBlockable == 's'): %>
                      'blocks'
<% endif %>
                    ]
                  ]
                ]
              ]
            ],
            'actionButtons' => ['create', 'index']
          ], ['update'])
          ;
      
      