ActionCollection::set( '<%= $key %>', [
  'label' => '<%= $humanName %>',
  'plugin' => '<%= $plugin %>',
  'controller' => '<%= $name %>',
  'action' => 'index',
  'icon' => 'fa fa-square',
  'actions' => [
    'view' => '/:slug'
  ]
]);