NavigationCollection::add( [
  'name' => '<%= $humanName %>',
  'parentName' => '<%= $humanName %>',
  'plugin' => '<%= $plugin %>',
  'controller' => '<%= $name %>',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);