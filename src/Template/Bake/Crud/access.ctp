Access::add( '<%= $key %>', [
  'name' => '<%= $humanName %>',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => '<%= $plugin %>',
          'controller' => '<%= $name %>',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => '<%= $plugin %>',
          'controller' => '<%= $name %>',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => '<%= $plugin %>',
          'controller' => '<%= $name %>',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => '<%= $plugin %>',
          'controller' => '<%= $name %>',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => '<%= $plugin %>',
          'controller' => '<%= $name %>',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => '<%= $plugin %>',
          'controller' => '<%= $name %>',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => '<%= $plugin %>',
          'controller' => '<%= $name %>',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);