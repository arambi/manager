  
  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->Auth->allow();
  }

  public function index()
  {
    $query = $this-><%= $name %>
      ->find( 'front')
    ;

    $contents = $this->paginate( $query);
    $this->set( compact( 'contents'));
  }

  public function view()
  {
    $content = $this-><%= $name %>
      ->find( 'front')
<% if( $hasSluggable == 's'): %>
      ->find( 'slug', ['slug' => $this->request->getParam( 'slug')])
<% endif %>
      ->first();
    
    if( !$content)
    {
      $this->Section->notFound();
    }

    $this->set( compact( 'content'));
  }
