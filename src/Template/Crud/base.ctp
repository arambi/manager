<div ng-controller="crudUpdateCtrl" class="lang-{{ data.currentLanguage.iso3 }}">
  <?= $this->element( 'flash') ?>
  <div ng-class="{ 'crud-draft': data.content.draft.id }" ng-if="data.crudConfig.view.template" ng-include="data.crudConfig.view.template + '.html'"></div>
</div>
