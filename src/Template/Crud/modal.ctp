<div class="modal-body animated fadeIn">
  <span class="btn-modal-close" ng-click="cancel()"><i class="fa fa-times"></i></span>
  <div ng-if="data.crudConfig.view.template" ng-include="data.crudConfig.view.template + '.html'"></div>
</div>