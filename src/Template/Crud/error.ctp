<div class="inmodal"
  <div class="modal-header">
    <h1 class="text-center">{{ code }}</h1>
  </div>
  <div class="modal-body text-center">
      
      <h3 class="font-bold">{{ message }}</h3>

      <div class="error-desc">
          El servidor encontró algo inesperado que no deje que complete la solicitud. Pedimos disculpas. <br>
      </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary m-t" ng-click="ok()"><?= __d( 'admin', 'Cerrar') ?></button>
  </div>
</div>
