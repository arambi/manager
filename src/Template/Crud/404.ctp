<div class="middle-box text-center animated fadeInDown">
    <h1>404</h1>
    <h3 class="font-bold"><?= __d( 'admin', 'Página no encontrada') ?></h3>

    <div class="error-desc">
        <?= __d( 'admin', 'La página que has solicitado no se ha encontrado o ha sido borrada del servidor.') ?>

    </div>

    <div class="m-t">
      <?= $this->Html->link( __d( 'admin', 'Ir a la página principal'), [
        'prefix' => 'admin',
        'plugin' => 'Manager',
        'controller' => 'Pages',
        'action' => 'home',
      ], [
        'class' => 'btn btn-primary'
      ]) ?>
    </div>
      
</div>
