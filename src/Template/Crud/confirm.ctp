<div class="modal-body animated fadeIn">
  <div ng-if="data.crudConfig.view.template" ng-include="data.crudConfig.view.template + '.html'"></div>
</div>