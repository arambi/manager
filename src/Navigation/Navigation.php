<?php

namespace Manager\Navigation;

use Cake\Utility\Inflector;
use ArrayObject;
use Cake\Routing\Router;


class Navigation
{
    public $key;

    public $icon;

    public $prefix = 'admin';

    public $plugin;

    public $controller;

    public $action;

    public $menuName;

    public $name;

    public $parentName;

    public $label;

    public $weight;

    public $parent = null;

    public $url = null;

    public $urlParams = null;

    public $properties = array(
        'key',
        'icon',
        'prefix',
        'plugin',
        'controller',
        'action',
        'name',
        'menuName',
        'parentName',
        'weight',
        'parent',
        'label',
        'url',
        'urlParams',
    );

    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }

        $this->setKey();
        $this->setUrl();
        $this->setNames();
    }


    public function setKey()
    {
        if (empty($this->key)) {
            $this->key = Inflector::tableize($this->plugin . $this->controller) . '_' . $this->action;
        }
    }

    public function setUrl()
    {
        if (!empty($this->urlParams)) {
            $this->url = $this->urlParams;
        } elseif (!empty($this->controller)) {
            $this->url = [
                'prefix' => $this->prefix,
                'plugin' => $this->plugin,
                'controller' => $this->controller,
                'action' => $this->action
            ];
        }

        return $this;
    }

    public function setNames()
    {
        if (empty($this->menuName)) {
            $this->menuName = $this->name;
        }

        $this->label = $this->menuName;
    }

    public function toArray()
    {
        $result = [];

        foreach ($this->properties as $prop) {
            $result[$prop] = $this->$prop;
        }

        return $result;
    }

    public function buildUrl()
    {
        $result = $this->toArray();
        $result['url'] = $result['url'] ?  Router::url($result['url']) : false;
        return $result;
    }
}
