<?php

namespace Manager\Navigation;

use Cake\Collection\Collection;
use User\Auth\Access;

class NavigationCollection
{
    public static $_items = [];

    public static $_nav = [];

    public static $collection;


    /**
     * Añade un elemento de menú
     * 
     * @param array $data Array con las propiedades el elemento
     */
    public static function add(array $data)
    {
        $navigation = new Navigation($data);
        static::$_items[$navigation->key] = $navigation;
    }

    public static function update($name, $new_data) {
        foreach (static::$_items as $item) {
            if ($item->name == $name) {
                foreach ($new_data as $key => $value) {
                    $item->$key = $value;
                }
                break;
            }
        }
    }



    /**
     * Toma un objeto Navigation disponible en static::$_items, dado un key
     *   
     * @param  string $key
     * @return Navigation
     */
    public static function get($key)
    {
        if (array_key_exists($key, static::$_items)) {
            return static::$_items[$key];
        }

        return null;
    }

    /**
     * Set/Get
     * Setea en static::$_nav la navegación, mediante un array de keys dado. Si uno dos elementos del array tiene más elementos, éstos serán los hijos
     * 
     * @param  array $data
     * @return array
     */
    public static function nav($data = null, $method = 'toArray')
    {
        if (!$data) {
            $collection = new Collection(self::itemsArray($method));
            static::$_nav = $collection->nest('key', 'parent')->toArray();
        } else {
            static::$_nav = self::setNav($data);
        }

        return static::$_nav;
    }


    public static function setNav($data)
    {
        $results = [];

        foreach ($data as $key => $childrens) {
            if (is_numeric($key)) {
                $key = $childrens;
                $childrens = null;
            }

            $node = self::get($key)->toArray();

            if (!empty($childrens)) {
                foreach ($childrens as $_key) {
                    $node['children'][] = self::get($_key)->toArray();
                }
            }

            $results[] = $node;
        }

        return $results;
    }

    public static function navJson()
    {
        return self::nav(null, 'buildURL');
    }

    public static function itemsArray($method = 'toArray')
    {
        $results = [];

        foreach (static::$_items as $key => $item) {
            $results[$key] = $item->$method();
        }

        return $results;
    }

    public static function getCurrent($params)
    {
        $collection = new Collection(self::itemsArray());
        $current = $collection->firstMatch($params);

        if ($current) {
            return self::get($current['key']);
        }

        return false;
    }

    public static function reset()
    {
        static::$_items = [];
        static::$_nav = [];
    }

    public static function hasPermissions($url, $group_id)
    {
        return Access::check($group_id, $url);
    }

    public static function hasChildrensPermissions($el, $group_id)
    {
        if (!isset($el['children'])) {
            return false;
        }

        foreach ($el['children'] as $_el) {
            if (!isset($_el['url'])) {
                continue;
            }

            $url = $_el['url'];

            if (empty($url)) {
                continue;
            }

            if (!isset($url['plugin']) && $url) {
                $url['plugin'] = false;
            }

            if (self::hasPermissions($url, $group_id)) {
                return true;
            }
        }

        return false;
    }

    public static function getAll($group_id)
    {
        $items = self::itemsArray('toArray');

        $return = [];

        Access::resetGroupAccess();

        foreach ($items as $item) {
            if (!$item['url'] || static::hasPermissions($item['url'], $group_id)) {
                $return[] = $item;
            }
        }

        return $return;
    }
}
