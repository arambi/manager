<?php 

namespace Manager\Crud;

use Manager\Crud\Field;
use Cake\ORM\Table;
use Cake\Utility\Inflector;

class FieldCollector
{
  protected $_table;

  protected $_entity = false;

  protected $_fields = [];

  public function __construct( Table $table)
  {
    $this->_table = $table;
  }

  public function add( $name, $values)
  {
    $field = new Field( $this->_table, $name, $values);
    $this->_fields [$name] = $field;
  }

  public function fields()
  {
    return $this->_fields;
  }

  public function get( $name)
  {
    if( !isset( $this->_fields [$name]))
    {
      if( strpos( $name, '.') !== false)
      {
        list( $key, $subkey) = explode( '.', $name);
      }
      
      $this->create( $name);
    }
    return $this->_fields [$name];
  }

  public function build( $name, $buildType)
  {
    return $this->get( $name)->build( $buildType);
  }

  public function table()
  {
    return $this->_table;
  }

  public function create( $name)
  {
    $column = $this->_table->schema()->column( $name);

    if( !$column)
    {
      throw new \RuntimeException( sprintf( "There is no key field %s", print_r( $name, true)));
    }

    $column ['label'] = Inflector::humanize( $name);

    $this->add( $name, $column);
  }

  public function has( $name)
  {
    return array_key_exists( $name, $this->_fields);
  }

}