<?php 

namespace Manager\Crud;
use Cake\ORM\Table;
use Cake\Utility\Inflector;

class Field
{
/**
 * Table
 * @var Table
 */
  protected $_table;

/**
 * El nombre del campo, generalmente es el nombre de la columna en la base de datos
 * @var string
 */
  protected $_name;

/**
 * Los valores seteados para que sean construidos por el Builder
 * @var array
 */
  protected $_values = [];

/**
 * Los datos construidos por Builder
 * @var array
 */
  protected $_builds = [];

/**
 * El tipo de campo (string, numeric, hasMany...)
 * @var string
 */
  protected $_type;

/**
 * Valores por defecto a la hora de construir $this->_values
 * @var array
 */
  protected $_defaults = [
    'translate' => false,
    'adapter' => false,
    'adapterConfig' => [],
    'show' => 'true', // Expresión javascript para ng-show de AngularJS
    'help' => '',
    'hasOne' => false, // Indica que es un campo de una asociación hasOne
    'hasSearch' => true, // Indica que se usará en la búsqueda en los índices,
    'before' => false,
    'after' => false,
    'change' => false,
    'customOptions' => false,
  ];

/**
 * Builders para cada tipo de campo
 * @var array
 */
  protected static $_builders = [
    'select_multiple' => 'Manager\Crud\Builder\SelectMultipleBuilder',
    'BelongsTo' => 'Manager\Crud\Builder\BelongsToBuilder',
    'checkboxes' => 'Manager\Crud\Builder\BelongsToManyBuilder',
    'hasMany' => 'Manager\Crud\Builder\HasManyBuilder',
    'BelongsToMany' => 'Manager\Crud\Builder\BelongsToManyBuilder',
    'autocomplete' => 'Manager\Crud\Builder\BelongsToManyBuilder',
    'autocomplete_bt' => 'Manager\Crud\Builder\AutocompleteBtBuilder',
    'multi' => 'Manager\Crud\Builder\MultiBuilder',
    'numeric' => 'Manager\Crud\Builder\NumericBuilder',
    'tags' => 'Manager\Crud\Builder\TagsBuilder',
    'upload' => 'Manager\Crud\Builder\UploadBuilder',
    'uploads' => 'Manager\Crud\Builder\UploadBuilder',
    'export' => 'Manager\Crud\Builder\ExportBuilder',
    'sectionsCategories' => 'Manager\Crud\Builder\SectionsCategoriesBuilder',
  ];

/**
 * El Builder por defecto para los tipos de campo
 * @var string
 */
  protected static $_defaultBuilder = 'Manager\Crud\Builder\Builder';

/**
 * Relación automática para los tipos de campo
 * Los keys son los valores que da Cakephp en el schema de la base de datos y los valores es el tipo de campo definido en Field
 * @var array
 */
  private $__fieldsRelations = [
    'integer' => 'numeric',
    'biginteger' => 'numeric',
    'string' => 'string',
    'boolean' => 'boolean',
    'binary' => 'text',
    'jsonable' => 'text',
    'float' => 'numeric',
    'decimal' => 'numeric',
    'text' => 'ckeditor',
    'date' => 'date',
    'time' => 'time',
    'datetime' => 'date',
  ];


  public function __construct( Table $table, $name, $values = null)
  {
    $this->_table = $table;

    if( $values && !is_array( $values))
    {
      $values = [
        'label' => $values
      ];
    }

    $this->_values = $values;
    $this->_name = $name;

    if( isset( $this->_values ['type']) && $this->_values ['type'] == 'multi')
    {
      foreach( $this->_values ['fields'] as $_name => $field)
      {
        $this->_values ['fields'][$_name] = new Field( $this->_table, $_name, $field);
      }
    }
  }

/**
 * Retorna la tabla
 * 
 * @return Table
 */
  public function table()
  {
    return $this->_table;
  }

/**
 * Retorna los valores por defecto
 * 
 * @return array 
 */
  public function defaults()
  {
    return $this->_defaults;
  }

/**
 * Retorna los valores seteados
 * 
 * @return array
 */
  public function values()
  {
    return $this->_values;
  }

  public function value( $key)
  {
    if( isset( $this->_values [$key]))
    {
      return $this->_values [$key];
    }

    return false;
  }

/**
 * Mezcla los valores pasados como argumento con los valores ya seteados
 * 
 * @param  array $values
 */
  public function mergeValues( $values)
  {
    if( is_array( $values))
    {
      $this->_values = array_merge( $this->_values, $values);
    }
  }

/**
 * Getter/Setter
 * Devuelve o setea el nombre del campo
 * 
 * @param  string|null $name
 * @return string
 */
  public function name( $name = null)
  {
    if( $name)
    {
      $this->_name = $name;
    }

    return $this->_name;
  }

/**
 * Devuelve el tipo de campo
 * 
 * @return string
 */
  public function type()
  {
    return $this->_type;
  }

/**
 * Construye la información de $this->_values
 * Llama a un Builder, que se encargará de hacer el trabajo
 *
 * ### Valores posibles de $buildType
 * - 'edit' Para vistas de edición
 * - 'index' Para listados de contenido
 * 
 * @param  string $buildType 
 * @return array
 */
  public function build( $buildType)
  {
    if( isset( $this->_build [$buildType]))
    {
      return $this->_build [$buildType];
    }

    if( !is_array( $this->_values) || !array_key_exists( 'type', $this->_values))
    {
      $type = $this->detectType();
    }
    else
    {
      $type = $this->_values ['type'];
    }

    $this->_type = $type;

    $builderClass = is_array( $this->_values) && isset( $this->_values ['builder'])
        ? $this->_values ['builder']
        : $this->getBuilder( $type);

    $builder = new $builderClass( $this, $buildType);

    $this->_build [$buildType] = $builder->build();
    
    return $this->_build [$buildType];
  }

/**
 * Devuelve el nombre de la clase del Builder, para un tipo de campo dado
 * 
 * @param  string $type
 * @return string
 */
  public function getBuilder( $type)
  {
    if( isset( static::$_builders [$type]))
    {
      return static::$_builders [$type];
    }

    return static::$_defaultBuilder;
  }

/**
 * Devuelve el tipo de campo
 * Si es una asociación devuelve el tipo de asociación
 * Si no, devuelve lo definido en $this->__fieldsRelations
 * 
 * @return string
 */
  private function detectType()
  {
    $association = $this->_table->associations()->getByProperty( $this->_name);
    
    if( $association && $this->table()->crud->hasAssociation( $association->alias()))
    {
      $type = $this->table()->crud->getAssociationType( $this->_name);
      if( $type)
      {
        return $type;
      }
    }

    $column = $this->_table->schema()->column( $this->_name);

    if( isset( $this->__fieldsRelations [$column ['type']]))
    {
      return $this->__fieldsRelations [$column ['type']];
    }

    throw new \RuntimeException( 'Cannot detect file type for `' . $this->name() . '` - `'. $column ['type'] .'` in model ' . $this->_table->alias());
  }


}