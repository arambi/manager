<?php 
namespace Manager\Crud;

class Flash
{
  protected static $_messages = [
    'success' => [],
    'info' => [],
    'warning' => [],
    'error' => []
  ];


  public static function add( $type, $message)
  {
    static::$_messages [$type][] = $message;
  }

  public static function success( $message)
  {
    self::add( 'success', $message);
  }

  public static function info( $message)
  {
    self::add( 'info', $message);
  }

  public static function warning( $message)
  {
    self::add( 'warning', $message);
  }

  public static function error( $message)
  {
    self::add( 'error', $message);
  }

  public static function get()
  {
    return static::$_messages;
  }
}