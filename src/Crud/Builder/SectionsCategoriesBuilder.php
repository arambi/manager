<?php 

namespace Manager\Crud\Builder;

use Manager\Crud\Builder\Builder;
use Section\Action\ActionCollection;


class SectionsCategoriesBuilder extends Builder
{  
  public function extraData()
  {
    if( !array_key_exists( 'id', $this->field->table()->crud->getContent()))
    {
      return;
    }

    $sectionTypeKey = $this->field->table()->crud->getContent()['sectionType']['key'];
    $sectionType = ActionCollection::get( $sectionTypeKey);

    if( !property_exists( $sectionType, 'sectionsCategories'))
    {
      return;
    }

    $options = [];

    foreach( $sectionType->sectionsCategories as $key => $values)
    {
      $values ['options'] = $values ['options']();
      $options [$key] = $values;
    }

    $this->values ['options'] = $options;
  }
}
