<?php 

namespace Manager\Crud\Builder;

use Manager\Crud\Builder\Builder;

class HasManyBuilder extends Builder
{
  
  public function extraData()
  {
    $assoc = $this->field->table()->associations()->getByProperty( $this->field->name());

    if( isset( $assoc->crud))
    {
      $assoc->crud->setContent( $this->field->table()->crud->getContent());
      $this->values ['crudConfig'] = $assoc->crud->serialize( 'index');
      $this->values ['crudConfigAdd'] = $assoc->crud->serialize( 'create');
    }
    
    $this->setAddUrl();
  }
  
}