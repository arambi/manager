<?php 

namespace Manager\Crud\Builder;

use Manager\Crud\Builder\Builder;

class MultiBuilder extends Builder
{
  
  public function extraData()
  {
    $values = $this->field->values();

    foreach( $values ['fields'] as $field)
    {
      $fields [] = $field->build( $this->type);
    }

    $this->values ['fields'] = $fields;
  }
  
}