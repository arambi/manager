<?php 

namespace Manager\Crud\Builder;

use Manager\Crud\Builder\Builder;
use \ArrayObject;
use Cake\Collection\Collection;
use Cake\Core\Configure;

class UploadBuilder extends Builder
{
  
  public function extraData()
  {
    $this->values ['upload'] = Configure::read( 'Upload.'. $this->values ['config']['type']);
  }
 
}