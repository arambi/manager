<?php

namespace Manager\Crud\Builder;

use Manager\Crud\Builder\Builder;
use \ArrayObject;
use Cake\Collection\Collection;

class BelongsToManyBuilder extends Builder
{

    public function extraData()
    {
        $assoc = $this->field->table()->associations()->getByProperty($this->field->name());

        if (isset($assoc->crud)) {
            $this->values['crudConfig'] = $assoc->crud->serialize('index');
        }

        $this->setAddUrl();
    }
    /**
     * Devuelve los datos para las options de un select para una asociación belongsToany
     *   
     * @param  string $property El nombre de la propiedad
     * @return array
     */
    public function associationData()
    {
        $this->setAddUrl();

        // if( !empty( $this->values ['options']))
        if (array_key_exists('options', $this->values)) {
            return;
        }

        $property = $this->field->name();
        $table = $this->field->table()->associations()->getByProperty($property);
        $this->values['displayField'] = $table->getDisplayField();
        $this->values['primaryKey'] = $table->getPrimaryKey();

        if ($this->field->type() == 'select_multiple') {
            return $this->_dataBelongsToManySelect($property);
        }

        $queryParams =  $this->field->table()->crud->getQueryParam($property);
        $queryIds = false;

        if (!empty($queryParams)) {
            $queryIds = collection($queryParams)->extract($table->getPrimaryKey())->toArray();
        }

        if (in_array($this->field->type(), ['checkboxes', 'select_multiple']) && $table) {
            $query = $table->find('all')
                ->order([
                    $table->getAlias() .'.'. $table->getDisplayField()
                ])
                ->formatResults(function ($result) use ($queryIds, $table) {
                    return $result->map(function ($row) use ($queryIds, $table) {
                        if ($queryIds && in_array($row->get($table->getPrimaryKey()), $queryIds)) {
                            $row->set('ticked', true);
                        }

                        return $row;
                    });
                })
                ->hydrate(true);

            $this->values['options'] = $query->toArray();
        }
    }

    /**
     * Devuelve los datos para las options de un select para una asociación belongsToMany multiple
     *   
     * @param  string $property El nombre de la propiedad
     * @return array
     */
    private function _dataBelongsToManySelect($property)
    {
        $table = $this->field->table()->associations()->getByProperty($property);
        $query = $table->find('threaded')
            ->order([
                $table->getAlias() . '.' . $table->getDisplayField()
            ])
            ->select([
                $table->primaryKey(),
                $table->displayField()
            ]);

        if ($table->hasField('position') && $table->hasField('parent_id')) {
            $query->select([
                $table->alias() . '.position',
                $table->alias() . '.parent_id'
            ]);

            $query->order([$table->alias() . '.position']);
        }

        $contents = $query->toArray();

        $return = new ArrayObject();
        $this->__buildDataBelongsTo($property, $contents, $return);

        $array = (array)$return;

        foreach ($array as $content) {
            if (isset($content->children)) {
                $content->unsetProperty('children');
            }
        }

        $_content = $this->field->table()->crud->getContent();

        if (!empty($_content[$property])) {
            $collection = new Collection($_content[$property]);

            foreach ($array as $content) {
                if (isset($content->id)) {
                    $has = $collection->match(['id' => $content->id]);

                    if (count($has->toArray()) > 0) {
                        $content->ticked = true;
                    }
                }
            }
        }

        $this->values['options'] = $array;
    }

    private function __buildDataBelongsTo($property, $contents, $return)
    {
        foreach ($contents as $content) {
            if (!empty($content->children)) {
                $content->msGroup = true;
            }

            $return[] = $content;

            if (!empty($content->children)) {
                $this->__buildDataBelongsTo($property, $content->children, $return);
            }
        }

        $return[] = new ArrayObject(['msGroup' => false]);
    }
}
