<?php 

namespace Manager\Crud\Builder;

use Manager\Crud\Builder\Builder;
use \ArrayObject;
use Cake\Collection\Collection;

class AutocompleteBtBuilder extends Builder
{
  
  public function extraData()
  {
    $assoc = $this->field->table()->associations()->getByProperty( $this->field->name());
    
    if( isset( $assoc->crud))
    {
      $this->values ['crudConfig'] = $assoc->crud->serialize( 'index');
    }
    
    $this->setAddUrl();
  }
 
}