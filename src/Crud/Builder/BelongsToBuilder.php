<?php 

namespace Manager\Crud\Builder;

use Manager\Crud\Builder\Builder;

class BelongsToBuilder extends Builder
{
  
  public function associationData()
  {
    $this->setAddUrl();

    $property = $this->field->name();
    $table = $this->field->table()->associations()->getByProperty( $property);
    $this->values ['displayField'] = $table->getDisplayField();
    $this->values ['primaryKey'] = $table->getPrimaryKey();

    if( !empty( $this->values ['options']))
    {
      return;
    }

    $this->values ['addNew'] = true;
    $query = $table->find();

    $queryParams =  $this->field->table()->crud->getQueryParam($property);
    $queryIds = false;

    if (!empty($queryParams)) {
        $queryIds = collection($queryParams)->extract($table->getPrimaryKey())->toArray();
    }

    $query = $table->find()
        ->order([
            $table->getAlias() .'.'. $table->getDisplayField()
        ])
        ->formatResults(function($result) use ($queryIds, $table) {
            return $result->map(function($row) use ($queryIds, $table) {
                if ($queryIds && in_array($row->get($table->getPrimaryKey()), $queryIds)) {
                    $row->set('ticked', true);
                }

                return $row;
            });
        });


    $order = $table->crud->order();
    
    if( $order === null)
    {
      $order = $table->alias() .'.'. $table->displayField();
    }
    
    $query->order( $order);

    $this->values ['options'] = [];

    $this->values ['options'][]  = ['id' => null, $table->displayField() => __d( 'admin', '-- Selecciona --')];
    $this->values ['options'] = array_merge( $this->values ['options'], $query->toArray());

    
  }
}