<?php 

namespace Manager\Crud\Builder;
use Cake\ORM\Table;
use Manager\Crud\Field;
use Cake\Utility\Inflector;
use Cake\Collection\Collection;
use Manager\FieldAdapter\FieldAdapterRegistry;

/**
 * Constructor de cada uno de los campos de CRUD
 *
 * Cada objeto hace referencia a un campo
 */
class Builder
{
/**
 * El tipo de vista, puede ser 'edit', que significa que está editando o 'index', para las listas de contenidos
 * @var string
 */
  public $type;

/**
 * Objeto del campo
 * @var Manager\Crud\Field
 */
  public $field;

/**
 * Array con los valores de opciones del campo
 * @var array
 */
  public $values = [];


  public function __construct( Field $field, $type)
  {
    $this->type = $type;
    $this->field = $field;
  }

  public function build()
  {
    $this->defaults();
    $this->isHasOne();
    $this->translate();
    $this->template();

    if( $this->type == 'index')
    {
      $this->templateIndex();
      $this->templateSearch();
    }
    
    $this->options();
    $this->associationData();
    $this->adapter();
    $this->extraData();
    $this->errors();
    $this->after();
    $this->before();

    return $this->values;
  }

/**
 * Construye las opciones para campos con listado de opciones (tipo select o checkboxes)
 * Las opciones pueden ser una función que recibe el parámetro del objeto Crud
 */
  public function options()
  {
    if( isset( $this->values ['options']) 
        && is_object( $this->values ['options']))
    {
      $func = $this->values ['options'];

      // Es una función que recibe el parámetro del objeto Crud
      $this->values ['options'] = $func( $this->field->table()->crud);
    }

    if( !empty( $this->values ['options']))
    {
      if( !isset( $this->values ['empty']))
      {
        $this->values ['empty'] = __d( 'admin', '-- Selecciona --');
      }
    }

    if( !empty( $this->values ['options']) && $this->values ['type'] == 'select')
    {
      $options = [];

      foreach( $this->values ['options'] as $key => $value)
      {
        $options [] = [
          'key' => $key,
          'value' => $value
        ];
      }

      $this->values ['options'] = $options;
    }
      
  }

  public function valueStatic( $key)
  {
    if( !$value = $this->field->value( $key))
    {
      return;
    }

    if( is_object( $value))
    {
      $this->values [$key] = $value( $this->field->table()->crud);
    }
  }

  public function after()
  {
    $this->valueStatic( 'after');
  }

  public function before()
  {
    $this->valueStatic( 'before');
  }

  public function isHasOne()
  {
    if( strpos( $this->field->name(), '.') !== false)
    {
      list( $model, $_key) = explode( '.', $this->field->name());
      
      if( isset( $this->field->table()->$model))
      {
        $column = $this->field->table()->$model->schema()->column( $_key);
        $this->values ['key'] = $_key;
        $this->field->name( $_key);
        $this->values ['hasOne'] = $this->field->table()->$model->property();
      }
      else
      {
        $this->field->name( $_key);
      }
    }
  }

  public function defaults()
  {
    $defaults = $this->field->defaults();
    $values = $this->field->values();

    if( !is_array( $values))
    {
      $this->values ['label'] = $values;
    }
    else
    {
      $this->values = $values;
    }

    $this->values ['key'] = $this->field->name();
    $this->values ['type'] = $this->field->type();
    $this->values = array_merge( $defaults, $this->values);

    if( !isset( $this->values ['label']))
    {
      $this->values ['label'] = Inflector::humanize( $this->field->name());
    }

    $this->values ['hasSort'] = in_array( $this->field->type(), [
      'string',
      'numeric',
      'date',
      'datetime',
      'boolean'
    ]);
  }

  public function errors()
  {
    $errors = $this->validateError();
    $this->values ['error'] = !empty( $errors) ? $errors : false;
  }

  /**
 * Devuelve un error de validación para un campo o false si no lo hubiera 
 * 
 * @param  string $key El campo
 * @return string | false
 */
  public function validateError()
  {
    $errors = $this->field->table()->crud->entityErrors();
    $name = $this->field->name();

    if( !$errors)
    {
      return false;
    }

    if( $this->values ['translate'] && !empty( $errors ['_translations']))
    {
      $return = [];

      foreach( $errors ['_translations'] as $lang => $_errors)
      {
        foreach( $_errors as $key => $__errors)
        {
          if( $key == $name)
          {
            foreach( $__errors as $error_name => $message)
            {
              $return [$lang][$error_name] = $message;
            }
          }
        }
      }

      return $return;
    }

    // Hay error del campo
    if( isset( $errors [$name]))
    {
      return $errors [$name];
    }

    // Es un campo de association BelongsTo
    // Como la propiedad es distinta al nombre del campo en la base de datos, se busca en error en el campo
    
    $association = $this->field->table()->associations()->getByProperty( $name);

    if( $association && $this->field->table()->crud->hasAssociation( $association->alias()))
    {
      $association = $this->field->table()->associations()->getByProperty( $name);
    
      if( strpos( get_class( $association), 'BelongsTo'))
      {
        // El nombre del campo en la base de datos
        $column = $this->field->table()->associations()->getByProperty( $name)->foreignKey();

        // Hay error en ese campo
        if( isset( $errors [$column]))
        {
          return $errors [$column];
        }
      }
    }

    return false;
  }

  public function translate()
  {
    // El campo tiene traducción
    if( $this->field->table()->hasBehavior( 'Translate') || $this->field->table()->hasBehavior( 'I18nTranslate'))
    {
      $translate_fields = $this->field->table()->translateFields();
      
      if( in_array( $this->field->name(), $translate_fields))
      {
        $this->values ['translate'] = true;
      }
    }
  }


  public function adapter()
  {
    $adapter = $this->field->type();

    if( $adapter && FieldAdapterRegistry::has( $adapter))
    {
      $this->values ['adapterConfig'] = FieldAdapterRegistry::get( $adapter)->get( $this->field->table()->alias(), $this->field->name());    
    }
  }


  public function template()
  {
    if( !isset( $this->values ['template']))
    {        
      $this->values ['template'] = 'Manager.fields.' . Inflector::underscore( $this->field->type());
    }

    $this->values ['template'] = str_replace( '.', '/', $this->values ['template']);
  }

  public function templateIndex()
  {
    if( !isset( $this->values ['templateIndex']))
    {
      $this->values ['templateIndex'] = 'Manager.indexes.' . Inflector::underscore( $this->field->type());
    }

    $this->values ['templateIndex'] = str_replace( '.', '/', $this->values ['templateIndex']);
  }

  public function templateSearch()
  {
    if( !isset( $this->values ['templateSearch']))
    {
      $this->values ['templateSearch'] = 'Manager.searches.' . Inflector::underscore( $this->field->type());
    }

    $this->values ['templateSearch'] = str_replace( '.', '/', $this->values ['templateSearch']);
  }

  public function setAddUrl()
  {
    $association = $this->field->table()->associations()->getByProperty( $this->field->name());
    
    if( $association)
    {
      $table = $this->field->table()->associations()->getByProperty( $this->field->name())->target();

      if( !$table->behaviors()->has( 'Crudable')) 
      {
        throw new \RuntimeException( sprintf( 'It is necessary that the model %s behavior is linked to the Crudable', $table->alias()));
      }

      $this->values ['addUrl'] = '/admin/'. strtolower( $table->crud->plugin()) .'/'. Inflector::tableize( $table->alias()) .'/create';
    }
    
  }

  public function associationData() {}


  public function extraData() {}



}