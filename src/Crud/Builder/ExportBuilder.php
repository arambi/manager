<?php 

namespace Manager\Crud\Builder;

use Manager\Crud\Builder\Builder;
use \ArrayObject;
use Cake\Collection\Collection;
use Cake\Core\Configure;

class ExportBuilder extends Builder
{
  
  public function extraData()
  {
    if( is_object( $this->values ['link']))
    {
      $this->values ['link'] = $this->values ['link']( $this->field);
    }
  }
 
}