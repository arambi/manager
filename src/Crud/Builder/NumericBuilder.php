<?php 

namespace Manager\Crud\Builder;

use Manager\Crud\Builder\Builder;

class NumericBuilder extends Builder
{
  
/**
 * Coloca el último parámetro del rango, en el caso de que se haya indicado
 * 
 * @return void
 */
  public function extraData()
  {
    if( isset( $this->values ['range']) && !isset( $this->values ['range'][2]))
    {
      $this->values ['range'][2] = 1;
    }
  }
  
}