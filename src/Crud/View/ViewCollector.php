<?php

namespace Manager\Crud\View;

use Cake\Event\EventManager;
use Cake\Event\Event;

/**
 * Colección de vistas usadas en el CRUD
 */

class ViewCollector
{

    /**
     * Array con las vistas disponibles en la colección
     * @var array
     */
    protected $_items = [];

    /**
     * La tabla asociada al CRUD
     * @var Table
     */
    protected $_table;

    /**
     * Instancia de Manager\Crud\FieldCollector
     * @var FieldCollector
     */
    protected $_fieldCollector;

    /**
     * Las clases de las vistas según el tipo de petición
     * `edit` para las ediciones o vistas
     * `index` para los listados de contenidos
     * @var array
     */
    protected static $_classes = [
        'edit' => 'Manager\Crud\View\Edit',
        'index' => 'Manager\Crud\View\Index'
    ];

    /**
     * Construye la instancia
     * 
     * @param FieldCollector $fieldCollector Instancia de Manager\Crud\FieldCollector
     */
    public function __construct($fieldCollector)
    {
        $this->_table = $fieldCollector->table();

        $this->_fieldCollector = $fieldCollector;
    }

    /**
     * Instancia y añade una vista a la colección
     * 
     * @param string $type    El tipo de vista (index o edit)
     * @param string $name    El nombre de la vista
     * @param array $values  Valores a setear
     * @param array|string $aliases Nombres alias para la misma vista
     */
    public function add($type, $name, $values, $aliases = null)
    {
        $class = static::$_classes[$type];

        $view = new $class($this->_fieldCollector, $name, $values);
        $this->_items[$name] = $view;

        if ($aliases) {
            foreach ((array)$aliases as $alias) {
                $this->_items[$alias] = new $class($this->_fieldCollector, $alias, $values);
            }
        }
    }

    public function get($name)
    {
        if (isset($this->_items[$name])) {
            return $this->_items[$name];
        }
    }

    public function addBoxToColumn($aliases, $key, $elements)
    {
        foreach ((array)$aliases as $alias) {
            $this->_items[$alias]->addBoxToColumn($key, $elements);
        }
    }

    public function addElementsToBox($aliases, $column, $box, $elements, $before)
    {
        foreach ((array)$aliases as $alias) {
            $this->_items[$alias]->addElementsToBox($column, $box, $elements, $before);
        }
    }

    public function removeElementFromBox($aliases, $column, $box, $element)
    {
        foreach ((array)$aliases as $alias) {
            $this->_items[$alias]->removeElementFromBox($column, $box, $element);
        }
    }

    public function removeElement($aliases, $element)
    {
        foreach ((array)$aliases as $alias) {
            $this->_items[$alias]->removeElement($element);
        }
    }

    public function addColumn($aliases, $data, $options = [])
    {
        $_defaults = [
            'position' => 'after',
        ];

        $options = $options + $_defaults;

        foreach ((array)$aliases as $alias) {
            $this->_items[$alias]->addColumn($data, $options);
        }
    }

    public function addFieldToIndex($aliases, $key, $beforeField = null)
    {
        foreach ((array)$aliases as $alias) {
            $this->_items[$alias]->addField($key, $beforeField);
        }
    }

    public function resetIndexElements($aliases, $elements)
    {
        foreach ((array)$aliases as $alias) {
            $this->_items[$alias]->resetElements($elements);
        }
    }

    public function addFieldToView($aliases, $key, $beforeField)
    {
        foreach ((array)$aliases as $alias) {
            $this->_items[$alias]->addField($key, $beforeField);
        }
    }

    /**
     * Devuelve `true` si el nombre dado está disponbible en la colección
     * 
     * @param  string  $name
     * @return boolean
     */
    public function has($name)
    {
        return array_key_exists($name, $this->_items);
    }

    /**
     * Construye una vista para su uso con JSON
     * 
     * @param  string $name El nombre de la vista
     * @return array
     */
    public function build($name)
    {
        // beforeBuild Event
        $event = new Event('Manager.View.' . $this->_table->alias() . '.beforeBuild', $this, [
            $this->_table
        ]);

        EventManager::instance()->dispatch($event);
        return $this->_items[$name]->build();
    }

    /**
     * Devuelve la instancia de FieldCollector
     * 
     * @return FieldCollector
     */
    public function fieldCollector()
    {
        return $this->_fieldCollector;
    }

    /**
     * Devuelve un array con los nombres que han sido seteados
     * 
     * @return array
     */
    public function keys()
    {
        return array_keys($this->_items);
    }
}
