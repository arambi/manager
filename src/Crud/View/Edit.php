<?php 

namespace Manager\Crud\View;

use Manager\Crud\View\ViewTrait;


class Edit
{
  use ViewTrait;

  protected $_name;

  protected $_fieldCollector;

  protected $_values = [];

  protected $_defaults = [
    'columns' => [],
    'saveButton' => true,
    'previewButton' => false,
    'title' => false,
    'urlSubmit' => false
  ];

  protected $_boxDefaults = [
    'show' => 'true',
  ];

  public function __construct( $fieldCollector, $name, $values)
  {
    $this->_name = $name;
    $this->_fieldCollector = $fieldCollector;

    $values = array_merge( $this->_defaults, $values);
    $this->_values = $values;

    if( !$this->_values ['title'])
    {
      $this->_values ['title'] = $this->viewTitles( $name);
    }
  }

  public function fieldKeys()
  {
    $fields = $this->_fieldCollector->fields();
    $keys = array_keys( $fields);
    $keys [] = $this->_fieldCollector->table()->primaryKey();
    return $keys;
  }

  public function build()
  {
    $this->viewDefaultOptions();
    $columns = $this->buildColumns();
    $this->buildFields();
    $return = $this->_values;

    $return ['columns'] = $columns;

    if( isset( $this->_values ['actionButtons']))
    {
      $return ['actionButtons'] = $this->buildActionButtons( $this->_values ['actionButtons']);
    }

    return $return;
  }

  private function buildFields()
  {
    $entityClass = $this->_fieldCollector->table()->entityClass();
    $_fields = $this->_fieldCollector->fields();

    $fields = [];

    foreach( array_keys( $_fields) as $key)
    {
      $fields [$key] = $this->_fieldCollector->build( $key, 'edit');
    }

    $this->_values ['fields'] = $fields;
  }

  private function buildColumns()
  {
    $columns = $this->_values ['columns'];
    $return = $this->_values ['columns'];

    foreach( $return as &$col)
    {
      foreach( $col ['box'] as &$box)
      {
        $fields = [];

        $box = array_merge( $this->_boxDefaults, $box);

        if( !isset( $box ['class']))
        {
          $box ['class'] = 'col-md-12';
        }
        
        foreach( $box ['elements'] as $key => $element)
        {
          if( !(is_numeric( $key) && is_string( $element)))
          {
            if( !$this->_fieldCollector->has( $key))
            {
              $this->_fieldCollector->add( $key, $element);
            }
        
            $this->_fieldCollector->get( $key)->mergeValues( $element);
            $element = $key;
          }

          $fields [] = $this->_fieldCollector->build( $element, 'edit');
        }

        $box ['elements'] = $fields;    
      }
    }

    return $return;
  }
  

  public function addField( $field, $beforeField = null)
  {
    foreach( $this->_values ['columns'] as &$column)
    {
      foreach( $column ['box'] as &$box)
      {
        foreach( $box ['elements'] as $key => $element)
        {
          if( $element == $beforeField)
          {
            array_splice( $box ['elements'], $key + 1, 0, $field);
          }
        }
      }
    }
  }

}