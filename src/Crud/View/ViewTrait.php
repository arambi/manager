<?php

namespace Manager\Crud\View;

use Cake\Utility\Inflector;
use Cake\Routing\Router;

/**
 * Trait de métodos para los objetos vistas en la edición
 */
trait ViewTrait
{


    public function set($key, $values)
    {
        $this->_values[$key] = $values;
        return $this;
    }

    public function get($key)
    {
        if (isset($this->_values[$key])) {
            return $this->_values[$key];
        }
    }

    /**
     * Devuelve el título humano para una key dada
     * 
     * @param  string $key
     * @return string | false
     */
    public function viewTitles($key)
    {
        $viewTitles = [
            'add' => 'Nuevo',
            'create' => 'Nuevo',
            'update' => 'Editar',
            'index' => 'Listado',
            'delete' => 'Eliminar',
            'view' => 'Ver'
        ];

        if (isset($viewTitles[$key])) {
            return $viewTitles[$key];
        }

        return false;
    }

    /**
     * Construye los botones de acciones, colocándoles las urls correspondientes
     * 
     * @param  array  $actions
     * @return array
     */
    public function buildActionButtons(array $actions)
    {
        $out = [];

        foreach ($actions as $action) {
            if (is_string($action)) {
                if ($action == 'delete') {
                    $out[] = [
                        'label' => $this->actionLabel($action),
                        'action' => $action,
                        'url' => $this->buildUrl(['action' => $action]) . '.json'
                    ];
                } else {
                    $out[] = [
                        'label' => $this->actionLabel($action),
                        'action' => $action,
                        'url' => $this->url(['action' => $action]) . '?_' . time()
                    ];
                }
            } elseif (is_array($action)) {
                $action['action'] = $action;

                if (isset($action['url'])) {
                    if (is_string($action['url']) && strpos($action['url'], '#') === false) {
                        $action['url'] = $this->url(['action' => $action['url']]);
                    }
                }

                $out[] = $action;
            } elseif (is_callable($action)) {
                $link = $action($this->_fieldCollector->table()->crud);

                if ($link) {
                    $out[] = $link;
                }
            }
        }

        return $out;
    }

    /**
     * Setea la propiedad _actionLabels
     * Se hace desde un método para poder ponerle traducción a los textos
     */
    private function __setActionLabels()
    {
        $this->__actionLabels = [
            'create' => __d('admin', 'Nuevo'),
            'index' => __d('admin', 'Listado'),
            'delete' => __d('admin', 'Eliminar'),
        ];
    }

    /**
     * Devuelve el label para una acción dada en `$key`
     * 
     * @param  string $key
     * @return string
     */
    public function actionLabel($key)
    {
        $this->__setActionLabels();
        return $this->__actionLabels[$key];
    }

    /**
     * Devuelve una URL para la edición
     * Los datos se mezclan con lo dado en `$params`
     * 
     * @param  array  $params
     * @return string
     */
    public function url(array $params)
    {
        return '#' . $this->buildUrl($params);
    }


    public function buildUrl(array $params)
    {
        $defaults = [
            'prefix' => 'admin',
            'plugin' => $this->_fieldCollector->table()->crud->plugin(),
            'controller' => Inflector::tableize($this->_fieldCollector->table()->alias()),
            'action' => false,
        ];

        $url = array_merge($defaults, $params);

        $return = '/' . $url['prefix'] . '/' . strtolower($url['plugin']) . '/' . $url['controller'];

        if ($url['action'] && $url['action'] != 'index') {
            $return .= '/' . $url['action'];
        }

        return $return;
    }
    /**
     * Setea las opciones por defecto para una vista
     * 
     * @return array 
     */
    public function viewDefaultOptions()
    {
        $url = !empty($this->_values['urlSubmit']) ? $this->_values['urlSubmit'] : $this->buildUrl([
            'prefix' => 'admin',
            'plugin' => $this->_fieldCollector->table()->crud->plugin(),
            'controller' => Inflector::tableize($this->_fieldCollector->table()->alias()),
            'action' => $this->_name,
        ]);

        $defaults = [
            'template' => 'Manager/' . $this->_name,
            // Calcula la URL del botón de submit, basado en $action y la convención Cakephp
            'submit' => [
                'url' => $url . '.json'
            ]
        ];

        $this->_values = array_merge($defaults, $this->_values);
    }

    public function addColumn($data, $options)
    {
        if ($options['position'] == 'after') {
            $this->_values['columns'][] = $data;
        } else {
            array_unshift($this->_values['columns'], $data);
        }
    }

    public function addBoxToColumn($key, $box)
    {
        foreach ($this->_values['columns'] as &$column) {
            if (isset($column['key']) && $column['key'] == $key) {
                $column['box'][] = $box;
            }
        }
    }

    public function addElementsToBox($column_name, $box_name, $elements, $before = false)
    {
        foreach ($this->_values['columns'] as &$column) {
            if (isset($column['key']) && $column['key'] == $column_name) {
                foreach ($column['box'] as &$box) {
                    if (isset($box['key']) && $box['key'] == $box_name) {
                        if (!$before) {
                            $box['elements'] = array_merge($box['elements'], $elements);
                        } else {
                            $key = array_search($before, $box['elements']);
                            $array = $box['elements'];

                            $box['elements'] = array_merge(
                                array_slice($array, 0, $key),
                                $elements,
                                array_slice($array, $key, count($array) - $key)
                            );
                        }
                    }
                }
            }
        }
    }

    public function removeElementFromBox($column_name, $box_name, $element)
    {
        foreach ($this->_values['columns'] as &$column) {
            if (isset($column['key']) && $column['key'] == $column_name) {
                foreach ($column['box'] as &$box) {
                    if (isset($box['key']) && $box['key'] == $box_name) {
                        foreach ($box['elements'] as $key => $_element) {
                            if ($box['elements'][$key] == $element) {
                                unset($box['elements'][$key]);
                            }
                        }
                    }
                }
            }
        }
    }

    public function removeElement($elements)
    {
        if (!is_array($elements)) {
            $elements = [$elements];
        }

        foreach ($this->_values['columns'] as &$column) {
            foreach ($column['box'] as &$box) {
                foreach ($box['elements'] as $key => $_element) {
                    if ( in_array($box['elements'][$key], $elements)) {
                        unset($box['elements'][$key]);
                    }
                }
            }
        }
    }
}
