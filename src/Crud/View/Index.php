<?php 

namespace Manager\Crud\View;

use Manager\Crud\View\ViewTrait;

/**
 * Vista para los indices
 * Se encarga de recopilar parámetros y construir los datos para ser usados en JSON
 */

class Index
{
  use ViewTrait;

/**
 * El nombre de la vista
 * @var string
 */
  protected $_name;

/**
 * Instancia de Manager\Crud\FieldCollector
 * @var FieldCollector
 */
  protected $_fieldCollector;

/**
 * Los valores seteados
 * @var array
 */
  protected $_values = [];

/**
 * Valores por defecto
 * @var array
 */
  protected $_defaults = [
    'saveButton' => false,
    'title' => false,
    'actionLink' => 'update',
    'exportCsv' => false,
    'csvFields' => [],
    'indexEdition' => true,
    'editLink' => true,
    'deleteLink' => true,
    'deleteLinkUrl' => 'delete',
    'deleteUrl' => 'delete',
    'extraLinks' => [],
    'topLinks' => [],
    'actionsBox' => [],
    'noSearch' => false,
    'duplicate' => false,
    'editIcon' => 'fa fa-pencil'
  ];

  public function __construct( $fieldCollector, $name, $values)
  {
    $this->_name = $name;
    $this->_fieldCollector = $fieldCollector;

    $values = array_merge( $this->_defaults, $values);
    
    $this->_values = $values;

    if( !$this->_values ['title'])
    {
      $this->_values ['title'] = $this->viewTitles( $name);
    }

    $this->_values ['sortable'] = $this->_fieldCollector->table()->crud->sortable();
  }

  public function addFilters($filters)
  {
    $_filters = isset($this->_values ['filters']) ? $this->_values ['filters'] : [];

    $this->_values ['filters'] = array_merge($_filters, $filters);
  }
  
  public function build()
  {
    $this->viewDefaultOptions();
    $return = $this->_values;
    $return ['fields'] = $this->buildFields();
    $return ['filters'] = $this->buildFilters();
    $return ['updateLink'] = $this->_fieldCollector->table()->crud->adminUrl() . $return ['actionLink'];

    if( isset( $this->_values ['actionButtons']))
    {
      $return ['actionButtons'] = $this->buildActionButtons( $this->_values ['actionButtons']);
    }

    return $return;
  }

  public function buildFilters()
  {
    $return = [];

    if( !isset( $this->_values ['filters']))
    {
      return $return;
    }
    
    foreach( $this->_values ['filters'] as $key)
    {
      $filter = $this->_fieldCollector->table()->crud->getFilter( $key);
      $this->_fieldCollector->add( $key, $filter);
      $return [] = $this->_fieldCollector->build( $key, 'index');
    }

    return $return;
  }

  public function buildFields()
  {
    if( !array_key_exists( 'fields', $this->_values))
    {
      return [];
    }
    
    $return = [];

    // Configuración de los campos que estarán en el index
    foreach( $this->_values ['fields'] as $key => $field)
    {
      if( !(is_numeric( $key) && is_string( $field)))
      {
        if( !$this->_fieldCollector->has( $key))
        {
          $this->_fieldCollector->add( $key, $field);
        }

        $this->_fieldCollector->get( $key)->mergeValues( $field);
        $field = $key;
      }

      $builded = $this->_fieldCollector->build( $field, 'index');
      $builded ['_index'] = $key;
      $return [$field] = $builded;
    }

    return $return;
  }

  public function addField( $key, $beforeField = null)
  {
    if( is_array( $key))
    {
      $this->_values ['fields'][key( $key)] = current( $key);
    }
    else
    {
      $this->_values ['fields'][] = $key;
    }
  }

  public function resetElements($elements)
  {
    $this->_values['fields'] = $elements;
  }

}