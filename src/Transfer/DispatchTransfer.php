<?php 

namespace Manager\Transfer;

class DispatchTransfer
{
  public static function get( $name)
  {
    list( $plugin, $classAlias) = pluginSplit( $name);
    $className = '\\' . $plugin . '\\Transfer\\' . $classAlias .'Transfer';
    return new $className();
  }
}