<?php

namespace Manager\Transfer;

use Prewk\XmlStringStreamer;
use Prewk\XmlStringStreamer\Stream;
use Prewk\XmlStringStreamer\Parser;
use Cake\Utility\Inflector;
use Cake\Datasource\ModelAwareTrait;
use ArrayObject;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\TableRegistry;
use Website\Lib\Website;
use Cake\ORM\Table;

class Transfer
{
    use ModelAwareTrait;
    use LocatorAwareTrait;

    protected $file;

    protected $csvFile;

    protected $csvIgnoreRows = [];

    protected $csvDelimiter = ';';

    protected $originTable;

    protected $primaryKey;

    protected $relations = [];

    protected $ignore = [];

    protected $table;

    protected $shell;

    protected $legacyPrefix = '';

    protected $virtuals = [];

    protected $mode = 'save';

    protected $legacyField = 'legacy_id';

    protected $connectionName = 'migrate';

    protected $usleep = 300000;

    protected $usleepNotransfer = 3000;

    protected $tableLimit = 400;

    protected $errors = [];

    public $dataCsv = [];

    public $keyIterateCsv;

    public $dontUnsetId = false;

    public $dontSumOffset = false;

    protected function setSite()
    {
        $site = $this->loadModel('Website.Sites')->find()->first();
        Website::set($site);
    }

    public function setMode($mode)
    {
        $this->mode = $mode;
        return $this;
    }

    public function setTableLimit($limit)
    {
        $this->tableLimit = $limit;
        return $this;
    }

    public function setConnectionName($name)
    {
        $this->connectionName = $name;
        return $this;
    }

    public function setLegacyField($field)
    {
        $this->legacyField = $field;
        return $this;
    }

    public function setPrimaryKey($key)
    {
        $this->primaryKey = $key;
        return $this;
    }

    public function setLegacyPrefix($prefix)
    {
        $this->legacyPrefix = $prefix;
        return $this;
    }

    public function setRelations($relations)
    {
        $this->relations = $relations;
        return $this;
    }

    public function setVirtuals($fields)
    {
        $this->virtuals = $fields;
        return $this;
    }

    public function setTable($table)
    {
        $this->table = $this->loadModel($table);
        return $this;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function setShell($shell)
    {
        $this->shell = $shell;
        return $this;
    }

    public function setIgnore($ignore)
    {
        $this->ignore = $ignore;
        return $this;
    }

    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    public function setCsvFile($file)
    {
        $this->csvFile = $file;
        return $this;
    }

    public function setOriginTable($name)
    {
        $this->originTable = $name;
        return $this;
    }

    public function setCsvIgnoreRows($value)
    {
        $this->csvIgnoreRows = $value;
        return $this;
    }

    public function setCsvDelimiter($value)
    {
        $this->csvDelimiter = $value;
        return $this;
    }


    public function setUsleep($value)
    {
        $this->usleep = $value;
        return $this;
    }

    public function setUsleepNotransfer($value)
    {
        $this->usleepNotransfer = $value;
        return $this;
    }

    public function markReaded($data)
    {
        $this->loadOriginTable($this->originTable)->query()->update()
            ->set([
                'readed' => true
            ])
            ->where([
                $this->primaryKey => $data[$this->primaryKey]
            ])
            ->execute();
    }

    public function getXml($file)
    {
        $stream = new Stream\File($file);
        $parser = new Parser\StringWalker();
        $streamer = new XmlStringStreamer($parser, $stream);
        return $streamer;
    }

    public function iterate()
    {
        if (!empty($this->file)) {
            $this->iterateXml();
        } elseif (!empty($this->csvFile)) {
            $this->iterateCsv();
        } else {
            $this->iterateTable();
        }
    }

    public function iterateXml()
    {
        $streamer = $this->getXml($this->file);

        while ($node = $streamer->getNode()) {
            $data = $this->buildData($node);

            if (method_exists($this, 'continueTransfer')) {
                $continue = $this->continueTransfer($data);

                if (!$continue) {
                    usleep($this->usleepNotransfer);
                    continue;
                }
            }

            usleep($this->usleep);
            $guard = new ArrayObject();
            $guard[$this->legacyField] = $this->legacyValueForSave($data);

            foreach ($data as $key => $value) {
                if (in_array($key, $this->ignore)) {
                    continue;
                }

                if ($key == $this->primaryKey) {
                    continue;
                }

                $guardKey = $this->guardKey($key);
                $guard[$guardKey] = $this->guardValue($value, $guardKey, $data, $guard);
            }

            // Virtuals
            foreach ($this->virtuals as $field) {
                $guard[$field] = $this->guardVirtual($field, $data, $guard);
            }

            if (method_exists($this, 'beforeEntity')) {
                $this->beforeEntity($guard, $data);
            }

            if (!empty($this->primaryKey) || method_exists($this, 'legacyValueForSave')) {
                $entity = $this->findLegacy($data);
            } else {
                $entity = false;
            }

            if (!$entity) {
                if (method_exists($this, 'notFoundedContent')) {
                    $entity = $this->notFoundedContent($data);

                    if (!$entity) {
                        $entity = $this->table->newEntity((array)$guard, $this->patchEntityOptions());
                    }
                } else {
                    $entity = $this->table->newEntity((array)$guard, $this->patchEntityOptions());
                }
            } else {
                $this->table->patchEntity($entity, (array)$guard, $this->patchEntityOptions());
            }

            if (method_exists($this, 'beforeSave')) {
                $save = $this->beforeSave($entity, $data);

                if ($save === false) {
                    continue;
                }
            }

            if ($this->mode == 'save') {
                if ($this->table->save($entity)) {
                    if ($this->shell) {
                        $this->shell->out('Guardado entity <' . $entity->id . '> de ' . $entity->getSource());
                    }
                } else {
                    if ($this->shell) {
                    }
                }
            }


            if (method_exists($this, 'afterSave')) {
                $this->afterSave($entity, $data, $guard);
            }

            gc_collect_cycles();
        }
    }

    public function loadOriginTable($name)
    {
        return TableRegistry::get($name, [
            'connectionName' => $this->connectionName,
            'table' => $name
        ]);
    }

    public function iterateTable()
    {
        $originTable = $this->loadOriginTable($this->originTable);

        $limit = $this->tableLimit;
        $offset = 0;

        while (true) {
            $query = $originTable->find()
                ->limit($limit);

            if (!$this->dontSumOffset) {
                $query->offset($offset);
            }

            if (method_exists($this, 'beforeFind')) {
                $this->beforeFind($query);
            }

            $contents = $query->all();

            if ($contents->count() == 0) {
                break;
            }

            foreach ($contents as $content) {
                $data = $content->toArray();

                if (method_exists($this, 'continueTransfer')) {
                    $continue = $this->continueTransfer($data);

                    if (!$continue) {
                        continue;
                    }
                }

                $guard = new ArrayObject();
                $guard[$this->legacyField] = $this->legacyValueForSave($data);

                foreach ($data as $key => $value) {
                    if (in_array($key, $this->ignore)) {
                        continue;
                    }

                    if ($key == $this->primaryKey && !$this->dontUnsetId) {
                        continue;
                    }

                    $guardKey = $this->guardKey($key);
                    $guard[$guardKey] = $this->guardValue($value, $guardKey, $data, $guard);
                }

                // Virtuals
                foreach ($this->virtuals as $field) {
                    $guard[$field] = $this->guardVirtual($field, $data, $guard);
                }

                if (method_exists($this, 'beforeEntity')) {
                    $this->beforeEntity($guard, $data);
                }

                if (!empty($this->primaryKey) || method_exists($this, 'legacyValueForSave')) {
                    $entity = $this->findLegacy($data);
                } else {
                    $entity = false;
                }

                if (!$entity) {
                    if (array_key_exists('id', $guard) && !$this->dontUnsetId) {
                        unset($guard['id']);
                    }

                    $entity = $this->table->newEntity((array)$guard, $this->patchEntityOptions());
                } else {
                    $this->table->patchEntity($entity, (array)$guard, $this->patchEntityOptions());
                }

                if (method_exists($this, 'beforeSave')) {
                    $this->beforeSave($entity, $data);
                }

                if ($this->mode == 'save') {
                    if ($this->table->save($entity)) {
                        if ($this->shell) {
                            $this->shell->out('Guardado entity <' . $entity->id . '> de ' . $entity->getSource());
                        }
                    } else {
                        if ($this->shell) {
                            $this->shell->out('Error al guardar ' . $entity->getSource());

                            foreach ($entity->getErrors() as $field => $errors) {
                                foreach ($errors as $error) {
                                    $this->shell->out($field . ' - ' . $error);
                                }
                            }
                        }
                    }
                }

                if (method_exists($this, 'afterSave')) {
                    $this->afterSave($entity, $data, $guard);
                }

                gc_collect_cycles();
            }


            if (!$this->dontSumOffset) {
                $offset = $offset + $limit;
            }
        }
    }

    private function __getCsv()
    {
        $dir = TMP . 'xml/RpSalidaWeb.csv';
        $content = file_get_contents($dir);
        $rows = explode("\r", $content);

        $this->keys = explode("\t", $rows[0]);

        unset($rows[0]);

        $records = [];

        foreach ($rows as $row) {
            $records[] = $this->getRow($row);
        }

        return $records;
    }

    private function getCsvRow($row, $csvKeys)
    {
        $data = [];
        $cols = explode($this->csvDelimiter, trim($row));

        foreach ($cols as $key => $value) {
            if (!isset($csvKeys[$key])) {
                continue;
            }

            $data[$csvKeys[$key]] = trim($value);
        }

        return $data;
    }

    protected function getCsvData($csv = false)
    {
        if ($csv === false) {
            $csv = $this->csvFile;
        }
        // iso-8859-1
        $contents = file_get_contents($csv);
        $rows = explode("\r", $contents);
        $csvKeys = explode($this->csvDelimiter, $rows[0]);
        $csvKeys = array_map('trim', $csvKeys);

        unset($rows[0]);

        $records = [];

        foreach ($rows as $row) {
            $records[] = $this->getCsvRow($row, $csvKeys);
        }

        return $records;
    }

    /**
     * Itera un CSV
     *
     * @return void
     */
    public function iterateCsv()
    {
        $file = fopen($this->csvFile, 'r');
        $has_header = false;
        $csvKeys = [];
        $key_csv = 0;

        while (!feof($file)) {
            $row = fgets($file);

            if (!$has_header) {
                $csvKeys = explode($this->csvDelimiter, $row);
                $csvKeys = array_map('trim', $csvKeys);
                $has_header = true;
                continue;
            }

            $data = $this->getCsvRow($row, $csvKeys);
            $key_csv++;
            $this->keyIterateCsv = $key_csv;

            if (in_array($key_csv, $this->csvIgnoreRows)) {
                continue;
            }

            if (method_exists($this, 'continueTransfer')) {
                $continue = $this->continueTransfer($data);

                if (!$continue) {
                    continue;
                }
            }

            $guard = new ArrayObject();
            $guard[$this->legacyField] = $this->legacyValueForSave($data);

            foreach ($data as $key => $value) {
                if (in_array($key, $this->ignore)) {
                    continue;
                }

                if ($key == $this->primaryKey) {
                    continue;
                }

                $guardKey = $this->guardKey($key);
                $guard[$guardKey] = $this->guardValue($value, $guardKey, $data, $guard);
            }

            // Virtuals
            foreach ($this->virtuals as $field) {
                $guard[$field] = $this->guardVirtual($field, $data, $guard);
            }

            if (method_exists($this, 'beforeEntity')) {
                $this->beforeEntity($guard, $data);
            }

            if (!empty($this->primaryKey) || method_exists($this, 'legacyValueForSave')) {
                $entity = $this->findLegacy($data);
            } else {
                $entity = false;
            }

            if (!$entity) {
                if (method_exists($this, 'notFoundedContent')) {
                    $entity = $this->notFoundedContent($data);

                    if (!$entity) {
                        $entity = $this->table->newEntity((array)$guard, $this->patchEntityOptions());
                    }
                } else {
                    $entity = $this->table->newEntity((array)$guard, $this->patchEntityOptions());
                }
            } else {
                $this->table->patchEntity($entity, (array)$guard, $this->patchEntityOptions());
            }

            if (method_exists($this, 'beforeSave')) {
                $this->beforeSave($entity, $data);
            }

            if ($this->mode == 'save') {
                $this->table->save($entity);
            }

            if ($this->shell) {
                $this->shell->out('Guardado entity <' . $entity->id . '> de ' . $entity->getSource());
            }

            if (method_exists($this, 'afterSave')) {
                $this->afterSave($entity, $data, $guard);
            }

            gc_collect_cycles();
        }
    }


    public function buildData($node)
    {
        $data = simplexml_load_string($node);

        if (is_object($data)) {
            return get_object_vars($data);
        }

        return false;
    }

    public function guardKey($key)
    {
        if (array_key_exists($key, $this->relations)) {
            return $this->relations[$key];
        }

        return Inflector::underscore($key);
    }

    public function guardValue($value, $key, $data, $guard)
    {
        $method = '_get' . Inflector::camelize($key);

        if (method_exists($this, $method)) {
            return $this->$method($value, $data, $guard);
        }

        if (is_object($value)) {
            if (method_exists($value, '__toString')) {
                $value = $value->__toString();
            }
        }

        return $value;
    }

    public function guardVirtual($key, $data, $guard)
    {
        $method = '_get' . Inflector::camelize($key);

        if (method_exists($this, $method)) {
            return $this->$method($data, $guard);
        }

        return null;
    }

    public function findLegacy($data)
    {
        if (!$this->table->hasField($this->legacyField)) {
            return false;
        }

        $entity = $this->table->find()
            ->where([
                $this->legacyField => $this->legacyValueForSave($data)
            ])
            ->first();

        return $entity;
    }


    public function findLegacyTable($id, $table, $legacy_field = null)
    {
        if ($legacy_field === null) {
            $legacy_field = $this->legacyField;
        }

        $content = $this->loadModel($table)->find()
            ->where([
                $legacy_field => $this->legacyValue($id)
            ])
            ->first();

        if ($content) {
            return $content->id;
        }

        return null;
    }

    public function legacyValue($id)
    {
        return $this->legacyPrefix . $id;
    }

    public function legacyValueForSave($data)
    {
        if (empty($this->primaryKey)) {
            return;
        }

        return $this->legacyValue($data[$this->primaryKey]);
    }

    /**
     * Genera un collection para un XML dado
     *
     * @return void
     */
    public function setDataCollection($file)
    {
        $streamer = $this->getXml($file);
        $contents = [];

        while ($node = $streamer->getNode()) {
            $data = $this->buildData($node);
            $contents[] = $data;
        }

        return collection($contents);
    }


    /**
     * Añade a la entity las opciones de una relación belongsToMany 
     * que se encuentra en una collection anteriormente creada a partir de un XML
     *
     * p.e.
     * 'entity' => $entity, 
     * 'collection' => $this->authors, 
     * 'primaryKey' => 'IdArt', 
     * 'foreignKey' => 'IdT_Autor', 
     * 'classModel' => 'Plugin.Authors', 
     * 'property' => 'authors' 
     * 
     * @param array $options
     * @return void
     */
    public function setBelongsToMany(array $options)
    {
        extract($options);

        if (isset($collection)) {
            $datas = $collection->match([
                $primaryKey => $guard[$this->legacyField]
            ])->toArray();
        } elseif (isset($table)) {
            $Table = $this->loadOriginTable($table);
            $datas = $Table->find()
                ->where([
                    $primaryKey => $guard[$this->legacyField]
                ])
                ->toArray();
        }
        if (!empty($datas)) {

            $legacy_ids = collection($datas)->extract($foreignKey)->toArray();

            $legacy_ids = array_map(function ($value) {
                return $this->legacyValue($value);
            }, $legacy_ids);

            $ids = $this->loadModel($classModel)->find()
                ->where([
                    $this->legacyField . ' IN' => $legacy_ids
                ])
                ->extract('id')
                ->toArray();

            $guard[$property] = [
                '_ids' => $ids
            ];
        } else {
            $guard[$property] = [
                '_ids' => []
            ];
        }
    }


    public function truncate()
    {
        $limit = 400;
        $offset = 0;

        if ($this->table->hasBehavior('UploadJsonable')) {
            $uploads = $this->table->uploadsFields();
        } else {
            $uploads = false;
        }


        while (true) {
            $primaryKey = $this->table->primaryKey();
            $query = $this->table->find()
                ->limit($limit)
                ->offset($offset);

            if (!is_array($primaryKey)) {
                $query->order([
                    $this->table->alias() . '.' . $primaryKey
                ]);
            }

            if (method_exists($this, 'findTruncate')) {
                $this->findTruncate($query);
            }

            $contents = $query->all();

            if ($contents->count() == 0) {
                break;
            }

            foreach ($contents as $content) {
                if ($uploads) {
                    foreach ($uploads as $upload) {
                        if (!empty($content->$upload)) {
                            if ($content->$upload instanceof \Upload\Model\Entity\AssetArray) {
                                $content->$upload = (array)$content->$upload;

                                if (empty($content->$upload)) {
                                    continue;
                                }
                            }

                            if (!is_numeric(key($content->$upload))) {
                                $assets = [$content->$upload];
                            } else {
                                $assets = $content->$upload;
                            }

                            foreach ($assets as $asset) {
                                foreach ((array)$asset->paths as $paths) {
                                    foreach ((array)$paths as $path) {
                                        if (file_exists(ROOT . '/webroot/' . $path)) {
                                            unlink(ROOT . '/webroot/' . $path);
                                        }
                                        if (file_exists(ROOT . '/webroot/' . str_replace('big_', 'mobile_big_', $path))) {
                                            unlink(ROOT . '/webroot/' . str_replace('big_', 'mobile_big_', $path));
                                        }
                                    }
                                }

                                $this->getTableLocator()->get('Upload.Uploads')->deleteAll([
                                    'id' => $asset->id
                                ]);
                            }
                        }
                    }
                }

                $this->table->delete($content);
            }

            $offset = $offset + $limit;
        }
    }


    public function patchEntityOptions()
    {
        return [];
    }

    public function setError($error)
    {
        $this->errors[] = $error;
    }
}
