<?php 

namespace Manager\TestSuite;

use Cake\TestSuite\TestCase;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Inflector;
use Exception;

/**
 * Cake TestCase class
 *
 * Realiza varias asserciones para comprobar que las configuraciones de CRUD son correctas
 *
 */
abstract class CrudTestCase extends TestCase
{
  public function assertCrudDataEdit( $action, $table)
  {
    $data = $table->crud->serialize( $action);
    $this->assertTrue( is_array( $data));
    $this->assertTrue( array_key_exists( 'view', $data));
    $this->assertTrue( array_key_exists( 'columns', $data ['view']));
  }

  public function assertCrudDataIndex( $action, $table)
  {
    $data = $table->crud->serialize( $action);
    $this->assertTrue( is_array( $data));
    $this->assertTrue( array_key_exists( 'view', $data));
    $this->assertTrue( array_key_exists( 'fields', $data ['view']));
  }

}