<?php
/**
 * Esta clase se encarga de realizar tareas de recopilación de Templates de crud
 *
 * Tareas que realiza:
 *   1. Busca y recopila en un array los paths a todos los templates para las vistas
 *   2. Busca y recopila en un array los paths a todos los templates de los campos
 *
 * Los templates deben ubicarse, en la aplicación o en cada plugin, en Template/Element/crud/ y Template/Element/crud/fields/
 */

namespace Manager\Lib;

use Cake\Core\Plugin;
use Cake\Filesystem\Folder;

class Templates
{
  protected static $_templates = [];

  public static function get()
  {
    // self::fields();
    self::views();
    self::indexes();
    return self::$_templates;
  }

  public static function fields()
  {
    $path = ROOT . 'src' .DS. 'Template' .DS. 'Element' .DS. 'crud' .DS. 'fields';
    self::add( $path, 'crud/fields/');

    foreach( Plugin::loaded() as $plugin)
    {
      $path = Plugin::path( $plugin) . 'src' .DS. 'Template' .DS. 'Element' .DS. 'crud' .DS. 'fields';
      self::add( $path, 'crud/fields/', $plugin);
    }
  
    return self::$_templates;    
  }

  public static function searches()
  {
    $path = ROOT . 'src' .DS. 'Template' .DS. 'Element' .DS. 'crud' .DS. 'searches';
    self::add( $path, 'crud/searches/');

    foreach( Plugin::loaded() as $plugin)
    {
      $path = Plugin::path( $plugin) . 'src' .DS. 'Template' .DS. 'Element' .DS. 'crud' .DS. 'searches';
      self::add( $path, 'crud/searches/', $plugin);
    }
  
    return self::$_templates;    
  }

  public static function indexes()
  {
    $path = ROOT . 'src' .DS. 'Template' .DS. 'Element' .DS. 'crud' .DS. 'indexes';
    self::add( $path, 'crud/indexes/');

    foreach( Plugin::loaded() as $plugin)
    {
      $path = Plugin::path( $plugin) . 'src' .DS. 'Template' .DS. 'Element' .DS. 'crud' .DS. 'indexes';
      self::add( $path, 'crud/indexes/', $plugin);
    }
  
    return self::$_templates;    
  } 

  public static function views()
  {
    $path = ROOT . 'src' .DS. 'Template' .DS. 'Element' .DS. 'crud';
    self::add( $path, 'crud/');

    foreach( Plugin::loaded() as $plugin)
    {
      $path = Plugin::path( $plugin) . 'src' .DS. 'Template' .DS. 'Element' .DS. 'crud';
      self::add( $path, 'crud/', $plugin);
    }
  
    return self::$_templates;    
  } 

  public static function add( $path, $folder, $plugin = false)
  {
    if( !is_dir( $path))
    {
      return;
    }

    $Folder = new Folder( $path);
    $files = $Folder->read( true, true, true);
    $folders = !empty( $files [0]) ? $files [0] : [];
    $files = $files [1];

    $files = array_map( function( $file) use ($path){
      $file = str_replace( '.ctp', '', str_replace( $path, '', $file));
      return substr( $file, 1);
    }, $files);

    $return = array();

    foreach( $files as $file)
    {
      $key = strpos( $file, '/') !== false
        ? substr( $file, strpos( $file, '/') + 1)
        : $file;

      $filepath = str_replace( 'crud/', '', $folder) . $key;

      $template = $plugin ? $plugin .'/'. $filepath : $filepath;
      self::$_templates [] = [
        'path' => $plugin ?  $plugin .'.'. $folder : $folder,
        'template' => $template,
        'element' => $key
      ];
    }

    foreach( $folders as $_path)
    {
      $_folder = str_replace( $path, '', $_path);
      // _d( $_folder);
      self::add( $_path, $folder . substr( $_folder, 1) . '/', $plugin);
    }
  }

}