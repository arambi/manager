<?php 
namespace Manager\Lib;

use Cake\Core\Configure;


class Partner
{
  public static function set( $config)
  {
    Configure::write( 'WebPartner', $config);
  }

  public static function get( $key)
  {
    return Configure::read( 'WebPartner.'. $key);
  }
}