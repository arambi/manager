<?php

namespace Manager\Lib;

use Cake\ORM\Table;
use Cake\Utility\Inflector;
use Manager\Lib\Templates;
use Manager\FieldAdapter\FieldAdapterRegistry;
use Cake\Routing\Router;
use Cake\Core\App;
use Closure;
use \ArrayObject;
use Cake\Collection\Collection;
use Manager\Crud\FieldCollector;
use Manager\Crud\View\ViewCollector;
use I18n\Lib\Lang;


/**
 * Class que se instancia como propiedad en un Table
 *
 * El objeto contendrá todas las propiedades de la edición del Model en un sistema de AngularJS
 *
 * El objeto se instancia automáticamente al inyectar Manager.CrudableBehavior a un model
 *
 */

class CrudConfig
{

    protected $_viewCollector;

    protected $_fieldCollector;


    /**
     * Los campos que se van a setear en la vista
     *   
     * @var array
     */
    private $__viewFields = [];
    /**
     * El nombre humano de la configuración
     * Contiene una clave 'singular' y otra 'plural'
     * 
     * @var array
     */
    protected $_name = [];

    /**
     * Función que devolverá el enlace al contenido
     * @var void
     */
    protected $_contentLink;

    /**
     * Las asociaciones que usará el model en la edición
     *     
     * @var array
     */
    protected $_associations = [];


    protected $_queryParams = [];

    protected $_associationsType = [
        'BelongsTo' => [],
        'HasMany' => [],
        'BelongsToMany' => [],
    ];

    /**
     * Ficheros extra de JS que se cargarán con AngularJS 
     * @var array
     */
    protected $_jsFiles = [];


    /**
     * Datos por defecto del contenido cuando se genera uno nuevo.
     * Serán los datos que se pasen a la vista cuando se crea un nuevo contenido
     * @var array
     */
    protected $_defaults = [];

    /**
     * Array de funciones que serán ejecutadas cuando se cree una nueva entidad
     * @var array
     */
    protected $_defaultFunctions = [];

    /**
     * Array de funciones que se ejecutarán cuando se inicialice el proceso de crud
     *
     * @var array
     */
    protected $_initializeFunctions = [];

    private $__adapters = [
        'ckeditor' => 'Manager.Ckeditor',
    ];

    /**
     * Los labels humanos que tendrán los botones de acciones
     * @var array
     */
    private $__actionLabels = [];

    /**
     * El plugin de la Table
     * Este dato no lo facilita el model de Cake, por eso hay que calcularlo
     * @var boolean
     */
    private $__plugin = false;

    /**
     * La Table asociada
     * @var boolean|Table object
     */
    protected $_table = false;


    /**
     * Los errores de la entity, si los hubiera
     * @var array
     */
    protected $_errors = [];

    /**
     * La entity actual, si la hubiera
     * @var Entity | false
     */
    protected $_entity = false;


    protected $_adminUrl = false;

    /**
     * Configuración extra que puede ser usada para cuestiones particulares en cada model
     * @var array
     */
    protected $_config = [];

    /**
     * Variable donde se guardarán los datos del contenido que se está editando
     * @var array
     */
    protected $_content = [];


    /**
     * El orden del contenido en el admin
     * @var array
     */
    protected $_order = [];

    /**
     * El límite de registros en el listado del admin
     * @var array
     */
    protected $_limit = [];

    /**
     * El orden por defecto en el frontend
     * @var array
     */
    protected $_orderFront = [];

    /**
     * Array con los contains que se harán en el admin
     * Será de tipo 'name' => $contain, donde 'name' es el nombre de la vista (p.e. 'index')
     * 
     * @var array
     */
    protected $_contains = [];

    /**
     * Actual action que se está ejectuando en el controller (p.e index, update, create)
     * 
     * @var string
     */
    protected $_action;

    /**
     * Filtros de búsqueda, que pueden ser añadidos en los índices de contenidos como búsqueda avanzada
     * @var array
     */
    protected $_filters = [];


    protected $_sortable = false;

    protected $_data = [];

    /**
     * Contructor
     * 
     * @param Table $table
     */
    public function __construct(Table $table)
    {
        $this->_table = $table;
        $this->_fieldCollector = new FieldCollector($table);
        $this->_viewCollector = new ViewCollector($this->_fieldCollector);
        $this->__plugin = $this->getPlugin($this->_table);

        $this->__setActionLabels();

        $this->__setAssociationsType();
    }

    public function beforeRender()
    {
        if ($link = $this->contentLink()) {
            $this->setContent([
                'content_link' => $link
            ]);
        }
    }

    public function set($key, $value)
    {
        $this->_data[$key] = $value;
    }

    public function get($key)
    {
        if (isset($this->_data[$key])) {
            return $this->_data[$key];
        }
    }

    /**
     * Devuelve la table asociada al crud
     * 
     * @return Table 
     */
    public function table()
    {
        return $this->_table;
    }

    /**
     * Get/Set el valor de la acción actual del controller
     * 
     * @param  string | null $action
     * @return string
     */
    public function action($action = null)
    {
        if ($action) {
            $this->_action = $action;
        }

        return $this->_action;
    }

    public function contentLink($method = null)
    {
        if ($method === null) {
            if (is_callable($this->_contentLink)) {
                $method = $this->_contentLink;

                return $method($this->_entity);
            }
        }

        if ($method === true) {
            $this->_contentLink = function ($content) {
                if (!$content) {
                    return null;
                }

                return Router::url([
                    'prefix' => false,
                    'plugin' => $this->plugin(),
                    'controller' => $this->_table->alias(),
                    'action' => 'view',
                    'slug' => $content->slug
                ], true);
            };
        }

        if (is_callable($method)) {
            $this->_contentLink = $method;
        }
    }

    /**
     * Devuelve la instancia de ViewCollector
     * 
     * @return ViewCollector
     */
    public function viewCollector()
    {
        return $this->_viewCollector;
    }

    /**
     * Devuelve la instancia de FieldCollector
     * 
     * @return FieldCollector
     */
    public function fieldCollector()
    {
        return $this->_fieldCollector;
    }


    public function sortable($value = null)
    {
        if ($value !== null) {
            $this->_sortable = $value;
        }

        return $this->_sortable;
    }

    /**
     * Setea un valor para el array de content, es decir, del contenido que se está editando
     * 
     * @param array $data
     */
    public function setContent($data)
    {
        $data = json_decode(json_encode($data), true);
        $this->_content = array_replace_recursive($this->_content, $data);
    }

    /**
     * Devuelve el contenido que se está editando
     * 
     * @return array
     */
    public function getContent()
    {
        return $this->_content;
    }



    private function __setActionLabels()
    {
        $this->__actionLabels = [
            'create' => 'Nuevo',
            'index' => 'Listado',
        ];
    }

    /**
     * Añade keys de configuración de datos para $this->_config
     * 
     * @param array $data key => values data
     */
    public function addConfig($data)
    {
        $this->_config = array_merge($this->_config, $data);
    }

    public function getConfig($key)
    {
        if (array_key_exists($key, $this->_config)) {
            return $this->_config[$key];
        }

        return null;
    }

    /**
     * Devuelve el plugin del model, o false si no lo hubiera
     * 
     * @param  Table $table
     * @return string | false
     */
    public function getPlugin($table)
    {
        // Calcula el plugin
        $class = App::className(get_class($table));
        $explode = explode('\\', $class);

        if ($explode[0] != 'App') {
            return $explode[0];
        }

        return false;
    }

    public function actionLabel($key)
    {
        return $this->__actionLabels[$key];
    }

    /**
     * Get/Set
     * Setea o toma las asociaciones que usará el model en la edición
     * 
     * @param  array/null $data
     * @return array
     */
    public function associations($data = null)
    {
        if ($data !== null && is_array($data)) {
            $this->addAssociations($data);
        }

        $this->__setAssociationsType();
        return $this->_associations;
    }

    public function removeAssociations()
    {
        $this->_associations = [];
        return $this;
    }

    public function addAssociations($data)
    {
        $this->_associations = array_merge($this->_associations, $data);
        $this->_associations = array_unique($this->_associations);
        return $this;
    }

    /**
     * Añade ficheros JS a la configuración
     * 
     * @param array $data
     */
    public function addJsFiles($data)
    {
        $this->_jsFiles = array_merge($this->_jsFiles, $data);

        return $this;
    }


    public function order($order = null)
    {
        if ($order !== null) {
            $this->_order = $order;
        }

        return $this->_order;
    }

    public function limit($limit = null)
    {
        if ($limit !== null) {
            $this->_limit = $limit;
        }

        return $this->_limit;
    }

    public function orderFront($order = null)
    {
        if ($order !== null) {
            $this->_orderFront = $order;
        }

        return $this->_orderFront;
    }

    /**
     * Get/Set
     * Setea o toma el plugin al que pertenece la tabla
     * 
     * @param  string|boolean $plugin 
     * @return string
     */
    public function plugin($plugin = false)
    {
        if ($plugin !== false) {
            $this->__plugin = $plugin;
        }

        return $this->__plugin;
    }

    /**
     * Devuelve la URL base para la edición
     *
     * @return string
     */
    public function adminUrl($url = false)
    {
        if (!$url && !$this->_adminUrl) {
            if ($this->plugin()) {
                $controller = Inflector::underscore($this->plugin()) . '/' . $this->controllerName();
            }

            $this->_adminUrl = '/admin/' . $controller . '/';
        } elseif ($url) {
            $this->_adminUrl = $url;
        }

        return $this->_adminUrl;
    }

    public function controllerName($table = null)
    {
        if (!$table) {
            $table = $this->_table;
        }

        $class = get_class($table);
        $parts = explode("\\", $class);

        $model = str_replace('Table', '', $parts[count($parts) - 1]);
        return Inflector::tableize($model);
    }

    /**
     * Get/Set
     * Setea o toma los valores por defecto que tendrá un contenido al crear uno nuevo
     * 
     * @param  array/null/callable $data
     * @return array
     */
    public function defaults($data = null)
    {
        if ($data !== null && is_array($data)) {
            $this->_defaults = array_merge($this->_defaults, $data);
        } elseif ($data !== null && is_callable($data)) {
            $this->_defaultFunctions[] = $data;
        } elseif ($data === null) {
            $defaults = $this->_defaults;

            foreach ($this->_defaultFunctions as $func) {
                $defaults = $func($defaults);
            }

            return $defaults;
        }
    }

    public function addInitializeFunction(callable $function)
    {
        $this->_initializeFunctions[] = $function;
    }

    public function initializeFunctions()
    {
        foreach ($this->_initializeFunctions as $function) {
            $function($this->_table);
        }
    }

    public function unsetDefaultFunctions()
    {
        $this->_defaultFunctions = [];
    }

    /**
     * Indica si el model tiene seteada el model asociado, es decir, si ha sido definido en $this->associations()
     *   
     * @param string  $assoc El alias de la asociación
     * @return boolean
     */
    public function hasAssociation($assoc)
    {
        return in_array($assoc, $this->_associations);
    }

    public function setEntity($entity)
    {
        $this->_entity = $entity;
    }


    /**
     * Devuelve los errores que pueda tener la entity actual
     * 
     * @return array
     */
    public function entityErrors()
    {
        if (!$this->_entity) {
            return false;
        }

        $errors = $this->_entity->errors();

        if (empty($errors)) {
            $this->_errors = false;
            return $this->_errors;
        }

        foreach (Lang::iso3() as $lang => $name) {
            if (array_key_exists($lang, $errors)) {
                foreach ($errors[$lang] as $field => $_errors) {
                    foreach ($_errors as $error_name => $message) {
                        $errors[$field][$error_name . '_' . $lang] = $message . ' (' . $name . ')';
                    }
                }

                unset($errors[$lang]);
            }
        }

        // Setea los errores en la propiedad
        $this->_errors = $errors;

        return $this->_errors;
    }

    /**
     * Indica si la acción está seteada en las vistas
     * 
     * @param  string  $action
     * @return boolean
     */
    public function hasAction($action)
    {
        return array_key_exists($action, $this->_views) || array_key_exists($action, $this->_indexes);
    }

    /**
     * Añade campos a la configuración
     * Sobreescribirá los campos que ya hayan sido definidos 
     * 
     * @param array $data
     */
    public function addFields($data)
    {
        foreach ($data as $key => $values) {
            $this->_fieldCollector->add($key, $values);
        }

        return $this;
    }

    /**
     * Añade nuevos filtros dado un key y un closure como valor
     * 
     * @param array $data Par key => closure
     */
    public function addFilters($data)
    {
        foreach ($data as $key => $values) {
            $values['key'] = $key;
            $this->_filters[$key] = $values;
        }

        return $this;
    }

    public function getFilter($key)
    {
        if (isset($this->_filters[$key])) {
            return $this->_filters[$key];
        }
    }

    public function setQueryParams($params)
    {
        $this->_queryParams = $params;
    }


    public function getQueryParams()
    {
        return $this->_queryParams;
    }

    public function getQueryParam($key)
    {
        if (isset($this->_queryParams[$key])) {
            return $this->_queryParams[$key];
        }
    }

    /**
     * Setea el nombre de la configuración
     * 
     * @param array $data
     */
    public function setName($data)
    {
        $this->_name = $data;
        return $this;
    }

    public function getName()
    {
        return $this->_name;
    }

    /**
     * Añade una vista de edición a la configuración
     * Sobreescribirá las configuraciones añadidas previamente 
     *   
     * @param string  $name    El nombre computable de la vista (p.e. 'update', 'create')
     * @param array  $data    Los datos de la configuración
     * @param mixed|boolean|array $aliases Un array con otros $name que usarán los mismos valores. 
     *                                     Por ejemplo, si se va a usar la misma vista para 'update' que para 'create'
     */

    public function addView($name, $data, $aliases = false)
    {
        $this->_viewCollector->add('edit', $name, $data, $aliases);
        return $this;
    }

    public function addColumn($aliases, $data, $options = [])
    {
        $this->_viewCollector->addColumn($aliases, $data, $options);
        return $this;
    }


    public function addBoxToColumn($aliases, $key, $elements)
    {
        $this->_viewCollector->addBoxToColumn($aliases, $key, $elements);
        return $this;
    }

    public function addElementsToBox($aliases, $column, $box, $elements, $before = false)
    {
        $this->_viewCollector->addElementsToBox($aliases, $column, $box, $elements, $before);
        return $this;
    }

    public function removeElementFromBox($aliases, $column, $box, $element)
    {
        $this->_viewCollector->removeElementFromBox($aliases, $column, $box, $element);
        return $this;
    }

    public function removeElement($aliases, $element)
    {
        $this->_viewCollector->removeElement($aliases, $element);
        return $this;
    }

    public function getView($name)
    {
        return $this->_viewCollector->get($name);
    }

    /**
     * Construye una vista desde los datos básicos
     * 
     * @param  string $name El nombre del key de la vista
     * @return array
     */
    public function buildView($name)
    {
        return $this->_viewCollector->build($name);
    }


    /**
     * Añade un index a la configuración
     * Sobreescribirá las configuraciones añadidas previamente
     * 
     * @param string  $name    El nombre computable del index (p.e. 'index', 'categories')
     * @param array  $data    Los datos de la configuración
     * @param mixed|boolean|array $aliases Un array con otros $name que usarán los mismos valores. 
     *                                     Por ejemplo, si se va a usar el mismo index para 'index' que para 'categories'
     */
    public function addIndex($name, $data, $aliases = false)
    {
        $this->_viewCollector->add('index', $name, $data, $aliases);
        return $this;
    }

    public function addFieldToIndex($aliases, $key, $beforeField = null)
    {
        $this->_viewCollector->addFieldToIndex($aliases, $key, $beforeField);
    }

    public function resetIndexElements($aliases, $key, $beforeField = null)
    {
        $this->_viewCollector->resetIndexElements($aliases, $key, $beforeField);
    }

    public function addFieldToView($aliases, $key, $beforeField)
    {
        $this->_viewCollector->addFieldToView($aliases, $key, $beforeField);
    }

    /**
     * Añade un contain para un vista
     * 
     * @param string  $name     El nombre de la vista
     * @param array   $contain  El contain para la búsqueda
     */
    public function addContain($name, $contain)
    {
        if (isset($this->_contains[$name])) {
            $this->_contains[$name] = array_merge($this->_contains[$name], $contain);
        } else {
            $this->_contains[$name] = $contain;
        }

        return $this;
    }

    /**
     * Devuelve un contain para la vista dada
     * 
     * @param  string $name El nombre de la vista (p.e. index)
     * @return array       
     */
    public function getContain($name)
    {
        if (isset($this->_contains[$name])) {
            return $this->_contains[$name];
        }

        return false;
    }


    /**
     * Se encarga de serializar los datos para que sean puestos en una petición json y ser usados por AngularJS
     * 
     * @return json
     */
    public function serialize($action)
    {
        if ($this->_viewCollector->has($action)) {
            $view = $this->buildView($action);
        } else {
            $view = false;
        }

        $return = [
            'name' => $this->_name,
            'view' => $view,
            'jsFiles' => $this->_jsFiles,
            'model' => $this->modelInfo(),
            'plugin' => $this->plugin(),
            'adminUrl' => $this->adminUrl()

        ];

        $return = array_merge($this->_config, $return);

        return $return;
    }

    /**
     * Retorna un array con información del Model. El propósito es setearlo en la vista
     * 
     * @return array
     */
    public function modelInfo()
    {
        return [
            'alias' => $this->_table->alias(),
            'displayField' => $this->_table->getDisplayField(),
            'primaryKey' => $this->_table->getPrimaryKey(),
        ];
    }

    private function __setAssociationsType()
    {
        foreach ($this->_associationsType as $type => $data) {
            $assocs = $this->_table->associations()->getByType($type);

            if (!empty($assocs)) {
                foreach ($assocs as $assoc) {
                    $this->_associationsType[$type][] = $assoc->property();
                }
            }
        }
    }

    /**
     * Devuelve una lista de options en forma de árbol
     * Válido para los models con parent_id y position 
     * 
     * @param  Table $table
     * @return array
     */
    public function treeList($table)
    {
        $options = [];

        // Si la table es la misma que el crud entonces se pasa como argumento el entity actual (si lo hubiera)
        // para que no se busque 
        if ($table->alias() == $this->table()->alias()) {
            $options['entity'] = $this->getContent();
        }

        $return[0] =  __d('admin', '-- Ninguna --');
        $return += $table->find('tree', $options)->toArray();
        return $return;
    }


    /**
     * Devuelve el tipo de asociación de un campo 
     * 
     * Este campo tiene que tener como tipo el nombre de model asociado y en mayúsculas (p.e. `categories`)
     * @param  array $field Datos del campo
     * @return string
     */
    public function getAssociationType($key)
    {
        foreach ($this->_associationsType as $type => $assocs) {
            if (in_array($key, $assocs)) {
                return $type;
            }
        }

        return false;
    }
}
