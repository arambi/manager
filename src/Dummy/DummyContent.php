<?php 

namespace Manager\Dummy;

use joshtronic\LoremIpsum;
use Cake\ORM\TableRegistry;
use I18n\Lib\Lang;
use Cake\Core\Configure;

/**
 * Clase para crear contenidos falsos
 */

class DummyContent
{
  protected $_ipsum;

  protected $_table;

  protected $_limit;

  protected $_data;

/**
 * Se le ha de pasar el nombre del model con el formato plugin.model
 * 
 * @param string $model
 */
  public function __construct( $model)
  {
    $this->_ipsum = new LoremIpsum();
    $this->_table = TableRegistry::get( $model);
    $this->Uploads = TableRegistry::get( 'Upload.Uploads');
  }

/**
 * Setea el límite de registros a guardar
 * 
 * @param integer $limit
 */
  public function setLimit( $limit)
  {
    $this->_limit = $limit;
    return $this;
  }

/**
 * Borra todos los registros del modelo
 * 
 * @return
 */
  public function truncate()
  {
    $contents = $this->_table->find();

    foreach( $contents as $content)
    {
      $this->_table->delete( $content);
    }
  }

/**
 * Guarda los registros
 * @return 
 */
  public function save()
  {
    for( $i = 0; $i < $this->_limit; $i++)
    {
      $this->generateData();
      $entity = $this->_table->getNewEntity( $this->_data);
      $this->_table->saveContent( $entity);
      $array = $this->_table->find( 'array')->where([$this->_table->alias() .'.id' => $entity->id])->first();
      $content = $this->_table->find( 'content')->where([$this->_table->alias() .'.id' => $entity->id])->first();
      $this->_table->patchContent( $content, $array);
      $this->_table->saveContent( $content);
      $this->resetData();
    }
  }

/**
 * Vacía el data
 */
  public function resetData()
  {
    $this->_data = [];
  }

/**
 * Genera los datos falsos, recorriendo cada uno de los campos definidos en el crud
 * @return 
 */
  public function generateData()
  {
    $fields = $this->_table->crud->fieldCollector()->fields();

    $data = [];

    foreach( $fields as $key => $field)
    {
      $build = $this->_table->crud->fieldCollector()->build( $key, 'edit');
      $type = $build ['type'];
      $method = '_get'. ucfirst( $type);

      if( method_exists( $this, $method))
      {
        $this->$method( $build);
      }
    }
  }
  
/**
 * Contenido para tipo string
 * 
 * @param  array $field 
 * @return 
 */
  protected function _getString( $field)
  {
    if( $field ['translate'])
    {
      foreach( Lang::get() as $lang)
      {
        $this->_data ['_translations'][$lang->iso3][$field ['key']] = $this->_ipsum->words( rand( 3, 6));
      }
    }
    else
    {
      $this->_data [$field ['key']] = $this->_ipsum->words( rand( 3, 6));
    }
      
  }

/**
 * Contenido para tipo ckeditor
 * 
 * @param  array $field 
 * @return 
 */
  protected function _getCkeditor( $field)
  {
    if( $field ['translate'])
    {
      foreach( Lang::get() as $lang)
      {
        if( in_array( $field ['key'], ['summary', 'summary2']))
        {
          $this->_data ['_translations'][$lang->iso3][$field ['key']] = $this->_ipsum->words( rand( 15, 50));
        }
        else
        {
          $this->_data ['_translations'][$lang->iso3][$field ['key']] = $this->_ipsum->paragraphs( rand( 1, 3));
        }
      }
    }
    else
    {
      if( in_array( $field ['key'], ['summary', 'summary2']))
      {
        $this->_data [$field ['key']] = $this->_ipsum->words( rand( 15, 50));
      }
      else
      {
        $this->_data [$field ['key']] = $this->_ipsum->paragraphs( rand( 1, 3));
      }
    }
  }


      
/**
 * Contenido para tipo text
 * 
 * @param  array $field 
 * @return 
 */
  protected function _getText( $field)
  {
    if( $field ['translate'])
    {
      foreach( Lang::get() as $lang)
      {
        $this->_data ['_translations'][$lang->iso3][$field ['key']] = $this->_ipsum->words( rand( 15, 50));
      }
    }
    else
    {
      $this->_data [$field ['key']] = $this->_ipsum->words( rand( 15, 50));
    }

      
  }

/**
 * Contenido para tipo hasMany
 * 
 * @param  array $field 
 * @return 
 */
  protected function _getHasMany( $field)
  {
    if( $field ['key'] == 'blocks')
    {
      $this->_getBlocks();
    }
    elseif( $field ['key'] != 'slugs')
    {
    }
  }


/**
 * Contenido para tipo date
 * 
 * @param  array $field 
 * @return 
 */
  protected function _getDate( $field)
  {
    $date = date( 'Y-m-d', strtotime( '-'. rand( 1, 10) . ' day'));
    $this->_data [$field ['key']] = $date;
  }

/**
 * Contenido para tipo date
 * 
 * @param  array $field 
 * @return 
 */
  protected function _getDatetime( $field)
  {
    $date = date( 'Y-m-d H:i:s', strtotime( '-'. rand( 1, 10) . ' day'));
    $this->_data [$field ['key']] = $date;
  }

/**
 * Contenido para tipo boolean
 * Si es published siempre lo pone a 1
 * 
 * @param  array $field 
 * @return 
 */
  protected function _getBoolean( $field)
  {
    if( $field ['key'] == 'published')
    {
      $this->_data [$field ['key']] = 1;
    }
    else
    {
      $this->data [$field ['key']] = rand( 0,1);
    }
  }

/**
 * Contenido para tipo upload
 * 
 * @param  array $field 
 * @return 
 */
  protected function _getUpload( $field)
  {
    $data = $this->asset( $field ['config']['type']);
    $this->_data [$field ['key']] = $data;
  }

  protected function _getUploads( $field)
  {
    $data = [];
    $limit = rand( 2, 5);

    for( $i = 0; $i < $limit; $i++)
    {
      $data [] = $this->asset( $field ['config']['type']);
    }
    
    $this->_data [$field ['key']] = $data;
  }

/**
 * Toma un contenido de http://loremflickr.com/900/650/nature y lo guarda en la tabla uploads
 * 
 * @param  array $field 
 * @return 
 */

/**
 * Toma un contenido de http://loremflickr.com/900/650/nature y lo guarda en la tabla uploads
 * 
 * @param  string  $content_type El tipo de upload
 * @return array
 */
  public function asset( $content_type)
  {
    $this->Uploads->addBehavior( 'Upload.Upload', [
      'filename' => Configure::read( 'Upload.'. $content_type .'.config')
    ]);

    $url = 'http://placeimg.com/900/650/any';

    $filename = 'photo_'. rand( 0, 999999999) . '.jpg';

    system( 'wget --timeout 2 -P '. ROOT .' -O tmp/'. $filename .' ' . $url);
    $mime = finfo_file( finfo_open( FILEINFO_MIME_TYPE), TMP .$filename);
    $filesize = filesize( TMP . $filename);

    if( $mime == 'text/html')
    {
      $this->Uploads->removeBehavior( 'Upload');
      return false;
    }

    $data = [
      'filename' => [
        'name' => $filename,
        'type' => $mime,
        'size' => $filesize,
        'tmp_name' => TMP . $filename,
        'error' => 0,
      ],
      'content_type' => $content_type,
    ];


    $upload = $this->Uploads->newEntity( $data);
    $saved = $this->Uploads->save( $upload);
    $new = $this->Uploads->findById( $upload->id)->first();      

    $this->Uploads->removeBehavior( 'Upload');
    $new->set( 'paths', $new->paths);

    $data = $new->toArray();
    return $data;
  }

/**
 * Genera un bloque de texto
 * 
 * @return 
 */
  protected function _getBlocks()
  {
    $block = [
      'subtype' => 'text',
    ];

    foreach( Lang::get() as $lang)
    {
      $block ['_translations'][$lang->iso3]['body'] = $this->_ipsum->paragraphs( rand( 1, 3), 'p');
    }

    $data = [
      0 => [
        'position' => 1,
        'columns' => [
          [
            'blocks' => [
              0 => $block
            ],
            'position' => 1,
            'cols' => 12,
          ]
        ]
      ]
    ];

    $this->_data ['rows'] = $data;
  }

}