<?php

namespace Manager\Model\Entity;

use Cake\Routing\Router;
use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;
use Cake\Core\App;
use Cake\I18n\Time;
use Cake\Utility\Text;
use Manager\Editable\Editable;
use Manager\View\Helper\EditableHelper;

trait CrudEntityTrait
{

    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
        $this->accessible('_translations', true);
    }

    protected function _getFullTitle()
    {
        $table = $this->getTable();
        $alias = $table->displayField();
        return $this->$alias;
    }

    public function menuTitle()
    {
        return $this->title;
    }

    /**
     * Devuelve el Table asociado a la Entity
     * 
     * @return Cake\ORM\Table
     */
    public function getTable()
    {
        $source = $this->getSource();

        if (strpos($source, '.') === false) {
            $classname = get_class($this);
            $parts = explode('\\', $classname);

            if ($parts[0] != 'App') {
                $plugin = $parts[0];
                $source = $plugin . '.' . $source;
            }
        }

        return TableRegistry::get($source);
    }

    public function getAccesibles()
    {
        return $this->_accessible;
    }

    public function getBelongsTo($value, $key)
    {
        if ($value !== null) {
            return $value;
        }
        
        $assoc = TableRegistry::getTableLocator()
            ->get($this->getSource())
            ->associations()
            ->getByProperty($key);

        $foreignKey = $assoc->getForeignKey();
        $table = $assoc->getTarget();
        $entity = $table->find()
            ->where([
                $table->getAlias() .'.'. $table->getPrimaryKey() => $this->get($foreignKey)
            ])
            ->first();
        
        $this->set('key', $entity);
        return $entity;
    }


    public function getOption($field_name)
    {
        $table = $this->getTable();
        $field = $table->crud->fieldCollector()->get($field_name);
        $options = $field->value('options');

        if (strpos($field_name, '.')) {
            list($one, $two) = explode('.', $field_name);
            $value = $this->$one->$two;
        } else {
            $value = $this->$field_name;
        }

        return $options[$value];
    }

    /**
     * Devuelve una fecha dado una propiedad
     * 
     * @param  string $prop
     * @return string
     */
    public function date($prop)
    {
        $date = Time::parse($this->get($prop));
        return $date->i18nFormat([\IntlDateFormatter::LONG, \IntlDateFormatter::NONE]);
    }

    /**
     * Corta un texto de la propiedad dada, hasta un máximo de caracteres 
     * 
     * @see Cake\Utility\Text::truncate()
     * @param  string $prop
     * @return string
     */
    public function truncate($prop, $limit = 100, array $options = [])
    {
        $default = [
            'exact' => false
        ];

        $options = array_merge($default, $options);
        return Text::truncate($this->$prop, $limit, $options);
    }

    public function link($field = 'slug', $action = 'view')
    {
        list($plugin, $controller) = pluginSplit($this->getSource());
        return Router::url([
            'prefix' => false,
            'plugin' => $plugin,
            'controller' => $controller,
            'action' => $action,
            'slug' => $this->$field
        ], true);
    }

    protected function _getWeblink()
    {
        return $this->link();
    }

    public function wwwLink($field, $blank = true, $class = '')
    {
        $url = $this->get($field);

        if (strpos($url, '//') === false) {
            $domain = $url;
            $url = 'http://' . $url;
        } else {
            $domain = str_replace('https://', '', $url);
            $domain = str_replace('http://', '', $domain);
        }


        return '<a class="'. $class . '"' . ($blank ? ' target="_blank"' : '')  . ' href="' . $url . '">' . $domain . '</a>';
    }

    public function wwwUrl($field)
    {
        $url = $this->get($field);

        if (strpos($url, '//') === false) {
            $url = 'http://' . $url;
        }

        return $url;
    }


    public function edit($field, $type = null)
    {
        return Editable::field($this, $field, $type);
    }

    public function removeStyle($field)
    {
        $text = $this->get($field);
        $output = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $text);
        return $output;
    }
}
