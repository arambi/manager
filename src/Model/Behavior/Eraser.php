<?php
namespace Manager\Model\Behavior;

use Cake\ORM\Entity;
use Cake\Collection\Collection;
use Cake\ORM\Table;

class Eraser
{
  protected $_table;

  protected $_items = [];

  protected $_pardoneds = [];

  public function __construct( Table $table)
  {
    $this->_table = $table;
  }

/**
 * Añade una entidad para borrar 
 *   
 * @param Entity $entity
 */
  public function addEntity( Entity $entity)
  {
    $this->_items [] = $entity;
  }

/**
 * Añade una entidad que no se debe borrar
 * 
 * @param Entity $entity
 */
  public function addPardoned( Entity $entity)
  {
    $this->_pardoneds [] = $entity;
  }

/**
 * Borra las entidades que estén en $this->_items pero que no estén en $this->_pardoneds
 */
  public function delete()
  {
    $collection = new Collection( $this->_pardoneds);
    $primaryKey = $this->_table->primaryKey();
    
    if( is_array( $primaryKey))
    {
      return;
    }

    foreach( $this->_items as $entity)
    {
      $id = $entity->get( $primaryKey);

      $has = $collection->firstMatch( [$primaryKey => $id]);

      if( !$has)
      {
        $this->_table->delete( $entity);
      }
    }
  }
}