<?php
namespace Manager\Model\Behavior;

use Cake\ORM\Entity;
use Cake\Collection\Collection;

/**
 * Se encarga de operaciones de preparación de datos para ser guardados por el CRUD
 */

class Patcher
{
/**
   * Table instance.
   *
   * @var \Cake\ORM\Table
   */
  protected $_table;

  public function __construct( $table)
  {
    $this->_table = $table;
  }

/**
 * Se encarga de verificar los datos antes de mezclarlos para su guardado
 * 
 * @param  Entity       $entity  [description]
 * @param  \ArrayObject $data    [description]
 * @param  array        $contain [description]
 */
  public function verifyDataAssociation( Entity $entity, \ArrayObject $data, array $contain)
  {
    $this->unsetEmptyDataAssociation( $entity, $data, $contain, $this->_table);
  }

  public function verifyNewDataAssociation( \ArrayObject $data, array $contain)
  {
    $this->unsetNewDataAssociation( $data, $contain, $this->_table);
  }

  public function unsetNewDataAssociation( \ArrayObject $data, array $contain, $table)
  {
    foreach( $contain as $assoc => $assocs)
    {
      $property = $table->$assoc->property();

      if( (isset( $data [$property]) && empty( $data [$property])))
      {
        unset( $data [$property]);
      }
    }
  }

/**
 * Se encarga de eliminar de $data las claves de las asociaciones que estén vacias tanto en la Entity como en el array de $data
 * 
 * @param  Entity       $entity  [description]
 * @param  \ArrayObject $data    [description]
 * @param  array        $contain [description]
 */
  public function unsetEmptyDataAssociation( Entity $entity, \ArrayObject $data, array $contain, $table)
  {
    foreach( $contain as $assoc => $assocs)
    {
      $property = $table->$assoc->property();

      // if( ($entity->has( $property) && empty( $entity->$property)) && (isset( $data [$property]) && empty( $data [$property])))
      if( (empty( $entity->$property)) && (isset( $data [$property]) && empty( $data [$property])))
      {
        unset( $data [$property]);
      }

      if( !empty( $assocs) && isset( $data [$property]))
      {
        if( $table->$assoc->type() == 'oneToMany' || $table->$assoc->type() == 'manyToMany')
        {
          foreach( $data [$property] as $key => $_data)
          {
            if( isset( $_data [$table->$assoc->primaryKey()]))
            {
              $collection = new Collection( $entity->get( $property));
              $_entity = $collection->firstMatch( [ $table->$assoc->primaryKey() => $_data [$table->$assoc->primaryKey()]]);
              
              if( $_entity)
              {
                $data [$property][$key] = $this->unsetEmptyDataAssociation( $_entity, new \ArrayObject( $_data), $assocs ['associated'], $table->$assoc);
              }
            }
          }
        }
      }
    }

    return (array)$data;
  }

  public function moveNewsToEnd( Entity $entity, \ArrayObject $data, array $contain, $table)
  {
    foreach( $contain as $assoc => $assocs)
    {
      $property = $table->$assoc->property();

      if( !empty( $assocs) && isset( $data [$property]))
      {
        if( $table->$assoc->type() == 'oneToMany' || $table->$assoc->type() == 'manyToMany')
        {
          foreach( $data [$property] as $key => $_data)
          {
            $id = $table->$assoc->primaryKey();

            if( !isset( $_data [$id]))
            {
              if( count( $data [$property]) > 1)
              {
                $data [$property][] = $_data;
                unset( $data [$property][$key]);
                // _d( $data);
              }
            }
          }

          $data [$property] = array_values( $data [$property]);
        }
      }
    }


    return (array)$data;
  }
}