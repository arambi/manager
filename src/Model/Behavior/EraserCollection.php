<?php
namespace Manager\Model\Behavior;

use Cake\ORM\Entity;
use Cake\Collection\Collection;
use Cake\ORM\Table;
use Manager\Model\Behavior\Eraser;

/**
 * Se encarga de gestionar el borrado de varias entidades ayudándose de la class Eraser que contendrá las entidades para cada Table
 *
 * Estas clases son útiles para borrados de hasMany, cuando se quieren borrar las entidades pero se hacen excepciones.
 * Por un lado tenemos las entidades que se quieren borrar (items) y por otro lado las que no queremos borrar (pardoneds)
 */

class EraserCollection
{
/**
 * Colección de objetos Eraser
 * Se crean con la clave del alias de la tabla
 * @var array
 */
  protected $_items = [];


  /**
 * Toma un eraser dado una tabla. Si no existe el eraser, se crean
 * 
 * @param  Table $table
 */
  protected function _getEraser( Table $table)
  {
    $alias = $table->alias();

    if( !isset( $this->_items [$alias]))
    {
      $this->_items [$alias] = new Eraser( $table);
    }

    return $this->_items [$alias];
  }

/**
 * Añade una entidad para borrar al Eraser
 *   
 * @param Table  $table 
 * @param Entity $entity
 */
  public function addEntity( Table $table, Entity $entity)
  {
    $this->_getEraser( $table)->addEntity( $entity);
  }

/**
 * Añade una entidad que no se debe borrar
 * 
 * @param Table  $table 
 * @param Entity $entity
 */
  public function addPardoned( Table $table, Entity $entity)
  {
    $this->_getEraser( $table)->addPardoned( $entity);
  }

/**
 * Llama al método delete() de cada Eraser para que éste borre las entidades
 */
  public function delete()
  {
    foreach( $this->_items as $eraser)
    {
      $eraser->delete();
    }
  }
}