<?php

namespace Manager\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\Event\EventManager;
use Cake\Core\Configure;

/**
 * Drafter behavior
 */
class DrafterBehavior extends Behavior
{

    public function initialize(array $config)
    {
        if (!$this->_table->hasBehavior('Crudable')) {
            throw new \RuntimeException(
                sprintf('It is necessary that the model %s is linked to the behavior Crudable', $this->_table->getAlias())
            );
        }

        if ($this->_table->isAdmin()) {
            $this->_table->hasMany('Drafts', [
                'className' => 'Manager.Drafts',
                'foreignKey' => 'foreign_key',
                'conditions' => [
                    'Drafts.model' => $this->_table->getAlias()
                ]
            ]);
        } else {
            $this->_table->hasOne('Drafts', [
                'className' => 'Manager.Drafts',
                'foreignKey' => 'content_id',
                'propertyName' => 'draft',
                'conditions' => [
                    'Drafts.model' => $this->_table->getAlias()
                ]
            ]);
        }
    }

    public function beforeFind(Event $event, Query $query)
    {
        $query->contain(['Drafts']);

        if (!Configure::read('Manager.preview') && !($this->_table->isAdmin() && in_array($this->_table->crud->action(), ['update', 'savedraft', 'deletedraft'])))
        // Hace que no se encuentre el registro
        // if( !(Configure::read( 'Manager.preview') || $this->_table->isAdmin()) && !in_array( $this->_table->crud->action(), ['update', 'savedraft', 'deletedraft']))
        {
            $query->where([
                $this->_table->getAlias() . '.is_draft' => false
            ]);
        }

        if (!$this->_table->isPreview() && !$this->_table->isAdmin() && !in_array($this->_table->crud->action(), ['update', 'deletedraft'])) {
        } else {
            $this->_table->crud->addConfig([
                'drafter' => true
            ]);

            $this->_table->hasOne('Original', [
                'className' => 'Manager.Drafts',
                'foreignKey' => 'content_id',
                'conditions' => [
                    'Original.model' => $this->_table->getAlias()
                ]
            ]);

            $query->contain(['Original']);

            $this->_table->hasMany('Drafters', [
                'className' => 'Manager.Drafts',
                'foreignKey' => 'foreign_key',
                'conditions' => [
                    'Drafters.model' => $this->_table->getAlias()
                ]
            ]);

            $query->contain(['Drafters']);
        }
    }

    /**
     * Crea un draft dado un id
     * 
     * @param  integer $id 
     * @return Entity
     */
    public function createDraft($id)
    {
        // Comprueba que el original para copiar es un draft o es un original
        // Si es un draft, el id de referencia (foreign_key) será el de la referencia de la copia
        $exists = $this->_table->Drafts->find()
            ->where([
                'Drafts.content_id' => $id
            ])
            ->first();

        // Duplica el contenido
        $duplicate = $this->_table->duplicate($id, ['is_draft' => 1]);

        // Pone la foreign_key
        if ($exists) {
            $foreign_key = $exists->foreign_key;
        } else {
            $foreign_key = $id;
        }

        // Guarda el Draft de relación
        $data = [
            'content_id' => $duplicate->id,
            'foreign_key' => $foreign_key,
            'model' => $this->_table->getAlias()
        ];

        $entity = $this->_table->Drafts->newEntity($data);
        $this->_table->Drafts->save($entity);
        return $duplicate;
    }

    /**
     * Copia un draft en el original y luego borra el draft
     * 
     * @param  integer  $id   El id del draft
     * @return entity
     */
    public function saveDraft($id)
    {
        // La relación de la tabla drafts
        $entity = $this->_table->Drafts->find()
            ->where([
                'Drafts.content_id' => $id
            ])
            ->first();

        // El contenido draft
        $draft = $this->_table->find('content')
            ->where([
                $this->_table->getAlias() . '.' . $this->_table->getPrimaryKey() => $id
            ])
            ->first();

        // El contenido original
        $original = $this->_table->find('content')
            ->where([
                $this->_table->getAlias() . '.' . $this->_table->getPrimaryKey() => $entity->foreign_key
            ])
            ->first();

        $event = new Event('Manager.Behavior.Drafter.' . $this->_table->getAlias() . '.beforeSaveDraft', $this, [
            $original,
            $draft
        ]);

        EventManager::instance()->dispatch($event);

        // COPIA TODOS LOS CAMPOS

        // Excepciones
        $exceptions = ['slugs'];

        foreach ($original->visibleProperties() as $prop) {
            if (!in_array($prop, $exceptions) && !in_array($prop, [$this->_table->getPrimaryKey()])) {
                $original->set($prop, $draft->$prop);
            }

            $original->set('is_draft', 0);
        }

        $this->_table->saveContent($original);
        $this->_table->delete($draft);
        $this->_table->Drafts->delete($entity);
        return $original;
    }
}
