<?php

namespace Manager\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Utility\Inflector;
use Cake\Utility\Hash;
use Cake\ORM\ResultSet;
use Cake\Core\App;
use Manager\Lib\CrudConfig;
use Manager\Model\Behavior\Patcher;
use I18n\Lib\Lang;
use Cake\I18n\I18n;
use \ArrayObject;
use Manager\Model\Behavior\EraserCollection;
use Cake\Event\EventManager;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Database\Schema\TableSchema;
use Cake\Datasource\EntityInterface;

/**
 * Crudable behavior
 */
class CrudableBehavior extends Behavior
{
    /**
     * Eraser para borrar entidades
     * @var EraserCollection
     */
    protected $_eraser;

    protected $_defaultConfig = [
        // Buscadores
        'implementedFinders' => [
            'array' => 'findContentArray',
            'content' => 'findContent',
            'tree' => 'findTreeList'
        ],
    ];

    /**
     * Asociaciones que ya se han seteado con getAssociations()
     * 
     * @var array
     */
    private $__associateds = [];

    private $__contains = [];

    /**
     * Asociaciones que ya han sido seteadas para usar en la búsqueda
     * @var array
     */
    private $__associatedsFind = [];

    /**
     * El contain de asociaciones para el guardado de contenidos
     * @var boolean|array
     */
    private $__containSave = false;

    /**
     * El contain de asociaciones para la búsqueda
     * @var array
     */
    private $__containFind = [];

    /**
     * Patcher instance.
     *
     * @var \Manager\Model\Behavior\Patcher
     */
    protected $_patcher;


    public function initialize(array $config)
    {
        $this->_table->crud = new CrudConfig($this->_table);

        $this->_patcher = new Patcher($this->_table);

        // Propiedad del objeto Table que indicará que estamos en modo de administración
        // Con esta propiedad se efectuarán las acciones oportunas
        $this->_table->isAdmin = false;
        // BeforeFilter Event
        $event = new Event('Manager.Behavior.Crudable.' . $this->_table->getAlias() . '.initialize', $this, [
            $this->_table
        ]);

        EventManager::instance()->dispatch($event);
    }

    public function beforeFind(Event $event, Query $query)
    {
        $class = get_class($event->getSubject());

        $plugin = explode("\\", $class)[0] == 'App' ? false : explode("\\", $class)[0];
        $alias = $this->_table->getAlias();

        if ($this->_table->isAdmin()) {
            $query->formatResults(function ($results) use ($alias, $plugin) {
                return $this->_setEditLinks($results, $alias, $plugin);
            }, $query::PREPEND);

            $query->formatResults(function ($results) use ($alias, $plugin) {
                return $this->_setProperties($results, $alias, $plugin);
            }, $query::PREPEND);
        }
    }


    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        $this->setDates($data);
    }

    /**
     * Setea URLs para la edición de los contenidos
     * 
     * @param ResultSet $results
     * @param string $alias
     * @param string $plugin
     * @return ReplaceIterator
     */
    protected function _setEditLinks($results, $alias, $plugin)
    {
        $controller = Inflector::tableize($alias);

        if ($plugin) {
            $controller = strtolower(Inflector::underscore($plugin)) . '/' . $controller;
        }

        return $results->map(function ($result) use ($controller) {
            if (!is_object($result)) {
                return $result;
            }

            $result->set('urls', [
                'updateHash' => '#/admin/' . $controller . '/update/' . $result->id,
                'update' => '/admin/' . $controller . '/update/' . $result->id . '.json',
                'updateUrl' => '/admin/' . $controller . '/update/' . $result->id,
                'delete' => '/admin/' . $controller . '/delete.json',
                'deleteUrl' => '/admin/' . $controller . '/delete',
                'base' => '/admin/' . $controller . '/',
            ]);

            return $result;
        });
    }

    /**
     * Setea las propiedades que se usarán en el admin
     * 
     * @param ResultSet
     * @return ReplaceIterator
     */
    protected function _setProperties($results)
    {
        return $results->map(function ($result) {
            if (is_object($result)) {
                $result->set('full_title', $result->full_title);
            }
            return $result;
        });
    }

    /**
     * Setea la propiedad `isAdmin` para el objeto Table y para todos los objetos Table de las asociaciones
     */
    public function setAdmin($value = true)
    {
        $this->_table->isAdmin = $value;
        $this->__setAssociationsProperty('isAdmin', $this->_table, $value);
    }

    public function setPreview($value = true)
    {
        $this->_table->isPreview = $value;
        $this->__setAssociationsProperty('isPreview', $this->_table, $value);
    }


    /**
     * Recursive
     * Setea la propiedad `isAdmin` para el objeto Table y para todos los objetos Table de las asociaciones
     */
    private function __setAssociationsProperty($property, $table, $value = true)
    {
        foreach ($table->associations()->keys() as $key) {
            $_table = $table->associations()->get($key)->target();

            if ($_table->hasBehavior('Crudable') && !$_table->isAdmin()) {
                $_table->$property = $value;
                $_associations = $_table->associations()->keys();

                if (!empty($_associations)) {
                    $this->__setAssociationsProperty($property, $_table, $value);
                }
            }
        }
    }

    /**
     * Indica si el entorno actual es de administración
     * 
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->_table->isAdmin;
    }

    /**
     * Indica si el entorno actual es de de modo preview
     * 
     * @return boolean
     */
    public function isPreview()
    {
        return Configure::read('Manager.preview');
    }


    /**
     * Prepara la query para el contenido 
     * 
     * @param  Query  $query
     * @param  array  $options
     * @return Query
     */
    public function findContent(Query $query, array $options)
    {
        // Contain de las asociaciones del model
        $contain = $this->containAssociations();

        // Si tiene traducciones, usa el behavior Translate
        if ($this->_table->hasBehavior('Translate') || $this->_table->hasBehavior('I18nTranslate')) {
            $query = $query->find('translations');
        }

        $query->contain($contain['simple']);

        if (!empty($contain['translateds'])) {
            $query->contain($contain['translateds']);
        }

        // if( array_key_exists( 'view', $options))
        // {
        //   $fields = $this->_table->crud->getView( 'update')->fieldKeys();
        //   $query->select( $fields);
        // }

        return $query;
    }

    /**
     * Prepara la query para el contenido para ser editado y da formato a los resultados para ser usado en la vista con JSON
     *   
     * @param  Query  $query 
     * @param  array  $options
     * @return Query
     */
    public function findContentArray(Query $query, array $options)
    {
        $class = get_class($this->_table);

        $plugin = explode("\\", $class)[0] == 'App' ? false : explode("\\", $class)[0];
        $alias = $this->_table->getAlias();

        $contain = $this->containAssociations();

        $query->contain($contain['simple']);

        if (!empty($contain['translateds'])) {
            $query->contain($contain['translateds']);
        }

        if ($this->_table->hasBehavior('Translate') || $this->_table->hasBehavior('I18nTranslate')) {
            $query = $query->find('translations');
        }

        $query
            ->formatResults(function ($results) use ($alias, $plugin) {
                return $this->_rowMapperArray($results, $alias, $plugin);
            }, $query::APPEND);

        $event = new Event('Manager.Behavior.Crudable.' . $this->_table->getAlias() . '.findContentArray', $this, [
            $query
        ]);

        EventManager::instance()->dispatch($event);

        return $query;
    }

    /**
     * Devuelve un array del objeto buscado, con los datos ordenados para ser tratados correctamente con JSON
     * Coloca información variada en las entidades
     *   updateHash: la URL de updatear, para ser utilizada por AngularJS en la vista
     *   update: la URL de updatear para el formulario a enviar
     *     
     * @param  mixed $results
     * @param  string $table   El nombre de la table
     * @return void
     */
    protected function _rowMapperArray($results, $alias, $plugin)
    {
        return $results->map(function ($result) {
            if (!is_object($result)) {
                return $result;
            }

            $result = $this->contentToArray($result);
            return $result;
        });
    }

    private function __setFieldTranslations($content, $table)
    {
        if ($this->_table->hasBehavior('Translate') || $this->_table->hasBehavior('I18nTranslate')) {
            $fields = $this->_table->translateFields();

            if (empty($fields)) {
                return;
            }

            $translations = $content->get('_translations');

            foreach (Lang::iso3() as $locale => $name) {
                if (!array_key_exists($locale, $translations)) {
                    $translations[$locale] = $this->_table->newEntity();
                }

                foreach ($fields as $field) {
                    if (!$translations[$locale]->has($field)) {
                        $translations[$locale]->set($field, '');
                    }
                }
            }

            $content->set('_translations', $translations);
        }
    }

    /**
     * Convierte en array una entidad
     * El trabajo fundamental de este método es colocar las traducciones para que AngularJS pueda tratarlas bien
     * 
     * @param  Entity $content
     * @return array
     */
    public function contentToArray($content)
    {
        $this->__setFieldTranslations($content, $this->_table);
        $contain = $this->getAssociations($this->_table);

        return $content->toArray();
    }


    protected function _setEmptyTranslations()
    {
        $fields = $this->_table->translateFields();
        $return = [];

        foreach ($fields as $field) {
            $return[$field] = '';
        }

        return $return;
    }

    public function containIndex()
    {
        $contain = $this->containAssociations();
        return $contain['simple'];
    }

    /**
     * Devuelve el contain() para el model principal para su uso en las búsquedas
     * El contain se compone de todas la asociaciones enlazadas con el model principal, 
     *   de tal manera que se devuelvan todos los datos enlazados
     *
     * @param  mixed $table Puede ser una Table o false. En este último caso, tomará la table desde donde se ha vinculado el behavior
     * @return array
     */

    public function containAssociations()
    {
        if (empty($this->__containFind)) {
            $this->__containFind['simple'] = $this->__containAssociations(false, false);
            $this->__associatedsFind = [];
            $this->__contains = [];
            $this->__containFind['translateds'] = $this->__containAssociations(false, true);
        }

        return $this->__containFind;
    }


    private function __containAssociations($table = false, $translateds = false, $nested = false)
    {
        // Si no se pasa $table, toma la table desde donde se ha vinculado el behavior
        if (!$table) {
            $table = $this->_table;
        }

        // El array a devolver
        $return = [];

        // Las asociaciones de la tabla
        $assocs = $table->associations();

        if (!$assocs) {
            return false;
        }


        // Itera las keys de las asociaciones
        foreach ($assocs->keys() as $key) {
            if (!$this->_table->crud->hasAssociation($assocs->get($key)->getAlias())) {
                continue;
            }

            if ($this->__hasAssociatedFind($assocs->get($key)->getAlias())) {
                continue;
            }

            $this->__addAssociatedsFind($assocs->get($key)->getAlias());

            if ($assocs->get($key)->type() == 'manyToOne') {
                // continue;
            }

            $_table = $assocs->get($key)->getTarget();

            if (!$nested) {
                $nested = $assocs->get($key)->getAlias();
            } else {
                $nested .= '.' . $assocs->get($key)->getAlias();
            }


            // Si el model tiene traducciones se asocian con un closure que busca el contenido usando el behavior Translate ('translations')
            // TODO ¿Será posible usar un closure y más asociaciones a la vez? De momento parece que no es posible
            if (($_table->hasBehavior('Translate') || $_table->hasBehavior('I18nTranslate')) && $translateds) {
                $_assocs = function ($query) use ($_table, $translateds) {
                    $query->find('translations');
                    $query->contain($this->__containAssociations($_table, $translateds));
                    return $query;
                };
            }
            // Si no toma posibles asociaciones que tenga la tabla asociada
            else {
                // $_assocs = function( $query) use ($_table){
                //     $query->contain( $this->__containAssociations( $_table));
                //     return $query;
                // };
                $_assocs = $this->__containAssociations($_table, $translateds, $nested);
            }

            // Añade a $return las asociaciones
            if (!empty($_assocs)) {
                $return[$_table->getAlias()] = $_assocs;
            } else {
                $return[] = $_table->getAlias();
            }
        }

        return $return;
    }

    /**
     * Toma una nueva entidad pasando un array de datos
     * Es una variación de Table::newEntity()
     * Fundamentalmente se ocupa de las traducciones
     * 
     * @param  array $data
     * @return Entity
     * @see Table::newEntity()
     */
    public function getNewEntity($data = [], $table = null)
    {
        if (!$table) {
            $table = $this->_table;
        }

        // Mezcla los datos por defecto
        $data = array_merge($table->crud->defaults(), $data);

        $data = new \ArrayObject($data);

        $contain = $this->getAssociations($table);

        $data = $this->newBelongsToMany($table, $data);
        $this->_patcher->verifyNewDataAssociation($data, $contain);
        $content = $table->newEntity((array)$data, ['associated' => $contain, 'translations' => true]);
        // $content = $this->patchTranslations( $content, $data);
        $content->set('id', null);
        return $content;
    }

    /**
     * Devuelve un array con los ids de la relación del contenido con los Models BelongsToMany
     * 
     * @param  Table $table 
     * @param  array $data  Los datos del contenido
     * @return array 
     */
    public function newBelongsToMany($table, $data)
    {
        $belongsToMany = $table->associations()->getByType('BelongsToMany');

        foreach ($belongsToMany as $assoc) {
            $alias = $assoc->getAlias();

            if (!$table->crud->hasAssociation($assoc->getAlias())) {
                continue;
            }

            $_data = isset($data[$assoc->property()]) ? $data[$assoc->property()] : [];

            $ids = Hash::extract((array)$_data, '{n}.id');
            $data[$assoc->property()] = ['_ids' => $ids];
        }

        return $data;
    }

    public function patchBelongsToMany($table, $data)
    {
        $belongsToMany = $table->associations()->getByType('BelongsToMany');

        foreach ($belongsToMany as $assoc) {
            $alias = $assoc->getAlias();

            if (!$table->crud->hasAssociation($assoc->getAlias())) {
                continue;
            }

            if (isset($data[$assoc->property()])) {
                $_data = $data[$assoc->property()];
                $ids = Hash::extract((array)$_data, '{n}.id');
                // $data [$assoc->property()] = ['_ids' => $ids];
                $save = [];

                foreach ($_data as $content) {
                    $save[] = [
                        $assoc->getPrimaryKey()  => $content['id'],
                        '_joinData' => @$content['_joinData']
                    ];
                }

                $data[$assoc->property()] = $save;
            }
        }

        return $data;
    }

    /**
     * Retorna las propiedades (columnas) vacías de una entidad
     * Se utiliza para el seteo de AngularJS en los registros nuevos
     * 
     * @param  Entity $entity 
     * @param  Table|false $table
     * @return array
     */
    public function emptyEntityProperties(Entity $content, $table = false)
    {
        if (!$table) {
            $table = $this->_table;
        }
        $return = new ArrayObject($content->toArray());

        // $fields = $content->__debugInfo()['[accessible]'];
        $fields = $content->getAccesibles();
        $fields = array_filter($fields, function ($key) {
            if ($key == '*') {
                return false;
            }
            return true;
        }, ARRAY_FILTER_USE_KEY);

        $translate_fields = [];

        // Recorre los campos de traducciones
        if ($table->hasBehavior('Translate') || $table->hasBehavior('I18nTranslate')) {
            $translate_fields = $table->translateFields();

            // Recorre los idiomas disponibles
            foreach (Lang::iso3() as $lang => $name) {
                if (!isset($return['_translations']) || !isset($return['_translations'][$lang])) {
                    foreach ($translate_fields as $field) {
                        $return['_translations'][$lang][$field] = '';
                    }
                } else {
                    if (is_object($return['_translations'][$lang])) {
                        $return['_translations'][$lang] = $return['_translations'][$lang]->toArray();
                    } else {
                        $return['_translations'][$lang] = $return['_translations'][$lang];
                    }

                    // Errores de validación en la entity
                    if (
                        !empty($content->_translations[$lang])
                        && !empty($content->_translations[$lang])
                        && is_object($content->_translations[$lang])
                        && !empty($content->_translations[$lang]->invalid())
                    ) {
                        foreach ($content->_translations[$lang]->invalid() as $field => $value) {
                            $return['_translations'][$lang][$field] = $value;
                        }
                    }

                    // unset( $return [$lang]['locale']);
                }
            }

            if (isset($return['_locale'])) {
                unset($return['_locale']);
            }

            $translate_fields = $table->translateFields();
        }

        foreach ($fields as $field => $value) {
            if (!isset($return[$field]) && !in_array($field, $translate_fields)) {
                $return[$field] = '';
            }
        }

        // Campos que son de models con asociación hasOne
        $assocs_has_one = $table->associations()->getByType('HasOne');

        foreach ($assocs_has_one as $assoc) {
            // Si no está indicada dentro de las asociaciones del crud, pasamos
            if (!$this->_table->crud->hasAssociation($assoc->getAlias())) {
                continue;
            }

            $property = $assoc->property();

            // Si la propiedad está vacía la añadimos los datos con cada columna vacía
            if (is_null($content->$property)) {
                $content->$property = $assoc->target()->getNewEntity();
            }

            $return[$property] = $this->emptyEntityProperties($content->$property, $assoc->target());
        }


        // Campos que son de models con asociación HasMany
        $fields_associations = [];

        $assocs_has_many = $table->associations()->getByType('HasMany');

        foreach ($assocs_has_many as $assoc) {
            if ($this->_table->crud->hasAssociation($assoc->getAlias())) {
                $fields_associations[] = $assoc->property();
            }
        }

        foreach ($fields_associations as $field) {
            if (isset($return[$field]) && !is_array($return[$field])) {
                $return[$field] = [];
            } else {
                if (isset($table->$field)) {
                    $assocs_has_many = $table->$field->associations()->type('HasMany');

                    if (!empty($assocs_has_many)) {
                        foreach ($content->$field as $key => $_content) {
                            $return[$field][$key] = $this->emptyEntityProperties($_content, $table->$field);
                        }
                    }
                }
            }
        }

        $assocs_has_many = $table->associations()->type('BelongsToMany');
        $fields_associations = [];

        foreach ($assocs_has_many as $assoc) {
            $fields_associations[] = $assoc->property();
        }

        foreach ($fields_associations as $field) {
            if (isset($return[$field]) && !is_array($return[$field])) {
                $return[$field] = [];
            }
        }

        if ($table->alias() == $this->_table->getAlias()) {
            $this->_table->dispatchEvent('Crud.afterSetEmptyProperties', [$return]);
        }


        return (array)$return;
    }

    /**
     * Mezcla un array de datos con una entidad
     * Es algo parecido a lo que hace Table::patchEntity()
     * Fundamentalmente se ocupa de las traducciones
     * 
     * @param  Entity $content
     * @param  array $data 
     * @return Entity
     */
    public function patchTranslations($content, $data)
    {
        $contain = $this->getAssociations($this->_table);

        if ($this->_table->hasBehavior('Translate') || $this->_table->hasBehavior('I18nTranslate')) {
            $locale = $this->_table->locale();
            $locales = Lang::iso3();

            // Los campos traducibles
            $fields = $this->_table->translateFields();

            foreach ($locales as $lang => $name) {
                // El idioma está en los datos
                if (array_key_exists($lang, $data)) {
                    foreach ($fields as $field) {
                        if (array_key_exists($field, $data[$lang])) {
                            if (is_array($content->_translations) && array_key_exists($lang, $content->_translations)) {
                                $content->_translations[$lang]->set($field, $data[$lang][$field], ['guard' => false]);
                            } else {
                                $content->translation($lang)->set($field, $data[$lang][$field], ['guard' => false]);
                            }
                        }
                    }

                    // Es el idioma actual
                    if ($lang == $locale) {
                        foreach ($data[$lang] as $column => $value) {
                            // Solo si el campo es traducible y si no está vacío 
                            if (in_array($column, $fields) && isset($value)) {
                                $content->set($column, $value);
                            }
                        }
                    }
                }
            }
        }

        $this->translateAssociations($content, $data, $contain, $this->_table);

        return $content;
    }

    /**
     * Los datos de traducción llegan desde el formulario con este formato
     * ['spa'] = ['title' => 'Título', 'body' => 'Cuerpo']
     * Este método, conjuntamente con __normalizeTranslateContent() se encarga de parsear los datos para convertirlos en un objeto Entity
     * No retorna nada sino que modifica el objeto $content
     *   
     * @param  Entity $content
     * @param  array $data
     * @param  array $contain
     * @param  Table $table
     * @return void
     */
    public function translateAssociations($content, $data, $contain, $table)
    {
        foreach ($contain as $assoc => $info) {
            // El nombre de la propiedad del objeto Entity
            $property = $table->$assoc->property();

            if ($table->$assoc->type() == 'manyToMany' || !$content->has($property)) {
                continue;
            }

            if ($table->$assoc->hasBehavior('Translate') || $table->$assoc->hasBehavior('I18nTranslate')) {
                // Indica si es una asociación tipo "Many" que hará que tenga varios regitros y por ende, debamos iterar
                $iterate = in_array($table->$assoc->type(), ['oneToMany', 'manyToMany']);

                if ($iterate) {
                    foreach ($content->$property as $key => $entity) {
                        if (isset($data[$property][$key])) {
                            if (!$entity->isNew()) {
                                $_data = Hash::extract($data[$property], '{n}[id=' . $entity->id . ']')[0];
                                $this->__normalizeTranslateContent($entity, $_data, $table->$assoc);
                            } else {
                                $this->__normalizeTranslateContent($entity, $data[$property][$key], $table->$assoc);
                            }
                        }
                    }
                } else {
                    $this->__normalizeTranslateContent($content->$property, $data[$property], $table);
                }
            } else {
            }

            if (!empty($info['associated']) && is_array($content->get($property))) {
                foreach ($content->get($property) as $key => &$entity) {
                    if (!$entity->isNew()) {
                        $_data = Hash::extract($data[$property], '{n}[id=' . $entity->id . ']')[0];
                        $this->translateAssociations($entity, $_data, $info['associated'], $table->$assoc);
                    } else {
                        $this->translateAssociations($entity, $data[$property][$key], $info['associated'], $table->$assoc);
                    }
                }
            }
        }
    }

    /**
     * Los datos de traducción llegan desde el formulario con este formato
     * ['spa'] = ['title' => 'Título', 'body' => 'Cuerpo']
     * Este método se encarga de parsear los datos para convertirlos en un objeto Entity
     * No retorna nada sino que modifica el objeto $content
     * 
     * @param  Entity $content
     * @param  array $data
     * @return void
     */
    public function __normalizeTranslateContent($content, $data, $table)
    {
        $locale = I18n::getLocale();
        $content->unsetProperty('_locale');

        foreach (Lang::iso3() as $lang => $name) {
            if (array_key_exists($lang, $data)) {
                $content->translation($lang)->set($data[$lang], ['guard' => false]);

                if ($lang == $locale) {
                    foreach ($data[$lang] as $column => $value) {
                        $content->set($column, $value);
                    }
                }
            }
        }
    }

    public function getAssociations()
    {
        if (!$this->__containSave) {
            $this->__containSave = $this->__getAssociations();
        }

        return $this->__containSave;
    }

    /**
     * [getAssociations description]
     * @param  boolean $table [description]
     * @return [type]         [description]
     */
    private function __getAssociations($table = false)
    {
        if (!$table) {
            $table = $this->_table;
        }

        $return = [];

        $assocs = $table->associations();

        if (!$assocs) {
            return false;
        }

        // Recorre las asociaciones vinculadas a la Table
        foreach ($assocs->keys() as $key) {
            // Si la asociación no ha sido seteada en Crud, pasamos
            if (!$this->_table->crud->hasAssociation($assocs->get($key)->getAlias())) {
                continue;
            }

            if ($this->__hasAssociated($assocs->get($key)->getAlias())) {
                continue;
            }

            $this->__addAssociateds($assocs->get($key));

            $_assocs = $this->__getAssociations($assocs->get($key)->getTarget());

            if ($_assocs) {
                $return[$assocs->get($key)->getAlias()] = ['associated' => $_assocs];
            } else {
                $return[$assocs->get($key)->getAlias()] = [];
            }
        }


        return $return;
    }

    private function __addAssociateds($assoc)
    {
        $class_name = get_class($assoc);

        if ($class_name == 'Cake\ORM\Association\BelongsToMany') {
            $this->__associateds[] = $assoc->alias();
            // $this->__associateds [] = $assoc->alias() . '._joinData';
        } else {
            $this->__associateds[] = $assoc->alias();
        }
    }

    private function __hasAssociated($key)
    {
        return in_array($key, $this->__associateds);
    }

    private function __addAssociatedsFind($assoc)
    {
        $this->__associatedsFind[] = $assoc;
    }

    private function __hasAssociatedFind($key)
    {
        return in_array($key, $this->__associatedsFind);
    }




    /**
     * Mezcla una entity con un array de datos 
     * @param  Entity $entity 
     * @param  array  $data
     * @param  array  $options
     * @return Entity
     */
    public function patchContent($entity, array $data, array $options = [])
    {
        $options['translations'] = true;
        $data = new \ArrayObject($data);
        $contain = $this->getAssociations($this->_table);
        $this->_patcher->verifyDataAssociation($entity, $data, $contain);
        $this->_patcher->moveNewsToEnd($entity, $data, $contain, $this->_table);
        $data = $this->patchBelongsToMany($this->_table, $data);
        $_data = (array)$data;
        $options = array_merge(['associated' => $contain], $options);
        $content = $this->_table->patchEntity($entity, (array)$_data, $options);
        // $content = $this->patchTranslations( $content, $data);
        return $content;
    }

    private function setDates(ArrayObject $data)
    {
        $schema = $this->_table->getSchema();

        foreach ($data as $key => $value) {
            $type = $schema->getColumnType($key);

            if ($type == 'date' && is_string($value) && !empty($value)) {
                $data[$key] = date('Y-m-d', strtotime($value));
            } elseif ($type == 'datetime' && is_string($value) && !empty($value)) {
                $data[$key] = date('Y-m-d H:i:s', strtotime($value));
            }
        }
    }

    /**
     * Guarda en contenido
     * 
     * @param  Entity $entity 
     * @param  array $options
     * @return Entity/null
     */
    public function saveContent($entity, $options = [])
    {
        $this->_eraser = new EraserCollection();
        $this->__proccessAssociations($entity);

        $entity->set('__saveSearch', true);

        if ($saved = $this->_table->save($entity, $options)) {
            if (empty($options['no_delete_hasmany'])) {
                $this->_eraser->delete();
            }
        }

        Cache::clearAll();
        return $saved;
    }

    /**
     * Duplica un registro
     * Se le puede pasar opcionalmente el parámetro $data con un array con los datos que se guardarán en la duplicación
     * 
     * @param  integer|mixed $id 
     * @param  arra  $data
     * @return Entity       
     */
    public function duplicate($id, $data = [])
    {
        $content = $this->_table->find('content')
            ->where([$this->_table->getAlias() . '.id' => $id])
            ->first();

        $content->unsetProperty('id');
        $content->unsetProperty('salt');
        $content->unsetProperty('created');
        $content->unsetProperty('modified');
        $content->isNew(true);

        $array = $this->_table->find('array')
            ->where([$this->_table->getAlias() . '.id' => $id])
            ->first();

        unset($array['id']);
        unset($array['salt']);
        unset($array['created']);
        unset($array['modified']);
        $this->__removeAssociationsId($this->_table, $content);
        $array = $this->__removeAssociationsId($this->_table, $array);

        // if (method_exists($this->_table, 'beforeDuplicate')) {
        //     $data = $this->_table->beforeDuplicate(null, $data);
        // }


        $array = array_merge($array, $data);
        $this->patchContent($content, $array);

        $event = new Event('Manager.Behavior.Crudable.' . $this->_table->getAlias() . '.beforeDuplicate', $this, [
            $content
        ]);

        EventManager::instance()->dispatch($event);

        $this->saveContent($content);
        return $content;
    }

    private function __removeAssociationsId($table, &$content)
    {
        $keys = $table->associations()->keys();

        foreach ($keys as $key) {
            $association = $table->associations()->getByProperty($key);

            if (!$association) {
                continue;
            }

            if (is_array($content)) {
                if (isset($content[$key])) {
                    if (in_array($association->type(), ['oneToMany'])) {
                        foreach ($content[$key] as &$_content) {
                            unset($_content['id']);
                            $this->__removeAssociationsId($association->target(), $_content);
                        }
                    } elseif (in_array($association->type(), ['oneToOne'])) {
                        unset($content[$key]['id']);
                        $this->__removeAssociationsId($association->target(), $content[$key]);
                    }
                }
            } else {
                if ($content->has($key)) {
                    if (in_array($association->type(), ['oneToMany'])) {
                        foreach ($content->$key as $_content) {
                            $_content->unsetProperty('id');
                            $_content->isNew(true);
                            $this->__removeAssociationsId($association->target(), $_content);
                        }
                    } elseif (in_array($association->type(), ['oneToOne'])) {
                        $content->$key->unsetProperty('id');
                        $content->$key->isNew(true);
                        $this->__removeAssociationsId($association->target(), $content->$key);
                    }
                }
            }
        }

        return $content;
    }

    /**
     * Se encarga de procesar los registros de las asociaciones:
     * 1. Borra los registros hasMany que han sido desvinculados
     * 
     * @param  Entity  $entity
     * @param  Table|boolean $table
     * @return void
     */
    private function __proccessAssociations($entity, $table = false)
    {
        if (!$table) {
            $table = $this->_table;
        }
        // HasMany Associations
        $hasMany = $table->associations()->type('HasMany');

        foreach ($hasMany as $assoc) {
            $alias = $assoc->getAlias();

            // No está marcada la asociación en crudConfig
            if (!$this->_table->crud->hasAssociation($assoc->getAlias())) {
                continue;
            }

            // Toma todas las entities
            $new = $entity->get($assoc->property());

            if (empty($new) && !is_array($new)) {
                continue;
            }

            foreach ($new as $_entity) {
                $this->_eraser->addPardoned($assoc->target(), $_entity);
                $this->__proccessAssociations($_entity, $table->$alias);
            }

            $new = array_map(function ($el) use ($assoc, $entity) {

                return $el->toArray();
            }, $new);

            $new_ids = Hash::extract((array)$new, '{n}.id');

            // Toma los datos originales
            $original = $entity->getOriginal($assoc->property());
            foreach ($original as $_entity) {
                if (!in_array($_entity->id, $new_ids)) {
                    $this->_eraser->addEntity($assoc->target(), $_entity);
                    $_entity->set($assoc->foreignKey(), null);
                }
            }
        }
    }


    public function setTranslatedFields($content, $fields)
    {
        $langs = array_keys(Lang::iso3());

        foreach ($langs as $lang) {
            if (!isset($content[$lang])) {
                $content[$lang] = [];
            }

            foreach ($fields as $field) {
                if (!isset($content[$lang][$field])) {
                    $content[$lang][$field] = '';
                }
            }
        }

        return $content;
    }

    /**
     * Ordena los contenidos
     * Aquí, `$data` es el resultado de un orden dado por la librería Javascript UI Sortable
     * Utiliza la función recursive __reordering
     * 
     * @param  array $data
     * @return void
     */
    public function reordering($data)
    {
        $this->__reordering($data);
    }

    /**
     * ver self::reordering()
     * 
     * @param  array  $data
     * @param  integer $parent_id 
     * @param  integer $position
     * @return void
     */
    private function __reordering($data, $parent_id = 0, $position = 1)
    {
        foreach ($data as $_content) {
            $content = $this->_table->get($_content['id']);
            $content
                ->set('position', $position)
                ->set('parent_id', $parent_id);
            $this->_table->save($content);

            $position++;

            if (!empty($_content['nodes'])) {
                $this->__reordering($_content['nodes'], $_content['id']);
            }
        }
    }

    /**
     * Devuelve un listado de contenidos en forma de árbol con el orden dado por la columna `position`
     * 
     * @param  array $conditions
     * @param array $order
     * @return array
     */
    public function tree($conditions = [], $order = [])
    {
        $order += [$this->_table->getAlias() . '.position' => 'asc'];
        $contents = $this->_table->find('threaded')
            ->where($conditions)
            ->order($order)
            ->toArray();

        $contents = $this->changeChildrenKey($contents);
        return $contents;
    }

    /**
     * Retorna un query que dará como resultado un listado en forma de árbol para ser usado en un select de HTML
     *
     * ### options
     * - `entity` Si se pasa un objeto Entity, no se devolverá en el array este registro
     * 
     * @param  Query  $query   [description]
     * @param  array  $options [description]
     * @return Query          
     */
    public function findTreeList(Query $query, array $options)
    {
        if (isset($options['entity']) && !empty($options['entity'][$this->_table->getPrimaryKey()])) {
            $query->where(function ($exp) use ($options) {
                return $exp->not([
                    $this->_table->getAlias() . '.' . $this->_table->getPrimaryKey() => $options['entity'][$this->_table->getPrimaryKey()]
                ]);
            });
        }

        $results = $query->find('threaded', [
            'parentField' => 'parent_id',
            'order' => ['position' => 'asc']
        ]);

        return $this->formatTreeList($results, $options);
    }

    /**
     * Da formato al árbol dado de la query findTreeList
     * 
     * @param  Query  $query   [description]
     * @param  array  $options [description]
     * @return Query
     */
    public function formatTreeList(Query $query, array $options = [])
    {
        return $query->formatResults(function ($results) use ($options) {
            $options += [
                'keyPath' => $this->_table->getPrimaryKey(),
                'valuePath' => $this->_table->getDisplayField(),
                'spacer' => '_'
            ];
            return $results
                ->listNested()
                ->printer($options['valuePath'], $options['keyPath'], $options['spacer']);
        });
    }

    public function treeParents($entity, $reverse = false, $finder = null)
    {
        $out = [];

        if ($finder === null) {
            $finder = 'all';
        }

        while (true) {
            $parent = $this->_table->find($finder)
                ->where([
                    $this->_table->getAlias() . '.id' => $entity->parent_id
                ])
                ->first();

            if ($parent) {
                $out[] = $parent;
                $entity = $parent;
            } else {
                break;
            }
        }

        if (!$reverse) {
            return array_reverse($out);
        }

        return $out;
    }


    /**
     * Cambia la clave `children` por `node` para que pueda usarse con la librería Javascript UI Sortable  
     * 
     * @param  array $datas
     * @return array
     */
    public function changeChildrenKey($datas)
    {
        foreach ($datas as $data) {
            if (!empty($data->children)) {
                $data->set('nodes', $this->changeChildrenKey($data->children));
                $data->unsetProperty('children');
            } else {
                $data->nodes = [];
            }
        }

        return $datas;
    }


    public function setBeforeDeleteError($entity, $data)
    {
        $entity->set('deleteErrors', $data);
    }
}
