<?php
namespace Manager\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

/**
 * Bulker behavior
 */
class BulkerBehavior extends Behavior
{

  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [
    // Buscadores
    'implementedFinders' => [
      'bulk' => 'bulk',
    ],
  ];

  public function bulk()
  {
    if( !$this->_table->behaviors()->has( 'Crudable')) 
    {
      throw new \RuntimeException( sprintf( 'It is necessary that the model %s behavior is linked to the Crudable', $this->_table->alias()));
    }

    $fields = $this->_table->crud->fieldCollector()->fields();
    _d( $fields);
  }
}
