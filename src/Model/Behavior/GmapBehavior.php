<?php
namespace Manager\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Geo\Geocoder\Geocoder;
use Cake\Event\Event;
use Cake\ORM\Entity;
use \ArrayObject;
use Website\Lib\Website;

/**
 * Gmap behavior
 */
class GmapBehavior extends Behavior
{

    protected $_defaultConfig = [
    'real' => false, 'address' => ['street', 'postal_code', 'city', 'country'],
    'require' => false, 'allowEmpty' => true, 'invalidate' => [], 'expect' => [],
    'lat' => 'lat', 'lng' => 'lng', 'formatted_address' => 'formatted_address',
    'host' => null, 'language' => 'de', 'region' => '', 'bounds' => '',
    'overwrite' => false, 'update' => [], 'on' => 'beforeSave',
    'min_accuracy' => Geocoder::TYPE_COUNTRY, 'allow_inconclusive' => true, 
    'log' => true, // log successfull results to geocode.log (errors will be logged to error.log in either case)
    'implementedFinders' => [
      'distance' => 'findDistance',
    ]
  ];

  public $Geocode;

  /**
   * Initiate behavior for the model using specified settings. Available settings:
   *
   * - address: (array | string, optional) set to the field name that contains the
   *      string from where to generate the slug, or a set of field names to
   *      concatenate for generating the slug.
   *
   * - real: (boolean, optional) if set to true then field names defined in
   *      label must exist in the database table. DEFAULTS TO: false
   *
   * - expect: (array)postal_code, locality, sublocality, ...
   *
   * - accuracy: see above
   *
   * - override: lat/lng override on changes?
   *
   * - update: what fields to update (key=>value array pairs)
   *
   * - before: validate/save (defaults to save)
   *      set to false if you only want to use the validation rules etc
   *
/**
 * Constructor
 *
 * Merges config with the default and store in the config property
 *
 * Does not retain a reference to the Table object. If you need this
 * you should override the constructor.
 *
 * @param Table $table The table this behavior is attached to.
 * @param array $config The config for this behavior.
 */
  public function __construct(Table $table, array $config = []) {
    parent::__construct($table, $config);

    $this->config( 'address', $config ['address'], false);
    $this->_table = $table;
  }

/**
 * @param \Cake\Event\Event $event The beforeSave event that was fired
 * @param \Cake\ORM\Entity $entity The entity that is going to be saved
 * @param \ArrayObject $options the options passed to the save method
 * @return void
 */
  public function beforeRules(Event $event, Entity $entity, ArrayObject $options) {
    if ($this->_config['on'] === 'beforeRules') {
      if(!$this->geocode($entity)) {
        $event->stopPropagation();
      }
    }
  }

/**
 * @param \Cake\Event\Event $event The beforeSave event that was fired
 * @param \Cake\ORM\Entity $entity The entity that is going to be saved
 * @param \ArrayObject $options the options passed to the save method
 * @return void
 */
  public function beforeSave(Event $event, Entity $entity, ArrayObject $options) {
    if ($this->_config['on'] === 'beforeSave') {
      if (!$this->geocode($entity)) {
        $event->stopPropagation();
      }
    }

    $entity->lat = str_replace( ',', '.', $entity->lat);
    $entity->lng = str_replace( ',', '.', $entity->lng);
  }

  /**
   * Run before a model is saved, used to set up slug for model.
   *
   * @param \Cake\ORM\Entity $entity The entity that is going to be saved
   * @return bool True if save should proceed, false otherwise
   */
  public function geocode(Entity $entity) {
    

    // Make address fields an array
    if (!is_array($this->_config['address'])) {
      $addressfields = [$this->_config['address']];
    } else {
      $addressfields = $this->_config['address'];
    }
    $addressfields = array_unique($addressfields);
    // Make sure all address fields are available
    if ($this->_config['real']) {
      foreach ($addressfields as $field) {
        if (!$this->_table->hasField($field)) {
          return false;
        }
      }
    }

    if( !$this->hasChanges( $entity, $addressfields))
    {
      return true;
    }

    $addressData = [];
    foreach ($addressfields as $field) {
      $fieldData = $entity->get($field);
      if (!empty($fieldData)) {
        $addressData[] = $fieldData;
      }
    }

    $entityData['geocoder_result'] = [];

    if ((!$this->_config['real'] || ($this->_table->hasField($this->_config['lat']) && $this->_table->hasField($this->_config['lng']))) &&
      ($this->_config['overwrite'] || !$entity->get($this->_config['lat']) || ((int)$entity->get($this->_config['lat']) === 0 && (int)$entity->get($this->_config['lng']) === 0))
    ) {
      /*
      //FIXME: whitelist in 3.x?
      if (!empty($this->_table->whitelist) && (!in_array($this->_config['lat'], $this->_table->whitelist) || !in_array($this->_config['lng'], $this->_table->whitelist))) {
        return $entity;
      }
      */
    }
    $geocode = $this->_geocode($addressData);

    if (empty($geocode) && !empty($this->_config['allowEmpty'])) {
      return true;
    }
    if (empty($geocode)) {
      return false;
    }

    $lat = $geocode->get( 0)->getLatitude();
    $lng = $geocode->get( 0)->getLongitude();
    // If both are 0, thats not valid, otherwise continue
    if (empty($lat) && empty($lng)) {
      /*
      // Prevent 0 inserts of incorrect runs
      if (isset($this->_table->data[$this->_table->alias][$this->_config['lat']])) {
        unset($this->_table->data[$this->_table->alias][$this->_config['lat']]);
      }
      if (isset($this->_table->data[$this->_table->alias][$this->_config['lng']])) {
        unset($this->_table->data[$this->_table->alias][$this->_config['lng']]);
      }
      */
      if ($this->_config['require']) {
        if ($fields = $this->_config['invalidate']) {
          //FIXME
          //$this->_table->invalidate($fields[0], $fields[1], isset($fields[2]) ? $fields[2] : true);
        }
        //return false;
      }
      return true;
    }

    // Valid lat/lng found
    $entityData[$this->_config['lat']] = $lat;
    $entityData[$this->_config['lng']] = $lng;
    /*
    if (!empty($this->_config['formatted_address'])) {
      $entityData[$this->_config['formatted_address']] = $geocode['formatted_address'];
    }

    $entityData['geocoder_result'] = $geocode;
    $entityData['geocoder_result']['address_data'] = implode(' ', $addressData);

    if (!empty($this->_config['update'])) {
      foreach ($this->_config['update'] as $key => $field) {
        if (!empty($geocode[$key])) {
          $entityData[$field] = $geocode[$key];
        }
      }
    }
    */
    $entity->set($entityData);

    return true;
  }

  public function hasChanges( Entity $entity, array $addressfields)
  {
    if( empty( $entity->id))
    {
      return true;
    }

    $old = $this->_table->get( $entity->id);

    if (empty($old->get($this->getConfig('lat')))) {
      return true;
    }

    foreach( $addressfields as $field)
    {
      if( $entity->get( $field) != $old->get( $field))
      {
        return true;
      }
    }

    return false;
  }

  /**
   * Custom finder for distance.
   *
   * Used to be a virtual field in 2.x via setDistanceAsVirtualField()
   *
   * Options:
   * - lat (required)
   * - lng (required)
   * - tableName
   * - distance
   *
   * @param \Cake\ORM\Query $query Query.
   * @param array $options Array of options as described above
   * @return \Cake\ORM\Query
   */
  public function findDistance(Query $query, array $options) {
    $options += ['tableName' => null];
    $sql = $this->distance($options['lat'], $options['lng'], null, null, $options['tableName']);
    $query->select(['distance' => $query->newExpr($sql)]);
    if (isset($options['distance'])) {
      $query->where(['distance <' => $options['distance']]);
    }
    return $query->order(['distance' => 'ASC']);
  }

  /**
   * Adds the distance to this point as a virtual field.
   * Make sure you have set configs lat/lng field names.
   *
   * @param string|float $lat Fieldname (Model.lat) or float value
   * @param string|float $lng Fieldname (Model.lng) or float value
   * @return void
   * @deprecated Use custom finder / findDistance instead.
   */
  public function setDistanceAsVirtualField($lat, $lng, $tableName = null) {
    $this->_table->virtualFields['distance'] = $this->distance($lat, $lng, null, null, $tableName);
  }

  /**
   * Forms a sql snippet for distance calculation on db level using two lat/lng points.
   *
   * @param string|float $lat Fieldname (Model.lat) or float value
   * @param string|float $lng Fieldname (Model.lng) or float value
   * @return string
   */
  public function distance($lat, $lng, $fieldLat = null, $fieldLng = null, $tableName = null) {
    if ($fieldLat === null) {
      $fieldLat = $this->_config['lat'];
    }
    if ($fieldLng === null) {
      $fieldLng = $this->_config['lng'];
    }
    if ($tableName === null) {
      $tableName = $this->_table->alias();
    }

    $value = $this->_calculationValue($this->_config['unit']);

    return $value . ' * ACOS(COS(PI()/2 - RADIANS(90 - ' . $tableName . '.' . $fieldLat . ')) * ' .
      'COS(PI()/2 - RADIANS(90 - ' . $lat . ')) * ' .
      'COS(RADIANS(' . $tableName . '.' . $fieldLng . ') - RADIANS(' . $lng . ')) + ' .
      'SIN(PI()/2 - RADIANS(90 - ' . $tableName . '.' . $fieldLat . ')) * ' .
      'SIN(PI()/2 - RADIANS(90 - ' . $lat . ')))';
  }

  /**
   * Snippet for custom pagination
   *
   * @return array
   */
  public function distanceConditions($distance = null, $fieldName = null, $fieldLat = null, $fieldLng = null, $tableName = null) {
    if ($fieldLat === null) {
      $fieldLat = $this->_config['lat'];
    }
    if ($fieldLng === null) {
      $fieldLng = $this->_config['lng'];
    }
    if ($tableName === null) {
      $tableName = $this->_table->alias();
    }
    $conditions = [
      $tableName . '.' . $fieldLat . ' <> 0',
      $tableName . '.' . $fieldLng . ' <> 0',
    ];
    $fieldName = !empty($fieldName) ? $fieldName : 'distance';
    if ($distance !== null) {
      $conditions[] = '1=1 HAVING ' . $tableName . '.' . $fieldName . ' < ' . intval($distance);
    }
    return $conditions;
  }

  /**
   * Snippet for custom pagination
   *
   * @return string
   */
  public function distanceField($lat, $lng, $fieldName = null, $tableName = null) {
    if ($tableName === null) {
      $tableName = $this->_table->alias();
    }
    $fieldName = (!empty($fieldName) ? $fieldName : 'distance');
    return $this->distance($lat, $lng, null, null, $tableName) . ' AS ' . $tableName . '.' . $fieldName;
  }

  /**
   * Snippet for custom pagination
   * still useful?
   *
   * @return string
   */
  public function distanceByField($lat, $lng, $byFieldName = null, $fieldName = null, $tableName = null) {
    if ($tableName === null) {
      $tableName = $this->_table->alias();
    }
    if ($fieldName === null) {
      $fieldName = 'distance';
    }
    if ($byFieldName === null) {
      $byFieldName = 'radius';
    }

    return $this->distance($lat, $lng, null, null, $tableName) . ' ' . $byFieldName;
  }

  /**
   * Snippet for custom pagination
   *
   * @return int count
   */
  public function paginateDistanceCount($conditions = null, $recursive = -1, $extra = []) {
    if (!empty($extra['radius'])) {
      $conditions[] = $extra['distance'] . ' < ' . $extra['radius'] .
        (!empty($extra['startRadius']) ? ' AND ' . $extra['distance'] . ' > ' . $extra['startRadius'] : '') .
        (!empty($extra['endRadius']) ? ' AND ' . $extra['distance'] . ' < ' . $extra['endRadius'] : '');
    }
    if (!empty($extra['group'])) {
      unset($extra['group']);
    }
    $extra['behavior'] = true;
    return $this->_table->paginateCount($conditions, $recursive, $extra);
  }

  /**
   * Returns if a latitude is valid or not.
   * validation rule for models
   *
   * @param float $latitude
   * @return bool
   */
  public function validateLatitude($latitude) {
    if (is_array($latitude)) {
      $latitude = array_shift($latitude);
    }
    return ($latitude <= 90 && $latitude >= -90);
  }

  /**
   * Returns if a longitude is valid or not.
   * validation rule for models
   *
   * @param float $longitude
   * @return bool
   */
  public function validateLongitude($longitude) {
    if (is_array($longitude)) {
      $longitude = array_shift($longitude);
    }
    return ($longitude <= 180 && $longitude >= -180);
  }

  /**
   * Uses the Geocode class to query
   *
   * @param array $addressFields (simple array of address pieces)
   * @return array
   */
  protected function _geocode($addressFields) {
    $address = implode(' ', $addressFields);
    if (empty($address)) {
      return [];
    }

    $geocodeOptions = [
      'log' => $this->_config['log'], 'min_accuracy' => $this->_config['min_accuracy'],
      'expect' => $this->_config['expect'], 'allow_inconclusive' => $this->_config['allow_inconclusive'],
      'host' => $this->_config['host'],
      'apiKey' => Website::get('settings.google_maps')
    ];

    $this->Geocode = new Geocoder($geocodeOptions);

    $config = ['language' => $this->_config['language']];
    try {
      if (!$this->Geocode->geocode($address, $config)) {
        return ['lat' => null, 'lng' => null, 'formatted_address' => ''];
      }
    } catch (\Exception $e) {
      _d( $e);
    }

    try {
      $return = $this->Geocode->geocode( $address);
    } catch (\Exception $e) {
      return false;
    }
    return $return;
  }

  /**
   * Get the current unit factor
   *
   * @param int $unit Unit constant
   * @return float Value
   */
  protected function _calculationValue($unit) {
    if (!isset($this->Geocode)) {
      $this->Geocode = new Geocoder();
    }
    return $this->Geocode->convert(6371.04, Geocoder::UNIT_KM, $unit);
  }
}
