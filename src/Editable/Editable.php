<?php 

namespace Manager\Editable;

use Cake\I18n\I18n;
use User\Auth\Access;
use Cake\Utility\Hash;
use Cake\Routing\Router;
use Cofree\Buffer\Buffer;
use Cofree\ParseJs\JsExpr;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cofree\ParseJs\ParseJs;
use Manager\FieldAdapter\CkeditorFieldAdapter;

class Editable
{

	public static $sessionKey = 'Auth.User';

  public static $noEdit = false;

  public static function setNoEdit()
  {
    self::$noEdit = true;
  }

  public function user( $key = null) 
  {
    $user = @$_SESSION ['Auth']['User'];

    if( !$user)
    {
      return false;
    }

		if ($key === null) 
    {
			return $user;
		}

		return Hash::get( $user, $key);
  }
  
  public static function field( $content, $field, $type = null, $default = null, $strip_tags = false)
  {
    if( $type === null)
    {
      $type = TableRegistry::getTableLocator()->get( $content->source())->getSchema()->getColumnType( $field);
    }

    if( !self::user() || self::$noEdit)
    {
      $body = $content->$field;

      if( $type != 'text' || $strip_tags)
      {
        $body = strip_tags( $body, '<br><strong><b><em>');
      }

      if( empty( $body) && $default)
      {
        $body = $default;
      }

      if( $type == 'text')
      {
        $body = str_replace( '<table', '<div class="c-table-responsive"><table', $body);
        $body = str_replace( '</table>', '</table></div>', $body);
      }
      
      return $body;
    }

    

    $permission = Access::check( self::user( 'group_id'), self::editUrl( $content->source()));

    if( !$permission)
    {
      $text = $content->$field;

      if( empty( $text) && $default)
      {
        $text = $default;
      }

      return $text;
    }

    $id = Inflector::variable( $content->source()) . '-'. $field . '-'. $content->id;

    $body = $content->$field;

    if( empty( $body))
    {
      $body = $default ? $default : __d( 'app', 'Haz click para editar el contenido');
    }
      
    if( $type != 'text')
    {
      $body = strip_tags( $body, 'br');
    }

    $return = '<div id="'. $id .'" contenteditable="true">'. $body .'</div>';
    $script = 'CKEDITOR.disableAutoInline = true; CKEDITOR.inline( "'. $id .'", '. self::getConfiguration( $content, $field, $type) .');';

    Buffer::start();
    echo '<script>'. $script . '</script>';
    Buffer::end();
    return $return;
  }

  public static function getConfiguration( $content, $field, $type)
  {
    $url = self::editUrl( $content->source());
    $type = $type ? $type : TableRegistry::getTableLocator()->get( $content->source())->getSchema()->getColumn( $field)['type'];
    list( $plugin, $controller) = pluginSplit( $content->source());
    $adapter = new CkeditorFieldAdapter();
    $ck_config = $adapter->get( $controller, $field);
    $url [] = $field;
    $url [] = $content->id;
    $url [] = I18n::getLocale();
    $url ['_ext'] = 'json';

    $ck_config ['inlinesave'] = [
      'postUrl' => Router::url( $url),
      'useColorIcon' => true,
      'onSave' => new JsExpr(
        "function(editor){
          editor.focusManager.blur( true);
          $('#save-in-line-spinner').show();
        }"
      ),
      'onSuccess' => new JsExpr(
        "function(editor){
          $('#save-in-line-spinner').hide();
        }"
      )
      
    ];

    if( $type == 'text')
    {
      $ck_config ['toolbar'][] = [
        'name' => 'editor',
        'items' => ['Inlinesave']
      ];
    }
    else
    {
      $ck_config ['enterModel'] = 'CKEDITOR.ENTER_BR';
      $ck_config ['toolbar'] = [[
        'name' => 'editor',
        'items' => ['Inlinesave']
      ]];

      $ck_config ['allowedContent'] = 'br';
    }
    


    return ParseJs::render( $ck_config);
  }

  public static function editUrl( $controller)
  {
    list( $plugin, $controller) = pluginSplit( $controller);
    $url = [
      'prefix' => 'admin',
      'plugin' => $plugin,
      'controller' => $controller,
      'action' => 'field'
    ];

    return $url;
  }
}