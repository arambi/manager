<?php

namespace Manager\Traits;

use Cake\ORM\TableRegistry;

/**
 * Pone los campos de enlace para un Crud
 * Estos campos proporcionan al contenido la posibilidad de tener un enlace de tipo section, url o doc
 *
 * Es necesario que el Model tenga
 *
 * - Un campo url
 * - Un campo settings que sea Jsonable
 * - Un campo docs 
 */
trait LinksConfigTrait
{
    public function setLinkFields($agree = true, $assets_multiple = false, $with_option = false)
    {
        if ($with_option) {
            $show = 'content.special';
        }  else {
            $show = 'true';
        }

        $this->crud->addFields([
            // Enlace
            'settings.link_type' => [
                'label' => 'Tipo de enlace',
                'type' => 'select',
                'options' => [
                    'section' => 'Una sección',
                    'url' => 'Una URL',
                    'file' => 'Un fichero'
                ],
                'show' => $show
            ],
            'settings.target_blank' => [
                'label' => 'Abrir en una ventana nueva',
                'type' => 'boolean',
                'show' => $show
            ],
            'url' => [
                'label' => __d('admin', 'URL'),
                'type' => 'string',
                'show' => "$show && content.settings.link_type == 'url' "
            ],
            'parent_id' => [
                'label' => __d('admin', 'Enlace a sección'),
                'type' => 'select',
                'options' => function ($crud) {
                    return TableRegistry::getTableLocator()->get('Section.Sections')->selectOptions();
                },
                'show' => "$show && content.settings.link_type == 'section' "
            ],
            'docs' => [
                'type' => $assets_multiple ? 'uploads' : 'upload',
                'label' => __d('admin', 'Fichero'),
                'config' => [
                    'type' => 'doc',
                    'langs_multiple' => $assets_multiple
                ],
                'show' => "$show && content.settings.link_type == 'file'"
            ],
        ]);

        if ($with_option) {
            $this->crud->addFields([
                'special' => 'Con enlace',
            ]);
        }

        if ($agree) {
            $elements = [];

            if ($with_option) {
                $elements [] = 'special';
            }

            $elements = array_merge($elements, [
                'settings.link_type',
                'url',
                'docs',
                'parent_id',
                'settings.target_blank',
            ]);

            $this->crud->addBoxToColumn(['update', 'create'], 'general', [
                'title' => __d('admin', 'Enlace'),
                'elements' => $elements
            ]);
        }
    }

    public function getLink($block, $assets_multiple = false)
    {
        if (empty($block->settings->link_type)) {
            return null;
        }

        if ($block->settings->link_type == 'section') {
            $url = TableRegistry::getTableLocator()->get('Section.Sections')->getUrl($block->parent_id);
        } elseif ($block->settings->link_type == 'url') {
            $url = $block->url;
        } elseif ($block->settings->link_type == 'doc' || $block->settings->link_type == 'file') {

            if ($assets_multiple) {
                $doc = $block->doc->currentLocaleMultiple();
                if ($doc) {
                    $url = $doc->paths[0];
                }
            } else {
                $url = $block->docs->paths[0];
            }
        }

        return $url;
    }
}
