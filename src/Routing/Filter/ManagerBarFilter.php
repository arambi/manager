<?php

namespace Manager\Routing\Filter;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Event\EventManagerTrait;
use Cake\ORM\TableRegistry;
use Cake\Routing\DispatcherFilter;
use Cake\Routing\Router;
use User\Auth\Access;

class ManagerBarFilter extends DispatcherFilter
{

  use EventManagerTrait;



  public function beforeDispatch(Event $event)
  {

  }

  public function afterDispatch(Event $event)
  {
    $request = $event->data ['request'];

    if( $request->getParam( 'prefix') != 'admin')
    {
      if( $request->session()->check( 'Auth.User'))
      {
        if( Access::hasAdminPermissions( $request->session()->read( 'Auth.User.group_id')))
        {
          $this->_injectScripts( $event->data ['response']);
        }
      }

      if( Configure::read( 'FrontLinks'))
      {
        $request->session()->write( 'Bar.frontLinks', Configure::read( 'FrontLinks'));
      }
    }
  }

  /**
   * Injects the JS to build the toolbar.
   *
   * The toolbar will only be injected if the response's content type
   * contains HTML and there is a </body> tag.
   *
   * @param string $id ID to fetch data from.
   * @param \Cake\Network\Response $response The response to augment.
   * @return void
   */
  protected function _injectScripts( $response)
  {
    if( strpos( $response->type(), 'html') === false) {
      return;
    }
    $body = $response->body();
    $pos = strpos( $body, '</html>');

    if( $pos === false) 
    {
      return;
    }

    $url = Router::url( [
      'prefix' => 'admin',
      'plugin' => 'Manager',
      'controller' => 'Pages',
      'action' => 'bar'
    ], true);
    $script = "<script>var __manager_base_url = '${url}';</script>";
    $script .= '<script src="' . Router::url('/manager/js/bar.js') . '"></script>';
    $body = substr( $body, 0, $pos) . $script . substr($body, $pos);
    $response->body($body);
  }
}
