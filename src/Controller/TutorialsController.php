<?php

namespace Manager\Controller;

use Cake\Core\Configure;
use Manager\Controller\AppController;

/**
 * Tutorials Controller
 *
 *
 * @method \Manager\Model\Entity\Tutorial[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TutorialsController extends AppController
{
    public function initialize()
    {
        if (isset($this->Auth)) {
            $this->Auth->allow();
        }
    }

    public function view()
    {
        $this->viewBuilder()->setLayout('ajax');
        $param = $this->request->getParam('video');
        $video = $this->getVideo($param);
        $this->set(compact('video'));
        $this->setRelateds($video);
    }
    
    private function getVideo($param)
    {
        return Configure::read('Tutorials.videos.' . $param);
    }

    private function setRelateds($video)
    {
        $relateds = [];

        if (empty($video['relateds'])) {
            return $relateds;
        }

        foreach ($video['relateds'] as $param) {
            $relateds [$param] = $this->getVideo($param);
        }

        $this->set( compact( 'relateds'));
    }
}
