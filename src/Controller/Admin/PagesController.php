<?php
namespace Manager\Controller\Admin;

use Manager\Controller\AppController;
use Manager\Navigation\NavigationCollection;
use Manager\Lib\CSSParser;
use Cake\Routing\Router;

/**
 * Pages Controller
 *
 * @property Manager\Model\Table\PagesTable $Pages
 */
class PagesController extends AppController 
{
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');

    $this->Auth->allow( 'ckeditorcss');
  }

	public function home()
	{
    $this->viewBuilder()->layout( 'Manager.admin');
    $this->viewBuilder()->theme( 'Manager');
    $this->viewBuilder()->templatePath( 'Crud');
	}

  public function bar()
  {
    $this->viewBuilder()->layout( 'Manager.bar');
    $this->viewBuilder()->theme( 'Manager');
    $this->viewBuilder()->templatePath( 'Crud');
  }

  public function dashboard()
  {
    $nav = NavigationCollection::nav();
    $group_id = $this->Auth->user( 'group_id');

    foreach( $nav as $key => $el)
    {
      if( !$el ['url'] && !NavigationCollection::hasChildrensPermissions( $el, $group_id))
      {
        unset( $nav [$key]);
      }
      elseif( $el ['url'] && !NavigationCollection::hasPermissions( $el ['url'], $group_id))
      {
        unset( $nav [$key]);
      }

      if( array_key_exists( $key, $nav))
      {
        $nav [$key]['url'] = $nav [$key]['url'] ? Router::url( $nav [$key]['url']) : false;
      }
    }

    $nav = array_values( $nav);

    foreach( $nav as &$_nav)
    {
      if( !$_nav ['url'] && !empty( $_nav ['children']))
      {
        $_nav ['url'] = Router::url( $_nav ['children'][0]['url']);
      }
      foreach( $_nav ['children'] as &$children)
      {
        $children ['url'] =  Router::url( $children ['url']);
      }
    }

    $this->set([
      'data' => [
        'crudConfig' => [
          'view' => [
            'template' => 'Manager/dashboard'
          ]
        ],
        'nav' => $nav
      ],
      '_serialize' => ['data']
    ]);
  }

  public function nav()
  {
    $nav = NavigationCollection::nav();

    foreach( $nav as &$el)
    {
      if( $el ['url'])
      {
        $el ['url'] = Router::url( $el ['url']);
      }

      if( !empty( $el ['children']))
      {
        foreach( $el ['children'] as &$_el)
        {
          if( $_el ['url'])
          {
            $_el ['url'] = Router::url( $_el ['url']);
          }
        }
      }
    }

    $this->set([
      'nav' => $nav,
      '_serialize' => true
    ]);
  }


  public function ckeditorcss()
  {
    $this->viewBuilder()->layout( 'ajax');
    $css = file_get_contents( WWW_ROOT. $this->request->query( 'file'));

    if( !empty( $css))
    {
      $parser = new CSSParser();
      $index = $parser->ParseCSS( $css);
      $classes = $parser->GetCSSArray( $index);

      $styles = [];

      foreach( $classes as $class => $styles)
      {
        $css = str_replace( $class, '.cke_editable_inline '. $class, $css);
        $parts = explode( ' ', $class);

        $el = $parts [0];

        if( strpos( $el, '.') !== false)
        {
          list( $tag, $className) = explode( '.', $el);
          $styles [] = [
            'name' => $className,
            'element' => $tag,
          ];
        }
        else
        {
          $styles [] = [
            'name' => $className,
          ];
        }
      }
    }

    $this->response->type( 'css');
    $this->set( compact( 'css'));
  }
	
}
