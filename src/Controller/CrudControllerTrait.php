<?php

namespace Manager\Controller;

use ArrayObject;
use Cake\I18n\I18n;
use Cake\Event\Event;
use Manager\Crud\Flash;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\Event\EventManager;
use Cake\Http\Exception\NotFoundException;

trait CrudControllerTrait
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Manager.CrudTool');
        $this->loadComponent('Manager.Filter');
        $this->loadComponent('Manager.Manager');
    }

    /**
     * Listado de contenidos paginado
     * Se realiza mediante el component Filter, que se encarga de construir el Query en base a los parámetros de búsqueda posibles
     *
     * Es posible modificar el objeto $query mediante un método en el controller llamado _index( $query), que recibirá como parámetro el objeto en sí.
     */
    public function index()
    {
        $this->Table->crud->setQueryParams($this->request->getQuery());
        $query = $this->Table->find();

        if (method_exists($this, '_index')) {
            $this->_index($query);
        }

        $this->Filter->query($query);

        $this->paginate['limit'] = $this->Table->crud->limit() ? $this->Table->crud->limit() : 20;

        if ($this->request->params['_ext'] === 'csv') {
            return $this->exportCSV($query);
        } else {
            $this->CrudTool->addSerialized([
                'contents' => $this->paginate($query),
            ]);
        }
    }

    private function exportCSV($query)
    {
        $this->CrudTool->serializeAction(false);
        $fields = $this->Table->crud->getView($this->CrudTool->getAction())->get('csvFields');
        $this->viewBuilder()->setLayout('ajax');
        $limit = 50;
        $offset = 0;
        $file = TMP . rand(0, 99999) . '_' . time() . '.csv';
        $output = fopen($file, 'w');
        $header = array_values($fields);

        fputcsv($output, $header, ';');

        while (true) {
            $_query = clone $query;
            $_query
                ->limit($limit)
                ->offset($offset)
                ->order($this->Table->getAlias() . '.' . $this->Table->getPrimaryKey());

            $contents = $_query->all();

            foreach ($contents as $content) {
                $data = $content->extract(array_keys($fields));
                $data = array_map('trim', $data);
                $data = array_map(function ($value) {
                    return strip_tags($value);
                }, $data);
                fputcsv($output, $data, ';');
            }

            $offset += $limit;

            if ($contents->count() < $limit) {
                break;
            }
        }

        fclose($output);

        $file_data = "\xEF\xBB\xBF";
        $file_data .= file_get_contents($file);
        file_put_contents($file, $file_data);
        $this->setResponse($this->response->withFile($file));
        $this->setResponse($this->response->withType('application/csv; charset=UTF-8'));
        $filename = $this->Table->crud->getName()['plural'] . '_' . Website::get('title') . '_' . date('d-m-Y') . '.csv';
        $this->setResponse($this->response->withDownload($filename));
        return $this->response;
    }

    /**
     * Action create para todos los controllers que enchufen este Trait
     *   
     * @return void
     */
    public function create()
    {
        $data = $this->request->data;

        if (method_exists($this, '_beforeCreate')) {
            $this->_beforeCreate($data);
        }

        $this->CrudTool->setAction('create');

        $content = $this->Table->getNewEntity($data);

        if (method_exists($this, '_beforeNewEntity')) {
            $this->_beforeNewEntity($content);
        }

        // Toma los parámetros pasados por query
        if (!empty($this->request->query)) {
            foreach ($this->request->query as $key => $value) {
                $content->set($key, $value);
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            // Guarda el contenido
            if ($this->Table->saveContent($content)) {
                if (method_exists($this, '_afterSave')) {
                    $this->_afterSave($this->Table->find('content')->where([$this->Table->alias() . '.' . $this->Table->getPrimaryKey() => $content->id])->first(), true);
                }

                Flash::success(__d('admin', 'El contenido ha sido guardado correctamente'));
                $this->update($content->id, true);
                $here = str_replace('.json', '', $this->request->here());
                $redirect = str_replace('create', 'update', $here) . '/' . $content->id;

                if (method_exists($this, '_getCreateRedirect')) {
                    $redirect = $this->_getCreateRedirect($content->id);
                }

                $this->set('redirect', $redirect);
            } else {
                // Si el contenido no se ha podido guardar, se setea la entidad
                $this->Table->crud->setEntity($content);
                $this->Table->crud->setContent($this->Table->emptyEntityProperties($content));
            }
        } else {
            $this->Table->crud->setContent($this->Table->emptyEntityProperties($content));
        }

        // Si se declara el método _create en el controller, se ejectutará en este instante
        if (method_exists($this, '_create')) {
            $this->_create();
        }
    }

    public function update($id = false, $created = false)
    {
        $data = $this->request->data;

        if (method_exists($this, '_beforeUpdate')) {
            $this->_beforeUpdate($id, $data);
        }

        // Setea la acción para que configuren los datos enviados a la vista para ser usados por AngularJS
        $this->CrudTool->setAction('update');
        $_id = $id ? $id : ($this->request->data('id') ? $this->request->data('id') : false);
        $query = $this->Table->find('content')->where([$this->Table->alias() . '.' . $this->Table->getPrimaryKey() => $_id]);

        if (method_exists($this, '_beforeFindUpdate')) {
            $this->_beforeFindUpdate($query);
        }

        $content = $query->first();

        if (!$created && $this->request->is(['patch', 'post', 'put'])) {
            $content = $this->Table->find('content')->where([$this->Table->alias() . '.' . $this->Table->getPrimaryKey() => $_id])->first();
            $this->Table->patchContent($content, $data);

            $options = new ArrayObject();

            if (method_exists($this, '_beforeSave')) {
                $this->_beforeSave($content, $options);
            }

            if ($this->Table->saveContent($content, (array)$options)) {
                $query = $this->Table->find('array')->where([$this->Table->alias() . '.' . $this->Table->getPrimaryKey() => $_id]);

                if (method_exists($this, '_afterSave')) {
                    $this->_afterSave($content);
                }

                if (method_exists($this, '_beforeUpdateFind')) {
                    $this->_beforeUpdateFind($query, $content);
                }

                $array = $query->first();
                $this->Table->crud->setEntity($content);
                $this->Table->crud->setContent($array);

                if (method_exists($this->Table, 'getPreviewUrl')) {
                    $url = $this->Table->getPreviewUrl($content);
                    $this->Table->crud->addConfig([
                        'previewUrl' => Router::url([
                            'plugin' => 'section',
                            'controller' => 'sections',
                            'action' => 'previewredirect',
                            'url' => $url
                        ], true)
                    ]);
                } else {
                    try {
                        if (method_exists($content, 'link')) {
                            $url = Router::url($content->link('slug'), true);
                            $this->Table->crud->addConfig([
                                'previewUrl' => Router::url([
                                    'plugin' => 'section',
                                    'controller' => 'sections',
                                    'action' => 'previewredirect',
                                    'url' => $url
                                ], true)
                            ]);
                        }
                    } catch (\Exception $e) {
                    }
                }

                Flash::success(__d('admin', 'El contenido ha sido guardado correctamente'));
            } else {
                $this->Table->crud->setEntity($content);
                $this->Table->crud->setContent($this->Table->emptyEntityProperties($content));
            }
        } else {
            $content = $this->Table->find('content')->where([$this->Table->alias() . '.' . $this->Table->getPrimaryKey() => $_id])->first();
            $query = $this->Table->find('array')->where([$this->Table->alias() . '.' . $this->Table->getPrimaryKey() => $_id]);

            if (method_exists($this, '_beforeUpdateFind')) {
                $this->_beforeUpdateFind($query, $content);
            }

            $array = $query->first();
            $this->Table->crud->setEntity($content);
            $this->Table->crud->setContent($array);
        }


        if (method_exists($this, '_update')) {
            $this->_update($content);
        }
    }

    public function draft($id)
    {
        $content = $this->Table->createDraft($id);
        $this->redirect('/admin#/' . Router::url([
            'action' => 'update',
            $content->id
        ]));
    }

    public function previewredirect()
    {
        $this->request->session()->write('Manager.preview', true);
        $this->redirect($this->request->query('url'));
    }

    public function saveDraft($id)
    {
        $this->Table->crud->action('savedraft');

        $content = $this->Table->saveDraft($id);
        $this->redirect('/admin#/' . Router::url([
            'action' => 'update',
            $content->id
        ]));
    }

    public function deletedraft($id, $salt)
    {
        $draft = $this->Table->Drafts->find()
            ->where([
                'Drafts.content_id' => $id
            ])
            ->first();

        if (!$draft) {
            throw new NotFoundException(__d('admin', 'No se ha encontrado el borrador'));
        }

        $this->Table->crud->action('deletedraft');

        $content = $this->Table->find()
            ->where([
                $this->Table->alias() . '.' . $this->Table->getPrimaryKey() => $id,
                $this->Table->alias() . '.salt' => $salt
            ])
            ->first();


        if (!$content) {
            throw new NotFoundException(__d('admin', 'No se ha encontrado el borrador'));
        }

        if ($this->Table->delete($content)) {
            $this->Table->Drafts->delete($draft);
            $this->redirect('/admin#/' . Router::url([
                'action' => 'update',
                $draft->foreign_key
            ]));
        }
    }

    public function delete()
    {
        if (!$this->request->data('id')) {
            throw new NotFoundException(__d('admin', 'No ha sido posible borrar el contenido'));
        }

        $content = $this->Table->find()->where([
            $this->Table->alias() . '.' . $this->Table->getPrimaryKey() => $this->request->data('id'),
        ])->first();

        if (!$content) {
            throw new NotFoundException(__d('admin', 'No ha sido posible borrar el contenido'));
        }

        if ($result = $this->Table->delete($content)) {
            $this->CrudTool->addSerialized([
                'isDeleted' => true,
                'deleteErrors' => $content->get('deleteErrors')
            ]);
        } else {
            $this->CrudTool->addSerialized([
                'isDeleted' => false,
                'deleteErrors' => $content->get('deleteErrors')
            ]);
        }
    }

    public function serialize($action)
    {
        $this->CrudTool->setAction($action);

        if (method_exists($this, '_beforeSerialize')) {
            $this->_beforeSerialize();
        }
    }

    public function order()
    {
        $this->Table->reordering($this->request->data);
        $this->set([
            'response' => $this->response,
            '_serialize' => 'response'
        ]);
    }

    public function sortable()
    {
        foreach ($this->request->data('data') as $position => $id) {
            $query = $this->Table->query();
            $query->update()
                ->set(['position' => $position])
                ->where([$this->Table->primaryKey() => $id])
                ->execute();
        }
    }

    public function duplicate($id)
    {
        $content = $this->Table->duplicate($id);
        $this->update($content->id);
    }

    public function autocomplete()
    {
        $query = $this->Table->find()->where([
            $this->Table->alias() . '.' . $this->Table->displayField() . ' LIKE' => '%' . $this->request->query['q'] . '%'
        ]);

        $event = new Event('Manager.Controller.' . $this->Table->alias() . '.beforeAutocomplete', $this, [
            $query,
            $this->request->query['q']
        ]);

        EventManager::instance()->dispatch($event);
        
        $contents = $query->all();
        

        $this->CrudTool->addSerialized([
            'results' => $contents->toArray()
        ]);
    }

    public function field($field, $id, $locale = false)
    {
        $data = $this->request->data;

        if (!array_key_exists('editabledata', $data)) {
            return;
        }

        $value = $data['editabledata'];

        $column = @$this->Table->getSchema()->getColumn($field);

        if ($column && $column['type'] == 'date') {
            $value = date('Y-m-d', strtotime($value));
        }

        $association = $this->Table->associations()->getByProperty($field);

        if ($association && $association->type() == 'manyToOne') {
            $field = $association->getForeignKey();
            $value = $value['id'];
        }

        $entity = $this->Table->find()
            ->where([
                $this->Table->getAlias() . '.' . $this->Table->getPrimaryKey() => $id
            ])
            ->first();

        $entity->set($this->Table->getPrimaryKey(), $id);

        if ($this->Table->hasBehavior(Configure::read('I18n.behaviorName')) && $this->Table->hasTranslate($field)) {
            $this->Table->patchEntity($entity, [
                '_translations' => [
                    $locale => [
                        $field => $value,
                    ]
                ]
            ]);
            // $entity->translation($locale)->set($field, $value);
        } else {
            $entity->set($field, $value);
        }

        if ($this->Table->save($entity)) {
        } else {
            \Cake\Log\Log::debug($entity->getErrors());;
        }

        $this->Table->crud->setContent($entity);
        $this->set('_serialize', true);
    }

    public function bulk()
    {
        $request = $this->getRequest();
        $this->set('_serialize', true);

        $method = '__' . $request->getData('method');
        $ids = $request->getData('ids');
        $all = $request->getData('all');

        if ((!empty($ids) || $all) && method_exists($this, $method)) {
            $this->$method($ids, $all);
        }
    }

    private function __delete($ids, $all)
    {
        if ($all) {
            $query = $this->Table->find();

            $limit = 20;

            while (true) {
                $query = $this->Table->find();
                $this->Filter->query($query, 'index');
                $query
                    ->limit($limit);

                $contents = $query->toArray();

                if (count($contents) == 0) {
                    break;
                }

                foreach ($contents as $content) {
                    $this->Table->delete($content);
                }
            }
        } else {
            foreach ($ids as $id) {
                $content = $this->Table->find()->where([
                    $this->Table->alias() . '.' . $this->Table->getPrimaryKey() => $id
                ])->first();

                if ($content) {
                    $this->Table->delete($content);
                }
            }
        }
    }
}
