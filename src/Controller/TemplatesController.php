<?php
namespace Manager\Controller;

use Manager\Controller\AppController;

/**
 * Templates Controller
 *
 * @property Manager\Model\Table\TemplatesTable $Templates
 */
class TemplatesController extends AppController 
{
	public function initialize()
	{
		parent::initialize();

		if( isset( $this->Auth))
		{
			$this->Auth->allow();
		}
	}

	public function base()
	{
		$this->viewBuilder()->layout( 'ajax');
		$this->viewBuilder()->theme( 'Manager');
    $this->viewBuilder()->templatePath( 'Crud');
	}

	public function modal()
	{
		$this->viewBuilder()->layout( 'ajax');
		$this->viewBuilder()->theme( 'Manager');
    $this->viewBuilder()->templatePath( 'Crud');
	}

	public function error()
	{
		$this->viewBuilder()->layout( 'ajax');
		$this->viewBuilder()->theme( 'Manager');
    $this->viewBuilder()->templatePath( 'Crud');
	}
}
