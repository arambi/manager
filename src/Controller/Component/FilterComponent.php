<?php
namespace Manager\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;

/**
 * Filter component
 */
class FilterComponent extends Component
{

  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [];

  protected $_searchParams = [];

/**
 * El controller
 *
 * @var \Cake\Controller\Controller
 */
  private $Controller = null;

  protected $_queryMethods = [
    'numeric' => '_queryNumeric',
    'string' => '_queryString',
    'info' => '_queryString',
    'select' => '_queryString',
    'ckeditor' => '_queryString',
    'boolean' => '_queryBoolean',
    'text' => '_queryText',
    'date' => '_queryDate',
    'time' => '_queryDate',
    'datetime' => '_queryDate',
    'BelongsTo' => '_queryBelongsTo',
    'BelongsToMany' => '_queryBelongsToMany',
    'checkboxes' => '_queryBelongsToMany',
    'select_multiple' => '_queryBelongsToMany',
    'autocomplete' => '_queryBelongsToMany'
  ];

  public function startup( $event)
  {
    $this->setController( $event->subject());
    $this->setTable( $this->Controller->Table);
  }


  public function queryMethod( $name)
  {
    return $this->_queryMethods [$name];
  }

/**
 * Setea el controller
 *
 * @param \Cake\Controller\Controller $controller Controller.
 * @return void
 */
  public function setController($controller)
  {
    $this->Controller = $controller;
  }

/**
 * Setea la tabla
 * 
 * @param Table $table
 */
  public function setTable( $table)
  {
    $this->Table = $table;
  }

/**
 * Devuelve true si la tabla tiene traducción
 * 
 * @return boolean
 */
  private function __hasTableTranslate()
  {
    return $this->Table->behaviors()->has( Configure::read( 'I18n.behaviorName'));
  }

/**
 * Devuelve true si el campo indicado tiene traducción
 * 
 * @param  string $key
 * @return boolean
 */
  private function __hasFieldTranslate( $key)
  {
    return $this->__hasTableTranslate() && $this->Table->hasTranslate( $key);
  }

  public function searchParams()
  {
    return $this->_searchParams;
  }

/**
 * Realiza un Query con los datos pasados en la query de la URL
 * 
 * @return Cake\ORM\Query
 */
  public function query( $query, $action = null)
  {
    $request = $this->getController()->getRequest();

    if( $action === null)
    {
      $action = $request->getParam('action');
    }

    $view = $this->Table->crud->buildView( $action);
    $fields = $view ['fields'];
    $filters = $view ['filters'];
    $params = $request->getQuery();

    if( $this->Table->behaviors()->has( 'I18nable'))
    {
      $this->Table->hasOne( 'I18n', [
        'foreignKey' => 'foreign_key',
        'conditions' => [
          'I18n.model' => $this->Table->alias(),
        ],
      ]);
    }

    if( $contain = $this->Table->crud->getContain( $action))
    {
      $query->contain( $contain);
    }
    else
    {
      $query->contain( $this->Table->containIndex());
    }    

    if( $this->Table->behaviors()->has( 'I18nable'))
    {
      $query->contain( 'I18n');
      $query->group( $this->Table->alias() .'.id');
    }

    foreach( $fields as $field)
    {
      if( !array_key_exists( $field ['key'], $params))
      {
        continue;
      }
      
      if( collection($filters)->firstMatch(['key' => $field['key']])) {
        continue;
      }

      $method = $this->queryMethod( $field ['type']);
      $this->$method( $query, $field, $params);
      $this->_searchParams [$field ['key']] = $params [$field ['key']];
    }

    foreach( $filters as $filter)
    {
      if( !array_key_exists( $filter ['key'], $params))
      {
        continue;
      }

      $filter ['filter']( $this->Table, $query, $params [$filter ['key']]);
      $this->_searchParams [$filter ['key']] = $params [$filter ['key']];
    }

    // El orden de los contenidos
    $this->order( $query);

    return $query;
  }


  public function order( $query)
  {
    $request = $this->getController()->getRequest();

    if( !$request->getQuery( 'sort'))
    {
      $query->order( $this->Table->crud->order());
    }
  }

/**
 * Hace un where en el Query dado, para un campo de texto tipo `string`
 * 
 * @param  Cake\ORM\Query $query
 * @param  array $field
 * @param  array $params
 * @return void
 */
  protected function _queryString( $query, $field, $params)
  {
    $key = $field ['key'];

    if( empty( $params [$key]))
    {
      return;
    }

    if( $field ['hasOne'])
    {
      $alias = $this->Table->association( $field ['hasOne'])->alias();
      $query->andWhere([ 
        $alias .'.'. $field ['key'] .' LIKE' => '%' . $params [$key] .'%',
      ]);
    }
    elseif( $this->__hasFieldTranslate( $key))
    {
      if( $this->Table->alias() == 'Posts')
      {
        $query->find( 'fulltext', [
          'text' => $params [$key],
          'all' => true
        ]);
      }
      else
      {
        if( Configure::read( 'I18n.behavior') == 'I18n.I18nTranslate')
        {
          $query->where([ 
            $this->Table->alias() . '.'. $field ['key'] .' LIKE' => '%' . $params [$key] .'%',
          ]);
        }
        else
        {
          $search_field = $this->Table->alias() .'_'. $field ['key'] .'_translation'; 
          $query->andWhere([ 
            $search_field . '.content LIKE' => '%' . $params [$key] .'%',
            $search_field . '.field' => $key
          ]);
        }
      }
    }
    elseif( in_array( $field ['type'], ['string', 'text', 'ckeditor']))
    {
      $query->andWhere([ $this->Table->alias() .'.'. $key .' LIKE' => '%' . $params [$key] .'%']);
    }
    else
    {
      $query->andWhere([ $this->Table->alias() .'.'. $key => $params [$key]]);
    }
  }


/**
 * Hace un where en el Query dado, para un campo de texto tipo `numeric`
 * 
 * @param  Cake\ORM\Query $query
 * @param  array $field
 * @param  array $params
 * @return void
 */
  protected function _queryNumeric( $query, $field, $params)
  {
    $key = $field ['key'];

    if( empty( $params [$key]))
    {
      return;
    }

    $query->andWhere([ $this->Table->alias() .'.'. $key => $params [$key]]);
  }

  protected function _queryText( $query, $field, $params)
  {
    $this->_queryString( $query, $field, $params);
  }

/**
 * Hace un where en el Query dado, para un campo de tipo `boolean`
 * 
 * @param  Cake\ORM\Query $query
 * @param  array $field
 * @param  array $params
 * @return void
 */
  protected function _queryBoolean( $query, $field, $params)
  {
    $key = $field ['key'];
    $query->where([ $key => $params [$key] ? 1 : 0]);
  }

/**
 * Hace un where en el Query dado, para un campo de tipo `date` o `datetime`
 * 
 * @param  Cake\ORM\Query $query
 * @param  array $field
 * @param  array $params
 * @return void
 */
  protected function _queryDate( $query, $field, $params)
  {
    $key = $field ['key'];

    if( !is_array( $params [$key]))
    {
      return;
    }

    $dateParams = $params [$key];

    if( array_key_exists( 'from', $dateParams))
    {
      $query->where([ $this->Table->alias() . '.'. $key .' >= ' => $params [$key]['from']]);
    }
    
    if( array_key_exists( 'to', $dateParams))
    {
      $query->where([ $this->Table->alias() . '.'. $key .' <= ' => $params [$key]['to']]);
    }
  }

/**
 * Hace un where en el Query dado, para una relación belongsTo
 * 
 * @param  Cake\ORM\Query $query
 * @param  array $field
 * @param  array $params
 * @return void
 */
  protected function _queryBelongsTo( $query, $field, $params)
  {
    $key = $field ['key'];

    $ids = [];

    foreach( $params [$key] as $_param)
    {
      $ids [] = $_param [$this->Table->getPrimaryKey()];
    }
    
    $association = $this->Table->associations()->getByProperty( $key);

    if( !empty( $ids))
    {
      $query->where([
        $this->Table->alias() .'.'. $association->foreignKey() .' IN' => $ids
      ]);
    }
  }

/**
 * Hace un where en el Query dado, para una relación belongsToMany
 * 
 * @param  Cake\ORM\Query $query
 * @param  array $field
 * @param  array $params
 * @return void
 */
  protected function _queryBelongsToMany( $query, $field, $params)
  {
    $key = $field ['key'];

    $ids = [];

    foreach( $params [$key] as $_param)
    {
      $ids [] = $_param ['id'];
    }
    
    $association = $this->Table->associations()->getByProperty( $key);

    if( !empty( $ids))
    {
      $query->matching( $association->alias(), function( $q) use( $association, $ids){
        return $q->where([
          $association->alias() .'.'. $association->primaryKey() .' IN' => $ids
        ]);
      });
    }
  }

}
