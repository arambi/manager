<?php
namespace Manager\Controller\Component;

use Cake\Core\App;
use I18n\Lib\Lang;
use Cake\I18n\I18n;
use Cake\ORM\Entity;
use Cake\Core\Plugin;
use Cake\Event\Event;
use Manager\Crud\Flash;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Event\EventManager;
use Manager\Lib\CrudConfig; 
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Manager\Navigation\NavigationCollection;

/**
 * CrudTool component
 */
class CrudToolComponent extends Component 
{

  public $components = ['Manager.Filter', 'Auth'];

/**
 * Default configuration.
 *
 * @var array
 */
	protected $_defaultConfig = [];

/**
 * Los datos JSON que se pasarán a la vista con setSerialized() 
 * @var array
 */
  protected $_serialized = [];


/**
 * El nombre de la acción actual, pasada desde el controller
 * Ver setAction() y getAction()
 * @var boolean
 */
  protected $_currentAction = false;


/**
 * Indica si la acción actual será serializada, es decir, si pasará como datos a la vista todo el proceso de CRUD
 * @var boolean
 */
  protected $_serializeAction = true;

  public $Table;


/**
 * initialize
 * @param  array  $config
 * @return void
 */
  public function initialize( array $config) 
  {
    
  }

/**
 * beforeFilter
 * @param  Event  $event
 * @return void
 */
  public function beforeFilter( Event $event)
  {
    $this->Controller = $this->_registry->getController();
    $this->__setTable();
    
    if( !$this->Table->behaviors()->has( 'Crudable')) 
    {
      throw new \RuntimeException( sprintf( 'It is necessary that the model %s behavior is linked to the Crudable', $this->Table->alias()));
    }
    
    $this->Table->setAdmin();
    $this->Table->crud->initializeFunctions();

    $event = new Event( 'Manager.Controller.CrudTool.'. $this->Table->alias() .'.beforeFilter', $this, [
      $this->Table
    ]);
    
    EventManager::instance()->dispatch( $event);
  }

/**
 * beforeRender
 * @param  Event  $event
 * @return
 */
  public function beforeRender( Event $event)
  {
    $controller = $event->getSubject();
    $this->setPagination();

    if( isset( $controller->request->params ['prefix']) && $controller->request->params ['prefix'] == 'admin')
    {
      $controller->helpers [] = 'Manager.AdminUtil';
      $controller->helpers [] = 'Manager.AdminNav';
      $this->Table->crud->addConfig([
        'currentUrl' => str_replace( '.json', '', $controller->request->here),
        'queryString' => env( 'QUERY_STRING'),
        'query' => $controller->request->query
      ]);


      if( $this->serializeAction())
      {
        // Coloca en la vista los datos
        if( $this->Table && isset( $this->Table->crud))
        {
          $languages = Lang::get();
          $this->addNavigation();
          $vars = [
            'languages' => $languages,
            'currentLanguage' => $languages [0],
            'locale' => I18n::getLocale(),
            'errors' => $this->Table->crud->entityErrors(),
            'search' => $this->Filter->searchParams(),
            'crudConfig' => $this->Table->crud->serialize( $this->getAction()),
            'flash' => Flash::get(),
            'administrator' => $this->getAdministrador(),
            'tutorials' => $this->getTutorials(),
          ];

          $this->Table->crud->beforeRender();

          if( empty( $controller->viewVars ['content']))
          {
            $vars ['content'] = $this->Table->crud->getContent();
          }

          $event = new Event( 'Manager.Controller.CrudTool.'. $this->Table->alias() .'.beforeSerialize', $this, [
            &$vars
          ]);
          EventManager::instance()->dispatch( $event);

          $this->addSerialized( $vars);
        }

        // Setea el JSON en la vista
        $this->setSerialized();
      }
    }
  }

  public function getTutorials()
  {
    $plugin = $this->Controller->request->getParam('plugin');
    $params = Configure::read('Tutorials.models.' .$plugin.'_'. $this->Table->getAlias());
    
    if (!$params) {
      return null;
    }

    $videos = [];

    foreach ($params as $param) {
      $video = Configure::read('Tutorials.videos.' . $param);
      $video['param'] = $param;
      $videos [] = $video;
    }

    return $videos;
  }

  public function getAdministrador()
  {
    $user = $this->Auth->user();

    if( $user)
    {
      return [
        'name' => $user ['name'],
        'email' => $user ['email'],
        'superadmin' => isset( $user ['group']['permissions'][0]) && $user ['group']['permissions'][0] == 'master'
      ];
    }
  }

/**
 * Store the controller.
 *
 * @param Controller $controller
 * @throws ForbiddenException
 */
  public function startup( Event $event) 
  {
  }

/**
 * Añade datos al array que pasará a la vista en formato JSON
 * 
 * @param array $data
 */
  public function addSerialized( $data)
  {
    foreach( $data as $key => $value)
    {
      $this->_serialized [$key] = $value;
    }
  }

  public function setPagination()
  {
    if( isset( $this->Controller->request->params ['paging'][$this->Table->alias()]))
    {
      $this->addSerialized( [
        'paging' => $this->Controller->request->params ['paging'][$this->Table->alias()]
      ]);
    }
  }

/**
 * Añade la configuración de la navegación, con el elemento actual
 */
  public function addNavigation()
  {
    $params = [
      'plugin' => $this->Controller->request->params ['plugin'],
      'controller' => $this->Controller->request->params ['controller'],
      'action' => $this->Controller->request->params ['action']
    ];

    $current = NavigationCollection::getCurrent( $params);

    if( $current)
    {
      $this->Table->crud->addConfig( array(
          'navigation' => $current->toArray()
      ));
    }
  }

/**
 * Set/Get el valor de $this->_serializeAction;
 *   
 * @return boolean
 */
  public function serializeAction()
  {
    $args = func_get_args();

    if( isset( $args [0]))
    {
      $this->_serializeAction = $args [0];
    }

    return $this->_serializeAction;
  }

  private function __setTable()
  {
    $this->Table = $this->getTable( $this->Controller->modelClass);
    $this->Controller->Table = $this->Table;
  }

  public function getTable( $modelClass)
  {
    list( , $model) = pluginSplit( $modelClass);

    return $this->Controller->$model;
  }

  public function setAction( $action)
  {
    $this->_currentAction = $action;
    $this->Table->crud->action( $action);
  }
  
  public function getAction()
  {
    $action = $this->_currentAction ? $this->_currentAction : $this->Controller->request->action;
    return $action;
  }

/**
 * Setea en la vista los datos guardados en $this->_serialized, en formato JSON
 *
 * @return  void
 */
  private function setSerialized()
  {
    $this->Controller->set( $this->_serialized);

    // Setea el view por defecto con el paradigma de CAKE
    if( empty( $this->Controller->viewVars ['crudConfig']['view']))
    {
      $this->Controller->viewVars ['crudConfig']['view']['template'] = 
        $this->request->param( 'plugin') .'/'. 
        strtolower( $this->request->param( 'controller')) .'/'. 
        strtolower( $this->request->param( 'action'));
    }

    $this->Controller->set([
      '_serialize' => true
    ]);
  }


  public function setAssociationData()
  {
    
  }

} 
