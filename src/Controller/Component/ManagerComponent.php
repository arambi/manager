<?php
namespace Manager\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Event\Event;
use Cake\Network\Response;
use Cake\Core\Configure;

/**
 * Manager component
 */
class ManagerComponent extends Component 
{
  public function beforeFilter( Event $event)
  {
    $this->Controller = $event->getSubject();

    if( isset( $_SERVER ['REQUEST_URI']) && strpos( $_SERVER ['REQUEST_URI'], '.') === false && 
      ($this->request->getSession()->read( 'Manager.preview') || $this->request->getQuery( 'managing') )
    )
    {
      Configure::write( 'Manager.preview', true);
      $this->request->session()->delete( 'Manager.preview');
    }
  }

  public function beforeRedirect( Event $event, $url, Response $response)
  {
    if( $url == '/admin/user/users/login' && $event->subject()->request->is( 'json'))
    {
      return false;
    }
  }

  public function to404()
  {
    $this->Controller->plugin = 'Manager';
    $this->Controller->viewPath = 'Crud';
    $this->Controller->view = '404';
    $this->Controller->render();
    $this->Controller->shutdownProcess();
  }


}
