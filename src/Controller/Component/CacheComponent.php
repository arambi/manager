<?php
namespace Manager\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Cache\Cache;

/**
 * Cache component
 */
class CacheComponent extends Component
{
  public $components = ['Auth'];

  public function first( $query, $param)
  {
    if( !$this->Auth->user())
    {
      $keyCache = $query->repository()->alias() . '.view.'. $param;
      $content = Cache::read( $keyCache);

      if( !$content)
      {
        $content = $query->first();
        Cache::write( $keyCache, $content);
      }
    }
    else
    {
      $content = $query->first();
    }

    return $content;
  }

  public function paginate( $query, $param)
  {
    if( !$this->Auth->user())
    {
      $md5 = md5( serialize( $this->request->param( 'paging')) . serialize( $this->request->query));
      $keyCache = $query->repository()->alias() . '.all.'. $param . $md5;
      $contents = Cache::read( $keyCache);

      if( !$contents)
      {
        $contents = $this->_registry->getController()->paginate( $query);
        Cache::write( $keyCache, $contents);
      }
    }
    else
    {
      $contents = $this->_registry->getController()->paginate( $query);
    }

    return $contents;
  }

  public function key()
  {
    $params = $this->request->params;
    unset( $params ['section']);
    unset( $params ['layout']);

    $data = array_merge( $params, $this->request->query);
    return md5(serialize( $data));
  }
}
