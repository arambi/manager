<?php
namespace Manager\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Utility\Inflector;
use Cake\Core\Configure;

/**
 * FrontBar component
 */
class FrontBarComponent extends Component
{

  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [];

  public function shutdown( Event $event)
  {
    $this->controller = $event->subject();
    $this->request = $this->controller->request;

    if( $this->__isAdmin())
    {
      return;
    }

    $this->__setLinks();
  }


  private function __setLinks()
  {
    if( $this->request->is( 'json'))
    {
      return;
    }
    
    $links = [];

    $content = $this->getMainContent();
    
    if( is_object( $content))
    {
      if (method_exists($content, 'adminLink')) {
        $links [] = $content->adminLink();
      } else {
        $url = Router::url([
          'prefix' => 'admin',
          'plugin' => $this->request->params ['plugin'],
          'controller' => $this->request->params ['controller'],
          'action' => 'update',
          $content->id
        ]);
  
        $links [] = '<a href="#'. $url . '"><i class="fa fa-edit"></i> ' . __d( 'admin', 'Editar contenido') . '</a>';
      }
    }

    if( !empty( $this->request->params ['section']))
    {
      $url = Router::url([
        'prefix' => 'admin',
        'plugin' => 'section',
        'controller' => 'sections',
        'action' => 'update',
        $this->request->params ['section']->id
      ]);

      $text = $content ? __d( 'admin', 'Editar sección') : __d( 'admin', 'Editar contenido');

      $links [] = '<a href="#'. $url . '"><i class="fa fa-edit"></i> ' . $text . '</a>';
    }

    $pieces = Configure::read( 'Pieces.collect');

    if( $pieces)
    {
      foreach( $pieces as $key => $piece_id)
      { 
        $url = Router::url([
          'prefix' => 'admin',
          'plugin' => 'section',
          'controller' => 'pieces',
          'action' => 'update',
          $piece_id
        ]);

        $links [] = '<a href="#'. $url . '"><i class="fa fa-edit"></i> Párrafo ' . ($key + 1) . '</a>';
      }

    }

    if( !empty( $links))
    {
      Configure::write( 'FrontLinks', $links);
    }
  }

  private function __isAdmin()
  {
    return isset( $this->request->params ['prefix']) && $this->request->params ['prefix'] == 'admin';
  }

  /**
 * Toma el contenido principal del web
 * Sirve para usar el contenido en titulos o enlaces a redes sociales
 * 
 * @return Entity 
 */
  public function getMainContent()
  {
    $viewVars = $this->controller->viewVars;
    $content = false;

    $variable = strtolower( Inflector::singularize( $this->request->controller));

    if( array_key_exists( 'content', $viewVars))
    {
      $content = $this->controller->viewVars ['content'];
    }
    elseif( array_key_exists( $variable, $viewVars))
    {
      $content = $this->controller->viewVars [$variable];
    }

    return $content;
  }
}
