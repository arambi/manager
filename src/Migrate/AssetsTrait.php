<?php 

namespace Manager\Migrate;

trait AssetsTrait
{

  private $files;
  
  public function setUrlFiles( $url)
  {
    $this->files = $url;
  }

  public function getPhotos( $data)
  {
    $values = [];

    foreach( $data ['photos'] as $photo)
    {
      $folder = $this->files . 'photos/org/'. $photo ['path'] . '/';
      $filename = 'ORG_'. $photo ['filename'] . $photo ['extension'];
      $upload = $this->asset( $filename, 'post', $folder, $photo);

      if( $upload)
      {
        $upload ['spa']['title'] = $this->decode( $photo ['text']);
        $values [] = $upload;
      }
    }

    if( empty( $values))
    {
      return null;
    }

    return $values;
  }

  public function getDocs( $data)
  {
    $values = [];

    foreach( $data ['docs'] as $photo)
    {
      $folder = $this->files . 'docs/'. $photo ['path'] . '/';
      $filename = $photo ['filename'] . $photo ['extension'];
      $upload = $this->asset( $filename, 'doc', $folder, $photo);

      if( $upload)
      {
        $upload ['spa']['title'] = $this->decode( $photo ['title']);
        $values [] = $upload;
      }
    }

    if( empty( $values))
    {
      return null;
    }
    return $values;
  }
}