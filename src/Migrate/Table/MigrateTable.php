<?php 

namespace Manager\Migrate\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Datasource\ConnectionManager;


class MigrateTable extends Table
{
  public $relations = [];

  public function initialize( array $config)
  {
    $this->connection( ConnectionManager::get( 'old'));
  }

  public function relationKey( $key)
  {
    if( array_key_exists( $key, $this->relations))
    {
      return $this->relations [$key];
    }

    return $key;
  }
}