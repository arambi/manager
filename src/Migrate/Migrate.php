<?php 

namespace Manager\Migrate;

use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\Core\Configure;
use Cake\Datasource\ModelAwareTrait;
use I18n\Lib\Lang;
use Manager\Migrate\UploadsTrait;

class Migrate
{
  use ModelAwareTrait;
  use UploadsTrait;

  public $oldTable;

  public $table;

  public $decodeText = false;

  private $namespace;

  private $setFieldTranslations = false;

  public function __construct( $namespace)
  {
    $this->setNamespace( $namespace);
    TableRegistry::get( 'Website.Sites')->setSite();

    $namespace = get_class( $this);
    $parts = explode( '\\', $namespace);
    $tablename = end( $parts);
    $tablename = str_replace( 'Migrate', '', $tablename);
    $className = $this->namespace . $tablename . 'Table';
    $this->oldTable = new $className([
      'alias' => $tablename,
    ]);

    $this->table = TableRegistry::get( $this->tableName);
    $this->Uploads = TableRegistry::get( 'Upload.Uploads');
  }

  public function setFieldTranslations( $value)
  {
    $this->setFieldTranslations = $value;
  }

  public function setNamespace( $namespace)
  {
    $this->namespace = $namespace;
  }

  public function truncate()
  {
    $limit = 400;
    $offset = 0;

    if( $this->table->hasBehavior( 'UploadJsonable'))
    {
      $uploads = $this->table->uploadsFields();
    }
    else
    {
      $uploads = false;
    }

    while( true)
    {
      $query = $this->table->find()
        ->limit( $limit)
        ->offset( $offset)
        ->order([
          $this->table->alias() . '.'. $this->table->primaryKey()
        ])
      ;

      if( method_exists( $this, 'findTruncate'))
      {
        $this->findTruncate( $query);
      }

      $contents = $query->all();

      if( $contents->count() == 0)
      {
        break;
      }

      foreach( $contents as $content)
      {
        if( $uploads)
        {
          foreach( $uploads as $upload)
          {
            if( !empty( $content->$upload))
            {              
              if( !is_numeric( key( $content->$upload)))
              {
                $assets = [$content->$upload];
              }
              else
              {
                $assets = $content->$upload;
              }

              foreach( $assets as $asset)
              {
                foreach( (array)$asset->paths as $paths)
                {
                  foreach( (array)$paths as $path)
                  {
                    if( file_exists( ROOT . '/webroot/' . $path))
                    {
                      unlink( ROOT . '/webroot/' . $path);
                    }
                  }
                }
              }
            }
          }
        }

        $this->table->delete( $content);
      }

      $offset = $offset + $limit;
    }
  }

  public function find( $query)
  {
  }

  public function getRecords( $before = false, $after = false)
  {
    $limit = 20;
    $offset = 0;

    while( true)
    {
      $query = $this->oldTable->find()
        ->limit( $limit)
        ->offset( $offset)
        ->order([
          $this->oldTable->alias() . '.'. $this->oldTable->primaryKey()
        ])
      ;

      $this->find( $query);
      $contents = $query->all();

      if( $contents->count() == 0)
      {
        break;
      }

      foreach( $contents as $content)
      {
        $entity = $this->createEntity( $content);
        if( $before)
        {
          $before( $entity, $content);
        }

        $this->table->saveContent( $entity);

        if( $after)
        {
          $after( $entity, $content);
        }
      }

      $offset = $offset + $limit;
    }      
  }

  public function createEntity( $content)
  {
    $data = [];
    $array = $content->toArray();

    foreach( $array as $key => $value)
    {
      $key = $this->oldTable->relationKey( $key);

      if( $this->table->hasField( $key) || in_array( $key, $this->table->associations()->keys()))
      {
        $data [$key] = $this->getValue( $key, $value, $array);
      }
    }
    
    if( $this->table->hasField( 'legacy_id'))
    {
      $data ['legacy_id'] = $array ['id'];
    }
    
    if( $this->setFieldTranslations)
    {
      $data = $this->setLanguagesField( $data);
    }
    $entity = $this->table->newEntity( $data);

    return $entity;
  }



  public function getValue( $key, $value, $array)
  {
    $method = 'get'.Inflector::camelize( $key);
    
    if( method_exists( $this, $method))
    {
      $value = $this->$method( $array);
    }
    else
    {
      $column = $this->table->schema()->column( $key);

      if( in_array( $column ['type'], ['string', 'text']))
      {
        $value = $this->decode( $value);
      }

      if( in_array( $column ['type'], ['date']) && !is_null( $value))
      {
        $value = $value->format( 'Y-m-d H:i:s');
      }
    }

    return $value;
  }

  public function decode( $string)
  {    
    if( is_array( $string))
    {
      return $string;
    }

    if( $this->decodeText)
    {
      $string = str_replace( 'â€œ', 'eXp01', $string);
      $string = str_replace( 'â€', 'eXp02', $string);
      $string = str_replace( 'â€¦', 'eX03', $string);
      $string = str_replace( 'â€˜', 'eXp04', $string);
      $string = str_replace( 'â€™', 'eXp05', $string);
      $string = str_replace( 'Ã“', 'eXp06', $string);
      $string = str_replace( 'Ãš', 'eXp07', $string);
      $string = str_replace( 'Ã‘', 'eXp08', $string);
      $string = str_replace( 'Ã', 'eXp09', $string);
      $string = str_replace( 'Ã', 'eXp10', $string);
      $string = str_replace( 'Ã‰', 'eXp11', $string);
      $string = utf8_decode( $string);
      $string = str_replace( 'eXp01', '“', $string);
      $string = str_replace( 'eXp02', '”', $string);
      $string = str_replace( 'eXp03', '…', $string);
      $string = str_replace( 'eXp04', '‘', $string);
      $string = str_replace( 'eXp05', '’', $string);
      $string = str_replace( 'eXp06', 'Ó', $string);
      $string = str_replace( 'eXp07', 'Ú', $string);
      $string = str_replace( 'eXp08', 'Ñ', $string);
      $string = str_replace( 'eXp09', 'Í', $string);
      $string = str_replace( 'eXp10', 'Á', $string);
      $string = str_replace( 'eXp11', 'É', $string);
      $string = html_entity_decode( $string);
    }

    return $string;
  }

  public function setLanguagesField( $data, $old)
  {
    $fields = $this->table->translateFields();
    $translations = $data->get( '_translations');

    if( $translations === null)
    {
      $translations = [];
    }

    $langs = array_keys( Lang::iso3());

    foreach( $langs as $lang)
    {
      $entity = array_key_exists( $lang, $translations) ? $translations [$lang] : $this->table->newEntity([]);

      foreach( $fields as $field)
      {
        $entity->set( $field, $data->get( $field));
      }

      if( !array_key_exists( $lang, $translations))
      {
        $translations [$lang] = $entity;
      }
    }

    $data->set( '_translations', $translations);
    return $data;
  }
}