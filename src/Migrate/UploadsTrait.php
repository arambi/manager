<?php

namespace Manager\Migrate;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

trait UploadsTrait
{

    public function asset($filename, $content_type, $folder, $original = false)
    {
        $UploadsTable = TableRegistry::getTableLocator()->get('Upload.Uploads');

        $UploadsTable->addBehavior('Upload.Upload', [
            'filename' => Configure::read('Upload.' . $content_type . '.config')
        ]);

        $url = $folder . $filename;

        $filepath = TMP . $filename;

        if (strpos($url, 'http') !== false) {
            system('wget --retry-connrefused --waitretry=1 --read-timeout=5 -P ' . ROOT . ' -O "tmp/' . $filename . '" "' . $url . '"');
        } else {
            system('cp "' . $url . '" "' . $filepath . '"');
        }

        if (!file_exists($filepath)) {
            $UploadsTable->removeBehavior('Upload');
            $this->errors[] = $filename;
            return false;
        }

        $filesize = filesize($filepath);
        $mime = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $filepath);
        $data = [
            'filename' => [
                'name' => $filename,
                'type' => $mime,
                'size' => $filesize,
                'tmp_name' => $filepath,
                'error' => 0,
            ],
            'content_type' => $content_type,
            'is_migration' => true
        ];


        $upload = $UploadsTable->newEntity($data);
        $new_data = [];

        if ($original) {
            $new_data['created'] = $original['created']->toUnixString();
        }


        try {
            $saved = $UploadsTable->save($upload, $new_data);
        } catch (\Exception $e) {
            _d($e);
        }
        $new = $UploadsTable->findById($upload->id)->first();
        $UploadsTable->removeBehavior('Upload');

        if (!$new) {
            return false;
        }

        $new->set('paths', $new->paths);

        $data = $new->toArray();
        return $data;
    }
}
