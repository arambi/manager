<?php 

namespace Manager\Validation;

use Cake\Validation\Validator;

class TitleValidator extends Validator
{
  public function __construct()
  {
    parent::__construct();
    
    $this->notEmpty( 'title', __d( 'admin', 'El título no puede estar vacío'));  
  }
}