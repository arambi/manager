<?php
namespace Manager\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Utility\Inflector;
use Manager\FieldAdapter\CkeditorFieldAdapter;
use User\Auth\Access;
use Cake\Routing\Router;
use Cofree\ParseJs\ParseJs;
use Cofree\ParseJs\JsExpr;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;
use Manager\Editable\Editable;

/**
 * Editable helper
 *
 * Helper para la edición inline de los campos, en el frontend
 */
class EditableHelper extends Helper
{

  public $helpers = ['Html', 'User.Auth', 'Cofree.Buffer'];

  public function beforeRender()
  {
    if( !$this->Auth->user())
    {
      return;
    }

    $text = __d( 'admin', 'Texto guardado');

    $styles = [
      'position: fixed',
      'left: calc(50% - 180px)',
      'top: calc(50% - 180px)',
      'color: rgba(0,0,0,.4)',
      'font-size: 180px',
      'display: none'
    ];
    $css2 = implode( ';', $styles);

    $script = <<<EOF
    $(function(){
      $('body')
        .append(
          $('<div>').attr({
            class: 'fa fa-circle-o-notch fa-spin fa-3x fa-fw',
            id: 'save-in-line-spinner',
            style: '$css2'
          })
        );
    })
EOF;
    
    $this->Html->script([
      '/manager/js/plugins/ckeditor/ckeditor.js',
      '/manager/js/plugins/ckeditor/adapters/jquery.js',
    ], [
      'once' => true,
      'block' => true
    ]);

    $this->Html->scriptBlock( $script, ['block' => true]);
  }

  public function field( $content, $field, $type = null, $default = null, $strip_tags = false)
  {
    if( $type === null)
    {
      $type = TableRegistry::get( $content->source())->getSchema()->getColumnType( $field);
    }

    if( !$this->Auth->user() || Editable::$noEdit)
    {
      $body = $content->$field;

      if( $type != 'text' || $strip_tags)
      {
        $body = strip_tags( $body, '<br>');
      }

      if( empty( $body) && $default)
      {
        $body = $default;
      }

      if( $type == 'text')
      {
        $body = str_replace( '<table', '<div class="c-table-responsive"><table', $body);
        $body = str_replace( '</table>', '</table></div>', $body);
      }
      
      return $body;
    }

    

    $permission = Access::check( $this->Auth->user( 'group_id'), $this->editUrl( $content->source()));

    if( !$permission)
    {
      $text = $content->$field;

      if( empty( $text) && $default)
      {
        $text = $default;
      }

      return $text;
    }

    $id = Inflector::variable( $content->source()) . '-'. $field . '-'. $content->id;

    $body = $content->$field;

    if( empty( $body))
    {
      $body = $default ? $default : __d( 'app', 'Haz click para editar el contenido');
    }
      
    if( $type != 'text')
    {
      $body = strip_tags( $body, 'br');
    }

    $return = '<div id="'. $id .'" contenteditable="true">'. $body .'</div>';
    $script = 'CKEDITOR.disableAutoInline = true; CKEDITOR.inline( "'. $id .'", '. $this->getConfiguration( $content, $field, $type) .');';

    $this->Buffer->start();
    echo $this->Html->scriptBlock( $script);
    $this->Buffer->end();
    return $return;
  }

  public function getConfiguration( $content, $field, $type)
  {
    $url = $this->editUrl( $content->source());
    $type = $type ? $type : TableRegistry::get( $content->source())->schema()->column( $field)['type'];
    list( $plugin, $controller) = pluginSplit( $content->source());
    $adapter = new CkeditorFieldAdapter();
    $ck_config = $adapter->get( $controller, $field);
    $url [] = $field;
    $url [] = $content->id;
    $url [] = I18n::locale();
    $url ['_ext'] = 'json';

    $ck_config ['inlinesave'] = [
      'postUrl' => Router::url( $url),
      'useColorIcon' => true,
      'onSave' => new JsExpr(
        "function(editor){
          editor.focusManager.blur( true);
          $('#save-in-line-spinner').show();
        }"
      ),
      'onSuccess' => new JsExpr(
        "function(editor){
          $('#save-in-line-spinner').hide();
        }"
      )
      
    ];

    if( $type == 'text')
    {
      $ck_config ['toolbar'][] = [
        'name' => 'editor',
        'items' => ['Inlinesave']
      ];
    }
    else
    {
      $ck_config ['enterModel'] = 'CKEDITOR.ENTER_BR';
      $ck_config ['toolbar'] = [[
        'name' => 'editor',
        'items' => ['Inlinesave']
      ]];

      $ck_config ['allowedContent'] = 'br';
    }
    


    return ParseJs::render( $ck_config);
  }

  public function editUrl( $controller)
  {
    list( $plugin, $controller) = pluginSplit( $controller);
    $url = [
      'prefix' => 'admin',
      'plugin' => $plugin,
      'controller' => $controller,
      'action' => 'field'
    ];

    return $url;
  }

}
