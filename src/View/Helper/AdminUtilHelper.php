<?php

namespace Manager\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Routing\Router;
use Cake\Filesystem\Folder;
use Cake\Core\Plugin;
use Cake\Log\Log;
use Manager\Lib\Templates;

/**
 * AdminUtil helper
 */
class AdminUtilHelper extends Helper
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    protected static $_customCSS = [];
    protected static $_customJS = [];
    protected static $_cssStyles = null;

    public $helpers = ['Url', 'Html'];
    /**
     * Devuelve una URL para el admin, dado unos parámetros
     * Usando la currentRoute se obtiene los parámetros necesarios usados en la URL actual.
     * Estos parámetros son omitidos por Router::url(), así que es necesario recurrir a Route para obtenerlos
     *
     * @param array $params 
     * @return array
     */
    public function url($params = array(), $full = false)
    {
        return $this->Url->build($params, $full);
        $route = Router::getNamedExpressions();


        // Las keys de la ruta, definidos en Router::connect( :key1/:key2)
        $keys = $route->keys;

        // Toma las rutas por defecto para la ruta actual
        $defaults = $route->defaults;

        // Recorre las keys definidas en la ruta actual y comprueba si están seteadas en CakesRequest::params
        foreach ($keys as $key) {
            if (isset($this->request->params[$key])) {
                $defaults[$key] = $this->request->params[$key];
            }
        }

        if (!isset($defaults['admin'])) {
            $defaults['admin'] = false;
        }

        return array_merge($defaults, $params);
    }

    /**
     * Devuelve la fecha con un formato
     * @param  string  $date   La fecha
     * @param  mixed $format El formato
     * @return string          
     */
    public function date($date, $format = false)
    {
        if (!$format) {
            $format = __d('admin', '%d/%m/%Y');
        }

        return $this->Time->format($date, $format);
    }

    /**
     * Devuelve la fecha y hora con un formato
     * @param  string  $date   La fecha
     * @param  mixed $format El formato
     * @return string          
     */
    public function datetime($date, $format = false)
    {
        if (!$format) {
            $format = __d('admin', '%d/%m/%Y %H:%M:%S');
        }

        return $this->Time->format($date, $format);
    }

    public function templates()
    {
        return Templates::get();
    }

    /**
     * Setea estáticamente las hojas de estilo que se usarán en el admin
     * Hay que llamar a está función con AdminUtilHelper::setCustomCSS()
     * 
     * @param array $files Array con la ruta hacia las hojas de estilo
     */
    public static function setCustomCSS(array $files)
    {
        static::$_customCSS = $files;
    }

    /**
     * Devuelve el customCSS que ha sido seteado con la clase estática AdminUtilHelper::setCustomCSS()
     * 
     * @return HTML
     */
    public function customCSS()
    {
        if (!empty(static::$_customCSS)) {
            return $this->Html->css(static::$_customCSS);
        }
    }

    public static function setCssStyles($styles)
    {
        static::$_cssStyles = $styles;
    }
    

    public function getCssStyles()
    {
        return '<style>' . static::$_cssStyles. '</style>';
    }



    /**
     * Setea estáticamente las hojas de estilo que se usarán en el admin
     * Hay que llamar a está función con AdminUtilHelper::setCustomCSS()
     * 
     * @param array $files Array con la ruta hacia las hojas de estilo
     */
    public static function setCustomJS(array $files)
    {
        static::$_customJS = $files;
    }

    /**
     * Devuelve el customJS que ha sido seteado con la clase estática AdminUtilHelper::setCustomCSS()
     * 
     * @return HTML
     */
    public function customJS()
    {
        if (!empty(static::$_customJS)) {
            return $this->Html->script(static::$_customJS);
        }
    }
}
