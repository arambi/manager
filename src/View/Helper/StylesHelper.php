<?php
namespace Manager\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Styles helper
 */
class StylesHelper extends Helper
{

  protected static $_configs = [];
  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [];

  public static function add( $key, $style = null)
  {
    if( is_array( $key))
    {
      foreach( $key as $_key => $value)
      {
        static::$_configs [$_key] = $value;
      }
    }
    else
    {
      static::$_configs [$key] = $style;
    }
  }

  public function get( $key)
  {
    if( array_key_exists( $key, static::$_configs))
    {
      return static::$_configs [$key] . ';';
    }
  }
  

}
