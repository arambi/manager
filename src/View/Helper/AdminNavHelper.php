<?php
namespace Manager\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use User\Auth\Access;
use Cake\Core\Configure;
use Cake\Network\Session;
use Manager\Navigation\NavigationCollection;
use Cake\ORM\TableRegistry;

/**
 * AdminNav helper
 */
class AdminNavHelper extends Helper 
{
  public $helpers = ['Html', 'Form', 'Session', 'Url', 'Auth'];
  
  private $__defaults = [
    'hash' => true
  ];

  private function __mergeOptions( $element)
  {
    return array_merge( $this->__defaults, $element);
  }

  public function nav( $options = array())
  {
    $_options = array(
      'ul' => array(
          0 => array(
              'class' => 'nav',
              'id' => 'side-menu'
          ),
          1 => array(
              'class' => 'nav nav-second-level'
          ),
          2 => [
              'class' => 'nav nav-third-level'
          ]
      ),
      'li' => array(
          0 => array(
              'class' => '',
          ),
          1 => array(
              'class' => '',
          )
      ),
      'a' => array(
          0 => array(
              'class' => '',
              'wrapper' => '<span class="nav-label">%s</span>',
              'wrapperChildrens' => '<span class="menu-text">%s</span><span class="fa arrow"></span>',
          ),
      )
    );

    $options = array_merge( $_options, $options);
    $dashboard_url = Configure::read( 'Admin.dashboard') ? Configure::read( 'Admin.dashboard') : '/admin';
    $frontend_url = Configure::read( 'Admin.frontendUrl') ? Configure::read( 'Admin.frontendUrl') : '/';
    
    $top = '<ul class="nav m-t">';

    if( !Configure::read( 'Admin.noWeb'))
    {
      $top .= '<li><a target="_blank" href="'. $frontend_url . '"><i class="fa fa-desktop"></i> '. __d( 'admin', 'Ir al web') .'</a></li>';
    }
   
    $top .= '<li><a  href="'. $dashboard_url .'"><i class="fa fa-home"></i> '. __d( 'admin', 'Inicio') .'</a></li></ul>';


    $group = TableRegistry::get( 'User.Groups')->get( $this->Auth->user( 'group_id'));

    if( !empty( $group->menu))
    {
      return $top . '<div class="custom-nav">' . $this->customNav( $group->menu). '</div>';
    }

    $return = $top . $this->_nav( NavigationCollection::nav(), $options);
    return $return;
  }

  public function customNav( $els, $level = 0)
  {
    $ul = [];

    foreach( $els as $el)
    {
      if( array_key_exists( 'url', $el) && !empty( $el ['url']))
      {
        $li = '';

        if( $this->hasPermissions( $el ['url']))
        {
          $li = '<li><a href="'. '#'. $this->Url->build( $el ['url']) . '">' . (!empty( $el ['icon']) ? '<i class="'. $el ['icon'] .'"></i> ' : '') . $el ['label'] . '</a>'; 
        }
      }
      else
      {
        $icon = !empty( $el ['icon']) ? '<i class="'. $el ['icon'] .'"></i> ' : '';

        if( $level == 0)
        {
          $li = '<li><span>'. $icon . $el ['label'] . '</span>';
        }
        else
        {
          $li = '<li><a href>'. $icon . $el ['label'] . '</a>';
        }
      }

      if( !empty( $el ['nodes']))
      {
        $li .= $this->customNav( $el ['nodes'], ($level + 1));
      }

      $li .= '</li>';

      $ul [] = $li;
    }

    return '<ul'. ($level == 1 ? ' side-navigation ' : '') .'>'. implode( "\n", $ul) . '</ul>';
  }

  public function _nav( $els, $options, $level = 0, $parent = false)
  {
    $ul = array();

    foreach( $els as $el)
    {
      $el = $this->__mergeOptions( $el);

      if( array_key_exists( 'html', $el))
      {
        if( $this->hasPermissions( $el ['url']))
        {
          $ul [] = $el ['html'];
        }
        continue;
      }

      $li = array();
      $url = isset( $el ['url']) ? $el ['url'] : $el;

      if( !isset( $url ['plugin']) && $url)
      {
        $url ['plugin'] = false;
      }


      if( (empty( $url) && $this->hasChildrensPermissions( $el)) || (!empty( $url) && $this->hasPermissions( $url)))
      {
        $a = array();
        
        $icon = $this->icon( $el);

        if(  $icon && (!$parent || $el ['icon'] != $parent ['icon']))
        {
          $a [] = $icon;
        }
        

        if( empty( $el ['children']))
        {
          if( isset( $options ['a'][$level]['wrapper']))
          {
            $a [] = sprintf( $options ['a'][$level]['wrapper'], $el ['label']);
          }
          else
          {
            $a [] = $el ['label'];
          }
        }
        else
        {
          if( isset( $options ['a'][$level]['wrapperChildrens']))
          {
            $a [] = sprintf( $options ['a'][$level]['wrapperChildrens'], $el ['label']);
          }
          else
          {
            $a [] =$el ['label'];
          }
        }
        
        if( empty( $el ['dontshowurl']))
        {
          $li [] = $this->Html->link( implode( "\n", $a), $this->__getUrl( $url, $el), array( 
            'escape' => false,
            'class' => isset( $options ['a'][$level]['class']) ? $options ['a'][$level]['class'] : false
        ));
        }
        else
        {
          $li [] = $this->Html->link( implode( "\n", $a), '', array( 
              'escape' => false,
              'class' => $options ['a'][$level]['class']
          ));
        }
        
        if( isset( $el ['children']) && !empty( $el ['children']))
        {
          $li [] = $this->_nav( $el ['children'], $options, ($level + 1), $el);
        }

        $class = $options ['li'][$level]['class'];
                
        if( $this->isActive)
        {
          $class .= ' '. $options ['li'][$level]['current'];
        }
        
        if( $this->isOpen)
        {
          $class .= ' open';
        }

        if( isset( $el ['children']) 
              && is_array( $el ['children']) 
              && !empty( $el ['dontshowurl']) 
              && isset( $options ['li'][$level]['classHasChildrens']))
        {
          $class .= ' '. $options ['li'][$level]['classHasChildrens'];
        }

        $attrs = array(
            'class' => $class
        );

        if( isset( $options ['li'][$level]['attrsChildrens']))
        {
          $attrs = array_merge( $attrs, $options ['li'][$level]['attrsChildrens']);
        }
        
        $ul [] = $this->Html->tag( 'li', implode( "\n",  $li), $attrs);
      }
    }

    $attrs = [
      'class' => $options ['ul'][$level]['class'],
      'id' => isset( $options ['ul'][$level]['id']) ? $options ['ul'][$level]['id'] : false,
      'ng-cloak' => true
    ];

    if( $level == 0)
    {
      $attrs ['side-navigation'] = '';
    }

    return $this->Html->tag( 'ul', implode( "\n", $ul), $attrs);
  }

/**
 * Devuelve la URL
 * Puede devolver un array con la URL o un string con la URL precedida de # para ser usada con AngularJS
 *     
 * @param  array $url
 * @param  array $element El element de navegación
 * @return mixed|string|array
 */
  private function __getUrl( $url, $element)
  {
    if( $element ['hash'])
    {
      return '#'. $this->Url->build( $url);
    }

    return $url;
  }
  
/**
 * Devuelve una etiqueta <i> con el class para el icon
 * @param  array $el El array del elemento definido en la configuración de la navegación
 * @param  string $css El CSS extra
 * @return HTML
 */
  public function icon( $el, $css = '')
  {
    if( !isset( $el ['icon']))
    {
      return false;
    }

    if( strpos( $el ['icon'], 'fa') === false) 
    {
      $el ['icon'] = 'icon-'. $el ['icon'];
    }
    else
    {
      $el ['icon'] = 'fa '. $el ['icon'];
    }

    return '<i class="'. $el ['icon'] .' '. $css .'"></i>';
  }

  public function hasPermissions( $url)
  {
    return Access::check( $this->request->session()->read( 'Auth.User.group_id'), $url);
  }

  public function hasChildrensPermissions( $el)
  {
    return NavigationCollection::hasChildrensPermissions( $el, $this->request->session()->read( 'Auth.User.group_id'));
  }
  
  public function link( $title, $url = null, $options = array(), $confirmMessage = false)
  {
    if( is_array( $url) && !isset( $url ['admin']))
    {
      $url ['admin'] = true;
    }
    
    if( $this->hasPermission( $url))
    {
      return $this->Html->link( $title, $url, $options, $confirmMessage);
    }
    else
    {
      return '';
    }
  }


  public function frontLinks()
  {
    $out = [];
    
    if( $this->request->session()->check( 'Bar.frontLinks'))
    {
      $links = $this->request->session()->read( 'Bar.frontLinks');

      foreach( $links as $link)
      {
        $out [] = '<li class="admin-bar-link-content">'. $link .'</li>';
      }

      $this->request->session()->delete( 'Bar.frontLinks');
    }

    return implode( "\n", $out);
  }
  
}
