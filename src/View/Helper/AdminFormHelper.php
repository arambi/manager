<?php
namespace Manager\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\View\Helper\FormHelper;
/**
 * AdminForm helper
 */
class AdminFormHelper extends FormHelper {

/**
 * Default configuration.
 *
 * @var array
 */
	protected $_defaultConfig = [];

  public $helpers = array( 'Url', 'Html') ;

    public $horizontal = false ;
    public $inline = false ;
    public $search = false ;

    private $colSize ;

    private $buttonTypes = array('primary', 'info', 'success', 'warning', 'danger', 'inverse', 'link') ;
    private $buttonSizes = array('mini', 'small', 'large') ;

    /**
     *
     * Add classes to options according to values of bootstrap-type and bootstrap-size for button.
     *
     * @param $options The initial options with bootstrap-type and/or bootstrat-size values
     *
     * @return The new options with class values (btn, and btn-* according to initial options)
     *
    **/
    protected function _addButtonClasses ($options) {
        $options = $this->addClass($options, 'btn btn-default') ;
        foreach ($this->buttonTypes as $type) {
            if (isset($options['bootstrap-type']) && $options['bootstrap-type'] == $type) {
                $options = $this->addClass($options, 'btn-'.$type) ;
                break ;
            }
        }
        foreach ($this->buttonSizes as $size) {
            if (isset($options['bootstrap-size']) && $options['bootstrap-size'] == $size) {
                $options = $this->addClass($options, 'btn-'.$size) ;
                break ;
            }
        }
        unset($options['bootstrap-size']) ;
        unset($options['bootstrap-type']) ;
        return $options ;
    }

    /**
     *
     * Try to match the specified HTML code with a button or a input with submit type.
     *
     * @param $html The HTML code to check
     *
     * @return true if the HTML code contains a button
     *
    **/
    protected function _matchButton ($html) {
        return strpos($html, '<button') !== FALSE || strpos($html, 'type="submit"') !== FALSE ;
    }

    /**
     *
     * Create a Twitter Bootstrap like form.
     *
     * New options available:
     *  - horizontal: boolean, specify if the form is horizontal
     *  - inline: boolean, specify if the form is inline
     *  - search: boolean, specify if the form is a search form
     *
     * Unusable options:
     *  - inputDefaults
     *
     * @param $model The model corresponding to the form
     * @param $options Options to customize the form
     *
     * @return The HTML tags corresponding to the openning of the form
     *
    **/
    public function create( $model = null, array $options = []) {
        $this->colSize = array(
            'label' => 2,
            'input' => 10,
            'error' => 2
        ) ;
        if (isset($options['cols'])) {
            $this->colSize = $options['cols'] ;
        }
        $this->horizontal = $this->_extractOption('horizontal', $options, false);
        unset($options['horizontal']);
        $this->search = $this->_extractOption('search', $options, false) ;
        unset($options['search']) ;
        $this->inline = $this->_extractOption('inline', $options, false) ;
        unset($options['inline']) ;
        if ($this->horizontal) {
            $options = $this->addClass($options, 'form-horizontal') ;
        }
        else if ($this->inline) {
            $options = $this->addClass($options, 'form-inline') ;
        }
        if ($this->search) {
            $options = $this->addClass($options, 'form-search') ;
        }
        $options['role'] = 'form' ;
        $options['inputDefaults'] = array(
            'div' => array(
                'class' => 'form-group'
            )
        ) ;
        return parent::create($model, $options) ;
    }

    /**
     *
     * Return the col size class for the specified column (label, input or error).
     *
    **/
    protected function _getColClass($what) {
        $size = $this->colSize[$what] ;
        if ($size) {
            return 'col-lg-'.$size ;
        }
        return '' ;
    }

    /**
     *
     * Create & return a error message (Twitter Bootstrap like).
     *
     * The error is wrapped in a <span> tag, with a class
     * according to the form type (help-inline or help-block).
     *
    **/
    public function error($field, $text = null, array $options = []) {
        $this->setEntity($field);
        $optField = $this->_magicOptions(array()) ;
        $options['wrap'] = $this->_extractOption('wrap', $options, 'span') ;
        $errorClass = 'help-block' ;
        if ($this->horizontal && $optField['type'] != 'checkbox' && $optField['type'] != 'radio') {
            $errorClass = 'help-inline '.$this->_getColClass('error') ;
        }
        $options = $this->addClass($options, $errorClass) ;
        return parent::error($field, $text, $options) ;
    }

    /**
     *
     * Create & return a label message (Twitter Boostrap like).
     *
    **/
    public function label($fieldName, $text = null, array $options = []) {
        $this->setEntity($fieldName);
        $optField = $this->_magicOptions(array()) ;
        if ($optField['type'] != 'checkbox' && $optField['type'] != 'radio') {
            if (!$this->inline) {
                $options = $this->addClass($options, 'control-label') ;
            }
            if ($this->horizontal) {
                $options = $this->addClass($options, $this->_getColClass('label')) ;
            }
            if ($this->inline) {
                $options = $this->addClass($options, 'sr-only') ;
            }
        }
        return parent::label($fieldName, $text, $options) ;
    }

    /**
     *
     * Create & return an input block (Twitter Boostrap Like).
     *
     * New options:
     *  - prepend:
     *      -> string: Add <span class="add-on"> before the input
     *      -> array: Add elements in array before inputs
     *  - append: Same as prepend except it add elements after input
     *
    **/
    public function input( $fieldName, array $options = []) 
    {
        $prepend = $this->_extractOption('prepend', $options, null) ;
        unset ($options['prepend']) ;
        $append = $this->_extractOption('append', $options, null) ;
        unset ($options['append']) ;
        $before = $this->_extractOption('before', $options, '') ;
        $after = $this->_extractOption('after', $options, '') ;
        $between = $this->_extractOption('between', $options, '') ;
        $label = $this->_extractOption('label', $options, false) ;
        $prepend = $this->_extractOption('prepend', $options, null) ;
        $this->setEntity($fieldName);
        $options = $this->_parseOptions($options) ;
        $options['format'] = array('label', 'before', 'input', 'between', 'error', 'after') ;

        $div_options = $options;

        $model = $this->_getModel( $this->model());

        if( isset( $div_options ['help']))
        {
          $options ['label'] = '<span tooltip-html-unsafe="'. $div_options ['help'] .'">'. $options ['label'] . '</span>';
        }

        $beforeClass = '' ;

        if ($options['type'] == 'checkbox') 
        {
            $class_label = 'i-checks';

            if( is_array( $options ['label']))
            {
              $label_name = $options ['label']['name'];

              if( isset( $options ['label']['class']))
              {
                $class_label = $options ['label']['class'];
              }
            }
            else
            {
              $label_name = $label;
            }

            $before = '<label class="'. $class_label .'">'.$before ;
            $between = $between. '<i></i> '. $label_name . '</label>' ;
            $options['format'] = array('before', 'input', 'label', 'between', 'error', 'after') ;
            
            if( !isset( $options ['div']))
            {
              $options['div'] = array(
                'class' => $options['type']
              );
            }
            
            $options['label'] = false;

            if( !isset( $options['hiddenField']) || $options['hiddenField']) 
            {
              $pre_options = $this->_initInputField($fieldName, $options);
              $options['hiddenField'] = false;
              $hiddenOptions = array(
                'id' => $pre_options['id'] . '_',
                'name' => $pre_options['name'],
                'value' => '0',
                'form' => isset($options['form']) ? $options['form'] : null,
                'secure' => false,
              );

              if (isset($options['disabled']) && $options['disabled']) 
              {
                $hiddenOptions['disabled'] = 'disabled';
              }

              $before = $this->hidden( $fieldName, $hiddenOptions) . $before;
            }

        }
        else if ($this->horizontal) {
            $beforeClass .= $this->_getColClass('input') ;
        }
        if ($prepend) {
            $beforeClass .= ' input-group' ;
            if (is_string($prepend)) {
                $before .= '<span class="input-group-'.($this->_matchButton($prepend) ? 'btn' : 'addon').'">'.$prepend.'</span>' ;
            }
            if (is_array($prepend)) {
                foreach ($prepend as $pre) {
                    $before .= $pre ;
                }
            }
        }
        if ($append) {
            $beforeClass .= ' input-group' ;
            if (is_string($append)) {
                $between = '<span class="input-group-'.($this->_matchButton($append) ? 'btn' : 'addon').'">'.$append.'</span>'.$between ;
            }
            if (is_array($append)) {
                foreach ($append as $apd) {
                    $between = $apd.$between ;
                }
            }
        }

        if ($beforeClass) {
            $before = '<div class="'.$beforeClass.'">'.$before ;
            $after = $after.'</div>' ;
        }

        $options['before'] = $before ;
        $options['after'] = $after ;
        $options['between'] = $between ;

        if ($options['type'] != 'checkbox' && $options['type'] != 'radio') {
            $options = $this->addClass($options, 'form-control') ;
        }

        // Verifica si el campo es traducible
        // Si lo es le coloca el idioma dentro
        if( is_object( $model) && $this->hasTranslation( $model, $this->field()) || isset( $options ['forceLanguages']))
        {
          // Guarda el between para luego modificarlo en cada idioma
          $between = $options ['between'];

          $out = array();

          $div_class = isset( $options ['div']['class']) ? $options ['div']['class'] : '';
          $plural = Inflector::camelize( Inflector::pluralize( $this->field()));

          if( isset( $this->request->data [$plural]))
          {
            $values = Hash::combine( $this->request->data [$plural], '{n}.locale', '{n}.content');
          }

          // Recorre los idiomas para crear un input por cada uno de ellos
          $languages = array_reverse( Configure::read( 'Config.languages'));

          if( isset( $options ['ng-model']))
          {
            $ng_model = $options ['ng-model'];
          }

          foreach( $languages as $key => $locale)
          {
            $options ['class'] = $options ['class'] . ' form-control locale';
            $options ['ng-show'] = "appSettings.language == '$locale'";
            $options ['escape'] = false;

            if( !empty( $values) && isset( $values [$locale]))
            {
              $options ['value'] = $values [$locale];
            }

            if(($key + 1) == count( $languages))
            {
              if( count( $languages) > 1)
              {
                CakeLog::write( 'debug', print_r( $languages, true));
                $options ['set-languages'] = "";
                $options ['before'] .= '<div class="input-group m-b">';
                $options ['after'] = $options ['after']. '</div>';
              }
              
              // Añade el input al array de salida
              // 
              if( isset( $options ['ng-model']))
              {
                $options ['ng-model'] = $ng_model . '.'. $locale;
              }

              $out [] = parent::input( $fieldName .'.'. $locale, $options);
            }
            else
            {
              $_options = $options;
              $_options ['div'] = false;
              $_options ['label'] = false;
              $_options ['after'] = false;

              if( isset( $options ['ng-model']))
              {
                $_options ['ng-model'] = $ng_model . '.'. $locale;
              }
              
              $options ['after'] .= parent::input( $fieldName .'.'. $locale, $_options);
            }            
          }

          $out [] = $this->error( $fieldName);

          return implode( "\n", $out);
        }
        else
        {
          $out = parent::input($fieldName, $options);
        }
        

      
        return $out;
    }

/**
 * Dibuja un checkbox tipo switch según plantilla angulr
 *    
 * @param  string $fieldName El fieldname de input
 * @param  array  $options   Opciones, entre ellas 'size', que se refiere a las classes de bootstrap text-lg, text-2x, etc. Hay que indicar solo el sufijo 
 * @param  array  $attributes Los atributos extra que llevará la etiqueta input
 * @return Html
 */ 
    public function switchCheckbox( $fieldName, $options = array(), $attributes = array())
    {
      $_options = array(
          'size' => 'base',
          'label' => false
      );

      $options = array_merge( $_options, $options);

      $input_attrs = array(
          'type' => 'checkbox',
          'label' => array(
              'class' => 'i-switch m-t-xs m-r',
              'name' => false
          ),
          'div' => array(
              'class' => 'form-group'
          ),
      );


      $input = $this->input( $fieldName, array_merge( $input_attrs, $attributes));

      $out [] = $this->Html->tag( 'div', $input, array(
          'class' => 'pull-left padder-md'
      ));

      $label = $this->Html->tag( 'label', $options ['label'], array(
          'class' => 'm-t-xs text-'. $options ['size']
      ));

      $out [] = $this->Html->tag( 'div', $label, array(
          'class' => 'pull-left pull-in'
      ));

      return $this->Html->tag( 'div', implode( "\n", $out), array(
          'class' => 'row'
      ));
    }

    public function ckeditorAngular( $fieldName, $options = array())
    {
      $model = $this->_getModel( $this->model());
      $this->setEntity( $fieldName);
      $out = array();

      $options ['type'] = 'textarea';

      if( (is_object( $model) && $this->hasTranslation( $model, $this->field())) || isset( $options ['forceLanguages']) )
      {
        $languages = array_reverse( Configure::read( 'Config.languages'));

        $_options = $options;

        foreach( $languages as $locale)
        {
          if( isset( $options ['ng-model']))
          {
            $_options ['ng-model'] = $options ['ng-model'] . '.'. $locale;
          }

          $_options ['label'] = false;
          $_options ['id'] = $this->model() . $fieldName . $locale . mt_rand();
          $_options ['div']['ng-show'] = "appSettings.language == '$locale'";

          $out [] = parent::input( $fieldName .'.'. $locale, $_options);
        }

        $return = $this->Html->tag( 'div', implode( "\n", $out), array(
            'set-languages' => "",
            'class' => 'form-group'
        ));

        return $return;
      }
    }

    public function colorpicker( $fieldName, $options = array())
    {
      if( !empty( $options ['label']))
      {
        $out [] = $this->Html->tag( 'label', $options ['label']);
        $options ['label'] = false;
      }

      $out [] = $this->input( $fieldName, array_merge( array(
          'label' => false,
          'type' => 'text',
          'colorpicker' => '',
          'div' => array(
              'class' => 'input-group'
          ),
          'before' => '<span style="background-color: {{ '. $options ['ng-model'] .' }}" class="input-group-addon h-full"></span>'
      ), $options
          
      ));

      return implode( "\n", $out);
    }

    /**
     *
     * Create & return a Twitter Like button.
     *
     * New options:
     *  - bootstrap-type: Twitter bootstrap button type (primary, danger, info, etc.)
     *  - bootstrap-size: Twitter bootstrap button size (mini, small, large)
     *
    **/
    public function button($title, array $options = array()) {
        $options = $this->_addButtonClasses($options) ;
        return parent::button($title, $options) ;
    }

    /**
     *
     * Create & return a Twitter Like button group.
     *
     * @param $buttons The buttons in the group
     * @param $options Options for div method
     *
     * Extra options:
     *  - vertical true/false
     *
    **/
    public function buttonGroup ($buttons, $options = array()) {
        $vertical = $this->_extractOption('vertical', $options, false) ;
        unset($options['vertical']) ;
        $options = $this->addClass($options, 'btn-group') ;
        if ($vertical) {
            $options = $this->addClass($options, 'btn-group-vertical') ;
        }
        return $this->Html->tag('div', implode('', $buttons), $options) ;
    }

    /**
     *
     * Create & return a Twitter Like button toolbar.
     *
     * @param $buttons The groups in the toolbar
     * @param $options Options for div method
     *
    **/
    public function buttonToolbar ($buttonGroups, $options = array()) {
        $options = $this->addClass($options, 'btn-toolbar') ;
        return $this->Html->tag('div', implode('', $buttonGroups), $options) ;
    }

    /**
     *
     * Create & return a twitter bootstrap dropdown button.
     *
     * @param $title The text in the button
     * @param $menu HTML tags corresponding to menu options (which will be wrapped
     *       into <li> tag). To add separator, pass 'divider'.
     * @param $options Options for button
     *
    **/
    public function dropdownButton ($title, $menu = array(), $options = array()) {

        $options['type'] = false ;
        $options['data-toggle'] = 'dropdown' ;
        $options = $this->addClass($options, "dropdown-toggle") ;

        $outPut = '<div class="btn-group">' ;
        $outPut .= $this->button($title.'<span class="caret"></span>', $options) ;
        $outPut .= '<ul class="dropdown-menu">' ;
        foreach ($menu as $action) {
            if ($action === 'divider') {
                $outPut .= '<li class="divider"></li>' ;
            }
            else {
                $outPut .= '<li>'.$action.'</li>' ;
            }
        }
        $outPut .= '</ul></div>' ;
        return $outPut ;
    }

    /**
     *
     * Create & return a Twitter Like submit input.
     *
     * New options:
     *  - bootstrap-type: Twitter bootstrap button type (primary, danger, info, etc.)
     *  - bootstrap-size: Twitter bootstrap button size (mini, small, large)
     *
     * Unusable options: div
     *
    **/
    public function submit($caption = null, array $options = []) {
        if (!isset($options['div'])) {
            $options['div'] = false ;
        }
        $options = $this->_addButtonClasses($options) ;
        return parent::submit($caption, $options) ;
    }

    /**
     *
     * End a form, Twitter Bootstrap like.
     *
     * New options:
     *  - bootstrap-type: Twitter bootstrap button type (primary, danger, info, etc.)
     *  - bootstrap-size: Twitter bootstrap button size (mini, small, large)
     *
    **/
    public function _end ($options = null,$secureAttributes = array()) {
        if ($options == null) {
            return parent::end($options,$secureAttributes) ;
        }
        if (is_string($options)) {
            $options = array('label' => $options) ;
        }
        if (!$this->inline) {
            if (!array_key_exists('div', $options)) {
                $options['div'] = array() ;
            }
            $options['div']['class'] = 'form-actions' ;
        }
        return parent::end($options,$secureAttributes) ;
    }

    /** SPECIAL FORM **/

    /**
     *
     * Create a basic bootstrap search form.
     *
     * @param $model The model of the form
     * @param $options The options that will be pass to the BootstrapForm::create method
     *
     * Extra options:
     *  - label: The input label (default false)
     *  - placeholder: The input placeholder (default "Search... ")
     *  - button: The search button text (default: "Search")
     *
    **/
    public function searchForm ($model = null, $options = array()) {

        $label = $this->_extractOption('label', $options, false) ;
        unset($options['label']) ;
        $placeholder = $this->_extractOption('placeholder', $options, 'Search... ') ;
        unset($options['placeholder']) ;
        $button = $this->_extractOption('button', $options, 'Search') ;
        unset($options['button']) ;

        $output = '' ;

        $output .= $this->create($model, array_merge(array('search' => true, 'inline' => (bool)$label), $options)) ;
        $output .= $this->input('search', array(
            'label' => $label,
            'placeholder' => $placeholder,
            'append' => array(
                $this->button($button, array('style' => 'vertical-align: middle'))
            )
        )) ;
        $output .= $this->end() ;

        return $output ;
    }

/**
* Modifica las options de un select para darle el estilo bootstrap
 *
 * @param array $elements
 * @param array $parents
 * @param string $showParents
 * @param array $attributes
 * @return array
 */
  protected function _selectOptions($elements = array(), $parents = array(), $showParents = null, $attributes = array()) {
    $select = array();
    $attributes = array_merge(
      array('escape' => true, 'style' => null, 'value' => null, 'class' => null),
      $attributes
    );
    $selectedIsEmpty = ($attributes['value'] === '' || $attributes['value'] === null);
    $selectedIsArray = is_array($attributes['value']);

    foreach ($elements as $name => $title) {
      $htmlOptions = array();
      if (is_array($title) && (!isset($title['name']) || !isset($title['value']))) {
        if (!empty($name)) {
          if ($attributes['style'] === 'checkbox') {
            $select[] = $this->Html->useTag('fieldsetend');
          } else {
            $select[] = $this->Html->useTag('optiongroupend');
          }
          $parents[] = $name;
        }
        $select = array_merge($select, $this->_selectOptions(
          $title, $parents, $showParents, $attributes
        ));

        if (!empty($name)) {
          $name = $attributes['escape'] ? h($name) : $name;
          if ($attributes['style'] === 'checkbox') {
            $select[] = $this->Html->useTag('fieldsetstart', $name);
          } else {
            $select[] = $this->Html->useTag('optiongroup', $name, '');
          }
        }
        $name = null;
      } elseif (is_array($title)) {
        $htmlOptions = $title;
        $name = $title['value'];
        $title = $title['name'];
        unset($htmlOptions['name'], $htmlOptions['value']);
      }

      if ($name !== null) {
        $isNumeric = is_numeric($name);
        if (
          (!$selectedIsArray && !$selectedIsEmpty && (string)$attributes['value'] == (string)$name) ||
          ($selectedIsArray && in_array((string)$name, $attributes['value'], !$isNumeric))
        ) {
          if ($attributes['style'] === 'checkbox') {
            $htmlOptions['checked'] = true;
          } else {
            $htmlOptions['selected'] = 'selected';
          }
        }

        if ($showParents || (!in_array($title, $parents))) {
          $title = ($attributes['escape']) ? h($title) : $title;

          $hasDisabled = !empty($attributes['disabled']);
          if ($hasDisabled) {
            $disabledIsArray = is_array($attributes['disabled']);
            if ($disabledIsArray) {
              $disabledIsNumeric = is_numeric($name);
            }
          }
          if (
            $hasDisabled &&
            $disabledIsArray &&
            in_array((string)$name, $attributes['disabled'], !$disabledIsNumeric)
          ) {
            $htmlOptions['disabled'] = 'disabled';
          }
          if ($hasDisabled && !$disabledIsArray && $attributes['style'] === 'checkbox') {
            $htmlOptions['disabled'] = $attributes['disabled'] === true ? 'disabled' : $attributes['disabled'];
          }

          if ($attributes['style'] === 'checkbox') {
            $htmlOptions['value'] = $name;

            $tagName = $attributes['id'] . Inflector::camelize(Inflector::slug($name));
            $htmlOptions['id'] = $tagName;
            $label = array('for' => $tagName);

            if (isset($htmlOptions['checked']) && $htmlOptions['checked'] === true) {
              $label['class'] = 'selected';
            }

            $name = $attributes['name'];

            if (empty($attributes['class'])) {
              $attributes['class'] = 'checkbox';
            } elseif ($attributes['class'] === 'form-error') {
              $attributes['class'] = 'checkbox ' . $attributes['class'];
            }
            $htmlOptions ['class'] = 'ace';
            $item = $this->Html->useTag('checkboxmultiple', $name, $htmlOptions);
            $label = '<label class="i-checks">'. $item .' <i></i> '. $title . '</label>';


            $select[] = $this->Html->div( 'checkbox', $label);
          } else {
            $select[] = $this->Html->useTag('selectoption', $name, $htmlOptions, $title);
          }
        }
      }
    }

    return array_reverse($select, true);
  }

  public function radio($fieldName, $options = [], array $attributes = []) {
    $attributes = $this->_initInputField($fieldName, $attributes);

    $showEmpty = $this->_extractOption('empty', $attributes);
    if ($showEmpty) {
      $showEmpty = ($showEmpty === true) ? __d('cake', 'empty') : $showEmpty;
      $options = array('' => $showEmpty) + $options;
    }
    unset($attributes['empty']);

    $legend = false;
    if (isset($attributes['legend'])) {
      $legend = $attributes['legend'];
      unset($attributes['legend']);
    } elseif (count($options) > 1) {
      $legend = Inflector::humanize( $this->field());
    }

    $label = true;
    if (isset($attributes['label'])) {
      $label = $attributes['label'];
      unset($attributes['label']);
    }

    $separator = null;
    if (isset($attributes['separator'])) {
      $separator = $attributes['separator'];
      unset($attributes['separator']);
    }

    $between = null;
    if (isset($attributes['between'])) {
      $between = $attributes['between'];
      unset($attributes['between']);
    }

    $value = null;
    if (isset($attributes['value'])) {
      $value = $attributes['value'];
    } else {
      $value = $this->value($fieldName);
    }

    $disabled = array();
    if (isset($attributes['disabled'])) {
      $disabled = $attributes['disabled'];
    }

    $out = array();

    $hiddenField = isset($attributes['hiddenField']) ? $attributes['hiddenField'] : true;
    unset($attributes['hiddenField']);

    if (isset($value) && is_bool($value)) {
      $value = $value ? 1 : 0;
    }

    $this->_domIdSuffixes = array();
    foreach ($options as $optValue => $optTitle) {
      $optionsHere = array('value' => $optValue, 'disabled' => false);

      if (isset($value) && strval($optValue) === strval($value)) {
        $optionsHere['checked'] = 'checked';
      }
      $isNumeric = is_numeric($optValue);
      if ($disabled && (!is_array($disabled) || in_array((string)$optValue, $disabled, !$isNumeric))) {
        $optionsHere['disabled'] = true;
      }
      $tagName = $attributes['id'] . $this->domIdSuffix($optValue);

      

      
      $allOptions = array_merge($attributes, $optionsHere);

      $input = $this->Html->useTag('radio', $attributes['name'], $tagName,
        array_diff_key($allOptions, array('name' => null, 'type' => null, 'id' => null)), ''
      );

      if ($label) {
        $labelOpts = is_array($label) ? $label : array();
        $labelOpts += array('for' => $tagName);

        $optTitle = $this->label( $tagName, $input . '<i></i>' . $optTitle, $labelOpts);
        if (is_array($between)) {
          $optTitle .= array_shift($between);
        }

        $out [] = $this->Html->tag( 'div', $optTitle, array(
            'class' => 'radio'
        ));
      } else {
        $out [] = $input;
      }
    }
    $hidden = null;

    if ($hiddenField) {
      if (!isset($value) || $value === '') {
        $hidden = $this->hidden($fieldName, array(
          'form' => isset($attributes['form']) ? $attributes['form'] : null,
          'id' => $attributes['id'] . '_',
          'value' => '',
          'name' => $attributes['name']
        ));
      }
    }
    $out = $hidden . implode($separator, $out);

    if (is_array($between)) {
      $between = '';
    }
    if ($legend) {
      $out = $this->Html->useTag('fieldset', '', $this->Html->useTag('legend', $legend) . $between . $out);
    }

    return $out;
  }


  public function dateField( $fieldName, $options = array())
  {
    $open_function = isset( $options ['openFunction']) ? $options ['openFunction'] : 'open($event)';

    $_options = array(
        'datepicker-popup' => 'dd MMMM yyyy',
        'ng-required' => 'true',
        'type' => 'text',
        'close-text' => 'Close',
        'after' => '<span class="input-group-btn">
          <button type="button" class="btn btn-default" ng-click="'. $open_function .'"><i class="glyphicon glyphicon-calendar"></i></button>
         </span>'
    );

    $options = array_merge( $_options, $options);
    if( !empty( $options ['label']))
    {
      $label = $options ['label'];
      $options ['label'] = false;
    }

    $out = array();

    $out [] = '<div class="input-group">';
    $out [] = $this->Html->tag( 'label', $label);
    $out [] = $this->input( $fieldName, $options);
    $out [] = '</div>';

    return implode( "\n", $out);
  }

  public function timeField( $fieldName, $options = array())
  {
    $_options = array(
        'show-meridian' => 'false',
        'ng-required' => 'true',
        'type' => 'text',
    );

    $options = array_merge( $_options, $options);
    if( !empty( $options ['label']))
    {
      $label = $options ['label'];
      $options ['label'] = false;
    }

    $out = array();

    $out [] = '<div class="input-group m-t-md">';
    $out [] = $this->Html->tag( 'label', $label);
    $out [] = $this->input( $fieldName, $options);
    $out [] = '</div>';

    return implode( "\n", $out);
  }

  /**
 * Retorna true si el campo del model tiene traducción
 *
 * @param object $model
 * @param string $field
 * @return boolean
 */
  public function hasTranslation( $model, $field)
  {
    $return = $model->Behaviors->hasMethod( 'translateModel')
        && array_key_exists( Inflector::camelize( Inflector::pluralize( $field)), $model->hasMany);

    return $return;
  }

/**
 * El inicio de un box
 * @param  string $title El título de cabecera
 * @return HTML
 */
  public function boxStart( $title = false, $help = false)
  {
    $span = array(
        'class' => 'h4'
    );

    if( $help)
    {
      $span ['tooltip-html-unsafe'] = $help; 
    }

    $title = $this->Html->tag( 'span', $title, $span);
    $html = <<<EOF
<div class="panel panel-default">
  <div class="panel-heading">$title</div>
  <div class="panel-body">
EOF;
    
    return $html;
  }


/**
 * El cierre de un box
 * @return [type] [description]
 */
  public function boxEnd()
  {
    $html = <<<EOF
  </div>
</div>
EOF;

    return $html;
  }

  public function runAngular()
  {
    $exclude = array(
        'controller', 'prefix', 'action'
    );
    $params = array();
    $route = Router::requestRoute();
    $keys = array_diff( $route->keys, $exclude);

    foreach( $keys as $key)
    {
      $params [$key] = $this->request->params [$key];
    }

    $params ['pass'] = json_encode( Router::getParam( 'pass'));
    $params ['language'] = Configure::read( 'Config.language');
    $params ['lang'] = Router::getParam( 'lang');

    if( !empty( $this->request->data))
    {
      $params ['data'] = $this->request->data;
    }
    else
    {
      $params ['data'] = false;
    }

    $params ['upload'] = Configure::read( 'Upload');

    $json = json_encode( $params);

    $script = <<<EOF
    angular.module( 'app').run(['\$rootScope', function( \$rootScope){
      \$rootScope.cake = $json;
    }]);
EOF;
  
    return $this->Html->scriptBlock( $script);
  }

}
