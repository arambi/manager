<?php

namespace Manager\Auth;

use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\Auth\FormAuthenticate;


class FormManagerAuthenticate extends FormAuthenticate
{
  
  public function unauthenticated(ServerRequest $request, Response $response)
  {
    return $response
      ->withStatus(401)
      ->withType('application/json')
      ->cors( $request)
      ->build();
  }
}
