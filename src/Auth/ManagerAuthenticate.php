<?php
namespace Manager\Auth;

use Exception;
use Firebase\JWT\JWT;
use Cake\Http\Response;
use Cake\Core\Configure;
use Cake\Utility\Security;
use Cake\Http\ServerRequest;
use Cake\Auth\BaseAuthenticate;
use Cake\Controller\ComponentRegistry;
use ADmad\JwtAuth\Auth\JwtAuthenticate;
use Cake\Http\Exception\UnauthorizedException;


class ManagerAuthenticate extends JwtAuthenticate
{
    
  public function unauthenticated(ServerRequest $request, Response $response)
  {
    return $response
      ->withStatus(401)
      ->withType('application/json')
      ->cors( $request)
      ->build();
  }
}
