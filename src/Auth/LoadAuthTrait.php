<?php
namespace Manager\Auth;

use Cake\Core\Configure;

trait LoadAuthTrait
{
  private function loadManagerAuth()
  {
    if( Configure::read( 'App.admin2'))
    {
      $this->getResponse()->cors($this->getRequest())->build();

      $this->loadComponent( 'Auth', [
        'storage' => 'Memory',
        'authenticate' => [
          'Manager.Manager' => [
            'parameter' => 'token',
            'userModel' => 'Users',
            'fields' => [
                'username' => 'id'
            ],
            'queryDatasource' => true
          ],
          'Manager.FormManager' => [
            'fields' => [ 'username' => 'email'],
          ]
        ],
        'authorize' => [
            'User.Crud' => [ 'actionPath' => 'controllers/']
        ],
        'loginAction' => [
            'prefix' => isset( $this->request->params ['prefix']) ? $this->request->params ['prefix'] : false,
            'plugin' => 'User',
            'controller' => 'Users',
            'action' => 'token'
        ],
        'unauthorizedRedirect' => false,
        'checkAuthIn' => 'Controller.initialize'
      ]);
    }
    else
    {
      $this->loadComponent( 'Auth', [
        'authenticate' => [
          'Form' => [
              'fields' => [ 'username' => 'email'],
          ]
        ],
        'authorize' => [
            'User.Crud' => [ 'actionPath' => 'controllers/']
        ],
        'loginAction' => [
            'prefix' => isset( $this->request->params ['prefix']) ? $this->request->params ['prefix'] : false,
            'plugin' => 'User',
            'controller' => 'Users',
            'action' => 'login'
        ]
      ]);
    }
  }
}